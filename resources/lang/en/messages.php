<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 04.07.18
 * Time: 16:28
 */

return [
    // Авторизация
    'auth.title'                 => 'Authentication',
    'auth.f_name'                => 'First Name',
    'auth.l_name'                => 'Last Name',
    'auth.email'                 => 'Email',
    'auth.password'              => 'Password',
    'auth.password_repeat'       => 'Password Repeat',
    'auth.bday'                  => 'Birthday',
    'auth.gender'                => 'Gender',
    'auth.accept_part_1'         => 'I accept the',
    'auth.accept_part_2'         => 'Terms and Conditions',
    'auth.accept_part_3'         => 'of the website',
    'auth.complete_registration' => 'Complete Registration!',
    'auth.remember_me'           => 'Remember Me',
    'auth.forgot_password'       => 'Forgot Password',
    'auth.login'                 => 'Login',
    'auth.or'                    => 'Or',
    'auth.login_with_gmail'      => 'Login With Gmail',
    'auth.have_account_part_1'   => 'Don’t you have an account?',
    'auth.have_account_part_2'   => 'Register Now!',
    'auth.have_account_part_3'   => 'it’s really simple and you can start enjoing all the benefits!',
    'auth.registration'          => 'Registration',
    // Текст страницы
    'auth.text.welcome'          => 'Welcome to the Challenge Social Network',
    'auth.text.sub'              => 'We are the best and biggest social network with 5 billion active users all around the world. Share you thoughts, write blog posts, show your favourite music via Stopify, earn badges and much more!',
    'auth.mail.hello'            => 'Hello',
    'auth.mail.confirm_email'    => 'Follow the link to confirm your email address',
    //End Auth

    // Profile.feed
    'feed.timeline'              => 'Timeline',
    'feed.about'                 => 'About',
    'feed.friends'               => 'Friends',
    'feed.challenges'            => 'Challenges',
    'feed.achievements'          => 'Achievements',
    //End Profile.feed
];