<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 04.07.18
 * Time: 16:28
 */

return [
    // Авторизация
    'auth.title'                 => 'Авторизація',
    'auth.f_name'                => 'Прізвище',
    'auth.l_name'                => "Імя",
    'auth.email'                 => 'Email',
    'auth.password'              => 'Пароль',
    'auth.password_repeat'       => 'Повтор пароля',
    'auth.bday'                  => 'Дата народження',
    'auth.gender'                => 'Стать',
    'auth.accept_part_1'         => 'Я приймаю',
    'auth.accept_part_2'         => 'Пользовательское соглашение',
    'auth.accept_part_3'         => 'сервісу',
    'auth.complete_registration' => 'Зареєструвати мене!',
    'auth.remember_me'           => "Запам'ятати мене",
    'auth.forgot_password'       => 'Забув пароль',
    'auth.login'                 => 'Вхід',
    'auth.or'                    => 'Або',
    'auth.login_with_gmail'      => 'Gmail',
    'auth.have_account_part_1'   => 'Досі немає облікового запису?',
    'auth.have_account_part_2'   => 'Створіть його!',
    'auth.have_account_part_3'   => 'це дуже просто і дозволить Вам використовувати усі можливості сервісу!',
    'auth.registration'          => 'Реєстрація',

    // Текст страницы
    'auth.text.welcome'          => 'Ласкаво просимо на сервіс обміну Челленджами!',
    'auth.text.sub'              => 'Спілкуйтесь з друзями, кидайте виклики, весело та активно проводьте час',
    'auth.mail.hello'            => 'Вітаю',
    'auth.mail.confirm_email'    => 'Перейдіть за посиланням аби підтвердити свою адрессу',
    //End Auth
    // Profile.feed
    'feed.timeline'              => 'Свытлина',
    'feed.about'                 => 'Про мене',
    'feed.friends'               => 'Друзы',
    'feed.challenges'            => 'Челленджы',
    'feed.achievements'          => 'Досягнення',
    //End Profile.feed
];