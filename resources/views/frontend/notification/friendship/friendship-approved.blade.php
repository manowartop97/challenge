<li>
    <div class="author-thumb">
        <img src="{{url($user_avatar)}}" alt="author" style="object-fit: cover; width: 34px; height:34px">
    </div>
    <div class="notification-event">
        <a href="{{route('page.show-user', ['id' => $user_id])}}" class="h6 notification-friend">{{$fullName}}</a>
        <span class="chat-message-item">@lang('messages.new_friendship_accepted')</span>
    </div>
    <div class="more">
        <a href="javascript:void(0);" data-route="{{route('notification.clear', ['id' => $notification_id])}}"
           class="removeNotification"
           style="color: black"><i class="fa fa-times"></i></a>
    </div>
</li>