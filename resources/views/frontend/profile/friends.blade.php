<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer $link
 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $friends
 * @var \App\Models\DB\Friendship\Friendship $friendship
 * @var \App\Models\DB\User\User $friend
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title') Друзья @endsection
@section('content')
    <div class="header-spacer"></div>
    <?= view('frontend.profile.blocks.personal-info_block', ['profile' => $profile, 'link' => $link]) ?>

    <div class="container">
        <div class="row">
            @foreach($friends->items() as $friendship)
                @php $friend = $friendship->user_id == Auth::id() ? $friendship->friend : $friendship->user @endphp
                <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-6">
                    <div class="ui-block">

                        <!-- Friend Item -->

                        <div class="friend-item">
                            <div class="friend-header-thumb">
                                <img src="{{asset('storage/'.$friend->getBackground())}}" alt="friend"
                                     style="max-height: 100px;min-height: 100px;">
                            </div>

                            <div class="friend-item-content">

                                <div class="more">

                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Report Profile</a>
                                        </li>
                                        <li>
                                            <a href="#">Block Profile</a>
                                        </li>
                                        <li>
                                            <a href="#">Turn Off Notifications</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="friend-avatar">
                                    <div class="author-thumb">
                                        <img src="{{asset('storage/'.$friend->getAvatar())}}" alt="author"
                                             style="width: 92px;height: 92px">
                                    </div>
                                    <div class="author-content">
                                        <a href="{{route('page.show-user',['id' => $friend->id])}}"
                                           class="h5 author-name">{{$friend->getFullName()}}</a>
                                        <div class="country">San Francisco, CA</div>
                                    </div>
                                </div>

                                <div class="swiper-container" data-slide="fade">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="friend-count" data-swiper-parallax="-500">
                                                <a href="#" class="friend-count-item">
                                                    <div class="h6">{{$friend->getFriendsCount()}}</div>
                                                    <div class="title">Friends</div>
                                                </a>
                                                <a href="#" class="friend-count-item">
                                                    <div class="h6">240</div>
                                                    <div class="title">Photos</div>
                                                </a>
                                                <a href="#" class="friend-count-item">
                                                    <div class="h6">16</div>
                                                    <div class="title">Videos</div>
                                                </a>
                                            </div>
                                            <div class="control-block-button" data-swiper-parallax="-100">
                                                <a href="#"
                                                   data-route="{{route('friendship.remove',['id' => $friend->id])}}"
                                                   class="accept-request request-del bg-orange remove_from_friendlist"
                                                   style="margin-right: 20px;border-radius: 100%;width: 50px;height: 50px;line-height: 54px;padding: 0;fill: #fff;font-size: 20px;">
												<span class="icon-minus"
                                                      style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
                                                </a>

                                                <a href="#" class="btn btn-control bg-purple">
                                                    @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
                                                </a>

                                            </div>
                                        </div>

                                        <div class="swiper-slide">
                                            <p class="friend-about" data-swiper-parallax="-500">
                                                Hi!, I’m Marina and I’m a Community Manager for “Gametube”. Gamer and
                                                full-time mother.
                                            </p>

                                            <div class="friend-since" data-swiper-parallax="-100">
                                                <span>Friends Since:</span>
                                                <div class="h6">December 2014</div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- If we need pagination -->
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>

                        <!-- ... end Friend Item -->
                    </div>
                </div>
            @endforeach
            <div class="col-md-12">
                {{$friends->links()}}
            </div>
        </div>
    </div>
    <?= view('frontend.profile.blocks.popups') ?>
@endsection