<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 */
?>
@extends('layouts.frontend.profile-layout.main')
@section('title')
    Личная информация
@endsection
@section('content')

    <?= view('frontend.profile.settings.blocks.header') ?>
    <!-- ... end Main Header Account -->


    <!-- Your Account Personal Information -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">@lang('messages.profile.sidebar.personal_information')</h6>
                    </div>
                    <span class="text-center">@include('flash::message')</span>
                    <div class="ui-block-content">
                        <!-- Personal Information Form  -->

                        <form method="post" action="{{route('profile-settings.update-personal')}}">
                            {{csrf_field()}}
                            <input type="hidden" name="user_id" value="{{Auth::id()}}">
                            <div class="row">

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <strong class="text-error">{{$errors->has('surname') ? $errors->first('surname') : ''}}</strong>
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.auth.f_name')</label>
                                        <input class="form-control" placeholder="" name="surname" type="text"
                                               value="{{old('surname') ?: $profile->surname}}">
                                    </div>
                                    <strong class="text-error">{{$errors->has('email') ? $errors->first('email') : ''}}</strong>
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.auth.email')</label>
                                        <input class="form-control" placeholder="" name="email" type="email"
                                               value="{{old('email') ?: $profile->user->email}}">
                                    </div>
                                    <strong class="text-error">{{$errors->has('birthday') ? $errors->first('birthday') : ''}}</strong>
                                    <div class="form-group date-time-picker label-floating">
                                        <label class="control-label">@lang('messages.auth.bday')</label>
                                        <input class="datetimepicker" autocomplete="off" name="birthday"
                                               value="{{old('birthday') ?: $profile->birthday}}"/>
                                        <span class="input-group-addon">
                                            @svg('month-calendar-icon', ['olymp-month-calendar-icon icon'])
                                        </span>
                                    </div>
                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <strong class="text-error">{{$errors->has('name') ? $errors->first('name') : ''}}</strong>
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.auth.l_name')</label>
                                        <input class="form-control" placeholder="" name="name" type="text"
                                               value="{{old('name') ?: $profile->name}}">
                                    </div>

                                    <strong class="text-error">{{$errors->has('phone') ? $errors->first('phone') : ''}}</strong>
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.profile.phone')</label>
                                        <input class="form-control" placeholder="" name="phone"
                                               value="{{old('phone') ?: $profile->phone}}" type="text">
                                    </div>
                                    <strong class="text-error">{{$errors->has('gender') ? $errors->first('gender') : ''}}</strong>
                                    <div class="form-group label-floating is-select">
                                        <label class="control-label">@lang('messages.auth.gender')</label>
                                        <select class="selectpicker form-control" name="gender">
                                            @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\User::GENDER_LIST) as $id => $title)
                                                <option value="{{$id}}" {{old('gender') ?: $profile->gender == $id  ? 'selected' : '' }}>{{$title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-select">
                                        <label class="control-label">@lang('messages.country')</label>
                                        <select class="selectpicker form-control">
                                            <option value="US">United States</option>
                                            <option value="AU">Australia</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-select">
                                        <label class="control-label">@lang('messages.city')</label>
                                        <select class="selectpicker form-control">
                                            <option value="SF">San Francisco</option>
                                            <option value="NY">New York</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                    <strong class="text-error">{{$errors->has('about') ? $errors->first('about') : ''}}</strong>
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.about')</label>
                                        <textarea class="form-control" name="about"
                                                  placeholder="">{{old('about') ?:$profile->about}}</textarea>
                                    </div>
                                </div>
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <strong class="text-error">{{$errors->has('facebook_link') ? $errors->first('facebook_link') : ''}}</strong>
                                    <div class="form-group with-icon label-floating">
                                        <label class="control-label">@lang('messages.facebook_link')</label>
                                        <input class="form-control" name="facebook_link" type="text"
                                               value="{{old('facebook_link') ?: $profile->facebook_link}}">
                                        <i class="fab fa-facebook-f c-facebook" aria-hidden="true"></i>
                                    </div>
                                    <strong class="text-error">{{$errors->has('twitter_link') ? $errors->first('twitter_link') : ''}}</strong>
                                    <div class="form-group with-icon label-floating">
                                        <label class="control-label">@lang('messages.twitter_link')</label>
                                        <input class="form-control" name="twitter_link" type="text"
                                               value="{{old('twitter_link') ?: $profile->twitter_link}}">
                                        <i class="fab fa-twitter c-twitter" aria-hidden="true"></i>
                                    </div>
                                    <strong class="text-error">{{$errors->has('instagram_link') ? $errors->first('instagram_link') : ''}}</strong>
                                    <div class="form-group with-icon label-floating">
                                        <label class="control-label">@lang('messages.instagram_link')</label>
                                        <input class="form-control" name="instagram_link"
                                               value="{{old('instagram_link') ?: $profile->instagram_link}}"
                                               type="text">
                                        <i class="fab fa-instagram c-instagram" aria-hidden="true"></i>
                                    </div>
                                    <strong class="text-error">{{$errors->has('vk_link') ? $errors->first('vk_link') : ''}}</strong>
                                    <div class="form-group with-icon label-floating">
                                        <label class="control-label">@lang('messages.vk_link')</label>
                                        <input class="form-control" name="vk_link" type="text"
                                               value="{{old('vk_link') ?: $profile->vk_link}}">
                                        <i class="fab fa-vk c-twitter" aria-hidden="true"></i>
                                    </div>
                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <input type="submit" class="btn btn-primary btn-lg full-width"
                                           value="{{trans('messages.save')}}">
                                </div>

                            </div>
                        </form>

                        <!-- ... end Personal Information Form  -->
                    </div>
                </div>
            </div>

            <?= view('frontend.profile.settings.blocks.sidebar') ?>
        </div>
    </div>
    <?=view('frontend.profile.blocks.popups')?>
@endsection
@section('scripts')
    <script src="{{asset('frontend/js/custom/init_datepicker.js')}}"></script>
@endsection

<style>
    .text-error {
        font-size: 11px;
    }

    .back-to-top .back-icon {
        height: 20px;
        width: 20px;
        margin-top: 13px;
    }

</style>