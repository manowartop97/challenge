<?php
/**
 * @var \App\Models\DB\User\AccountSetting $settings
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title')
    Настройки аккаунта
@endsection
@section('content')

    <?= view('frontend.profile.settings.blocks.header') ?>

    <!-- Your Account Personal Information -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">@lang('messages.profile.sidebar.account_settings')</h6>
                    </div>
                    <div class="ui-block-content">
                        <span class="text-center">@include('flash::message')</span>
                        <!-- Personal Account Settings Form -->
                        <form method="post" action="{{route('profile-settings.update-account-settings')}}">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-select">
                                        <label class="control-label">@lang('messages.who_can_request_friends') ?</label>
                                        <select class="selectpicker form-control" name="who_can_request_friends">
                                            @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\AccountSetting::VARIANTS_LIST_FOR_FRIENDS) as $id => $title)
                                                <option value="{{$id}}" {{old('who_can_request_friends') == $id || $settings->who_can_request_friends == $id ? 'selected' : ''}}>{{$title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-select">
                                        <label class="control-label">@lang('messages.who_can_view_posts')?</label>
                                        <select class="selectpicker form-control" name="who_can_view_posts">
                                            @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\AccountSetting::VARIANTS_LIST) as $id => $title)
                                                <option value="{{$id}}" {{old('who_can_view_posts') == $id || $settings->who_can_view_posts == $id ? 'selected' : ''}}>{{$title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-select">
                                        <label class="control-label">@lang('messages.who_can_invite_to_challenge')
                                            ?</label>
                                        <select class="selectpicker form-control" name="who_can_invite_to_challenge">
                                            @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\AccountSetting::VARIANTS_LIST) as $id => $title)
                                                <option value="{{$id}}" {{old('who_can_invite_to_challenge') == $id || $settings->who_can_invite_to_challenge == $id ? 'selected' : ''}}>{{$title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-select">
                                        <label class="control-label">@lang('messages.who_can_invite_to_event')?</label>
                                        <select class="selectpicker form-control" name="who_can_invite_to_event">
                                            @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\AccountSetting::VARIANTS_LIST) as $id => $title)
                                                <option value="{{$id}}" {{old('who_can_invite_to_event') == $id || $settings->who_can_invite_to_event == $id ? 'selected' : ''}}>{{$title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="description-toggle">
                                        <div class="description-toggle-content">
                                            <div class="h6">@lang('messages.is_telegram_notifications')</div>
                                            <p>@lang('messages.is_telegram_notifications_text')</p>
                                        </div>
                                        <input type="hidden" name="is_telegram_notifications" value="0">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" value="1" name="is_telegram_notifications" {{$settings->is_telegram_notifications || old('is_telegram_notification') ? 'checked' : ''}}>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="description-toggle">
                                        <div class="description-toggle-content">
                                            <div class="h6">@lang('messages.is_email_notifications')</div>
                                            <p>@lang('messages.is_email_notifications_text')</p>
                                        </div>
                                        <input type="hidden" name="is_email_notifications" value="0">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" value="1" name="is_email_notifications"  {{$settings->is_email_notifications || old('is_email_notifications') ? 'checked' : ''}}>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="description-toggle">
                                        <div class="description-toggle-content">
                                            <div class="h6">@lang('messages.is_friends_birthday_notifications')</div>
                                        </div>
                                        <input type="hidden" name="is_friends_birthday_notifications" value="0">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" value="1" name="is_friends_birthday_notifications"  {{$settings->is_friends_birthday_notifications || old('is_friends_birthday_notifications') ? 'checked' : ''}}>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="description-toggle">
                                        <div class="description-toggle-content">
                                            <div class="h6">@lang('messages.is_notification_sound_on')</div>
                                        </div>
                                        <input type="hidden" name="is_notification_sound_on" value="0">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" value="1" name="is_notification_sound_on"  {{$settings->is_notification_sound_on || old('is_notification_sound_on') ? 'checked' : ''}}>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="description-toggle">
                                        <div class="description-toggle-content">
                                            <div class="h6">@lang('messages.is_chat_sound_on')</div>
                                        </div>
                                        <input type="hidden" name="is_chat_sound_on" value="0">
                                        <div class="togglebutton">
                                            <label>
                                                <input type="checkbox" value="1" name="is_chat_sound_on"  {{$settings->is_chat_sound_on || old('is_chat_sound_on') ? 'checked' : ''}}>
                                            </label>
                                        </div>
                                    </div>

                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <input type="submit" class="btn btn-primary btn-lg full-width" value="{{trans('messages.save')}}">
                                </div>
                            </div>
                        </form>

                        <!-- ... end Personal Account Settings Form  -->

                    </div>
                </div>
            </div>

            <?= view('frontend.profile.settings.blocks.sidebar') ?>
        </div>
    </div>

    <?=view('frontend.profile.blocks.popups')?>
@endsection
@section('scripts')
@endsection

<style>
    .back-to-top .back-icon {
        height: 20px;
        width: 20px;
        margin-top: 13px;
    }
</style>