<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12  responsive-display-none">
    <div class="ui-block">

        <!-- Your Profile  -->
        <div class="your-profile">
            <div class="ui-block-title ui-block-title-small">
                <h6 class="title">@lang('messages.profile.sidebar.your_profile')</h6>
            </div>

            <div id="accordion" role="tablist" aria-multiselectable="true">
                <div class="card">
                    <div class="card-header" role="tab" id="headingOne">
                        <h6 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                               aria-controls="collapseOne">
                                @lang('messages.profile.sidebar.profile-settings')
                                @svg('dropdown-arrow-icon', ['olymp-dropdown-arrow-icon'])
                            </a>
                        </h6>
                    </div>

                    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                        <ul class="your-profile-menu">
                            <li class="{{Request::is('profile/settings/personal-info') ? 'active' : ''}}">
                                <a href="{{route('profile-settings.personal')}}">@lang('messages.profile.sidebar.personal_information')</a>
                            </li>
                            <li class="{{Request::is('profile/settings/account-settings') ? 'active' : ''}}">
                                <a href="{{route('profile-settings.account-settings')}}">@lang('messages.profile.sidebar.account_settings')</a>
                            </li>
                            <li class="{{Request::is('profile/settings/password') ? 'active' : ''}}">
                                <a href="{{route('profile-settings.change-password')}}">@lang('messages.profile.sidebar.change_password')</a>
                            </li>
                            <li class="{{Request::is('profile/settings/hobbies') ? 'active' : ''}}">
                                <a href="{{route('profile-settings.hobbies')}}">@lang('messages.profile.sidebar.hobbies_and_interests')</a>
                            </li>
                            <li class="{{Request::is('profile/settings/education-and-employment') ? 'active' : ''}}">
                                <a href="{{route('profile-settings.adu-and-employment')}}">@lang('messages.profile.sidebar.education_and_employment')</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="ui-block-title">
                <a href="{{route('notifications')}}"
                   class="h6 title">@lang('messages.profile.sidebar.notifications')</a>
                <a href="#" class="items-round-little bg-primary">8</a>
            </div>
            <div class="ui-block-title">
                <a href="34-YourAccount-ChatMessages.html" class="h6 title">@lang('messages.profile.sidebar.chat')</a>
            </div>
            <div class="ui-block-title">
                <a href="{{route('friends.requests')}}"
                   class="h6 title">@lang('messages.profile.sidebar.friend_requests')</a>
                <a href="#" class="items-round-little bg-blue">4</a>
            </div>
            <div class="ui-block-title ui-block-title-small">
                <h6 class="title">FAVOURITE PAGE</h6>
            </div>
            <div class="ui-block-title">
                <a href="36-FavPage-SettingsAndCreatePopup.html" class="h6 title">Create Fav Page</a>
            </div>
            <div class="ui-block-title">
                <a href="36-FavPage-SettingsAndCreatePopup.html" class="h6 title">Fav Page Settings</a>
            </div>
        </div>

        <!-- ... end Your Profile  -->


    </div>
</div>

<style>
    .active > a {
        color: black !important;
    }
</style>