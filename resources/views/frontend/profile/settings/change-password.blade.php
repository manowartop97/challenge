@extends('layouts.frontend.profile-layout.main')
@section('title')
    @lang('messages.profile.sidebar.change_password')
@endsection
@section('content')

    <?= view('frontend.profile.settings.blocks.header') ?>
    <!-- ... end Main Header Account -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">@lang('messages.profile.sidebar.change_password')</h6>
                    </div>
                    <span class="text-center">@include('flash::message')</span>
                    <div class="ui-block-content">
                        <!-- Change Password Form -->
                        <form method="post" action="{{route('profile-settings.change-password-action')}}">
                            {{csrf_field()}}
                            <div class="row">

                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.enter_current_password')</label>
                                        <input class="form-control" name="old_password" placeholder="" type="password">
                                        <strong class="text-error">{{$errors->has('old_password') ? $errors->first('old_password') : ''}}</strong>
                                    </div>
                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">@lang('messages.enter_new_password')</label>
                                        <input class="form-control" placeholder="" name="new_password" type="password">
                                        <strong class="text-error">{{$errors->has('new_password') ? $errors->first('new_password') : ''}}</strong>
                                    </div>
                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label">@lang('messages.enter_new_password_repeat')</label>
                                        <input class="form-control" placeholder="" name="new_password_confirmation"
                                               type="password">
                                        <strong class="text-error">{{$errors->has('new_password_confirmation') ? $errors->first('new_password_confirmation') : ''}}</strong>
                                    </div>
                                </div>

                                <div class="col col-lg-12 col-sm-12 col-sm-12 col-12">
                                    <div class="remember">
                                        <div class="checkbox">
                                        </div>
                                        <a href="#" data-toggle="modal" class="forgot"
                                           data-target="#reset-password">@lang('messages.forgot_password')</a>
                                    </div>
                                </div>
                                <div class="modal fade" id="reset-password" tabindex="-1" role="dialog"
                                     aria-labelledby="reset-password"
                                     aria-hidden="true">
                                    <div class="modal-dialog window-popup update-header-photo" role="document">
                                        <div class="modal-content">
                                            <a href="#" class="close icon-close" data-dismiss="modal"
                                               aria-label="Close">
                                                @svg('close-icon', ['olymp-close-icon'])
                                            </a>

                                            <div class="modal-header">
                                                <h6 class="title">@lang('messages.new_password_will_be_sent_to_email')</h6>
                                            </div>

                                            <div class="modal-body">
                                                <br>
                                                <div class="col-md-12">
                                                    <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <a href="{{route('profile-settings.reset-password')}}"
                                                           class="btn btn-primary btn-lg full-width">@lang('messages.send_me_new_password')</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <input type="submit" class="btn btn-primary btn-lg full-width"
                                           value="@lang('messages.change_password_btn')">
                                </div>

                            </div>
                        </form>

                        <!-- ... end Change Password Form -->
                    </div>
                </div>
            </div>

            <?= view('frontend.profile.settings.blocks.sidebar') ?>
        </div>
    </div>

    <?=view('frontend.profile.blocks.popups')?>
@endsection
@section('scripts')
@endsection

<style>
    #reset-password {
        margin-top: 15%;
    }

    .back-to-top .back-icon {
        height: 20px;
        width: 20px;
        margin-top: 13px;
    }
</style>