<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title')
    @lang('messages.hobbies_and_interests')
@endsection
@section('content')

    <?= view('frontend.profile.settings.blocks.header') ?>
    <!-- ... end Main Header Account -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Hobbies and Interests</h6>
                    </div>
                    <div class="ui-block-content">
                        <span class="text-center">@include('flash::message')</span>
                        <!-- Form Hobbies and Interests -->
                        <form method="post" action="{{route('profile-settings.update-hobbies')}}">
                            {{csrf_field()}}
                            <div class="row">

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.hobbies_and_interests')</label>
                                        <textarea class="form-control" name="hobbies_and_interests"
                                                  placeholder="">{{old('hobbies_and_interests') ?:$profile->hobbies_and_interests}}</textarea>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.favourite_games')</label>
                                        <textarea class="form-control" name="favourite_games" placeholder="">{{old('favourite_games') ?:$profile->favourite_games}}</textarea>
                                    </div>

                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.favourite_music')</label>
                                        <textarea class="form-control" name="favourite_music" placeholder="">{{old('favourite_music') ?:$profile->favourite_music}}</textarea>
                                    </div>
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('messages.favourite_movies')</label>
                                        <textarea class="form-control" name="favourite_movies" placeholder="">{{old('favourite_movies') ?:$profile->favourite_movies}}</textarea>
                                    </div>
                                    <input type="submit" class="btn btn-primary btn-lg full-width"
                                           value="@lang('messages.save')">
                                </div>

                            </div>
                        </form>
                        <!-- ... end Form Hobbies and Interests -->
                    </div>
                </div>
            </div>
            <?= view('frontend.profile.settings.blocks.sidebar') ?>
        </div>
    </div>


    <?=view('frontend.profile.blocks.popups')?>
@endsection
@section('scripts')
@endsection

<style>
    .back-to-top .back-icon {
        height: 20px;
        width: 20px;
        margin-top: 13px;
    }
</style>