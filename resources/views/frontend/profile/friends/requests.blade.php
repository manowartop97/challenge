<?php
/**
 * @var \App\Models\DB\Friendship\Friendship $friendsRequest
 * @var bool $isIncome
 */
?>
@extends('layouts.frontend.profile-layout.main')
@section('title')
    Запросы в друзья
@endsection
@section('content')

    <?= view('frontend.profile.settings.blocks.header') ?>
    <!-- ... end Main Header Account -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">{{$isIncome ? trans('messages.income_requests') : trans('messages.outcome_requests')}}</h6>
                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                            <ul class="more-dropdown more-with-triangle">
                                <li>
                                    @if($isIncome)
                                        <a href="{{route('friends.outcome-requests')}}">@lang('messages.outcome_requests')</a>
                                    @else
                                        <a href="{{route('friends.requests')}}">@lang('messages.income_requests')</a>
                                    @endif

                                </li>
                            </ul>
                        </div>
                    </div>


                    <!-- Notification List Frien Requests -->

                    <ul class="notification-list friend-requests">

                        @foreach($friends as $friendsRequest)
                            @php $user = $isIncome ? $friendsRequest->user : $friendsRequest->friend; @endphp
                            <li>
                                <div class="author-thumb">
                                    <img src="{{url('storage/'.$user->getAvatar())}}"
                                         class="friends-requests-avatar" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="{{route('page.show-user',['id' => $user->id])}}"
                                       class="h6 notification-friend">{{$user->getFullName()}}</a>
                                    <span class="chat-message-item">Mutual Friend: Sarah Hetfield</span>
                                </div>
                                @if($isIncome)
                                    <span class="notification-icon">
											<a href="#" data-route="{{route('friendship.accept', ['id' => $user->id])}}"
                                               class="accept-request accept_friendship_request_page">
												<span class="icon-add">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
                                                @lang('messages.accept_friendship')
											</a>

											<a href="#" data-route="{{route('friendship.reject', ['id' => $user->id])}}"
                                               class="accept-request request-del reject_friendship_request_page">
												<span class="icon-minus">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
											</a>

										</span>
                                @else
                                    <span class="notification-icon">
											<a href="#" data-route="{{route('friendship.remove', ['id' => $user->id])}}"
                                               class="accept-request request-del remove_friend_button_request_page">
												<span class="icon-minus">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
											</a>

										</span>
                                @endif
                            </li>
                        @endforeach


                        {{--<li>--}}
                        {{--<div class="author-thumb">--}}
                        {{--<img src="{{asset('frontend/img/avatar15-sm.jpg')}}" alt="author">--}}
                        {{--</div>--}}
                        {{--<div class="notification-event">--}}
                        {{--<a href="#" class="h6 notification-friend">Tony Stevens</a>--}}
                        {{--<span class="chat-message-item">4 Friends in Common</span>--}}
                        {{--</div>--}}
                        {{--<span class="notification-icon">--}}
                        {{--<a href="#" class="accept-request">--}}
                        {{--<span class="icon-add">--}}
                        {{--@svg('happy-face-icon', ['olymp-happy-face-icon'])--}}
                        {{--</span>--}}
                        {{--Accept Friend Request--}}
                        {{--</a>--}}

                        {{--<a href="#" class="accept-request request-del">--}}
                        {{--<span class="icon-minus">--}}
                        {{--@svg('happy-face-icon', ['olymp-happy-face-icon'])--}}
                        {{--</span>--}}
                        {{--</a>--}}

                        {{--</span>--}}

                        {{--<div class="more">--}}
                        {{--@svg('three-dots-icon', ['olymp-three-dots-icon'])--}}
                        {{--@svg('little-delete', ['olymp-little-delete'])--}}
                        {{--</div>--}}
                        {{--</li>--}}

                        {{--<li class="accepted">--}}
                        {{--<div class="author-thumb">--}}
                        {{--<img src="{{asset('frontend/img/avatar15-sm.jpg')}}" alt="author">--}}
                        {{--</div>--}}
                        {{--<div class="notification-event">--}}
                        {{--You and <a href="#" class="h6 notification-friend">Mary Jane Stark</a> just became--}}
                        {{--friends. Write on <a href="#" class="notification-link">his wall</a>.--}}
                        {{--</div>--}}
                        {{--<span class="notification-icon">--}}
                        {{--@svg('happy-face-icon', ['olymp-happy-face-icon'])--}}
                        {{--</span>--}}

                        {{--<div class="more">--}}
                        {{--@svg('three-dots-icon', ['olymp-three-dots-icon'])--}}
                        {{--@svg('little-delete', ['olymp-little-delete'])--}}
                        {{--</div>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                        {{--<div class="author-thumb">--}}
                        {{--<img src="{{asset('frontend/img/avatar15-sm.jpg')}}" alt="author">--}}
                        {{--</div>--}}
                        {{--<div class="notification-event">--}}
                        {{--<a href="#" class="h6 notification-friend">Stagg Clothing</a>--}}
                        {{--<span class="chat-message-item">9 Friends in Common</span>--}}
                        {{--</div>--}}
                        {{--<span class="notification-icon">--}}
                        {{--<a href="#" class="accept-request">--}}
                        {{--<span class="icon-add">--}}
                        {{--@svg('happy-face-icon', ['olymp-happy-face-icon'])--}}
                        {{--</span>--}}
                        {{--Accept Friend Request--}}
                        {{--</a>--}}

                        {{--<a href="#" class="accept-request request-del">--}}
                        {{--<span class="icon-minus">--}}
                        {{--@svg('happy-face-icon', ['olymp-happy-face-icon'])--}}
                        {{--</span>--}}
                        {{--</a>--}}

                        {{--</span>--}}

                        {{--<div class="more">--}}
                        {{--@svg('three-dots-icon', ['olymp-three-dots-icon'])--}}
                        {{--@svg('little-delete', ['olymp-little-delete'])--}}
                        {{--</div>--}}
                        {{--</li>--}}

                    </ul>

                    <!-- ... end Notification List Frien Requests -->
                </div>

            </div>

            <?= view('frontend.profile.settings.blocks.sidebar') ?>
        </div>
    </div>
    <?=view('frontend.profile.blocks.popups')?>
@endsection
