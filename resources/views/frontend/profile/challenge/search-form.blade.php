<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <form class="challengeFilterForm" method="get">
        @if(Auth::id() === $profile->user_id)
        <ul class="cat-list-bg-style align-center sorting-menu cat-list__item-bg-blue filter_route" data-route="{{route('challenge.all')}}">
            <li class="cat-list__item {{!Request::get('status')? 'active' : ''}}" data-filter="0">
                <a href="" class="filter_status">@lang('messages.all')</a>
            </li>
            @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\Challenge\Challenge::STATUS_LIST) as $id => $title)
                <li class="cat-list__item {{Request::get('status') == $id ? 'active' : ''}}" data-filter="{{$id}}">
                    <a href="" class="filter_status">{{$title}}</a>
                </li>
            @endforeach
        </ul>
        @endif
        <div class="ui-block-title">
            <div class="col-md-12">
                <div class="row">
                    <div class="col col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="form-group label-floating">
                            <label class="control-label">@lang('messages.search_field_title')</label>
                            <input class="form-control challengeTitle" placeholder="" name="title" type="text" value="">
                        </div>
                    </div>
                    <div class="col col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                        <a href="#" class="btn btn-primary btn-lg btn-block searchSubmit">@lang('messages.search')
                            <div class="ripple-container"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>