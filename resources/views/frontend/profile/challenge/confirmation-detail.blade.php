<?php
/**
 * @var \App\Models\DB\Challenge\ChallengeConfirmation $confirmation
 */
?>

@extends('layouts.frontend.profile-layout.main')
<link rel="stylesheet" href="{{asset('frontend/css/custom/challenge.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/jquery-confirm.css')}}">
@section('title')
    @lang('messages.challenge.show')
@endsection
@section('content')
    <div class="header-spacer"></div>
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title bg-blue">
                        <h6 class="title c-white">@lang('messages.challenge_confirmation_show')</h6>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <span class="text-center">@include('flash::message')</span>
                        <div class="ui-block">
                            <div class="ui-block-title">

                                <!-- Post -->

                                <article class="hentry post">

                                    <div class="post__author author vcard inline-items">
                                        <img src="{{url($confirmation->challenge->getTitleImage())}}" alt="author">

                                        <div class="author-date">
                                            <a class="h6 post__author-name fn"
                                               href="{{route('challenge.show', ['id' => $confirmation->challenge_id])}}">{{$confirmation->challenge->title}}</a>
                                            <div class="post__date">
                                                <time class="published" datetime="2017-03-24T18:18">
                                                    {{$confirmation->challenge->created_at->diffForHumans()}}
                                                </time>
                                            </div>
                                        </div>
                                    </div>

                                    <p>{{nl2br($confirmation->challenge->description)}}</p>

                                    <div class="post-additional-info inline-items">
                                        <span><i class="fa fa-calendar-alt"></i> @lang('messages.challenge_start_doing')
                                            : {{$confirmation->challengeInvitation->created_at->diffForHumans()}}</span>
                                        <span><i class="fa fa-calendar-check"></i> @lang('messages.challenge_end_doing')
                                            : {{$confirmation->created_at->diffForHumans()}}</span>
                                    </div>
                                </article>
                            </div>
                        </div>

                        <div class="ui-block">

                            <div class="ui-block-title">
                                <h4 class="text-center">@lang('messages.challenge_confirmation')</h4>
                            </div>
                            <!-- Post -->

                            <article class="hentry post">

                                <div class="post__author author vcard inline-items">
                                    <img src="{{url('storage/'.$confirmation->challengeInvitation->participant->getAvatar())}}"
                                         alt="author">

                                    <div class="author-date">
                                        <a class="h6 post__author-name fn"
                                           href="02-ProfilePage.html">{{$confirmation->challengeInvitation->participant->getFullName()}}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="2017-03-24T18:18">
                                                {{$confirmation->created_at->diffForHumans()}}
                                            </time>
                                        </div>
                                    </div>
                                </div>

                                <p>{{nl2br($confirmation->comment)}}</p>

                                @if($confirmation->additional_media)
                                    <div class="post-block-photo js-zoom-gallery">
                                        @foreach($confirmation->getAdditionalMedia() as $media)
                                            @if($media['type'] === \App\Repositories\Challenge\ChallengeRepository::TYPE_VIDEO)
                                                <div class="video-player custom col col-4-width" style="margin: 0px;">
                                                    <img src="{{url($media['thumbnail'])}}" alt="photo">
                                                    <a href="{{url($media['name'])}}" class="play-video"
                                                       style="opacity: 1">
                                                        @svg('play-icon', 'olymp-play-icon')
                                                    </a>

                                                    <div class="video-content">
                                                    </div>

                                                    <div class="overlay"></div>
                                                </div>
                                            @else
                                                <a href="{{url($media['name'])}}"
                                                   class="col col-4-width"><img
                                                            src="{{url($media['name'])}}" alt="photo"></a>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            </article>
                            <!-- ... end Post -->
                        </div>
                        @if(Auth::id() === $confirmation->challenge->user_id)
                            <div class="col-md-12">
                                <a href=""
                                   class="btn btn-md-4 btn-success">@lang('messages.challenge_confirmation_accept')</a>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#rejection_reason"
                                   class="btn btn-md-4 btn-danger">@lang('messages.challenge_confirmation_reject')</a>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
    <?= view('frontend.profile.blocks.popups') ?>



    <div class="modal fade" id="rejection_reason" tabindex="-1" role="dialog"
         aria-labelledby="create-friend-group-add-friends" aria-hidden="true">
        <div class="modal-dialog window-popup create-friend-group create-friend-group-add-friends" role="document">
            <div class="modal-content">
                <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                    @svg('close-icon', 'olymp-close-icon')
                </a>

                <div class="modal-header">
                    <h6 class="title">@lang('messages.challenge_confirmation_reject')</h6>
                </div>

                <div class="modal-body">
                    <form class="form-group invitations_form label-floating is-select"
                          action="{{route('invitation.send')}}"
                          method="post">
                        {{csrf_field()}}
                        <span class="text-lg-center">@lang('messages.challenge_confirmation_rejection_text')</span>
                        <div class="form-group">
                            <textarea class="form-control" id="rejection_comment" name="rejection_comment"
                                      style="height: 120px"></textarea>
                            <strong class="text-error" id="invitation_text-error"></strong>
                        </div>
                        <input type="submit" class="btn btn-danger btn-lg full-width" value="Отклонить"
                               style="padding: 1rem;">
                    </form>


                </div>
            </div>
        </div>
    </div>




@endsection
@section('scripts')
    <script src="{{asset('frontend/js/jquery-confirm.js')}}"></script>
@endsection