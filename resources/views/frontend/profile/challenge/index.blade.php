<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer $link
 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $challenges
 * @var \App\Models\DB\Challenge\Challenge $challenge
 * @var array $friendships
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title')
    @lang('messages.feed.challenges')
@endsection
<link rel="stylesheet" href="{{asset('frontend/css/jquery-confirm.css')}}">
@section('content')
    <!-- ... end Responsive Header-BP -->
    <div class="header-spacer header-spacer"></div>
    <!-- Main Header Music -->
    {{--<div class="main-header">--}}
        {{--<div class="content-bg-wrap bg-music"></div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">--}}
                    {{--<div class="main-header-content">--}}
                        {{--<h1>@lang('messages.take_part_in_challenges')</h1>--}}
                        {{--<p>@lang('messages.take_part_in_challenges_2')</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<img class="img-bottom" src="{{asset('frontend/img/badges-bottom.png')}}" alt="friends">--}}
    {{--</div>--}}

    <div class="container">
        @if(Auth::id() === $profile->user_id)
            <div class="ui-block responsive-flex">
                <div class="ui-block-title">
                    <div class="h6 title">@lang('messages.challenges_list')</div>

                    <div class="align-right">
                        <a href="{{route('challenge.new-form')}}" class="btn btn-primary btn-md-2">@lang('messages.create_challenge')</a>
                        <a href="{{route('profile.challenge-confirmations')}}" class="btn btn-md-2 btn-border-think custom-color c-grey">@lang('messages.challenge_confirmations')</a>
                    </div>

                </div>
            </div>
        @endif
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <?= view('frontend.profile.challenge.search-form', ['profile' => $profile]) ?>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row challenges_data">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">@lang('messages.challenges_list')</h6>
                    </div>
                    <table class="event-item-table">
                        <tbody>
                        @forelse($challenges->items() as $challenge)
                            <tr class="event-item challengeItem">
                                <td class="author">
                                    <div class="event-author inline-items">
                                        <div class="author-thumb">
                                            <img src="{{url($challenge->getTitleImage())}}"
                                                 style="width: 34px; min-height: 34px;object-fit: cover;object-position: top;"
                                                 alt="author">
                                        </div>
                                        <div class="author-date">
                                            <a href="{{route('challenge.show', ['id' => $challenge->id])}}"
                                               class="author-name h6">{{$challenge->title}}</a>
                                            @if($challenge->deadline)
                                                <time class="published" datetime="">{{$challenge->deadline}}
                                                </time>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td class="location">
                                    <div class="place inline-items">
                                        <span>@lang('messages.created')
                                            : {{$challenge->created_at->diffForHumans()}}</span>
                                    </div>
                                </td>
                                <td class="description">
                                    <p class="description"><strong
                                                class="text-error">{{\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\Challenge\Challenge::STATUS_LIST)[$challenge->status]}}</strong>
                                    </p>
                                </td>
                                <td class="users">
                                    @foreach($challenge->tags as $tag)
                                        <a href="{{route('search.challenges', ['tag' => $tag->name])}}">#{{$tag->name}}</a>
                                    @endforeach
                                </td>
                                <td class="add-event">
                                    <div class="more">
                                        <a href="javascript:void(0);" class="btn btn-breez btn-sm">Действия</a>
                                        <ul class="more-dropdown">
                                            @if(Auth::id() === $challenge->user_id)
                                                <li>
                                                    <a href="{{route('challenge.edit-form', ['id' => $challenge->id])}}">@lang('messages.update_challenge')</a>
                                                </li>
                                                @if($challenge->status !== \App\Models\DB\Challenge\Challenge::STATUS_DELETED)
                                                    <li>
                                                        <a href=""
                                                           data-route="{{route('challenge.delete', ['id' => $challenge->id])}}"
                                                           data-no="@lang('messages.no')"
                                                           data-yes="@lang('messages.yes')"
                                                           data-title="@lang('messages.are_you_sure_to_delete')"
                                                           class="deleteChallengeFromIndex">@lang('messages.challenge_delete')</a>
                                                    </li>
                                                @endif
                                            @endif
                                            <li>
                                                <a href="{{route('challenge.show', ['id' => $challenge->id])}}">@lang('messages.challenge_show')</a>
                                            </li>
                                            <li>
                                                <a href="#" data-id="{{$challenge->id}}" class="invitation_btn"
                                                   data-toggle="modal"
                                                   data-target="#invite_to_challenge">@lang('messages.challenge_invite')</a>
                                            </li>
                                            @if($challenge->status === \App\Models\DB\Challenge\Challenge::STATUS_DELETED &&Auth::id() === $challenge->user_id)
                                                <li>
                                                    <a data-route="{{route('challenge.re-new', ['id' => $challenge->id])}}"
                                                       href="javascript:void(0);"
                                                       class="renewChallengeBtn">@lang('messages.challenge_re-new')</a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <h3 class="text-center">@lang('messages.no_challenges')</h3>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        {{$challenges->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= view('frontend.profile.blocks.popups') ?>
    <?= view('frontend.profile.challenge.popups.invite-popup', [
        'friendships' => $friendships,
        'reload'      => false
    ]) ?>

@endsection
@section('scripts')
    <script src="{{asset('frontend/js/custom/invitation.js')}}"></script>
    <script src="{{asset('frontend/js/custom/challenge-filtering.js')}}"></script>
    <script src="{{asset('frontend/js/custom/challenge-delete.js')}}"></script>
    <script src="{{asset('frontend/js/jquery-confirm.js')}}"></script>
@endsection

<style>
    .back-to-top img {
        margin-top: 13px;
    }
</style>