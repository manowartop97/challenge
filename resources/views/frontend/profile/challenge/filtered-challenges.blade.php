<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
    <div class="ui-block">

        <div class="ui-block-title ui-block-title-small">
            <h6 class="title">@lang('messages.challenges_list')</h6>
        </div>


        <table class="event-item-table">
            <tbody>

            @foreach($challenges->items() as $challenge)
                <tr class="event-item">

                    <td class="author">
                        <div class="event-author inline-items">
                            <div class="author-thumb">
                                <img src="{{url($challenge->getTitleImage())}}"
                                     style="width: 34px; min-height: 34px;" alt="author">
                            </div>
                            <div class="author-date">
                                <a href="{{route('challenge.show', ['id' => $challenge->id])}}"
                                   class="author-name h6">{{$challenge->title}}</a>
                                @if($challenge->deadline)
                                    <time class="published" datetime="">{{$challenge->deadline}}
                                    </time>
                                @endif
                            </div>
                        </div>
                    </td>
                    <td class="location">
                        <div class="place inline-items">
                                        <span>@lang('messages.created')
                                            : {{$challenge->created_at->diffForHumans()}}</span>
                        </div>
                    </td>
                    <td class="description">
                        <p class="description"><strong
                                    class="text-error">{{\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\Challenge\Challenge::STATUS_LIST)[$challenge->status]}}</strong>
                        </p>
                    </td>
                    <td class="users">
                        @foreach($challenge->tags as $tag)
                            <a href="{{route('search.challenges', ['tag' => $tag->name])}}">#{{$tag->name}}</a>
                        @endforeach
                    </td>
                    <td class="add-event">
                        <div class="more">
                            <a href="javascript:void(0);" class="btn btn-breez btn-sm">Действия</a>
                            <ul class="more-dropdown">
                                <li>
                                    <a href="{{route('challenge.edit-form', ['id' => $challenge->id])}}">@lang('messages.update_challenge')</a>
                                </li>
                                @if($challenge->status !== \App\Models\DB\Challenge\Challenge::STATUS_DELETED)
                                    <li>
                                        <a href=""
                                           data-route="{{route('challenge.delete', ['id' => $challenge->id])}}"
                                           data-no="@lang('messages.no')" data-yes="@lang('messages.yes')"
                                           data-title="@lang('messages.are_you_sure_to_delete')"
                                           class="deleteChallengeFromIndex">@lang('messages.challenge_delete')</a>
                                    </li>
                                @endif
                                <li>
                                    <a href="{{route('challenge.show', ['id' => $challenge->id])}}">@lang('messages.challenge_show')</a>
                                </li>
                                <li>
                                    <a href="#" data-id="{{$challenge->id}}" class="invitation_btn"
                                       data-toggle="modal"
                                       data-target="#invite_to_challenge">@lang('messages.challenge_invite')</a>
                                </li>
                                @if($challenge->status === \App\Models\DB\Challenge\Challenge::STATUS_DELETED)
                                    <li>
                                        <a data-route="{{route('challenge.re-new', ['id' => $challenge->id])}}"
                                           href="javascript:void(0);"
                                           class="renewChallengeBtn">@lang('messages.challenge_re-new')</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="col-md-12">
            {{$challenges->links()}}
        </div>
    </div>
</div>