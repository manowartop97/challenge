<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer $link
 * @var \Spatie\Tags\Tag $tag
 */
?>

@extends('layouts.frontend.profile-layout.main')
<link rel="stylesheet" href="{{asset('frontend/css/tags/mab-jquery-taginput.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/overhang.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/custom/challenge.css')}}">
@section('title')
    @lang('messages.create_challenge')
@endsection
@section('content')
    <div class="header-spacer"></div>
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title bg-blue">
                        <h6 class="title c-white">@lang('messages.create_challenge')</h6>
                    </div>
                    <div class="ui-block-content">
                        <form method="post" class="challenge_form" action="{{route('challenge.create')}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <input type="hidden" id="user_id" name="user_id" value="{{Auth::id()}}">
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="ui-block" data-mh="friend-groups-item" style="height: 220px;">
                                                <div class="friend-item friend-groups">
                                                    <div class="friend-item-content">
                                                        <div class="friend-avatar">
                                                            <div class="author-thumb title_preview">
                                                                <img src="{{asset('frontend/img/choose-photo1.jpg')}}"
                                                                     alt="title_image"
                                                                     style="width: 120px; height: 120px; object-fit: cover;">
                                                            </div>
                                                            <div class="author-content">
                                                                <div class="h5 author-name">
                                                                    <div class="file-upload">
                                                                        <label for="title_image"
                                                                               class="file-upload__label">@lang('messages.challenge.title_image')</label>
                                                                        <input id="title_image"
                                                                               class="file-upload__input"
                                                                               type="file" name="title_image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ... end Friend Item -->
                                            </div>
                                            <strong class="text-error" id="title_image-error"></strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('messages.challenge.title') <span
                                                        class="text-danger">*</span> </label>
                                            <input class="form-control" id="title" type="text" name="title"
                                                   placeholder=""
                                                   value="{{old('title')}}">
                                            <span class="material-input"></span>
                                            <strong class="text-error" id="title-error"></strong>
                                        </div>

                                    </div>

                                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('messages.challenge.tags') <span
                                                        class="text-danger">*</span></label>
                                            <input class="form-control" type="text" id="tags" name="tags" placeholder=""
                                                   value="{{old('tags')}}">
                                            <span class="material-input"></span>
                                            <strong class="text-error" id="tags-error"></strong>
                                        </div>

                                    </div>
                                    <br>
                                    {{--<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">--}}
                                        {{--<div class="description-toggle">--}}
                                            {{--<div class="description-toggle-content">--}}
                                                {{--<div class="h6">@lang('messages.challenge.is_group')</div>--}}
                                                {{--<p>@lang('messages.challenge.is_group_2')</p>--}}
                                            {{--</div>--}}
                                            {{--<input type="hidden" name="is_group_challenge" value="0">--}}
                                            {{--<div class="togglebutton">--}}
                                                {{--<label>--}}
                                                    {{--<input type="checkbox" id="is_group_challenge" value="1"--}}
                                                           {{--name="is_group_challenge" {{old('is_group_challenge') ? 'checked' : ''}}>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                            {{--<strong class="text-error" id="is_group_challenge-error"></strong>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group date-time-picker">
                                            <label class="control-label">@lang('messages.challenge.deadline') <a
                                                        href="#" data-toggle="tooltip" data-placement="right"
                                                        data-original-title="@lang('messages.challenge_deadline_info')"><i
                                                            class="fa fa-question-circle"></i></a></label>
                                            <input class="datetimepicker" id="deadline"
                                                   value="{{old('deadline') ?: ''}}"
                                                   name="deadline" autocomplete="off"/>
                                            <span class="input-group-addon" style="top:15%">
                                            @svg('calendar-icon', ['olymp-calendar-icon'])
										</span>
                                            <strong class="text-error" id="deadline-error"></strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('messages.challenge.description') <span
                                                        class="text-danger">*</span></label>
                                            <textarea class="form-control" id="description" name="description"
                                                      style="height: 240px">{{old('description')}}</textarea>
                                            <span class="material-input"></span>
                                            <strong class="text-error" id="description-error"></strong>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="file-upload">
                                        <label for="additional_media"
                                               class="file-upload__label">@lang('messages.challenge.additional_media')</label>
                                        <input id="additional_media" class="file-upload__input"
                                               type="file" name="additional_media[]" multiple>
                                        <strong class="text-error" id="additional_media-error"></strong>
                                    </div>

                                    <br>
                                    <div class="post-block-photo js-zoom-gallery" id="preview">

                                    </div>
                                    <div class="col-md-12 clearMedia" style="display: none;">
                                        <button class="btn btn-lg full-width btn-border-think c-grey btn-transparent custom-color clear_additional_media"
                                                data-message="{{trans('messages.are_you_sure_to_delete_attachments')}}">@lang('messages.clear_attachments')
                                            <div class="ripple-container"></div>
                                        </button>
                                    </div>
                                </div>

                                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <input type="submit" class="btn btn-blue btn-lg full-width"
                                           value="@lang('messages.create_challenge')">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('frontend/js/tags/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset('frontend/js/tags/mab-jquery-taginput.js')}}"></script>
    <script src="{{asset('frontend/js/custom/challenge.js')}}"></script>
    <script src="{{asset('frontend/js/custom/init_datepicker.js')}}"></script>
    <script src="{{asset('frontend/js/overhang.min.js')}}"></script>
    <script>
        /**
         * Тег инпут
         */
        var tagsArray = [];

        @foreach($tags as $tag)
        tagsArray.push({tag: '{{$tag->name}}'});
        @endforeach
        var tags = new Bloodhound({
            datumTokenizer: function (d) {
                return Bloodhound.tokenizers.whitespace(d.tag);
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: tagsArray
        });
        tags.initialize();
    </script>

    <style>
        .custom_label {
            padding: 4px 10px;
            border-radius: 3px;
            background-color: #9a9fbf;
            color: #fff;
            display: inline-block;
            margin-right: 3px;
        }

        .back-to-top .back-icon {
            height: 20px;
            width: 20px;
            margin-top: 13px;
        }

        .custom_label > .remove_tag {
            margin-top: 2px;
        }

        .image-block {
            border: black solid 1px;
            box-sizing: border-box;
            float: left;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            width: 24%;
            height: 0;
            padding-bottom: 25%;
            margin-bottom: 5px;
            margin-left: 0.5%;
        }

        /* Optional, arbitrary column-count change for smaller screens: */
        @media (max-width: 700px) {
            .image-block {
                width: 49%;
                padding-bottom: 50%;
            }
        }
    </style>
@endsection