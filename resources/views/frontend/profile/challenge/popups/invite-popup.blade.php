<?php
/**
 * @var \App\Models\DB\Friendship\Friendship $friendship
 */
?>

<div class="modal fade" id="invite_to_challenge" tabindex="-1" role="dialog"
     aria-labelledby="create-friend-group-add-friends" aria-hidden="true">
    <div class="modal-dialog window-popup create-friend-group create-friend-group-add-friends" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                @svg('close-icon', 'olymp-close-icon')
            </a>

            <div class="modal-header">
                <h6 class="title">@lang('messages.challenge_invite_friends_modal_text')</h6>
            </div>

            <div class="modal-body">
                <form class="form-group invitations_form label-floating is-select" data-reload="{{$reload}}"
                      action="{{route('invitation.send')}}"
                      method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="challenge_id" id="challenge_id">
                    <input type="hidden" name="invitor_id" value="{{Auth::id()}}">
                    <strong class="text-error" id="ids-error"></strong>
                    <select class="selectpicker form-control style-2 show-tick" name="ids[]" multiple
                            data-max-options=""
                            data-virtual-scroll="5"
                            data-live-search="true">
                        @foreach($friendships as $friendship)
                            @php $friend = Auth::id() === $friendship->user_id ? $friendship->friend : $friendship->user @endphp
                            <option title="{{$friend->getFullName()}}" data-content='<div class="inline-items">
										<div class="author-thumb">
											<img src="{{url('storage/'.$friend->getAvatar())}}" alt="author">
										</div>
											<div class="h6 author-title">{{$friend->getFullName()}}</div>

										</div>'>{{$friend->id}}
                            </option>
                        @endforeach
                    </select>

                    <hr>
                    <span class="text-lg-center">@lang('messages.invitation_text')</span>
                    <textarea class="form-control" id="invitation_text" name="invitation_text"
                              style="height: 120px"></textarea>
                    <strong class="text-error" id="invitation_text-error"></strong>
                    <span class="material-input"></span>
                    <strong class="text-error" id="invitation_text-error"></strong>


                </form>

                <a href="#"
                   class="btn btn-blue invite_friends_popup btn-lg full-width">@lang('messages.challenge_invite')</a>
            </div>
        </div>
    </div>
</div>

<style>
    #invite_to_challenge img {
        width: 28px;
        height: 28px;
        object-fit: cover;
    }

    #invite_to_challenge > div > div > div.modal-body > form > div > div > ul {
        overflow: scroll !important;
        overflow-x: hidden !important;
        height: 250px !important;
    }

    #invite_to_challenge > div > div > div.modal-body > form > div > div > ul {
        height: 280px !important;
        max-height: 280px !important;
    }

    #invite_to_challenge > div > div > div.modal-body > form > div {
        height: 350px !important;
    }
</style>