<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer $link
 * @var \Spatie\Tags\Tag $tag
 * @var \App\Models\DB\Challenge\Challenge $challenge
 */
?>

@extends('layouts.frontend.profile-layout.main')
<link rel="stylesheet" href="{{asset('frontend/css/tags/mab-jquery-taginput.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/overhang.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/custom/challenge.css')}}">
@section('title')
    @lang('messages.update_challenge')
@endsection
@section('content')
    <div class="header-spacer"></div>
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title bg-blue">
                        <h6 class="title c-white">@lang('messages.update_challenge')</h6>
                    </div>
                    <div class="ui-block-content">
                        <form method="post" class="edit_challenge_form"
                              action="{{route('challenge.update', ['id' => $challenge->id])}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <input type="hidden" id="user_id" name="user_id" value="{{$challenge->user_id}}">
                                <input type="hidden" id="is_title_image"
                                       value="{{$challenge->getTitleImage() ? 1 : 0}}">
                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="ui-block" data-mh="friend-groups-item" style="height: 220px;">
                                                <div class="friend-item friend-groups">
                                                    <div class="friend-item-content">
                                                        <div class="friend-avatar">
                                                            <div class="author-thumb title_preview">
                                                                <img src="{{url($challenge->getTitleImage())}}"
                                                                     alt="title_image"
                                                                     style="width: 120px; height: 120px; object-fit: cover;">
                                                            </div>
                                                            <div class="author-content">
                                                                <div class="h5 author-name">
                                                                    <div class="file-upload">
                                                                        <label for="title_image"
                                                                               class="file-upload__label">@lang('messages.challenge.title_image')</label>
                                                                        <input id="title_image"
                                                                               class="file-upload__input"
                                                                               type="file" name="title_image">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- ... end Friend Item -->
                                            </div>
                                            <strong class="text-error" id="title_image-error"></strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('messages.challenge.title') <span
                                                        class="text-danger">*</span> </label>
                                            <input class="form-control" id="title" type="text" name="title"
                                                   placeholder=""
                                                   value="{{$challenge->title}}">
                                            <span class="material-input"></span>
                                            <strong class="text-error" id="title-error"></strong>
                                        </div>

                                    </div>

                                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('messages.challenge.tags') <span
                                                        class="text-danger">*</span></label>
                                            <input class="form-control" type="text" id="tags" name="tags" placeholder=""
                                                   value="{{implode('|', $challenge->tags()->pluck('slug', 'slug')->all())}}">
                                            <span class="material-input"></span>
                                            <strong class="text-error" id="tags-error"></strong>
                                        </div>

                                    </div>
                                    <br>
                                    {{--<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">--}}
                                        {{--<div class="description-toggle">--}}
                                            {{--<div class="description-toggle-content">--}}
                                                {{--<div class="h6">@lang('messages.challenge.is_group')</div>--}}
                                                {{--<p>@lang('messages.challenge.is_group_2')</p>--}}
                                            {{--</div>--}}
                                            {{--<input type="hidden" name="is_group_challenge" value="0">--}}
                                            {{--<div class="togglebutton">--}}
                                                {{--<label>--}}
                                                    {{--<input type="checkbox" id="is_group_challenge" value="1"--}}
                                                           {{--name="is_group_challenge" {{$challenge->is_group_challenge ? 'checked' : ''}}>--}}
                                                {{--</label>--}}
                                            {{--</div>--}}
                                            {{--<strong class="text-error" id="is_group_challenge-error"></strong>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group date-time-picker">
                                            <label class="control-label">@lang('messages.challenge.deadline') <a
                                                        href="#" data-toggle="tooltip" data-placement="right"
                                                        data-original-title="@lang('messages.challenge_deadline_info')"><i
                                                            class="fa fa-question-circle"></i></a></label>
                                            <input class="datetimepicker" id="deadline"
                                                   value="{{$challenge->deadline}}"
                                                   name="deadline" autocomplete="off"/>
                                            <span class="input-group-addon" style="top:15%">
                                            @svg('calendar-icon', ['olymp-calendar-icon'])
										</span>
                                            <strong class="text-error" id="deadline-error"></strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">@lang('messages.challenge.description') <span
                                                        class="text-danger">*</span></label>
                                            <textarea class="form-control" id="description" name="description"
                                                      style="height: 240px">{{$challenge->description}}</textarea>
                                            <span class="material-input"></span>
                                            <strong class="text-error" id="description-error"></strong>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="file-upload">
                                        <label for="additional_media"
                                               class="file-upload__label">@lang('messages.challenge.additional_media')</label>
                                        <input id="additional_media" class="file-upload__input"
                                               type="file" name="additional_media[]" multiple>
                                        <strong class="text-error" id="additional_media-error"></strong>
                                    </div>
                                    <input type="hidden" name="is_additional_media" id="is_additional_media"
                                           value="{{$challenge->additional_media ? 1 : 0}}">
                                    <br>
                                    <div class="post-block-photo js-zoom-gallery" id="preview">
                                        @if($challenge->additional_media)
                                            @foreach($challenge->getAdditionalMedia() as $media)
                                                @if($media['type'] === \App\Repositories\Challenge\ChallengeRepository::TYPE_VIDEO)
                                                    <div class="video-player custom col col-3-width"
                                                         style="margin: 0px;">
                                                        <img src="{{url($media['thumbnail'])}}" alt="photo">
                                                        <a href="{{url($media['name'])}}" class="play-video"
                                                           style="opacity: 1">
                                                            @svg('play-icon', 'olymp-play-icon')
                                                        </a>

                                                        <div class="video-content">
                                                        </div>

                                                        <div class="overlay"></div>
                                                    </div>
                                                @else
                                                    <a href="{{url($media['name'])}}"
                                                       class="col col-3-width"><img
                                                                src="{{url($media['name'])}}" alt="photo"></a>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-lg full-width btn-border-think c-grey btn-transparent custom-color clear_additional_media clearMedia"
                                                style="{{$challenge->additional_media ? '' : 'display:none;'}}"
                                                data-message="{{trans('messages.are_you_sure_to_delete_attachments')}}">@lang('messages.clear_attachments')
                                            <div class="ripple-container"></div>
                                        </button>
                                    </div>
                                </div>

                                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                    <input type="submit" class="btn btn-blue btn-lg full-width"
                                           value="@lang('messages.update_challenge')">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('frontend/js/tags/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset('frontend/js/tags/mab-jquery-taginput.js')}}"></script>
    <script src="{{asset('frontend/js/custom/challenge.js')}}"></script>
    <script src="{{asset('frontend/js/custom/init_datepicker.js')}}"></script>
    <script src="{{asset('frontend/js/overhang.min.js')}}"></script>
    <script>
        /**
         * Тег инпут
         */
        var tagsArray = [];

        @foreach($tags as $tag)
        tagsArray.push({tag: '{{$tag->name}}'});
                @endforeach
        var tags = new Bloodhound({
                datumTokenizer: function (d) {
                    return Bloodhound.tokenizers.whitespace(d.tag);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: tagsArray
            });
        tags.initialize();
    </script>
@endsection