<?php
/**
 * @var \App\Models\DB\Challenge\Challenge $challenge
 */
?>

@extends('layouts.frontend.profile-layout.main')
<link rel="stylesheet" href="{{asset('frontend/css/custom/challenge.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/jquery-confirm.css')}}">
@section('title')
    @lang('messages.challenge.show')
@endsection
@section('content')
    <div class="header-spacer"></div>
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title bg-blue">
                        <h6 class="title c-white">@lang('messages.challenge_show')</h6>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <span class="text-center">@include('flash::message')</span>
                        <div class="ui-block">
                            <!-- Search Result -->
                            <article class="hentry post searches-item">

                                <div class="post__author author vcard inline-items">
                                    <img src="{{url($challenge->getTitleImage())}}" alt="author">

                                    <div class="author-date">
                                        <a class="h6 post__author-name fn" href="#">{{$challenge->title}}</a>
                                        <div class="country"><a
                                                    href="#">@lang('messages.created')
                                                : {{$challenge->created_at->diffForHumans()}}</a>
                                        </div>
                                    </div>

                                    <div class="more">
                                        @svg('three-dots-icon', 'olymp-three-dots-icon')
                                        <ul class="more-dropdown">
                                            @if(Auth::id() !== $challenge->user_id)
                                                <li>
                                                    <a href="#">@lang('messages.challenge_report')</a>
                                                </li>
                                            @endif
                                            <li>
                                                <a href=""
                                                   data-route="{{route('challenge.delete', ['id' => $challenge->id])}}"
                                                   data-no="@lang('messages.no')" data-yes="@lang('messages.yes')"
                                                   data-title="@lang('messages.are_you_sure_to_delete')"
                                                   class="deleteChallengeFromDetail">@lang('messages.challenge_delete')</a>
                                            </li>
                                            <li>
                                                <a href="#">Turn Off Notifications</a>
                                            </li>
                                            <li>
                                                <a href="#">Select as Featured</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="user-description">
                                    {!! nl2br($challenge->description) !!}
                                </p>
                                @if($challenge->additional_media)
                                    <div class="post-block-photo js-zoom-gallery">
                                        @foreach($challenge->getAdditionalMedia() as $media)
                                            @if($media['type'] === \App\Repositories\Challenge\ChallengeRepository::TYPE_VIDEO)
                                                <div class="video-player custom col col-4-width" style="margin: 0px;">
                                                    <img src="{{url($media['thumbnail'])}}" alt="photo">
                                                    <a href="{{url($media['name'])}}" class="play-video"
                                                       style="opacity: 1">
                                                        @svg('play-icon', 'olymp-play-icon')
                                                    </a>

                                                    <div class="video-content">
                                                    </div>

                                                    <div class="overlay"></div>
                                                </div>
                                            @else
                                                <a href="{{url($media['name'])}}"
                                                   class="col col-4-width"><img
                                                            src="{{url($media['name'])}}" alt="photo"></a>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                                <div class="post-additional-info">
                                    <div class="post__author author vcard inline-items">
                                        <img src="{{url('storage/'.$challenge->user->getAvatar())}}" alt="author">

                                        <div class="author-date">
                                            <a class="h6 post__author-name fn"
                                               href="{{route('page.show-user', ['id' => $challenge->user_id])}}">{{$challenge->user->getFullName()}}</a>
                                            <div class="country"><a
                                                        href="#"></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="friend-count">
                                        <a href="#" class="friend-count-item">
                                            <div class="h6 participantsCount">{{$participantsCount}}</div>
                                            <div class="title">@lang('messages.challenge_participants')</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6 invitationsCount">{{$invitationsCount}}</div>
                                            <div class="title">@lang('messages.challenge_invitations_count')</div>
                                        </a>
                                        <a href="#" class="friend-count-item">
                                            <div class="h6 yourInvitationsCount">{{$yourInvitationsCount}}</div>
                                            <div class="title">@lang('messages.challenge_your_invitations_count')</div>
                                        </a>
                                    </div>

                                </div>

                            </article>
                            <!-- ... end Search Result -->
                        </div>
                    </div>
                    @if(Auth::id() !== $challenge->user_id)
                        <div class="col-md-12">
                            @if($challenge->is_group_challenge)
                                <a href="#" class="btn btn-blue btn-lg">@lang('messages.challenge_take-part_group')
                                    <div class="ripple-container"></div>
                                </a>
                            @else
                                <a href="{{route('challenge.confirmation', ['id' => $challenge->id])}}"
                                   class="btn btn-purple confirmChallengeBtn btn-lg"
                                   style="display: {{$challenge->isActiveInvitationExists() && !$challenge->isConfirmationCreated() ? '' : 'none'}}">@lang('messages.challenge_finish')
                                </a>
                                @if($challenge->isActiveInvitationExists())
                                    @if(!$challenge->isConfirmationCreated())
                                        <a href="#" class="btn btn-danger btn-lg loader cancelTakePartBtn"
                                           data-route="{{route('invitation.cancel-participation', ['id' => $challenge->id])}}">@lang('messages.challenge_cancel_take-part')
                                        </a>
                                    @else
                                       <h3> <a href="">@lang('messages.challenge_is_not_confirmed_yet')</a></h3>

                                    @endif
                                @else
                                    @if(!$challenge->isChallengeDone())
                                        <a href="#" class="btn btn-success btn-lg loader takePartBtn"
                                           data-route="{{route('invitation.take-part', ['id' => $challenge->id])}}">@lang('messages.challenge_take-part')
                                        </a>
                                    @endif
                                @endif
                            @endif
                            @if($challenge->isChallengeDone())
                                <a href="#" data-id="{{$challenge->id}}" data-toggle="modal"
                                   data-target="#invite_to_challenge"
                                   class="btn btn-blue btn-lg invitation_btn">@lang('messages.challenge_invite')
                                    <div class="ripple-container"></div>
                                </a>
                            @endif
                        </div>
                    @else
                        <div class="col-md-12">
                            <a href="#" data-id="{{$challenge->id}}" data-toggle="modal"
                               data-target="#invite_to_challenge"
                               class="btn btn-blue btn-lg invitation_btn">@lang('messages.challenge_invite')
                                <div class="ripple-container"></div>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <?= view('frontend.profile.blocks.popups') ?>
    <?= view('frontend.profile.challenge.popups.invite-popup', [
        'friendships' => Auth::user()->getFriends(),
        'reload'      => true
    ]) ?>

@endsection
@section('scripts')
    <script src="{{asset('frontend/js/custom/invitation.js')}}"></script>
    <script src="{{asset('frontend/js/custom/challenge-delete.js')}}"></script>
    <script src="{{asset('frontend/js/jquery-confirm.js')}}"></script>
@endsection