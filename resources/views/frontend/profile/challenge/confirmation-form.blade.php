<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer $link
 * @var \Spatie\Tags\Tag $tag
 * @var \App\Models\DB\Challenge\Challenge $challenge
 */
?>

@extends('layouts.frontend.profile-layout.main')
<link rel="stylesheet" href="{{asset('frontend/css/overhang.min.css')}}">
<link rel="stylesheet" href="{{asset('frontend/css/custom/challenge.css')}}">
@section('title')
    @lang('messages.challenge_finish')
@endsection
@section('content')
    <div class="header-spacer"></div>
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title bg-purple">
                        <h6 class="title c-white">@lang('messages.challenge_finish'): <strong>"{{$challenge->title}}
                                "</strong></h6>
                    </div>
                    <div class="ui-block-content">
                        <div class="col-md-12">
                            <div class="ui-block">
                                <div class="ui-block-content" style="background-color: coral">
                                    <h5 class="text-center">@lang('messages.challenge_confirmation_instruction')</h5>
                                </div>
                            </div>
                        </div>
                        <form method="post" class="confirm_challenge_form" action="{{route('challenge.confirm')}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="challenge_id" id="challenge_id" value="{{$challenge->id}}">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">@lang('messages.challenge.confirmation_text')</label>
                                        <textarea class="form-control" id="comment" name="comment"
                                                  style="height: 240px">{{old('comment')}}</textarea>
                                        <span class="material-input"></span>
                                        <strong class="text-error" id="comment-error"></strong>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12">
                                <div class="file-upload">
                                    <label for="additional_media"
                                           class="file-upload__label">@lang('messages.challenge.additional_media')</label>
                                    <input id="additional_media" class="file-upload__input"
                                           type="file" name="additional_media[]" multiple>
                                    <strong class="text-error" id="additional_media-error"></strong>
                                </div>

                                <br>
                                <div class="post-block-photo js-zoom-gallery" id="preview">

                                </div>
                                <div class="col-md-12 clearMedia" style="display: none;">
                                    <button class="btn btn-lg full-width btn-border-think c-grey btn-transparent custom-color clear_additional_media"
                                            data-message="{{trans('messages.are_you_sure_to_delete_attachments')}}">@lang('messages.clear_attachments')
                                        <div class="ripple-container"></div>
                                    </button>
                                </div>
                            </div>

                            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <input type="submit" class="btn btn-purple btn-lg full-width"
                                       value="@lang('messages.challenge_finish')">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('frontend/js/tags/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset('frontend/js/custom/challenge.js')}}"></script>
    <script src="{{asset('frontend/js/overhang.min.js')}}"></script>

    <style>
        .custom_label {
            padding: 4px 10px;
            border-radius: 3px;
            background-color: #9a9fbf;
            color: #fff;
            display: inline-block;
            margin-right: 3px;
        }

        .back-to-top .back-icon {
            height: 20px;
            width: 20px;
            margin-top: 13px;
        }

        .custom_label > .remove_tag {
            margin-top: 2px;
        }

        .image-block {
            border: black solid 1px;
            box-sizing: border-box;
            float: left;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            width: 24%;
            height: 0;
            padding-bottom: 25%;
            margin-bottom: 5px;
            margin-left: 0.5%;
        }

        /* Optional, arbitrary column-count change for smaller screens: */
        @media (max-width: 700px) {
            .image-block {
                width: 49%;
                padding-bottom: 50%;
            }
        }
    </style>
@endsection