<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer $link
 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $confirmations
 * @var \App\Models\DB\Challenge\ChallengeConfirmation $confirmation
 * @var int $type
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title')
    @lang('messages.confirmations')
@endsection
<link rel="stylesheet" href="{{asset('frontend/css/jquery-confirm.css')}}">
@section('content')
    <!-- ... end Responsive Header-BP -->
    <div class="header-spacer header-spacer"></div>
    <div class="container">
        <div class="ui-block responsive-flex">
            <div class="ui-block-title">
                <div class="h6 title">@lang('messages.confirmations')</div>
                <div class="align-right">
                    <a href="{{route('profile.challenge-confirmations', ['type' => \App\Repositories\Challenge\ConfirmationsRepository::TYPE_INCOME])}}"
                       class="{{!Request::get('type') || Request::get('type') == \App\Repositories\Challenge\ConfirmationsRepository::TYPE_INCOME ? 'btn btn-primary btn-md-2' : 'btn btn-md-2 btn-border-think custom-color c-grey'}}">@lang('messages.confirmations_outcome')</a>
                    <a href="{{route('profile.challenge-confirmations', ['type' => \App\Repositories\Challenge\ConfirmationsRepository::TYPE_OUTCOME])}}"
                       class="{{Request::get('type') == \App\Repositories\Challenge\ConfirmationsRepository::TYPE_OUTCOME ? 'btn btn-primary btn-md-2' : 'btn btn-md-2 btn-border-think custom-color c-grey'}}">@lang('messages.confirmations_income')</a>

                </div>

            </div>
        </div>
    </div>
    <ul class="cat-list-bg-style align-center sorting-menu cat-list__item-bg-blue filter_route"
        data-route="{{route('challenge.all')}}">
        <li class="cat-list__item {{!Request::get('status')? 'active' : ''}}" data-filter="0">
            <a href="{{route('profile.challenge-confirmations', ['type' => Request::get('type'), 'status' => 0])}}" class="filter_status">@lang('messages.all')</a>
        </li>
        @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\Challenge\ChallengeConfirmation::STATUS_LIST) as $id => $title)
            <li class="cat-list__item {{Request::get('status') == $id ? 'active' : ''}}" data-filter="{{$id}}">
                <a href="{{route('profile.challenge-confirmations', ['type' => Request::get('type'), 'status' => $id])}}"
                   class="filter_status">{{$title}}</a>
            </li>
        @endforeach
    </ul>

    <div class="container">
        <div class="row">
            @forelse($confirmations->items() as $confirmation)
                <div class="col col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">
                        <div class="birthday-item inline-items">
                            <div class="author-thumb">
                                <img src="{{Request::get('type') == \App\Repositories\Challenge\ConfirmationsRepository::TYPE_OUTCOME ? url($confirmation->challenge->getTitleImage()) : url('storage/'.$confirmation->challengeInvitation->participant->getAvatar())}}"
                                     alt="author" style="width: 40px; height: 40px;">
                            </div>
                            <div class="birthday-author-name">
                                <a href="{{route('profile.confirmation-detail', ['id' => $confirmation->id])}}" class="h6 author-name">{{$confirmation->challenge->title}} </a>
                                <div class="birthday-date">{{$confirmation->created_at->diffForHumans()}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">
                        <div class="ui-block-title"><h4 class="text-center">@lang('messages.no_confirmations')</h4></div>
                    </div>
                </div>
            @endforelse
            <div class="col-md-12">
                {{$confirmations->appends(request()->except('page'))->links()}}
            </div>
        </div>
    </div>
    <?= view('frontend.profile.blocks.popups') ?>

@endsection
@section('scripts')
    <script src="{{asset('frontend/js/jquery-confirm.js')}}"></script>
@endsection

<style>
    .back-to-top img {
        margin-top: 13px;
    }
</style>