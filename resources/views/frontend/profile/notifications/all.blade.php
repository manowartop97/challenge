@extends('layouts.frontend.profile-layout.main')
@section('title')
    Уведомления
@endsection
@section('content')

    <?= view('frontend.profile.settings.blocks.header') ?>
    <!-- ... end Main Header Account -->
    <div class="container">
        <div class="row">
            <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Notifications</h6>
                        <a href="#" class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        </a>
                    </div>


                    <!-- Notification List -->

                    <ul class="notification-list">
                        <li>
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar11-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Mathilda Brinker</a> commented on your new <a
                                        href="#" class="notification-link">profile status</a>.
                                <span class="notification-date"><time class="entry-date updated"
                                                                      datetime="2004-07-24T18:18">4 hours ago</time></span>
                            </div>
                            <span class="notification-icon">
												@svg('comments-post-icon', ['olymp-comments-post-icon'])
											</span>

                            <div class="more">
                                @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                @svg('little-delete', ['olymp-little-delete'])
                            </div>
                        </li>

                        <li class="un-read">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar2-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                You and <a href="#" class="h6 notification-friend">Nicholas Grissom</a> just became
                                friends. Write on <a href="#" class="notification-link">his wall</a>.
                                <span class="notification-date"><time class="entry-date updated"
                                                                      datetime="2004-07-24T18:18">9 hours ago</time></span>
                            </div>
                            <span class="notification-icon">

												@svg('happy-face-icon', ['olymp-happy-face-icon'])
											</span>

                            <div class="more">
                                @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                @svg('little-delete', ['olymp-little-delete'])
                            </div>
                        </li>

                        <li class="with-comment-photo">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar3-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Sarah Hetfield</a> commented on your <a
                                        href="#" class="notification-link">photo</a>.
                                <span class="notification-date"><time class="entry-date updated"
                                                                      datetime="2004-07-24T18:18">Yesterday at 5:32am</time></span>
                            </div>
                            <span class="notification-icon">
												@svg('comments-post-icon', ['olymp-comments-post-icon'])
											</span>

                            <div class="comment-photo">
                                <img src="{{asset('frontend/img/comment-photo.jpg')}}" alt="photo">
                                <span>“She looks incredible in that outfit! We should see each...”</span>
                            </div>
                            <div class="more">
                                @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                @svg('little-delete', ['olymp-little-delete'])
                            </div>
                        </li>

                        <li>
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar4-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Chris Greyson</a> liked your <a href="#"
                                                                                                           class="notification-link">profile
                                    status</a>.
                                <span class="notification-date"><time class="entry-date updated"
                                                                      datetime="2004-07-24T18:18">March 18th at 8:22pm</time></span>
                            </div>
                            <span class="notification-icon">
											@svg('heart-icon', ['olymp-heart-icon'])
										</span>
                            <div class="more">
                                @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                @svg('little-delete', ['olymp-little-delete'])
                            </div>
                        </li>

                        <li>
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar5-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Green Goo Rock</a> invited you to attend to
                                his event Goo in <a href="#" class="notification-link">Gotham Bar</a>.
                                <span class="notification-date"><time class="entry-date updated"
                                                                      datetime="2004-07-24T18:18">March 5th at 6:43pm</time></span>
                            </div>
                            <span class="notification-icon">
												@svg('calendar-icon', ['olymp-calendar-icon'])
											</span>

                            <div class="more">
                                @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                @svg('little-delete', ['olymp-little-delete'])
                            </div>
                        </li>

                        <li>
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar6-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">James Summers</a> commented on your new <a
                                        href="#" class="notification-link">profile status</a>.
                                <span class="notification-date"><time class="entry-date updated"
                                                                      datetime="2004-07-24T18:18">March 2nd at 8:29pm</time></span>
                            </div>
                            <span class="notification-icon">
												@svg('comments-post-icon', ['olymp-comments-post-icon'])
											</span>

                            <div class="more">
                                @svg('three-dots-icon', ['olymp-three-dots-icon'])

                                @svg('little-delete', ['olymp-little-delete'])
                            </div>
                        </li>

                        <li>
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar7-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Marina Valentine</a> commented on your new <a
                                        href="#" class="notification-link">profile status</a>.
                                <span class="notification-date"><time class="entry-date updated"
                                                                      datetime="2004-07-24T18:18">March 2nd at 10:07am</time></span>
                            </div>
                            <span class="notification-icon">

												@svg('comments-post-icon', ['olymp-comments-post-icon'])
											</span>

                            <div class="more">
                                @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                @svg('little-delete', ['olymp-little-delete'])
                            </div>
                        </li>
                    </ul>

                    <!-- ... end Notification List -->

                </div>


                <!-- Pagination -->

                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1
                                <div class="ripple-container">
                                    <div class="ripple ripple-on ripple-out"
                                         style="left: -10.3833px; top: -16.8333px; background-color: rgb(255, 255, 255); transform: scale(16.7857);"></div>
                                </div>
                            </a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                        <li class="page-item"><a class="page-link" href="#">12</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                </nav>

                <!-- ... end Pagination -->

            </div>

            <?= view('frontend.profile.settings.blocks.sidebar') ?>
        </div>
    </div>

    <?=view('frontend.profile.blocks.popups')?>
@endsection
@section('scripts')
@endsection