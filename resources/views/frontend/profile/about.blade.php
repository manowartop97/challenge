<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer $link
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title')
    Личная информация
@endsection
@section('content')
    <div class="header-spacer"></div>
    <?= view('frontend.profile.blocks.personal-info_block', ['profile' => $profile, 'link' => $link]) ?>
    <div class="container">
        <div class="row">
            <div class="col col-xl-8 order-xl-2 col-lg-8 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">@lang('messages.hobbies_and_interests')</h6>
                        <a href="#" class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        </a>
                    </div>
                    <div class="ui-block-content">
                        <div class="row">
                            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">


                                <!-- W-Personal-Info -->

                                <ul class="widget w-personal-info item-block">
                                    <li>
                                        <span class="title">@lang('messages.hobbies_and_interests'):</span>
                                        <span class="text">
                                            {{$profile->hobbies_and_interests ?: trans('messages.not_set')}}
                                        </span>
                                    </li>
                                    <li>
                                        <span class="title">@lang('messages.favourite_movies'):</span>
                                        <span class="text">{{$profile->favourite_movies ?: trans('messages.not_set')}}</span>
                                    </li>
                                </ul>

                                <!-- ... end W-Personal-Info -->
                            </div>
                            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">


                                <!-- W-Personal-Info -->

                                <ul class="widget w-personal-info item-block">
                                    <li>
                                        <span class="title">@lang('messages.favourite_music'):</span>
                                        <span class="text">{{$profile->favourite_music ?: trans('messages.not_set')}}</span>
                                    </li>
                                    <li>
                                        <span class="title">@lang('messages.favourite_games'):</span>
                                        <span class="text">{{$profile->favourite_games ?: trans('messages.not_set')}}</span>
                                    </li>
                                </ul>

                                <!-- ... end W-Personal-Info -->
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="ui-block">--}}
                    {{--<div class="ui-block-title">--}}
                        {{--<h6 class="title">Education and Employement</h6>--}}
                        {{--<a href="#" class="more">--}}
                            {{--@svg('three-dots-icon', ['olymp-three-dots-icon'])--}}
                        {{--</a>--}}
                    {{--</div>--}}
                    {{--<div class="ui-block-content">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col col-lg-6 col-md-6 col-sm-12 col-12">--}}


                                {{--<!-- W-Personal-Info -->--}}

                                {{--<ul class="widget w-personal-info item-block">--}}
                                    {{--<li>--}}
                                        {{--<span class="title">The New College of Design</span>--}}
                                        {{--<span class="date">2001 - 2006</span>--}}
                                        {{--<span class="text">Breaking Good, RedDevil, People of Interest, The Running Dead, Found,  American Guy.</span>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<span class="title">Rembrandt Institute</span>--}}
                                        {{--<span class="date">2008</span>--}}
                                        {{--<span class="text">Five months Digital Illustration course. Professor: Leonardo Stagg.</span>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<span class="title">The Digital College </span>--}}
                                        {{--<span class="date">2010</span>--}}
                                        {{--<span class="text">6 months intensive Motion Graphics course. After Effects and Premire. Professor: Donatello Urtle. </span>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}

                                {{--<!-- ... end W-Personal-Info -->--}}

                            {{--</div>--}}
                            {{--<div class="col col-lg-6 col-md-6 col-sm-12 col-12">--}}


                                {{--<!-- W-Personal-Info -->--}}

                                {{--<ul class="widget w-personal-info item-block">--}}
                                    {{--<li>--}}
                                        {{--<span class="title">Digital Design Intern</span>--}}
                                        {{--<span class="date">2006-2008</span>--}}
                                        {{--<span class="text">Digital Design Intern for the “Multimedz” agency. Was in charge of the communication with the clients.</span>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<span class="title">UI/UX Designer</span>--}}
                                        {{--<span class="date">2008-2013</span>--}}
                                        {{--<span class="text">UI/UX Designer for the “Daydreams” agency. </span>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<span class="title">Senior UI/UX Designer</span>--}}
                                        {{--<span class="date">2013-Now</span>--}}
                                        {{--<span class="text">Senior UI/UX Designer for the “Daydreams” agency. I’m in charge of a ten person group, overseeing all the proyects and talking to potential clients.</span>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}

                                {{--<!-- ... end W-Personal-Info -->--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>

            <div class="col col-xl-4 order-xl-1 col-lg-4 order-lg-1 col-md-12 order-md-2 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">@lang('messages.personal_info')</h6>
                        <a href="#" class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        </a>
                    </div>
                    <div class="ui-block-content">


                        <!-- W-Personal-Info -->

                        <ul class="widget w-personal-info">
                            <li>
                                <span class="title">@lang('messages.status'):</span>
                                <span class="text">{{$profile->status_string ?: trans('messages.not_set')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.about'):</span>
                                <span class="text">{{$profile->about ?: trans('messages.not_set')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.birthday'):</span>
                                <span class="text">{{$profile->birthday}}</span>
                            </li>
                            <li>
                                <span class="title">Lives in:</span>
                                <span class="text">San Francisco, California, USA</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.registration_date'):</span>
                                <span class="text">{{$profile->created_at->format('d.m.Y')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.auth.gender'):</span>
                                <span class="text">{{\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\User::GENDER_LIST)[$profile->gender]}}</span>
                            </li>
                            <li>
                                <span class="title">Email:</span>
                                <a href="#" class="text">{{$profile->user->email}}</a>
                            </li>
                            <li>
                                <span class="title">@lang('messages.profile.phone'):</span>
                                <span class="text">{{$profile->phone}}</span>
                            </li>
                        </ul>

                        <!-- ... end W-Personal-Info -->
                        <!-- W-Socials -->

                        @if($profile->facebook_link || $profile->twitter_link || $profile->vk_link || $profile->instagram_link)
                            <div class="widget w-socials">
                                <h6 class="title">@lang('messages.social_networks'):</h6>

                                @if($profile->facebook_link)
                                    <a href="{{$profile->facebook_link}}" class="social-item bg-facebook">
                                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                        Facebook
                                    </a>
                                @endif

                                @if($profile->twitter_link)
                                    <a href="{{$profile->twitter_link}}" class="social-item bg-twitter">
                                        <i class="fab fa-twitter" aria-hidden="true"></i>
                                        Twitter
                                    </a>
                                @endif

                                @if($profile->vk_link)
                                    <a href="{{$profile->vk_link}}" class="social-item bg-facebook">
                                        <i class="fab fa-vk" aria-hidden="true"></i>
                                        VK
                                    </a>
                                @endif

                                @if($profile->instagram_link)
                                    <a href="{{$profile->instagram_link}}" class="social-item bg-orange">
                                        <i class="fab fa-instagram" aria-hidden="true"></i>
                                        Instagram
                                    </a>
                                @endif
                            </div>
                    @endif


                        <!-- ... end W-Socials -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= view('frontend.profile.blocks.popups') ?>

@endsection
@section('scripts')
@endsection