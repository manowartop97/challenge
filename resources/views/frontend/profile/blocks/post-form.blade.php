<div class="ui-block">
    <form class="comment-form create_post_form inline-items" method="post" action="{{route('post.create')}}">
        {{csrf_field()}}
        <div class="post__author author vcard inline-items">
            <img src="{{url('storage/'.Auth::user()->getAvatar())}}" alt="author"
                 style="width: 28px; height: 28px;object-fit: cover;">

            <div class="form-group with-icon-right ">
                <input type="file" style="display: none;" id="post_attachments" name="post_attachments" multiple
                       data-files="1">
                <input type="hidden" class="hidden_author_id" name="author_id" value="{{Auth::id()}}">
                <input type="hidden" class="hidden_user_id" name="user_id" value="{{$user_id}}">
                <strong id="post_attachments-error" class="text-error"></strong>
                <div class="post-block-photo js-zoom-gallery" id="preview">

                </div>
                <textarea class="form-control post-text" name="text" placeholder=""></textarea>
                <strong id="text-error" class="text-error"></strong>
                <div class="add-options-message">
                    <a href="#" class="options-message" onclick="$('#post_attachments').trigger('click')"
                       data-toggle="modal"
                       data-target="#choose-post-attachments">
                        @svg('camera-icon', ['olymp-camera-icon'])
                    </a>
                </div>
            </div>
        </div>

        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color clearAttachments"
                data-message="{{trans('messages.are_you_sure_to_delete_attachments')}}">@lang('messages.clear_attachments')
            <div class="ripple-container"></div>
        </button>
        <button class="btn btn-md-2 btn-primary submit-post-button">@lang('messages.send_post')
            <div class="ripple-container"></div>
        </button>
    </form>
</div>

<style>
    .preview_image {
        width: 100% !important;
        object-fit: cover;
        height: 120px !important;
        border-radius: 5px !important;
    }

    @media (max-width: 700px) {
        .preview_image {
            height: 150px !important;
        }
    }


</style>