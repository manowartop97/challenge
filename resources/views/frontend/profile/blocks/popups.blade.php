<!-- Window-popup Update Header Photo -->

<div class="modal fade" id="update-header-photo" tabindex="-1" role="dialog" aria-labelledby="update-header-photo"
     aria-hidden="true">
    <div class="modal-dialog window-popup update-header-photo" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                @svg('close-icon', ['olymp-close-icon'])
            </a>

            <div class="modal-header">
                <h6 class="title">@lang('messages.update_header_photo')</h6>
            </div>

            <div class="modal-body">
                <form method="post" class="upload_photo_from_computer_form" action="{{route('profile-settings.update-avatar')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="file" class="upload_from_computer_input" onchange="this.form.submit()" name="file" style="display: none">
                </form>
                <a href="#" class="upload-photo-item avatar_from_computer">
                    @svg('computer-icon', ['olymp-computer-icon'])
                    <h6>@lang('messages.upload_photo')</h6>
                    <span>@lang('messages.browse_your_computer')</span>
                </a>

                <a href="#" class="upload-photo-item" data-toggle="modal" data-target="#choose-from-my-photo">
                    @svg('photos-icon', ['olymp-photos-icon'])
                    <h6>@lang('messages.choose_from_my_photos')</h6>
                    <span>@lang('messages.choose_from_your_uploaded_photos')</span>
                </a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="update-background-header-photo" tabindex="-1" role="dialog" aria-labelledby="update-background-header-photo"
     aria-hidden="true">
    <div class="modal-dialog window-popup update-header-photo" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                @svg('close-icon', ['olymp-close-icon'])
            </a>

            <div class="modal-header">
                <h6 class="title">@lang('messages.update_header_photo')</h6>
            </div>

            <div class="modal-body">
                <form method="post" class="upload_background_from_computer_form" action="{{route('profile-settings.update-header')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="file" class="upload_background_from_computer_input" onchange="this.form.submit()" name="file" style="display: none">
                </form>
                <a href="#" class="upload-photo-item background_from_computer">
                    @svg('computer-icon', ['olymp-computer-icon'])
                    <h6>@lang('messages.upload_photo')</h6>
                    <span>@lang('messages.browse_your_computer')</span>
                </a>

                <a href="#" class="upload-photo-item" data-toggle="modal" data-target="#choose-from-my-photo">
                    @svg('photos-icon', ['olymp-photos-icon'])
                    <h6>@lang('messages.choose_from_my_photos')</h6>
                    <span>@lang('messages.choose_from_your_uploaded_photos')</span>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- ... end Window-popup Update Header Photo -->

<!-- Window-popup Choose from my Photo -->

<div class="modal fade" id="choose-from-my-photo" tabindex="-1" role="dialog" aria-labelledby="choose-from-my-photo"
     aria-hidden="true">
    <div class="modal-dialog window-popup choose-from-my-photo" role="document">

        <div class="modal-content">
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                @svg('close-icon', ['olymp-close-icon'])
            </a>
            <div class="modal-header">
                <h6 class="title">@lang('messages.choose_from_my_photos')</h6>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-expanded="true">
                            @svg('photos-icon', ['olymp-photos-icon'])
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-expanded="false">
                            @svg('albums-icon', ['olymp-albums-icon'])
                        </a>
                    </li>
                </ul>
            </div>

            <div class="modal-body">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">

                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo1.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo2.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo3.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>

                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo4.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo5.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo6.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>

                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo7.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo8.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <div class="radio">
                                <label class="custom-radio">
                                    <img src="{{asset('frontend/img/choose-photo9.jpg')}}" alt="photo">
                                    <input type="radio" name="optionsRadios">
                                </label>
                            </div>
                        </div>


                        <a href="#" class="btn btn-primary btn-lg btn--half-width">@lang('messages.confirm_photo')</a>
                        <a href="#" class="btn btn-secondary btn-lg btn--half-width">@lang('messages.cancel')</a>

                    </div>
                    <div class="tab-pane" id="profile" role="tabpanel" aria-expanded="false">

                        <div class="choose-photo-item" data-mh="choose-item">
                            <figure>
                                <img src="{{asset('frontend/img/choose-photo10.jpg')}}" alt="photo">
                                <figcaption>
                                    <a href="#">South America Vacations</a>
                                    <span>Last Added: 2 hours ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <figure>
                                <img src="{{asset('frontend/img/choose-photo11.jpg')}}" alt="photo">
                                <figcaption>
                                    <a href="#">Photoshoot Summer 2016</a>
                                    <span>Last Added: 5 weeks ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <figure>
                                <img src="{{asset('frontend/img/choose-photo12.jpg')}}" alt="photo">
                                <figcaption>
                                    <a href="#">Amazing Street Food</a>
                                    <span>Last Added: 6 mins ago</span>
                                </figcaption>
                            </figure>
                        </div>

                        <div class="choose-photo-item" data-mh="choose-item">
                            <figure>
                                <img src="{{asset('frontend/img/choose-photo13.jpg')}}" alt="photo">
                                <figcaption>
                                    <a href="#">Graffity & Street Art</a>
                                    <span>Last Added: 16 hours ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <figure>
                                <img src="{{asset('frontend/img/choose-photo14.jpg')}}" alt="photo">
                                <figcaption>
                                    <a href="#">Amazing Landscapes</a>
                                    <span>Last Added: 13 mins ago</span>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="choose-photo-item" data-mh="choose-item">
                            <figure>
                                <img src="{{asset('frontend/img/choose-photo15.jpg')}}" alt="photo">
                                <figcaption>
                                    <a href="#">The Majestic Canyon</a>
                                    <span>Last Added: 57 mins ago</span>
                                </figcaption>
                            </figure>
                        </div>


                        <a href="#"
                           class="btn btn-primary btn-lg disabled btn--half-width">@lang('messages.confirm_photo')</a>
                        <a href="#" class="btn btn-secondary btn-lg btn--half-width">@lang('messages.cancel')</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
