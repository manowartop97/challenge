<?php
/**
 * @var \App\Models\DB\Posts\Post $post
 */
?>

<form class="comment-form update_post_form inline-items" method="post"
      action="{{route('post.edit', ['id' => $post->id])}}"
      data-id="{{$post->id}}">
    {{csrf_field()}}
    <div class="post__author author vcard inline-items">
        <img src="{{url('storage/'.Auth::user()->getAvatar())}}" alt="author"
             style="width: 28px; min-height: 28px; height: auto">

        <div class="form-group with-icon-right ">
            <input type="file" style="display: none;" id="post_edit_attachments" name="post_attachments" multiple
                   data-files="{{$post->attachments_json ? 1 : 0}}">
            <input type="hidden" class="hidden_author_id" name="author_id" value="{{$post->author_id}}">
            <input type="hidden" class="hidden_user_id" name="user_id" value="{{$post->user_id}}">
            <strong id="post_attachments-error" class="text-error"></strong>
            <div class="post-block-photo js-zoom-gallery" id="preview">
                @foreach($post->getAttachments() as $attachment)
                    @if($attachment['type'] === \App\Repositories\Challenge\ChallengeRepository::TYPE_VIDEO)
                        <div class="video-player custom col col-3-width" style="margin: 0px;">
                            <img class="preview_image" src="{{url($attachment['thumbnail'])}}" alt="photo">
                            <a href="{{url($attachment['name'])}}" class="play-video"
                               style="opacity: 1">
                                @svg('play-icon', 'olymp-play-icon')
                            </a>

                            <div class="video-content">
                            </div>

                            <div class="overlay"></div>
                        </div>
                    @else
                        <a href="{{url($attachment['name'])}}"
                           class="col col-3-width"><img class="preview_image"
                                                        src="{{url($attachment['name'])}}" alt="photo"></a>
                    @endif
                @endforeach
            </div>

            <textarea class="form-control post-text" name="text" placeholder="">{{$post->text}}</textarea>
            <strong id="text-error" class="text-error"></strong>
            <div class="add-options-message">
                <a href="#" class="options-message" onclick="$('#post_edit_attachments').trigger('click')"
                   data-toggle="modal"
                   data-target="#choose-post-attachments">
                    @svg('camera-icon', ['olymp-camera-icon'])
                </a>
            </div>
        </div>
    </div>
    <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color clearAttachments"
            data-message="{{trans('messages.are_you_sure_to_delete_attachments')}}">@lang('messages.clear_attachments')
        <div class="ripple-container"></div>
    </button>
    <button class="btn btn-md-2 btn-primary edit-post-submit-button">@lang('messages.send_post')
        <div class="ripple-container"></div>
    </button>
</form>


<style>
    .preview_image {
        width: 100% !important;
        object-fit: cover;
        height: 120px !important;
        border-radius: 5px !important;
    }

    @media (max-width: 700px) {
        .preview_image {
            height: 150px !important;
        }
    }


</style>

<script>
    var loader = $('#preloader');
    document.getElementById('post_edit_attachments').addEventListener('change', function (event) {
        loader.show();
        var preview = $('.update_post_form').find('#preview');
        preview.html('');
        $.each(event.target.files, function (index, file) {
            var fileReader = new FileReader();
            if (file.type.match('image')) {
                fileReader.onload = function () {
                    var html = '<a href="' + fileReader.result + '" class="col col-3-width"><img class="preview_image" src="' + fileReader.result + '" alt="photo"></a>';
                    $(html).appendTo(preview);
                };
                fileReader.readAsDataURL(file);
            } else {
                fileReader.onload = function () {
                    var blob = new Blob([fileReader.result], {type: file.type});
                    var url = URL.createObjectURL(blob);
                    console.log(url);
                    var video = document.createElement('video');
                    var timeupdate = function () {
                        if (snapImage()) {
                            video.removeEventListener('timeupdate', timeupdate);
                            video.pause();
                        }
                    };
                    video.addEventListener('loadeddata', function () {
                        if (snapImage()) {
                            video.removeEventListener('timeupdate', timeupdate);
                        }
                    });
                    var snapImage = function () {
                        var canvas = document.createElement('canvas');
                        canvas.width = video.videoWidth;
                        canvas.height = video.videoHeight;
                        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                        var image = canvas.toDataURL();
                        var success = image.length > 100000;
                        if (success) {
                            // var imageDiv = document.createElement('div');
                            // imageDiv.className = 'image-block';
                            // imageDiv.style.backgroundImage = "url(" + image + ")";
                            // document.getElementById('preview').appendChild(imageDiv);
                            var html = '<div class="video-player custom col col-3-width" style="margin: 0px;">\n' +
                                '                                                        <img class="preview_image" src="' + image + '" alt="photo">\n' +
                                '                                                        <a href="' + url + '" class="play-video" style="opacity: 1">\n' +
                                '                                                            <svg class="olymp-play-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fill="#9A9FBF" d="M12.355 5.332L4.548.545C3.975.196 3.392.021 2.809.021 1.413.021 0 1.115 0 3.205V6h2V3.204c0-.749.314-1.184.809-1.184.204 0 .438.074.694.229l7.815 4.792c.879.533.924 1.406.044 1.939l-7.859 4.79c-.256.157-.491.23-.695.23C2.314 14 2 13.566 2 12.818v-.793-.024H0V12.818C0 14.907 1.413 16 2.808 16c.583 0 1.167-.175 1.735-.521l7.86-4.79C13.416 10.074 14 9.107 14 8.038c.001-1.086-.599-2.072-1.645-2.706z"></path><path fill="#9A9FBF" d="M0 8h2v2H0z"></path></svg>                                                        </a>\n' +
                                '\n' +
                                '                                                        <div class="video-content">\n' +
                                '                                                        </div>\n' +
                                '\n' +
                                '                                                        <div class="overlay"></div>\n' +
                                '                                                    </div>';

                            $(html).appendTo(preview);

                            URL.revokeObjectURL(url);
                        }
                        return success;
                    };
                    video.addEventListener('timeupdate', timeupdate);
                    video.preload = 'metadata';
                    video.src = url;
                    // Load video in Safari / IE11
                    video.muted = true;
                    video.playsInline = true;
                    video.play();
                };
                fileReader.readAsArrayBuffer(file);
            }
        });
        $('#post_edit_attachments').data('files', 1);
        $('.clearMedia').show();
        loader.hide();
    });
</script>

<script>
    $(document).find('.js-zoom-gallery').each(function () {
        $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            gallery: {
                enabled: true
            },
            removalDelay: 500, //delay removal by X to allow out-animation
            callbacks: {
                beforeOpen: function () {
                    // just a hack that adds mfp-anim class to markup
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = 'mfp-zoom-in';
                }
            },
            closeOnContentClick: true,
            midClick: true
        });
    });
</script>