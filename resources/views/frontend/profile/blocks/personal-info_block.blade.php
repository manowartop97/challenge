<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 */
?>

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">
                        <img src="{{url('storage/'.$profile->user->getBackground())}}" class="profile-background"
                             alt="nature">
                    </div>
                    <div class="profile-section">
                        <div class="row">
                            <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                                <ul class="profile-menu">
                                    <li>
                                        <a href="{{route('profile.index')}}"
                                           class="{{$link === \App\Repositories\User\ProfileRepository::LINK_FEED ? 'active' : ''}}">@lang('messages.feed.timeline')</a>
                                    </li>
                                    <li>
                                        <a href="{{route('profile.about')}}"
                                           class="{{$link === \App\Repositories\User\ProfileRepository::LINK_ABOUT ? 'active' : ''}}">@lang('messages.feed.about')</a>
                                    </li>
                                    <li>
                                        <a href="{{route('profile.friends')}}"
                                           class="{{$link === \App\Repositories\User\ProfileRepository::LINK_FRIENDS ? 'active' : ''}}">@lang('messages.feed.friends')</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
                                <ul class="profile-menu">
                                    <li>
                                        <a href="{{route('profile.challenges')}}"
                                           class="{{$link === \App\Repositories\User\ProfileRepository::LINK_CHALLENGES ? 'active' : ''}}">@lang('messages.feed.challenges')</a>
                                    </li>
                                    <li>
                                        <a href="09-ProfilePage-Videos.html"
                                           class="{{$link === \App\Repositories\User\ProfileRepository::LINK_ACHIEVEMENTS ? 'active' : ''}}">@lang('messages.feed.achievements')</a>
                                    </li>
                                    <li>
                                        <div class="more">
                                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                            <ul class="more-dropdown more-with-triangle">
                                                <li>
                                                    <a href="#">Report Profile</a>
                                                </li>
                                                <li>
                                                    <a href="#">Block Profile</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="control-block-button">
                            <a href="{{route('friends.requests')}}" class="btn btn-control bg-blue">
                                @svg('happy-face-icon', ['olymp-happy-face-icon'])
                            </a>

                            <a href="#" class="btn btn-control bg-purple">
                                @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
                            </a>

                            <div class="btn btn-control bg-primary more">
                                @svg('settings-icon', ['olymp-settings-icon'])
                                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                    <li>
                                        <a href="#" data-toggle="modal"
                                           data-target="#update-header-photo">@lang('messages.update_profile_photo')</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal"
                                           data-target="#update-background-header-photo">@lang('messages.update_header_photo')</a>
                                    </li>
                                    <li>
                                        <a href="{{route('profile-settings.account-settings')}}">@lang('messages.profile.sidebar.account_settings')</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="top-header-author">
                        <a href="{{route('profile.index')}}" class="author-thumb">
                            <img class="avatar-image" src="{{url('storage/'.$profile->user->getAvatar())}}"
                                 alt="author">
                        </a>
                        <div class="author-content">
                            <a href="{{route('profile.index')}}"
                               class="h4 author-name">{{$profile->user->getFullName()}}</a>
                            <div class="country">San Francisco, CA</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>