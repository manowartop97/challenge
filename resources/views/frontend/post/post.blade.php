<?php
/**
 * @var \App\Models\DB\Posts\Post $post
 */
?>
<div class="ui-block" data-featured="{{$post->is_featured}}">
    <!-- Post -->
    <article class="hentry post">

        <div class="post__author author vcard inline-items">
            <img src="{{url('storage/'.$post->author->getAvatar())}}" alt="author" style="object-fit: cover;">

            <div class="author-date">
                <a class="h6 post__author-name fn"
                   href="{{route('page.show-user', ['id' => $post->author_id])}}">{{$post->author->getFullName()}}</a>
                <div class="post__date">
                    <time class="published">
                        {{$post->created_at->diffForHumans()}}
                    </time>
                </div>
            </div>

            <div class="more">
                @svg('three-dots-icon', 'olymp-three-dots-icon')
                <ul class="more-dropdown">
                    @if($post->isCurrentUserOwner())
                        <li>
                            <a href="#" class="edit_post_btn"
                               data-route="{{route('post.edit-form', ['id' => $post->id])}}">@lang('messages.update_post')</a>
                        </li>
                        <li>
                            <a href="#" class="delete_btn"
                               data-message="{{trans('messages.are_you_sure_to_delete_post')}}"
                               data-route="{{route('post.delete', ['id' => $post->id])}}">@lang('messages.delete_post')</a>
                        </li>
                    @elseif($post->isCurrentUserReceiver())
                        <li>
                            <a href="#">@lang('messages.report_post')</a>
                        </li>
                        <li>
                            <a href="#" class="delete_btn"
                               data-message="{{trans('messages.are_you_sure_to_delete_post')}}"
                               data-route="{{route('post.delete', ['id' => $post->id])}}">@lang('messages.delete_post')</a>
                        </li>
                    @else
                        <li>
                            <a href="#">@lang('messages.report_post')</a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <p>{!! nl2br($post->text) !!}</p>
        @php $attachments = $post->getAttachments() @endphp
        @if(!empty($attachments))
            <div class="post-block-photo js-zoom-gallery">
                @foreach($attachments as $attachment)
                    @if($attachment['type'] === \App\Repositories\Challenge\ChallengeRepository::TYPE_VIDEO)
                        <div class="video-player custom col col-3-width-custom" style="margin: 0px;">
                            <img src="{{url($attachment['thumbnail'])}}" alt="photo">
                            <a href="{{url($attachment['name'])}}" class="play-video"
                               style="opacity: 1">
                                @svg('play-icon', 'olymp-play-icon')
                            </a>

                            <div class="video-content">
                            </div>

                            <div class="overlay"></div>
                        </div>
                    @else
                        <a href="{{url($attachment['name'])}}"
                           class="col col-3-width-custom"><img
                                    src="{{url($attachment['name'])}}" alt="photo"></a>
                    @endif
                @endforeach
            </div>
        @endif
        <div class="post-additional-info inline-items">
            <a href="#"
               class="post-add-icon inline-items like_btn_sm like_btn {{$post->isLiked() ? 'is-liked-sm' : ''}}"
               data-route="{{route($post->isLiked() ? 'post.unlike' : 'post.like', ['id' => $post->id])}}">
                @svg('heart-icon', 'olymp-heart-icon')
                <span>{{$post->likes_count ?: 0}}</span>
            </a>

            <div class="comments-shared">
                <a href="#" class="post-add-icon inline-items">
                    @svg('speech-balloon-icon', 'olymp-speech-balloon-icon')
                    <span>{{$post->comments_count ?: 0}}</span>
                </a>

                {{--<a href="#" class="post-add-icon inline-items">--}}
                    {{--@svg('share-icon', 'olymp-share-icon')--}}
                    {{--<span>16</span>--}}
                {{--</a>--}}
            </div>


        </div>

        <div class="control-block-button post-control-button">
            @if(Auth::id() === $post->user_id)
                <a href="#" data-route="{{route('post.make-featured', ['id' => $post->id])}}"
                   class="btn btn-control {{$post->is_featured ? 'featured_post' : ''}} featured-post make_featured"
                   data-toggle="tooltip" data-placement="top"
                   data-original-title="@lang('messages.make_featured')">
                    @svg('star-icon', ['class' => 'olymp-star-icon'])
                </a>
            @endif
            <a href="#" class="btn btn-control like_btn_lg like_btn {{$post->isLiked() ? 'is-liked-lg' : ''}}"
               data-route="{{route($post->isLiked() ? 'post.unlike' : 'post.like', ['id' => $post->id])}}">
                @svg('like-post-icon', 'olymp-like-post-icon')
            </a>

            <a href="#" class="btn btn-control">
                @svg('comments-post-icon', 'olymp-comments-post-icon')
            </a>

            <a href="#" class="btn btn-control">
                @svg('share-icon', 'olymp-share-icon')
            </a>

        </div>

    </article>

    <!-- ... end Post -->
</div>
<script>
        $(document).find('.js-zoom-gallery').each(function () {
            $(this).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true
                },
                removalDelay: 500, //delay removal by X to allow out-animation
                callbacks: {
                    beforeOpen: function () {
                        // just a hack that adds mfp-anim class to markup
                        this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                        this.st.mainClass = 'mfp-zoom-in';
                    }
                },
                closeOnContentClick: true,
                midClick: true
            });
        });
</script>