<?php
/**
 * @var \App\Models\DB\User\User $user
 * @var integer $link
 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $posts
 * @var \App\Models\DB\Posts\Post $post
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title') @lang('messages.user_profile') @endsection
@section('content')
    <link rel="stylesheet" href="{{asset('frontend/css/custom/post.css')}}">
    <div class="header-spacer"></div>
    {{--personal-info--}}
    <?= view('frontend.page.blocks.personal-info_block', ['profile' => $user->profile, 'link' => $link]) ?>
    {{--personal-info--}}

    <!-- ... end Top Header-Profile -->
    <div class="container">
        <div class="row">
            <!-- Main Content -->
            <div class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

                <?= view('frontend.profile.blocks.post-form', ['user_id' => $user->id]) ?>

                <div id="newsfeed-items-grid">

                    @foreach($posts->items() as $post)
                        <?= view('frontend.post.post-to-render', ['post' => $post]) ?>
                    @endforeach

                </div>

                {{$posts->links()}}
                {{--<a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html"--}}
                {{--data-container="newsfeed-items-grid">--}}
                {{--@svg('three-dots-icon', ['olymp-three-dots-icon'])--}}
                {{--</a>--}}
            </div>

            <!-- ... end Main Content -->


            <!-- Left Sidebar -->

            <div class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">@lang('messages.personal_info')</h6>
                    </div>
                    <div class="ui-block-content">

                        <!-- W-Personal-Info -->

                        <ul class="widget w-personal-info item-block">
                            <li>
                                <span class="title">@lang('messages.status'):</span>
                                <span class="text status_string_text">{{$user->profile->status_string ?: trans('messages.not_set')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.about'):</span>
                                <span class="text">{{$user->profile->about ?: trans('messages.not_set')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.hobbies_and_interests'):</span>
                                <span class="text">{{$user->profile->hobbies_and_interests ?: trans('messages.not_set')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.favourite_music'):</span>
                                <span class="text">{{$user->profile->favourite_music ?: trans('messages.not_set')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.favourite_movies'):</span>
                                <span class="text">{{$user->profile->favourite_movies ?: trans('messages.not_set')}}</span>
                            </li>
                            <li>
                                <span class="title">@lang('messages.favourite_games'):</span>
                                <span class="text">{{$user->profile->favourite_games ?: trans('messages.not_set')}}</span>
                            </li>
                        </ul>

                        <!-- .. end W-Personal-Info -->
                        <!-- W-Socials -->
                        @if($user->profile->facebook_link || $user->profile->twitter_link || $user->profile->vk_link || $user->profile->instagram_link)
                            <div class="widget w-socials">
                                <h6 class="title">@lang('messages.social_networks'):</h6>

                                @if($user->profile->facebook_link)
                                    <a href="{{$user->profile->facebook_link}}" class="social-item bg-facebook">
                                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                        Facebook
                                    </a>
                                @endif

                                @if($user->profile->twitter_link)
                                    <a href="{{$user->profile->twitter_link}}" class="social-item bg-twitter">
                                        <i class="fab fa-twitter" aria-hidden="true"></i>
                                        Twitter
                                    </a>
                                @endif

                                @if($user->profile->vk_link)
                                    <a href="{{$user->profile->vk_link}}" class="social-item bg-facebook">
                                        <i class="fab fa-vk" aria-hidden="true"></i>
                                        VK
                                    </a>
                                @endif

                                @if($user->profile->instagram_link)
                                    <a href="{{$user->profile->instagram_link}}" class="social-item bg-orange">
                                        <i class="fab fa-instagram" aria-hidden="true"></i>
                                        Instagram
                                    </a>
                                @endif
                            </div>
                    @endif

                    <!-- ... end W-Socials -->
                    </div>
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">James’s Badges</h6>
                    </div>
                    <div class="ui-block-content">

                        <!-- W-Badges -->

                        <ul class="widget w-badges">
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge1.png')}}" alt="author">
                                    <div class="label-avatar bg-primary">2</div>
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge4.png')}}" alt="author">
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge3.png')}}" alt="author">
                                    <div class="label-avatar bg-blue">4</div>
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge6.png')}}" alt="author">
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge11.png')}}" alt="author">
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge8.png')}}" alt="author">
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge10.png')}}" alt="author">
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge13.png')}}" alt="author">
                                    <div class="label-avatar bg-breez">2</div>
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge7.png')}}" alt="author">
                                </a>
                            </li>
                            <li>
                                <a href="24-CommunityBadges.html">
                                    <img src="{{asset('frontend/img/badge12.png')}}" alt="author">
                                </a>
                            </li>
                        </ul>

                        <!--.. end W-Badges -->
                    </div>
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">My Spotify Playlist</h6>
                    </div>

                    <!-- W-Playlist -->

                    <ol class="widget w-playlist">
                        <li class="js-open-popup" data-popup-target=".playlist-popup">
                            <div class="playlist-thumb">
                                <img src="{{asset('frontend/img/playlist6.jpg')}}" alt="thumb-composition">
                                <div class="overlay"></div>
                                <a href="#" class="play-icon">
                                    @svg('play-icon', ['olymp-play-icon'])
                                </a>
                            </div>

                            <div class="composition">
                                <a href="#" class="composition-name">The Past Starts Slow...</a>
                                <a href="#" class="composition-author">System of a Revenge</a>
                            </div>

                            <div class="composition-time">
                                <time class="published" datetime="2017-03-24T18:18">3:22</time>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Add Song to Player</a>
                                        </li>
                                        <li>
                                            <a href="#">Add Playlist to Player</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </li>

                        <li class="js-open-popup" data-popup-target=".playlist-popup">
                            <div class="playlist-thumb">
                                <img src="{{asset('frontend/img/playlist7.jpg')}}" alt="thumb-composition">
                                <div class="overlay"></div>
                                <a href="#" class="play-icon">
                                    @svg('play-icon', ['olymp-play-icon'])
                                </a>
                            </div>

                            <div class="composition">
                                <a href="#" class="composition-name">The Pretender</a>
                                <a href="#" class="composition-author">Kung Fighters</a>
                            </div>

                            <div class="composition-time">
                                <time class="published" datetime="2017-03-24T18:18">5:48</time>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Add Song to Player</a>
                                        </li>
                                        <li>
                                            <a href="#">Add Playlist to Player</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </li>
                        <li class="js-open-popup" data-popup-target=".playlist-popup">
                            <div class="playlist-thumb">
                                <img src="{{asset('frontend/img/playlist8.jpg')}}" alt="thumb-composition">
                                <div class="overlay"></div>
                                <a href="#" class="play-icon">
                                    @svg('play-icon', ['olymp-play-icon'])
                                </a>
                            </div>

                            <div class="composition">
                                <a href="#" class="composition-name">Blood Brothers</a>
                                <a href="#" class="composition-author">Iron Maid</a>
                            </div>

                            <div class="composition-time">
                                <time class="published" datetime="2017-03-24T18:18">3:06</time>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Add Song to Player</a>
                                        </li>
                                        <li>
                                            <a href="#">Add Playlist to Player</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </li>
                        <li class="js-open-popup" data-popup-target=".playlist-popup">
                            <div class="playlist-thumb">
                                <img src="{{asset('frontend/img/playlist9.jpg')}}" alt="thumb-composition">
                                <div class="overlay"></div>
                                <a href="#" class="play-icon">
                                    @svg('play-icon', ['olymp-play-icon'])
                                </a>
                            </div>

                            <div class="composition">
                                <a href="#" class="composition-name">Seven Nation Army</a>
                                <a href="#" class="composition-author">The Black Stripes</a>
                            </div>

                            <div class="composition-time">
                                <time class="published" datetime="2017-03-24T18:18">6:17</time>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Add Song to Player</a>
                                        </li>
                                        <li>
                                            <a href="#">Add Playlist to Player</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </li>
                        <li class="js-open-popup" data-popup-target=".playlist-popup">
                            <div class="playlist-thumb">
                                <img src="{{asset('frontend/img/playlist10.jpg')}}" alt="thumb-composition">
                                <div class="overlay"></div>
                                <a href="#" class="play-icon">
                                    @svg('play-icon', ['olymp-play-icon'])
                                </a>
                            </div>

                            <div class="composition">
                                <a href="#" class="composition-name">Killer Queen</a>
                                <a href="#" class="composition-author">Archiduke</a>
                            </div>

                            <div class="composition-time">
                                <time class="published" datetime="2017-03-24T18:18">5:40</time>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Add Song to Player</a>
                                        </li>
                                        <li>
                                            <a href="#">Add Playlist to Player</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </li>
                    </ol>

                    <!-- .. end W-Playlist -->
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Twitter Feed</h6>
                    </div>

                    <!-- W-Twitter -->

                    <ul class="widget w-twitter">
                        <li class="twitter-item">
                            <div class="author-folder">
                                <img src="{{asset('frontend/img/twitter-avatar1.png')}}" alt="avatar">
                                <div class="author">
                                    <a href="#" class="author-name">Space Cowboy</a>
                                    <a href="#" class="group">@james_spiegelOK</a>
                                </div>
                            </div>
                            <p>Tomorrow with the agency we will run 5 km for charity. Come and give us your support!
                                <a href="#" class="link-post">#Daydream5K</a></p>
                            <span class="post__date">
							<time class="published" datetime="2017-03-24T18:18">
								2 hours ago
							</time>
						</span>
                        </li>
                        <li class="twitter-item">
                            <div class="author-folder">
                                <img src="{{asset('frontend/img/twitter-avatar1.png')}}" alt="avatar">
                                <div class="author">
                                    <a href="#" class="author-name">Space Cowboy</a>
                                    <a href="#" class="group">@james_spiegelOK</a>
                                </div>
                            </div>
                            <p>Check out the new website of “The Bebop Bar”! <a href="#"
                                                                                class="link-post">bytle/thbp53f</a>
                            </p>
                            <span class="post__date">
							<time class="published" datetime="2017-03-24T18:18">
								16 hours ago
							</time>
						</span>
                        </li>
                        <li class="twitter-item">
                            <div class="author-folder">
                                <img src="{{asset('frontend/img/twitter-avatar1.png')}}" alt="avatar">
                                <div class="author">
                                    <a href="#" class="author-name">Space Cowboy</a>
                                    <a href="#" class="group">@james_spiegelOK</a>
                                </div>
                            </div>
                            <p>The Sunday is the annual agency camping trip and I still haven’t got a tent
                                <a href="#" class="link-post">#TheWild #Indoors</a></p>
                            <span class="post__date">
							<time class="published" datetime="2017-03-24T18:18">
								Yesterday
							</time>
						</span>
                        </li>
                    </ul>


                    <!-- .. end W-Twitter -->
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Last Videos</h6>
                    </div>
                    <div class="ui-block-content">

                        <!-- W-Latest-Video -->

                        <ul class="widget w-last-video">
                            <li>
                                <a href="https://vimeo.com/ondemand/viewfromabluemoon4k/147865858"
                                   class="play-video play-video--small">
                                    @svg('play-icon', ['olymp-play-icon'])
                                </a>
                                <img src="{{asset('frontend/img/video8.jpg')}}" alt="video">
                                <div class="video-content">
                                    <div class="title">System of a Revenge - Hypnotize...</div>
                                    <time class="published" datetime="2017-03-24T18:18">3:25</time>
                                </div>
                                <div class="overlay"></div>
                            </li>
                            <li>
                                <a href="https://youtube.com/watch?v=excVFQ2TWig" class="play-video play-video--small">
                                    @svg('play-icon', ['olymp-play-icon'])
                                </a>
                                <img src="{{asset('frontend/img/video7.jpg')}}" alt="video">
                                <div class="video-content">
                                    <div class="title">Green Goo - Live at Dan’s Arena</div>
                                    <time class="published" datetime="2017-03-24T18:18">5:48</time>
                                </div>
                                <div class="overlay"></div>
                            </li>
                        </ul>

                        <!-- .. end W-Latest-Video -->
                    </div>
                </div>

            </div>

            <!-- ... end Left Sidebar -->


            <!-- Right Sidebar -->

            <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Last Photos</h6>
                    </div>
                    <div class="ui-block-content">

                        <!-- W-Latest-Photo -->

                        <ul class="widget w-last-photo js-zoom-gallery">
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                            <li>
                                <a href="{{asset('frontend/img/last-phot13-large.jpg')}}">
                                    <img src="{{asset('frontend/img/last-phot13-large.jpg')}}" alt="photo">
                                </a>
                            </li>
                        </ul>


                        <!-- .. end W-Latest-Photo -->
                    </div>
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Blog Posts</h6>
                    </div>
                    <!-- W-Blog-Posts -->

                    <ul class="widget w-blog-posts">
                        <li>
                            <article class="hentry post">

                                <a href="#" class="h4">My Perfect Vacations in South America and Europe</a>

                                <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut
                                    labore et.</p>

                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        7 hours ago
                                    </time>
                                </div>

                            </article>
                        </li>
                        <li>
                            <article class="hentry post">

                                <a href="#" class="h4">The Big Experience of Travelling Alone</a>

                                <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut
                                    labore et.</p>

                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        March 18th, at 6:52pm
                                    </time>
                                </div>

                            </article>
                        </li>
                    </ul>

                    <!-- .. end W-Blog-Posts -->
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <a href="{{route('page.friends',['id' => $user->id])}}"><h6
                                    class="title">@lang('messages.friends')
                                ({{$user->getFriendsCount()}})</h6></a>
                    </div>
                    <div class="ui-block-content">

                        <!-- W-Faved-Page -->
                        @if($friends->total() === 0)
                            @lang('messages.dont_have_friends')
                        @else
                            <ul class="widget w-faved-page">
                                @foreach($friends->items() as $friend)
                                    @php $userFriend = $friend->user_id == $user->id ? $friend->friend : $friend->user @endphp
                                    <li>
                                        <a href="{{route('page.show-user', ['id' => $userFriend->id])}}"
                                           data-toggle="tooltip" data-placement="top"
                                           data-original-title="{{$userFriend->getFullName()}}">
                                            <img src="{{url('storage/'.$userFriend->getAvatar())}}"
                                                 style="width:34px;height: 34px" alt="author">
                                        </a>
                                    </li>
                                @endforeach
                                @if($friendsCount > \App\Repositories\Friendship\FriendshipRepository::FRIENDS_FOR_FRONT_DISPLAY)
                                    <li class="all-users">
                                        <a href="#">+{{$friendsCount - \App\Repositories\Friendship\FriendshipRepository::FRIENDS_FOR_FRONT_DISPLAY}}</a>
                                    </li>
                                @endif
                            </ul>
                    @endif
                    <!-- .. end W-Faved-Page -->
                    </div>
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Favourite Pages</h6>
                    </div>

                    <!-- W-Friend-Pages-Added -->

                    <ul class="widget w-friend-pages-added notification-list friend-requests">
                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">The Marina Bar</a>
                                <span class="chat-message-item">Restaurant / Bar</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>

                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar42-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Tapronus Rock</a>
                                <span class="chat-message-item">Rock Band</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>

                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar43-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Pixel Digital Design</a>
                                <span class="chat-message-item">Company</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>
                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar44-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Thompson’s Custom Clothing Boutique</a>
                                <span class="chat-message-item">Clothing Store</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>

                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar45-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Crimson Agency</a>
                                <span class="chat-message-item">Company</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>
                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar46-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Mannequin Angel</a>
                                <span class="chat-message-item">Clothing Store</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>
                        </li>
                    </ul>

                    <!-- .. end W-Friend-Pages-Added -->
                </div>

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">James's Poll</h6>
                    </div>
                    <div class="ui-block-content">

                        <!-- W-Pool -->

                        <ul class="widget w-pool">
                            <li>
                                <p>If you had to choose, which actor do you prefer to be the next Darkman? </p>
                            </li>
                            <li>
                                <div class="skills-item">
                                    <div class="skills-item-info">
									<span class="skills-item-title">
										<span class="radio">
											<label>
												<input type="radio" name="optionsRadios">
												Thomas Bale
											</label>
										</span>
									</span>
                                        <span class="skills-item-count">
										<span class="count-animate" data-speed="1000" data-refresh-interval="50"
                                              data-to="62" data-from="0"></span>
										<span class="units">62%</span>
									</span>
                                    </div>
                                    <div class="skills-item-meter">
                                        <span class="skills-item-meter-active bg-primary" style="width: 62%"></span>
                                    </div>

                                    <div class="counter-friends">12 friends voted for this</div>

                                    <ul class="friends-harmonic">
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="all-users">+3</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <div class="skills-item">
                                    <div class="skills-item-info">
									<span class="skills-item-title">
										<span class="radio">
											<label>
												<input type="radio" name="optionsRadios">
												Ben Robertson
											</label>
										</span>
									</span>
                                        <span class="skills-item-count">
										<span class="count-animate" data-speed="1000" data-refresh-interval="50"
                                              data-to="27" data-from="0"></span>
										<span class="units">27%</span>
									</span>
                                    </div>
                                    <div class="skills-item-meter">
                                        <span class="skills-item-meter-active bg-primary" style="width: 27%"></span>
                                    </div>
                                    <div class="counter-friends">7 friends voted for this</div>

                                    <ul class="friends-harmonic">
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="all-users">+3</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <div class="skills-item">
                                    <div class="skills-item-info">
									<span class="skills-item-title">
										<span class="radio">
											<label>
												<input type="radio" name="optionsRadios">
												Michael Streiton
											</label>
										</span>
									</span>
                                        <span class="skills-item-count">
										<span class="count-animate" data-speed="1000" data-refresh-interval="50"
                                              data-to="11" data-from="0"></span>
										<span class="units">11%</span>
									</span>
                                    </div>
                                    <div class="skills-item-meter">
                                        <span class="skills-item-meter-active bg-primary" style="width: 11%"></span>
                                    </div>

                                    <div class="counter-friends">2 people voted for this</div>

                                    <ul class="friends-harmonic">
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <img src="{{asset('frontend/img/friend-harmonic1.jpg')}}" alt="friend">
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="all-users">+3</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>

                        <!-- .. end W-Pool -->
                        <a href="#" class="btn btn-md-2 btn-border-think custom-color c-grey full-width">Vote Now!</a>
                    </div>
                </div>

            </div>

            <!-- ... end Right Sidebar -->

        </div>
    </div>
    <!-- ... end Window-popup Choose from my Photo -->

    <!-- Playlist Popup -->

    <div class="window-popup playlist-popup" tabindex="-1" role="dialog" aria-labelledby="playlist-popup"
         aria-hidden="true">

        <a href="" class="icon-close js-close-popup">
            @svg('close-icon', ['olymp-close-icon'])
        </a>

        <table class="playlist-popup-table">

            <thead>

            <tr>

                <th class="play">
                    PLAY
                </th>

                <th class="cover">
                    COVER
                </th>

                <th class="song-artist">
                    SONG AND ARTIST
                </th>

                <th class="album">
                    ALBUM
                </th>

                <th class="released">
                    RELEASED
                </th>

                <th class="duration">
                    DURATION
                </th>

                <th class="spotify">
                    GET IT ON SPOTIFY
                </th>

                <th class="remove">
                    REMOVE
                </th>
            </tr>

            </thead>

            <tbody>
            <tr>
                <td class="play">
                    <a href="#" class="play-icon">

                        @svg('play-icon', ['olymp-play-icon'])
                    </a>
                </td>
                <td class="cover">
                    <div class="playlist-thumb">
                        <img src="{{asset('frontend/img/playlist19.jpg')}}" alt="thumb-composition">
                    </div>
                </td>
                <td class="song-artist">
                    <div class="composition">
                        <a href="#" class="composition-name">We Can Be Heroes</a>
                        <a href="#" class="composition-author">Jason Bowie</a>
                    </div>
                </td>
                <td class="album">
                    <a href="#" class="album-composition">Ziggy Firedust</a>
                </td>
                <td class="released">
                    <div class="release-year">2014</div>
                </td>
                <td class="duration">
                    <div class="composition-time">
                        <time class="published" datetime="2017-03-24T18:18">6:17</time>
                    </div>
                </td>
                <td class="spotify">
                    <i class="fab fa-spotify composition-icon" aria-hidden="true"></i>
                </td>
                <td class="remove">
                    <a href="#" class="remove-icon">

                        @svg('close-icon', ['olymp-close-icon'])
                    </a>
                </td>
            </tr>

            <tr>
                <td class="play">
                    <a href="#" class="play-icon">
                        @svg('play-icon', ['olymp-play-icon'])
                    </a>
                </td>
                <td class="cover">
                    <div class="playlist-thumb">
                        <img src="{{asset('frontend/img/playlist6.jpg')}}" alt="thumb-composition">
                    </div>
                </td>
                <td class="song-artist">
                    <div class="composition">
                        <a href="#" class="composition-name">The Past Starts Slow and Ends</a>
                        <a href="#" class="composition-author">System of a Revenge</a>
                    </div>
                </td>
                <td class="album">
                    <a href="#" class="album-composition">Wonderize</a>
                </td>
                <td class="released">
                    <div class="release-year">2014</div>
                </td>
                <td class="duration">
                    <div class="composition-time">
                        <time class="published" datetime="2017-03-24T18:18">6:17</time>
                    </div>
                </td>
                <td class="spotify">
                    <i class="fab fa-spotify composition-icon" aria-hidden="true"></i>
                </td>
                <td class="remove">
                    <a href="#" class="remove-icon">
                        @svg('close-icon', ['olymp-close-icon'])
                    </a>
                </td>
            </tr>

            <tr>
                <td class="play">
                    <a href="#" class="play-icon">
                        @svg('play-icon', ['olymp-play-icon'])
                    </a>
                </td>
                <td class="cover">
                    <div class="playlist-thumb">
                        <img src="{{asset('frontend/img/playlist7.jpg')}}" alt="thumb-composition">
                    </div>
                </td>
                <td class="song-artist">
                    <div class="composition">
                        <a href="#" class="composition-name">The Pretender</a>
                        <a href="#" class="composition-author">Kung Fighters</a>
                    </div>
                </td>
                <td class="album">
                    <a href="#" class="album-composition">Warping Lights</a>
                </td>
                <td class="released">
                    <div class="release-year">2014</div>
                </td>
                <td class="duration">
                    <div class="composition-time">
                        <time class="published" datetime="2017-03-24T18:18">6:17</time>
                    </div>
                </td>
                <td class="spotify">
                    <i class="fab fa-spotify composition-icon" aria-hidden="true"></i>
                </td>
                <td class="remove">
                    <a href="#" class="remove-icon">
                        @svg('close-icon', ['olymp-close-icon'])
                    </a>
                </td>
            </tr>

            <tr>
                <td class="play">
                    <a href="#" class="play-icon">
                        @svg('play-icon', ['olymp-play-icon'])
                    </a>
                </td>
                <td class="cover">
                    <div class="playlist-thumb">
                        <img src="{{asset('frontend/img/playlist8.jpg')}}" alt="thumb-composition">
                    </div>
                </td>
                <td class="song-artist">
                    <div class="composition">
                        <a href="#" class="composition-name">Seven Nation Army</a>
                        <a href="#" class="composition-author">The Black Stripes</a>
                    </div>
                </td>
                <td class="album">
                    <a href="#" class="album-composition ">Icky Strung (LIVE at Cube Garden)</a>
                </td>
                <td class="released">
                    <div class="release-year">2014</div>
                </td>
                <td class="duration">
                    <div class="composition-time">
                        <time class="published" datetime="2017-03-24T18:18">6:17</time>
                    </div>
                </td>
                <td class="spotify">
                    <i class="fab fa-spotify composition-icon" aria-hidden="true"></i>
                </td>
                <td class="remove">
                    <a href="#" class="remove-icon">
                        @svg('close-icon', ['olymp-close-icon'])
                    </a>
                </td>
            </tr>

            <tr>
                <td class="play">
                    <a href="#" class="play-icon">
                        @svg('play-icon', ['olymp-play-icon'])
                    </a>
                </td>
                <td class="cover">
                    <div class="playlist-thumb">
                        <img src="{{asset('frontend/img/playlist9.jpg')}}" alt="thumb-composition">
                    </div>
                </td>
                <td class="song-artist">
                    <div class="composition">
                        <a href="#" class="composition-name">Leap of Faith</a>
                        <a href="#" class="composition-author">Eden Artifact</a>
                    </div>
                </td>
                <td class="album">
                    <a href="#" class="album-composition">The Assassins’s Soundtrack</a>
                </td>
                <td class="released">
                    <div class="release-year">2014</div>
                </td>
                <td class="duration">
                    <div class="composition-time">
                        <time class="published" datetime="2017-03-24T18:18">6:17</time>
                    </div>
                </td>
                <td class="spotify">
                    <i class="fab fa-spotify composition-icon" aria-hidden="true"></i>
                </td>
                <td class="remove">
                    <a href="#" class="remove-icon">
                        @svg('close-icon', ['olymp-close-icon'])
                    </a>
                </td>
            </tr>

            <tr>
                <td class="play">
                    <a href="#" class="play-icon">
                        @svg('play-icon', ['olymp-play-icon'])
                    </a>
                </td>
                <td class="cover">
                    <div class="playlist-thumb">
                        <img src="{{asset('frontend/img/playlist10.jpg')}}" alt="thumb-composition">
                    </div>
                </td>
                <td class="song-artist">
                    <div class="composition">
                        <a href="#" class="composition-name">Killer Queen</a>
                        <a href="#" class="composition-author">Archiduke</a>
                    </div>
                </td>
                <td class="album">
                    <a href="#" class="album-composition ">News of the Universe</a>
                </td>
                <td class="released">
                    <div class="release-year">2014</div>
                </td>
                <td class="duration">
                    <div class="composition-time">
                        <time class="published" datetime="2017-03-24T18:18">6:17</time>
                    </div>
                </td>
                <td class="spotify">
                    <i class="fab fa-spotify composition-icon" aria-hidden="true"></i>
                </td>
                <td class="remove">
                    <a href="#" class="remove-icon">
                        @svg('close-icon', ['olymp-close-icon'])
                    </a>
                </td>
            </tr>
            </tbody>
        </table>

        <audio id="mediaplayer" data-showplaylist="true">
            {{--<source src="mp3/Twice.mp3" title="Track 1" data-poster="track1.png" type="audio/mpeg">--}}
            {{--<source src="mp3/Twice.mp3" title="Track 2" data-poster="track2.png" type="audio/mpeg">--}}
            {{--<source src="mp3/Twice.mp3" title="Track 3" data-poster="track3.png" type="audio/mpeg">--}}
            {{--<source src="mp3/Twice.mp3" title="Track 4" data-poster="track4.png" type="audio/mpeg">--}}
        </audio>
    </div>

@endsection
@section('scripts')
    <script src="{{asset('frontend/js/custom/post-form.js')}}"></script>
    <script src="{{asset('frontend/js/preview/post-preview.js')}}"></script>
@endsection