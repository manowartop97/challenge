<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 * @var integer|null $link
 */
?>

<div class="container">
    <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block">
                <div class="top-header">
                    <div class="top-header-thumb">
                        <img src="{{url('storage/'.$profile->user->getBackground())}}" class="profile-background"
                             alt="nature">
                    </div>
                    @if(is_null($link))
                        <?= view('frontend.page.blocks.private-profile-section', ['profile' => $profile, 'link' => $link]) ?>
                    @else
                        <?= view('frontend.page.blocks.public-profile-section', ['profile' => $profile, 'link' => $link]) ?>
                    @endif
                    <div class="top-header-author">
                        <a href="{{route('page.show-user', ['id' => $profile->user_id])}}" class="author-thumb">
                            <img class="avatar-image" src="{{url('storage/'.$profile->user->getAvatar())}}"
                                 alt="author">
                        </a>
                        <div class="author-content">
                            <a href="{{route('page.show-user', ['id' => $profile->user_id])}}"
                               class="h4 author-name">{{$profile->user->getFullName()}}</a>
                            <div class="country">San Francisco, CA</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
