<div class="profile-section">
    <div class="row">
        <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
            <ul class="profile-menu">
                <li>
                    <a href="{{route('page.show-user',['id' => $profile->user_id])}}"
                       class="{{$link === \App\Repositories\User\ProfileRepository::LINK_FEED ? 'active' : ''}}">@lang('messages.feed.timeline')</a>
                </li>
                <li>
                    <a href="{{route('page.show-user-info', ['id' => $profile->user_id])}}"
                       class="{{$link === \App\Repositories\User\ProfileRepository::LINK_ABOUT ? 'active' : ''}}">@lang('messages.feed.about')</a>
                </li>
                <li>
                    <a href="{{route('profile.friends')}}"
                       class="{{$link === \App\Repositories\User\ProfileRepository::LINK_FRIENDS ? 'active' : ''}}">@lang('messages.feed.friends')</a>
                </li>
            </ul>
        </div>
        <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
            <ul class="profile-menu">
                <li>
                    <a href="{{route('page.challenges', ['id' => $profile->user_id])}}"
                       class="{{$link === \App\Repositories\User\ProfileRepository::LINK_CHALLENGES ? 'active' : ''}}">@lang('messages.feed.challenges')</a>
                </li>
                <li>
                    <a href="09-ProfilePage-Videos.html"
                       class="{{$link === \App\Repositories\User\ProfileRepository::LINK_ACHIEVEMENTS ? 'active' : ''}}">@lang('messages.feed.achievements')</a>
                </li>
                <li>
                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        <ul class="more-dropdown more-with-triangle">
                            <li>
                                <a href="#">Report Profile</a>
                            </li>
                            <li>
                                <a href="#">Block Profile</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="control-block-button">
        @if(Auth::user()->isInvitedMeToFriendship($profile->user_id))
            <span class="notification-icon">
                <a href="#"
                   data-route="{{route('friendship.accept', ['id' => $profile->user_id])}}"
                   class="accept-request accept_friendship_invitation"
                   style="margin-right: 20px;border-radius: 100%;width: 50px;height: 50px;line-height: 54px;padding: 0;fill: #fff;font-size: 20px;">
                    <span class="icon-add"
                          style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
                        @svg('happy-face-icon', ['olymp-happy-face-icon'])
                    </span>
                </a>

                <a href="#"
                   data-route="{{route('friendship.reject', ['id' => $profile->user_id])}}"
                   class="accept-request request-del reject_friendship_invitation"
                   style="margin-right: 20px;border-radius: 100%;width: 50px;height: 50px;line-height: 54px;padding: 0;fill: #fff;font-size: 20px;">
                    <span class="icon-minus"
                          style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
                        @svg('happy-face-icon', ['olymp-happy-face-icon'])
                    </span>
                </a>

            </span>
        @else
            @if(Auth::user()->isUserInFriendList($profile->user_id))
                <a href="" data-toggle="tooltip" data-placement="top"
                   data-original-title="@lang('messages.remove_from_friends')"
                   data-route="{{route('friendship.remove', ['id' => $profile->user_id])}}"
                   class="btn btn-control bg-danger remove_friend_button">
                                        <span class="icon-minus"
                                              style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
                </a>
            @else
                <a href="#" data-toggle="tooltip" data-placement="top"
                   data-original-title="@lang('messages.add_to_friends')"
                   data-route="{{route('friendship.new', ['id' => $profile->user_id])}}"
                   class="btn btn-control bg-green invite_friend_button">
                                        <span class="icon-add"
                                              style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
                </a>
            @endif
        @endif
        <a href="#" class="btn btn-control bg-purple chat_btn">
            @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
        </a>

        <div class="btn btn-control bg-primary more">
            @svg('settings-icon', ['olymp-settings-icon'])
            <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                <li>
                    <a href="#" data-toggle="modal"
                       data-target="#update-header-photo">@lang('messages.update_profile_photo')</a>
                </li>
                <li>
                    <a href="#" data-toggle="modal"
                       data-target="#update-background-header-photo">@lang('messages.update_header_photo')</a>
                </li>
                <li>
                    <a href="{{route('profile-settings.account-settings')}}">@lang('messages.profile.sidebar.account_settings')</a>
                </li>
            </ul>
        </div>
    </div>
</div>