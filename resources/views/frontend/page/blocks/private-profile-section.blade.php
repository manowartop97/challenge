<?php
/**
 * @var \App\Models\DB\User\UserProfile $profile
 */
?>
<div class="profile-section">
    <div class="row">
        <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
            <ul class="profile-menu">
                <li>
                    <a href="#" class="friend-count-item">
                        <div class="h6">{{$profile->user->getFriendsCount()}}</div>
                        <div class="title">@lang('messages.friends')</div>
                    </a>
                </li>
                <li>
                    <a href="#" class="friend-count-item">
                        <div class="h6">240</div>
                        <div class="title">Photos</div>
                    </a>
                </li>
                <li>
                    <a href="#" class="friend-count-item">
                        <div class="h6">12</div>
                        <div class="title">Videos</div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
            <ul class="profile-menu">
                <li>
                    <a href="#" class="friend-count-item">
                        <div class="h6">835</div>
                        <div class="title">Posts</div>
                    </a>
                </li>
                <li>
                    <a href="#" class="friend-count-item">
                        <div class="h6">12.6K</div>
                        <div class="title">Comments</div>
                    </a>
                </li>
                <li>
                    <a href="#" class="friend-count-item">
                        <div class="h6">970</div>
                        <div class="title">Likes</div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="control-block-button">
        @if(Auth::user()->isInvitedMeToFriendship($profile->user_id))
            <span class="notification-icon">
											<a href="#"
                                               data-route="{{route('friendship.accept', ['id' => $profile->user_id])}}"
                                               class="accept-request accept_friendship_invitation"
                                               style="margin-right: 20px;border-radius: 100%;width: 50px;height: 50px;line-height: 54px;padding: 0;fill: #fff;font-size: 20px;">
												<span class="icon-add"
                                                      style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
											</a>

											<a href="#"
                                               data-route="{{route('friendship.reject', ['id' => $profile->user_id])}}"
                                               class="accept-request request-del reject_friendship_invitation"
                                               style="margin-right: 20px;border-radius: 100%;width: 50px;height: 50px;line-height: 54px;padding: 0;fill: #fff;font-size: 20px;">
												<span class="icon-minus"
                                                      style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
											</a>

										</span>
        @else
            @if(Auth::user()->isUserInFriendList($profile->user_id))
                <a href="" data-toggle="tooltip" data-placement="top"
                   data-original-title="@lang('messages.remove_from_friends')"
                   data-route="{{route('friendship.remove', ['id' => $profile->user_id])}}"
                   class="btn btn-control bg-danger remove_friend_button">
                                        <span class="icon-minus"
                                              style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
                </a>
            @else
                <a href="#" data-toggle="tooltip" data-placement="top"
                   data-original-title="@lang('messages.add_to_friends')"
                   data-route="{{route('friendship.new', ['id' => $profile->user_id])}}"
                   class="btn btn-control bg-green invite_friend_button">
                                        <span class="icon-add"
                                              style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;">
													@svg('happy-face-icon', ['olymp-happy-face-icon'])
												</span>
                </a>
            @endif
        @endif
        <a href="#" class="btn btn-control bg-purple chat_btn">
            @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
        </a>

        <div class="btn btn-control bg-primary more">
            @svg('settings-icon', ['olymp-settings-icon'])
            <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                <li>
                    <a href="#" data-toggle="modal"
                       data-target="#update-header-photo">@lang('messages.update_profile_photo')</a>
                </li>
                <li>
                    <a href="#" data-toggle="modal"
                       data-target="#update-background-header-photo">@lang('messages.update_header_photo')</a>
                </li>
                <li>
                    <a href="{{route('profile-settings.account-settings')}}">@lang('messages.profile.sidebar.account_settings')</a>
                </li>
            </ul>
        </div>
    </div>
</div>