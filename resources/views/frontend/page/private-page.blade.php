<?php
/**
 * @var \App\Models\DB\User\User $user
 * @var string $title
 */
?>

@extends('layouts.frontend.profile-layout.main')
@section('title') {{$title}} @endsection
@section('content')
    <div class="header-spacer"></div>
    {{--personal-info--}}
    <?= view('frontend.page.blocks.personal-info_block', ['profile' => $user->profile, 'link' => null]) ?>
    <section class="medium-padding120">
        <div class="container">
            <div class="row">
                <div class="col col-xl-4 col-lg-12 col-md-12 m-auto">
                    <div class="logout-content">
                        <div class="logout-icon">
                            <i class="fas fa-times"></i>
                        </div>
                        <h6>@lang('messages.user_has_private_profile')</h6>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection