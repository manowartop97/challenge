<?php
/**
 * @var \Illuminate\Contracts\Pagination\LengthAwarePaginator $users
 */
?>
@extends('layouts.frontend.profile-layout.main')
@section('title')
    @lang('messages.search')
@endsection
@section('content')
    <!-- ... end Responsive Header-BP -->
    <div class="header-spacer"></div>


    <div class="container">
        <div class="row">

            <!-- Main Content -->

            <div class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
                @if($query)
                    <div class="ui-block">
                        <div class="ui-block-title">
                            <div class="h6 title">{{$users->total()}} @lang('messages.results_for_query'): “<span
                                        class="c-primary">{{$query}}</span>”
                            </div>
                        </div>
                    </div>
                @endif

                <div class="ui-block">
                    <div class="ui-block-title">
                        <ul class="cat-list-bg-style align-center sorting-menu cat-list__item-bg-blue filter_route"
                            data-route="{{route('challenge.all')}}">
                            <li class="cat-list__item {{Request::is('search/people')? 'active' : ''}}"
                                data-filter="0">
                                <a href="{{route('search.people')}}">@lang('messages.search_people')</a>
                            </li>
                            <li class="cat-list__item {{Request::is('search/challenges')? 'active' : ''}}"
                                data-filter="0">
                                <a href="{{route('search.challenges')}}">@lang('messages.search_challenges')</a>
                            </li>
                        </ul>
                        <h6 class="title"></h6>
                    </div>

                    <form action="{{route('search.people')}}" method="get">
                        <br>
                        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group label-floating">
                                <label class="control-label">@lang('messages.name')</label>
                                <input class="form-control" name="q" placeholder="" type="text"
                                       value="{{Request::get('q')}}">
                                <strong class="text-error">{{$errors->has('q') ? $errors->first('q') : ''}}</strong>
                            </div>

                            <div class="form-group label-floating is-select">
                                <label class="control-label">@lang('messages.auth.gender')</label>
                                <select class="selectpicker form-control" name="gender" size="auto">
                                    <option value="">@lang('messages.auth.gender')</option>
                                    @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\User::GENDER_LIST) as $id => $title)
                                        <option value="{{$id}}" {{Request::get('gender') == $id ? 'selected' : '' }}>{{$title}}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('gender'))
                                    <strong class="text-error">{{$errors->first('gender')}}</strong>
                                @endif
                            </div>

                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <input type="submit" class="btn btn-primary btn-block full-width loader"
                                       value="@lang('messages.search')">
                            </div>
                        </div>

                    </form>
                </div>

                <div class="ui-block">

                    <div id="search-items-grid">
                        <ul class="notification-list friend-requests">
                            @foreach($users->items() as $profile)
                                <li>
                                    <div class="author-thumb">
                                        <img src="{{url('storage/'.$profile->user->getAvatar())}}"
                                             class="friends-requests-avatar" alt="author">
                                    </div>
                                    <div class="notification-event">
                                        <a href="{{route('page.show-user',['id' => $profile->user_id])}}"
                                           class="h6 notification-friend">{{$profile->user->getFullName()}}</a>
                                        <span class="chat-message-item">Mutual Friend: Sarah Hetfield</span>
                                    </div>

                                    <div class="more">
                                        @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                        @svg('little-delete', ['olymp-little-delete'])
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                {{--pagination--}}
                <div class="col-md-12">
                    {{$users->onEachSide(1)->links()}}
                </div>
                {{--<a id="load-more-button" href="#" class="btn btn-control btn-more"--}}
                {{--data-load-link="search-items-to-load.html" data-container="search-items-grid">--}}
                {{--@svg('three-dots-icon', ['olymp-three-dots-icon'])--}}
                {{--</a>--}}
            </div>

            <!-- ... end Main Content -->


            <!-- Left Sidebar -->

            <div class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Pages You May Like</h6>
                    </div>

                    <!-- W-Friend-Pages-Added -->

                    <ul class="widget w-friend-pages-added notification-list friend-requests">
                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">The Marina Bar</a>
                                <span class="chat-message-item">Restaurant / Bar</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>

                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Tapronus Rock</a>
                                <span class="chat-message-item">Rock Band</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>

                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Pixel Digital Design</a>
                                <span class="chat-message-item">Company</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>
                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Thompson’s Custom Clothing Boutique</a>
                                <span class="chat-message-item">Clothing Store</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>

                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Crimson Agency</a>
                                <span class="chat-message-item">Company</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>
                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Mannequin Angel</a>
                                <span class="chat-message-item">Clothing Store</span>
                            </div>
                            <span class="notification-icon" data-toggle="tooltip" data-placement="top"
                                  data-original-title="ADD TO YOUR FAVS">
							<a href="#">
								@svg('star-icon', ['olymp-star-icon'])
							</a>
						</span>
                        </li>
                    </ul>

                    <!-- .. end W-Friend-Pages-Added -->
                </div>


            </div>

            <!-- ... end Left Sidebar -->


            <!-- Right Sidebar -->

            <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">

                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Friend Suggestions</h6>

                        <a href="#" class="more">@svg('three-dots-icon', ['olymp-three-dots-icon'])</a>
                    </div>


                    <!-- W-Action -->

                    <ul class="widget w-friend-pages-added notification-list friend-requests">
                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Francine Smith</a>
                                <span class="chat-message-item">8 Friends in Common</span>
                            </div>
                            <span class="notification-icon">
							<a href="#" class="accept-request">
								<span class="icon-add without-text">
									@svg('happy-face-icon', ['olymp-happy-face-icon'])
								</span>
							</a>
						</span>
                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Hugh Wilson</a>
                                <span class="chat-message-item">6 Friends in Common</span>
                            </div>
                            <span class="notification-icon">
							<a href="#" class="accept-request">
								<span class="icon-add without-text">
									@svg('happy-face-icon', ['olymp-happy-face-icon'])
								</span>
							</a>
						</span>
                        </li>

                        <li class="inline-items">
                            <div class="author-thumb">
                                <img src="{{asset('frontend/img/avatar41-sm.jpg')}}" alt="author">
                            </div>
                            <div class="notification-event">
                                <a href="#" class="h6 notification-friend">Karen Masters</a>
                                <span class="chat-message-item">6 Friends in Common</span>
                            </div>
                            <span class="notification-icon">
							<a href="#" class="accept-request">
								<span class="icon-add without-text">
									@svg('happy-face-icon', ['olymp-happy-face-icon'])
								</span>
							</a>
						</span>
                        </li>

                    </ul>

                    <!-- ... end W-Action -->
                </div>
            </div>

            <!-- ... end Right Sidebar -->

        </div>
    </div>
    <?= view('frontend.profile.blocks.popups') ?>
@endsection
@section('scripts')
@endsection