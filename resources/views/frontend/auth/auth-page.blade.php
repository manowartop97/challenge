@extends('layouts.frontend.landing.main')
<link rel="stylesheet" href="{{asset('frontend/css/overhang.min.css')}}">
@section('title')
    Авторизация
@endsection
@section('content')
    <div class="main-header main-header-fullwidth main-header-has-header-standard">


        <!-- Header Standard Landing  -->

        <div class="header--standard header--standard-landing" id="header--standard">
            <div class="container">
                <div class="header--standard-wrap">

                    <a href="#" class="logo">
                        <div class="img-wrap">
                            <img src="{{asset('frontend/img/logo.png')}}" alt="Olympus">
                            <img src="{{asset('frontend/img/logo-colored-small.png')}}" alt="Olympus" class="logo-colored">
                        </div>
                        <div class="title-block">
                            <h6 class="logo-title">Challenge</h6>
                        </div>
                    </a>

                    <a href="#" class="open-responsive-menu js-open-responsive-menu">
                        @svg('menu-icon', 'olymp-menu-icon')
                    </a>

                    <div class="nav nav-pills nav1 header-menu">
                        <div class="mCustomScrollbar">
                            <ul>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Home</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown"
                                       href="#" role="button" aria-haspopup="false" aria-expanded="false" tabindex='1'>Profile</a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Profile Page</a>
                                        <a class="dropdown-item" href="#">Newsfeed</a>
                                        <a class="dropdown-item" href="#">Post Versions</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown dropdown-has-megamenu">
                                    <a href="#" class="nav-link dropdown-toggle" data-hover="dropdown"
                                       data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false"
                                       tabindex='1'>Forums</a>
                                    <div class="dropdown-menu megamenu">
                                        <div class="row">
                                            <div class="col col-sm-3">
                                                <h6 class="column-tittle">Main Links</h6>
                                                <a class="dropdown-item" href="#">Profile Page<span
                                                            class="tag-label bg-blue-light">new</span></a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                            </div>
                                            <div class="col col-sm-3">
                                                <h6 class="column-tittle">BuddyPress</h6>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page<span
                                                            class="tag-label bg-primary">HOT!</span></a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                            </div>
                                            <div class="col col-sm-3">
                                                <h6 class="column-tittle">Corporate</h6>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                            </div>
                                            <div class="col col-sm-3">
                                                <h6 class="column-tittle">Forums</h6>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                                <a class="dropdown-item" href="#">Profile Page</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Terms & Conditions</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Events</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">Privacy Policy</a>
                                </li>
                                <li class="close-responsive-menu js-close-responsive-menu">
                                    @svg('close-icon', 'olymp-close-icon')
                                </li>
                                <li class="nav-item js-expanded-menu">
                                    <a href="#" class="nav-link">
                                        @svg('menu-icon', 'olymp-menu-icon')
                                        @svg('close-icon', 'olymp-close-icon')
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ... end Header Standard Landing  -->
        <div class="header-spacer--standard"></div>

        <div class="content-bg-wrap bg-landing"></div>

        <div class="container">
            <div class="row display-flex">
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                    <div class="landing-content">
                        <h1>@lang('messages.auth.text.welcome')</h1>
                        <p>@lang('messages.auth.text.sub')</p>
                    </div>
                </div>

                <div class="col col-xl-5 ml-auto col-lg-6 col-md-12 col-sm-12 col-12">


                    <!-- Login-Registration Form  -->

                    <div class="registration-login-form">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link authTabLink registerTab" data-id="1" data-toggle="tab"
                                   href="#register"
                                   role="tab">
                                    @svg('login-icon', ['olymp-login-icon'])
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link authTabLink loginTab" data-id="2" data-toggle="tab" href="#login"
                                   role="tab">
                                    @svg('register-icon', ['olymp-register-icon'])
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane" id="register" role="tabpanel" data-mh="log-tab">
                                <div class="title h6">@lang('messages.auth.registration')</div>
                                <form class="content" method="post" action="{{route('register')}}">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group label-floating {{old('surname') ? 'is-focused' : ''}}">
                                                <label class="control-label">@lang('messages.auth.f_name')</label>
                                                <input class="form-control" placeholder=""
                                                       value="{{old('surname') ?: ''}}"
                                                       name="surname" type="text">
                                                @if($errors->has('surname'))
                                                    <strong class="text-error">{{$errors->first('surname')}}</strong>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group label-floating{{old('name') ? 'is-focused' : ''}}">
                                                <label class="control-label">@lang('messages.auth.l_name')</label>
                                                <input class="form-control" placeholder="" name="name"
                                                       value="{{old('name') ?: ''}}" type="text">
                                                @if($errors->has('name'))
                                                    <strong class="text-error">{{$errors->first('name')}}</strong>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-12">
                                            <div class="form-group label-floating{{old('email') ? 'is-focused' : ''}}">
                                                <label class="control-label">@lang('messages.auth.email')</label>
                                                <input class="form-control" placeholder="" name="email"
                                                       value="{{old('email') ?: ''}}" type="email">
                                                @if($errors->has('email'))
                                                    <strong class="text-error">{{$errors->first('email')}}</strong>
                                                @endif
                                            </div>
                                            <div class="form-group label-floating">
                                                <label class="control-label">@lang('messages.auth.password')</label>
                                                <input class="form-control" placeholder="" name="password"
                                                       type="password">
                                                @if($errors->has('password'))
                                                    <strong class="text-error">{{$errors->first('password')}}</strong>
                                                @endif
                                            </div>
                                            <div class="form-group label-floating">
                                                <label class="control-label">@lang('messages.auth.password_repeat')</label>
                                                <input class="form-control" name="password_confirmation"
                                                       placeholder=""
                                                       type="password">
                                                @if($errors->has('password_confirmation'))
                                                    <strong class="text-error">{{$errors->first('password_confirmation')}}</strong>
                                                @endif
                                            </div>

                                            <div class="form-group date-time-picker label-floating {{old('birthday') ? 'is-focused' : ''}}">
                                                <label class="control-label">@lang('messages.auth.bday')</label>
                                                <input class="datetimepicker" value="{{old('birthday') ?: ''}}"
                                                       name="birthday" autocomplete="off"/>
                                                <span class="input-group-addon">
                                            @svg('calendar-icon', ['olymp-calendar-icon'])
										</span>
                                                @if($errors->has('birthday'))
                                                    <strong class="text-error">{{$errors->first('birthday')}}</strong>
                                                @endif
                                            </div>

                                            <div class="form-group label-floating is-select">
                                                <label class="control-label">@lang('messages.auth.gender')</label>
                                                <select class="selectpicker form-control" name="gender" size="auto">
                                                    <option value="">@lang('messages.auth.gender')</option>
                                                    @foreach(\App\Components\Translator\Helpers\ArrayTranslator::translate(\App\Models\DB\User\User::GENDER_LIST) as $id => $title)
                                                        <option value="{{$id}}" {{old('gender') == $id ? 'selected' : '' }}>{{$title}}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('gender'))
                                                    <strong class="text-error">{{$errors->first('gender')}}</strong>
                                                @endif
                                            </div>

                                            <div class="remember">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="agreed" type="checkbox">
                                                        @lang('messages.auth.accept_part_1') <a
                                                                href="#">@lang('messages.auth.accept_part_2')</a> @lang('messages.auth.accept_part_3')
                                                    </label>
                                                </div>
                                                @if($errors->has('agreed'))
                                                    <br>
                                                    <strong class="text-error">{{$errors->first('agreed')}}</strong>
                                                @endif
                                            </div>

                                            <input type="submit" class="btn btn-purple btn-lg full-width loader"
                                                   value="@lang('messages.auth.complete_registration')">
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane" id="login" role="tabpanel" data-mh="log-tab">
                                <div class="title h6">@lang('messages.auth.login')</div>
                                <form class="content" method="post" action="{{route('login')}}">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-xl-12 col-lg-12 col-md-12">
                                            <div class="form-group label-floating {{old('email') ? 'is-focused' : ''}}">
                                                <label class="control-label">@lang('messages.auth.email')</label>
                                                <input class="form-control" placeholder="" type="email" name="email"
                                                       value="{{old('email') ?: ''}}">
                                                @if($errors->has('email'))
                                                    <strong class="text-error">{{$errors->first('email')}}</strong>
                                                @endif
                                            </div>
                                            <div class="form-group label-floating">
                                                <label class="control-label">@lang('messages.auth.password')</label>
                                                <input class="form-control" placeholder="" name="password"
                                                       type="password">
                                                @if($errors->has('password'))
                                                    <strong class="text-error">{{$errors->first('password')}}</strong>
                                                @endif
                                            </div>

                                            <div class="remember">

                                                <div class="checkbox">
                                                    <label>
                                                        <input name="optionsCheckboxes" type="checkbox">
                                                        @lang('messages.auth.remember_me')
                                                    </label>
                                                </div>
                                                <a href="#" data-toggle="modal" data-target="#forgot-password"
                                                   class="forgot">@lang('messages.auth.forgot_password')</a>
                                            </div>

                                            <input type="submit" class="btn btn-lg btn-primary full-width loader"
                                                   value="@lang('messages.auth.login')">
                                            <div class="or"></div>

                                            <a href="#"
                                               class="btn btn-lg btn-google bg-facebook full-width btn-icon-left"><i
                                                        class="fab fa-google"
                                                        aria-hidden="true"></i>@lang('messages.auth.login_with_gmail')
                                            </a>

                                            <p>@lang('messages.auth.have_account_part_1') <a
                                                        href="#">@lang('messages.auth.have_account_part_2')</a> @lang('messages.auth.have_account_part_3')
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- ... end Login-Registration Form  -->
                </div>
            </div>
        </div>

        <img class="img-bottom" src="{{asset('frontend/img/group-bottom.png')}}" alt="friends">
        <img class="img-rocket" src="{{asset('frontend/img/rocket.png')}}" alt="rocket">
    </div>


    <!-- Clients Block -->

    <section class="crumina-module crumina-clients">
        <div class="container">
            <div class="row">
                <div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
                    <a class="clients-item" href="#">
                        <img src="{{asset('frontend/img/client1.png')}}" class="" alt="logo">
                    </a>
                </div>
                <div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
                    <a class="clients-item" href="#">
                        <img src="{{asset('frontend/img/client2.png')}}" class="" alt="logo">
                    </a>
                </div>
                <div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
                    <a class="clients-item" href="#">
                        <img src="{{asset('frontend/img/client3.png')}}" class="" alt="logo">
                    </a>
                </div>
                <div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
                    <a class="clients-item" href="#">
                        <img src="{{asset('frontend/img/client4.png')}}" class="" alt="logo">
                    </a>
                </div>
                <div class="col col-xl-2 m-auto col-lg-2 col-md-6 col-sm-6 col-6">
                    <a class="clients-item" href="#">
                        <img src="{{asset('frontend/img/client5.png')}}" class="" alt="logo">
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- ... end Clients Block -->


    <!-- Section Img Scale Animation -->

    <section class="align-center pt80 section-move-bg-top img-scale-animation scrollme">
        <div class="container">
            <div class="row">
                <div class="col col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
                    <img class="main-img" src="{{asset('frontend/img/scale1.png')}}" alt="screen">
                </div>
            </div>

            <img class="first-img1" alt="img" src="{{asset('frontend/img/scale2.png')}}">
            <img class="second-img1" alt="img" src="{{asset('frontend/img/scale3.png')}}">
            <img class="third-img1" alt="img" src="{{asset('frontend/img/scale4.png')}}">
        </div>
        <div class="content-bg-wrap bg-section2"></div>
    </section>

    <!-- ... end Section Img Scale Animation -->

    <section class="medium-padding120">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{asset('frontend/img/icon-fly.png')}}" alt="screen">
                </div>

                <div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title">Why Join <span class="c-primary">Olympus Social Network</span>?
                        </h2>
                        <p class="heading-text">Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum
                            dolore eu fugiat nulla pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="medium-padding120">
        <div class="container">
            <div class="row">
                <div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title">Meet New People <span class="c-primary">all over the World</span>
                        </h2>
                        <p class="heading-text">Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum
                            dolore eu fugiat nulla pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa.
                        </p>
                    </div>
                </div>

                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{asset('frontend/img/image1.png')}}" alt="screen">
                </div>
            </div>
        </div>
    </section>


    <section class="medium-padding120">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{asset('frontend/img/image2.png')}}" alt="screen">
                </div>

                <div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title">The Best UI/UX and <span class="c-primary">Awesome Features</span>
                        </h2>
                        <p class="heading-text">Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum
                            dolore eu fugiat nulla pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="medium-padding120">
        <div class="container">
            <div class="row">
                <div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title">Find People with <span
                                    class="c-primary">Your Same Interests</span>
                        </h2>
                        <p class="heading-text">Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum
                            dolore eu fugiat nulla pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa.
                        </p>
                    </div>
                </div>

                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{asset('frontend/img/image3.png')}}" alt="screen">
                </div>
            </div>
        </div>
    </section>


    <!-- Planer Animation -->

    <section class="medium-padding120 bg-section3 background-cover planer-animation">
        <div class="container">
            <div class="row mb60">
                <div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading align-center">
                        <div class="heading-sup-title">SOCIAL NETWORK</div>
                        <h2 class="h1 heading-title">Community Reviews</h2>
                        <p class="heading-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod
                            tempor incididunt ut labore</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="swiper-container pagination-bottom" data-show-items="3">
                    <div class="swiper-wrapper">
                        <div class="ui-block swiper-slide">


                            <!-- Testimonial Item -->

                            <div class="crumina-module crumina-testimonial-item">
                                <div class="testimonial-header-thumb"></div>

                                <div class="testimonial-item-content">

                                    <div class="author-thumb">
                                        <img src="{{asset('frontend/img/avatar3.jpg')}}" alt="author">
                                    </div>

                                    <h3 class="testimonial-title">Amazing Community</h3>

                                    <ul class="rait-stars">
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>

                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="far fa-star star-icon"></i>
                                        </li>
                                    </ul>

                                    <p class="testimonial-message">Lorem ipsum dolor sit amet, consectetur
                                        adipisicing
                                        elit, sed do eiusmod tempor
                                        incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                        nostrud
                                        exercitation ullamco.
                                    </p>

                                    <div class="author-content">
                                        <a href="#" class="h6 author-name">Mathilda Brinker</a>
                                        <div class="country">Los Angeles, CA</div>
                                    </div>
                                </div>
                            </div>

                            <!-- ... end Testimonial Item -->
                        </div>

                        <div class="ui-block swiper-slide">


                            <!-- Testimonial Item -->

                            <div class="crumina-module crumina-testimonial-item">
                                <div class="testimonial-header-thumb"></div>

                                <div class="testimonial-item-content">

                                    <div class="author-thumb">
                                        <img src="{{asset('frontend/img/avatar17.jpg')}}" alt="author">
                                    </div>

                                    <h3 class="testimonial-title">This is the Best Social Network ever!</h3>

                                    <ul class="rait-stars">
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>

                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                    </ul>

                                    <p class="testimonial-message">Duis aute irure dolor in reprehenderit in
                                        voluptate
                                        velit esse cillum dolore eu
                                        nulla pariatur laborum.
                                    </p>

                                    <div class="author-content">
                                        <a href="#" class="h6 author-name">Marina Valentine</a>
                                        <div class="country">Long Island, NY</div>
                                    </div>
                                </div>
                            </div>

                            <!-- ... end Testimonial Item -->

                        </div>

                        <div class="ui-block swiper-slide">


                            <!-- Testimonial Item -->

                            <div class="crumina-module crumina-testimonial-item">
                                <div class="testimonial-header-thumb"></div>

                                <div class="testimonial-item-content">

                                    <div class="author-thumb">
                                        <img src="{{asset('frontend/img/avatar1.jpg')}}" alt="author">
                                    </div>

                                    <h3 class="testimonial-title">Incredible Design!</h3>

                                    <ul class="rait-stars">
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>

                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="fa fa-star star-icon"></i>
                                        </li>
                                        <li>
                                            <i class="far fa-star star-icon"></i>
                                        </li>
                                    </ul>

                                    <p class="testimonial-message">Sed ut perspiciatis unde omnis iste natus error
                                        sit
                                        voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
                                        quae
                                        ab
                                        illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                                        explicabo.
                                    </p>

                                    <div class="author-content">
                                        <a href="#" class="h6 author-name">Nicholas Grissom</a>
                                        <div class="country">San Francisco, CA</div>
                                    </div>
                                </div>
                            </div>

                            <!-- ... end Testimonial Item -->
                        </div>
                    </div>

                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>

        <img src="{{asset('frontend/img/planer.png')}}" alt="planer" class="planer">
    </section>

    <!-- ... end Section Planer Animation -->

    <section class="medium-padding120">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{asset('frontend/img/image4.png')}}" alt="screen">
                </div>

                <div class="col col-xl-5 col-lg-5 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="h1 heading-title">Release all the Power with the <span
                                    class="c-primary">Olympus App!</span></h2>
                        <p class="heading-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod
                            tempor
                            incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                            exercitation
                            ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>


                    <ul class="list--styled">
                        <li>
                            <i class="far fa-check-circle" aria-hidden="true"></i>
                            Build your profile in just minutes, it’s that simple!
                        </li>
                        <li>
                            <i class="far fa-check-circle" aria-hidden="true"></i>
                            Unlimited messaging with the best interface.
                        </li>
                    </ul>

                    <a href="#" class="btn btn-market">
                        <img class="icon" src="{{asset('frontend/img/apple-logotype.svg')}}" alt="app store">
                        <div class="text">
                            <span class="sup-title">AVAILABLE ON THE</span>
                            <span class="title">App Store</span>
                        </div>
                    </a>

                    <a href="#" class="btn btn-market">
                        <img class="icon" src="{{asset('frontend/img/google-play.svg')}}" alt="google">
                        <div class="text">
                            <span class="sup-title">ANDROID APP ON</span>
                            <span class="title">Google Play</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <!-- Section Subscribe Animation -->

    <section class="medium-padding100 subscribe-animation scrollme bg-users">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading c-white custom-color">
                        <h2 class="h1 heading-title">Olympus Newsletter</h2>
                        <p class="heading-text">Subscribe to be the first one to know about updates, new features
                            and
                            much more!
                        </p>
                    </div>


                    <!-- Subscribe Form  -->

                    <form class="form-inline subscribe-form" method="post">
                        <div class="form-group label-floating is-empty">
                            <label class="control-label">Enter your email</label>
                            <input class="form-control bg-white" placeholder="" type="email">
                        </div>

                        <button class="btn btn-blue btn-lg">Send</button>
                    </form>

                    <!-- ... end Subscribe Form  -->

                </div>
            </div>

            <img src="{{asset('frontend/img/paper-plane.png')}}" alt="plane" class="plane">
        </div>
    </section>

    <!-- ... end Section Subscribe Animation -->
    <section class="medium-padding120">
        <div class="container">
            <div class="row mb60">
                <div class="col col-xl-4 col-lg-4 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading align-center">
                        <div class="heading-sup-title">OLYMPUS BLOG</div>
                        <h2 class="h1 heading-title">Latest News</h2>
                        <p class="heading-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod
                            tempor incididunt ut labore</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


                    <!-- Post -->

                    <article class="hentry blog-post">

                        <div class="post-thumb">
                            <img src="{{asset('frontend/img/post1.jpg')}}" alt="photo">
                        </div>

                        <div class="post-content">
                            <a href="#" class="post-category bg-blue-light">THE COMMUNITY</a>
                            <a href="#" class="h4 post-title">Here’s the Featured Urban photo of August! </a>
                            <p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new
                                catalog.</p>

                            <div class="author-date">
                                by
                                <a class="h6 post__author-name fn" href="#">Maddy Simmons</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        - 7 hours ago
                                    </time>
                                </div>
                            </div>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="{{asset('frontend/img/icon-chat27.png')}}" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{{asset('frontend/img/icon-chat2.png')}}" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    26
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        @svg('speech-balloon-icon', 'olymp-speech-balloon-icon')
                                        <span>0</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->
                </div>
                <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


                    <!-- Post -->

                    <article class="hentry blog-post">

                        <div class="post-thumb">
                            <img src="{{asset('frontend/img/post2.jpg')}}" alt="photo">
                        </div>

                        <div class="post-content">
                            <a href="#" class="post-category bg-primary">OLYMPUS NEWS</a>
                            <a href="#" class="h4 post-title">Olympus Network added new photo filters!</a>
                            <p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new
                                catalog.</p>

                            <div class="author-date">
                                by
                                <a class="h6 post__author-name fn" href="#">JACK SCORPIO</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        - 12 hours ago
                                    </time>
                                </div>
                            </div>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="{{asset('frontend/img/icon-chat4.png')}}" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{{asset('frontend/img/icon-chat26.png')}}" alt="icon">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="{{asset('frontend/img/icon-chat16.png')}}" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    82
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        @svg('speech-balloon-icon', 'olymp-speech-balloon-icon')
                                        <span>14</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->
                </div>
                <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


                    <!-- Post -->

                    <article class="hentry blog-post">

                        <div class="post-thumb">
                            <img src="{{asset('frontend/img/post3.jpg')}}" alt="photo">
                        </div>

                        <div class="post-content">
                            <a href="#" class="post-category bg-purple">INSPIRATION</a>
                            <a href="#" class="h4 post-title">Take a look at these truly awesome worspaces</a>
                            <p>Here’s a photo from last month’s photoshoot. We got really awesome shots for the new
                                catalog.</p>

                            <div class="author-date">
                                by
                                <a class="h6 post__author-name fn" href="#">Maddy Simmons</a>
                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        - 2 days ago
                                    </time>
                                </div>
                            </div>

                            <div class="post-additional-info inline-items">

                                <ul class="friends-harmonic">
                                    <li>
                                        <a href="#">
                                            <img src="{{asset('frontend/img/icon-chat28.png')}}" alt="icon">
                                        </a>
                                    </li>
                                </ul>
                                <div class="names-people-likes">
                                    0
                                </div>

                                <div class="comments-shared">
                                    <a href="#" class="post-add-icon inline-items">
                                        @svg('speech-balloon-icon', 'olymp-speech-balloon-icon')
                                        <span>22</span>
                                    </a>
                                </div>

                            </div>
                        </div>

                    </article>

                    <!-- ... end Post -->
                </div>
            </div>
        </div>
    </section>


    <!-- Section Call To Action Animation -->

    <section class="align-right pt160 pb80 section-move-bg call-to-action-animation scrollme">
        <div class="container">
            <div class="row">
                <div class="col col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
                    <a href="#" class="btn btn-primary btn-lg" data-toggle="modal"
                       data-target="#registration-login-form-popup">Start Making Friends Now!</a>
                </div>
            </div>
        </div>
        <img class="first-img" alt="guy" src="{{asset('frontend/img/guy.png')}}">
        <img class="second-img" alt="rocket" src="{{asset('frontend/img/rocket1.png')}}">
        <div class="content-bg-wrap bg-section1"></div>
    </section>

    <!-- ... end Section Call To Action Animation -->


    <div class="modal fade" id="registration-login-form-popup" tabindex="-1" role="dialog"
         aria-labelledby="registration-login-form-popup" aria-hidden="true">
        <div class="modal-dialog window-popup registration-login-form-popup" role="document">
            <div class="modal-content">
                <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                    @svg('close-icon', 'olymp-close-icon')
                </a>
                <div class="modal-body">
                    <div class="registration-login-form">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home1" role="tab">
                                    @svg('login-icon', 'olymp-login-icon')
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">
                                    @svg('register-icon', 'olymp-register-icon')
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="home1" role="tabpanel" data-mh="log-tab">
                                <div class="title h6">Register to Olympus</div>
                                <form class="content">
                                    <div class="row">
                                        <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">First Name</label>
                                                <input class="form-control" placeholder="" type="text">
                                            </div>
                                        </div>
                                        <div class="col col-12 col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">Last Name</label>
                                                <input class="form-control" placeholder="" type="text">
                                            </div>
                                        </div>
                                        <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">Your Email</label>
                                                <input class="form-control" placeholder="" type="email">
                                            </div>
                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">Your Password</label>
                                                <input class="form-control" placeholder="" type="password">
                                            </div>

                                            <div class="form-group date-time-picker label-floating">
                                                <label class="control-label">Your Birthday</label>
                                                <input name="datetimepicker" value="10/24/1984"/>
                                                <span class="input-group-addon">
                                                    @svg('calendar-icon', 'olymp-calendar-icon')
										</span>
                                            </div>

                                            <div class="form-group label-floating is-select">
                                                <label class="control-label">Your Gender</label>
                                                <select class="selectpicker form-control">
                                                    <option value="MA">Male</option>
                                                    <option value="FE">Female</option>
                                                </select>
                                            </div>

                                            <div class="remember">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="optionsCheckboxes" type="checkbox">
                                                        I accept the <a href="#">Terms and Conditions</a> of the
                                                        website
                                                    </label>
                                                </div>
                                            </div>

                                            <a href="#" class="btn btn-purple btn-lg full-width">Complete
                                                Registration!</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane" id="profile1" role="tabpanel" data-mh="log-tab">
                                <div class="title h6">Login to your Account</div>
                                <form class="content">
                                    <div class="row">
                                        <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">Your Email</label>
                                                <input class="form-control" placeholder="" type="email">
                                            </div>
                                            <div class="form-group label-floating is-empty">
                                                <label class="control-label">Your Password</label>
                                                <input class="form-control" placeholder="" type="password">
                                            </div>

                                            <div class="remember">

                                                <div class="checkbox">
                                                    <label>
                                                        <input name="optionsCheckboxes" type="checkbox">
                                                        Remember Me
                                                    </label>
                                                </div>
                                                <a href="#" class="forgot">Forgot my Password</a>
                                            </div>

                                            <a href="#" class="btn btn-lg btn-primary full-width">Login</a>

                                            <div class="or"></div>

                                            <a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i
                                                        class="fab fa-facebook-f" aria-hidden="true"></i>Login with
                                                Facebook</a>

                                            <a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i
                                                        class="fab fa-twitter" aria-hidden="true"></i>Login with
                                                Twitter</a>


                                            <p>Don’t you have an account?
                                                <a href="#">Register Now!</a> it’s really simple and you can start
                                                enjoing all the benefits!
                                            </p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Footer Full Width -->

    <div class="footer footer-full-width" id="footer">
        <div class="container">
            <div class="row">
                <div class="col col-lg-4 col-md-4 col-sm-6 col-6">


                    <!-- Widget About -->

                    <div class="widget w-about">

                        <a href="02-ProfilePage.html" class="logo">
                            <div class="img-wrap">
                                <img src="{{asset('frontend/img/logo-colored.png')}}" alt="Olympus">
                            </div>
                            <div class="title-block">
                                <h6 class="logo-title">olympus</h6>
                                <div class="sub-title">SOCIAL NETWORK</div>
                            </div>
                        </a>
                        <p>Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut
                            labore et
                            lorem.</p>
                        <ul class="socials">
                            <li>
                                <a href="#">
                                    <i class="fab fa-facebook-square" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-youtube" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-google-plus-g" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fab fa-instagram" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <!-- ... end Widget About -->

                </div>

                <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                    <!-- Widget List -->

                    <div class="widget w-list">
                        <h6 class="title">Main Links</h6>
                        <ul>
                            <li>
                                <a href="#">Landing</a>
                            </li>
                            <li>
                                <a href="#">Home</a>
                            </li>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="#">Events</a>
                            </li>
                        </ul>
                    </div>

                    <!-- ... end Widget List -->

                </div>
                <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                    <div class="widget w-list">
                        <h6 class="title">Your Profile</h6>
                        <ul>
                            <li>
                                <a href="#">Main Page</a>
                            </li>
                            <li>
                                <a href="#">About</a>
                            </li>
                            <li>
                                <a href="#">Friends</a>
                            </li>
                            <li>
                                <a href="#">Photos</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                    <div class="widget w-list">
                        <h6 class="title">Features</h6>
                        <ul>
                            <li>
                                <a href="#">Newsfeed</a>
                            </li>
                            <li>
                                <a href="#">Post Versions</a>
                            </li>
                            <li>
                                <a href="#">Messages</a>
                            </li>
                            <li>
                                <a href="#">Friend Groups</a>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                    <div class="widget w-list">
                        <h6 class="title">Olympus</h6>
                        <ul>
                            <li>
                                <a href="#">Privacy</a>
                            </li>
                            <li>
                                <a href="#">Terms & Conditions</a>
                            </li>
                            <li>
                                <a href="#">Forums</a>
                            </li>
                            <li>
                                <a href="#">Statistics</a>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">


                    <!-- SUB Footer -->

                    <div class="sub-footer-copyright">
					<span>
						Copyright <a href="index.html">Olympus Buddypress + WP</a> All Rights Reserved 2017
					</span>
                    </div>

                    <!-- ... end SUB Footer -->

                </div>
            </div>
        </div>
    </div>

    <!-- ... end Footer Full Width -->


    <!-- Window-popup-CHAT for responsive min-width: 768px -->


    <a class="back-to-top" href="#">
        <img src="{{asset('frontend/icons/back-to-top.svg')}}" alt="arrow" class="back-icon" style="margin-top: 15px">
    </a>


@endsection
@section('scripts')
    <script src="{{asset('frontend/js/jquery-ui.js')}}"></script>
    <script src="{{asset('frontend/js/custom/init_datepicker.js')}}"></script>
    <script src="{{asset('frontend/js/custom/login-page.js')}}"></script>
    <script src="{{asset('frontend/js/overhang.min.js')}}"></script>
@endsection