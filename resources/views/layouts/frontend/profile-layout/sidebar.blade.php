<div class="fixed-sidebar">
    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left">

        <a href="{{route('profile.index')}}" class="logo">
            <div class="img-wrap">
                <img src="{{asset('frontend/img/logo.png')}}" alt="Olympus">
            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark">
            <ul class="left-menu">
                <li>
                    <a href="#" class="js-sidebar-open">
                        @svg('menu-icon', 'olymp-menu-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="03-Newsfeed.html">
                        @svg('newsfeed-icon', 'olymp-newsfeed-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="16-FavPagesFeed.html">
                        @svg('star-icon', 'olymp-star-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="17-FriendGroups.html">
                        @svg('happy-faces-icon', 'olymp-happy-faces-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="18-MusicAndPlaylists.html">
                        @svg('headphones-icon', 'olymp-headphones-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="19-WeatherWidget.html">
                        @svg('weather-icon', 'olymp-weather-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="20-CalendarAndEvents-MonthlyCalendar.html">
                        @svg('calendar-icon', 'olymp-calendar-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="24-CommunityBadges.html">
                        @svg('badge-icon', 'olymp-badge-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="25-FriendsBirthday.html">
                        @svg('cupcake-icon', 'olymp-cupcake-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="26-Statistics.html">
                        @svg('stats-icon', 'olymp-stats-icon left-menu-icon')
                    </a>
                </li>
                <li>
                    <a href="27-ManageWidgets.html">
                        @svg('manage-widgets-icon', 'olymp-manage-widgets-icon left-menu-icon')
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1">
        <a href="02-ProfilePage.html" class="logo">
            <div class="img-wrap">
                <img src="{{asset('frontend/img/logo.png')}}" alt="Olympus">
            </div>
            <div class="title-block">
                <h6 class="logo-title">olympus</h6>
            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark">
            <ul class="left-menu">
                <li>
                    <a href="#" class="js-sidebar-open">
                        @svg('close-icon', 'olymp-close-icon left-menu-icon')
                        <span class="left-menu-title"> @lang('messages.collapse_menu')</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('profile.index')}}">
                        @svg('newsfeed-icon', 'olymp-newsfeed-icon left-menu-icon')
                        <span class="left-menu-title"> @lang('messages.feed')</span>
                    </a>
                </li>
                <li>
                    <a href="16-FavPagesFeed.html">
                        @svg('star-icon', 'olymp-star-icon left-menu-icon')
                        <span class="left-menu-title"> Fav Pages Feed</span>
                    </a>
                </li>
                <li>
                    <a href="17-FriendGroups.html">
                        @svg('happy-faces-icon', 'olymp-happy-faces-icon left-menu-icon')
                        <span class="left-menu-title"> Friend Groups</span>
                    </a>
                </li>
                <li>
                    <a href="18-MusicAndPlaylists.html">
                        @svg('headphones-icon', 'olymp-headphones-icon left-menu-icon')
                        <span class="left-menu-title"> Music & Playlists</span>
                    </a>
                </li>
                <li>
                    <a href="19-WeatherWidget.html">
                        @svg('weather-icon', 'olymp-weather-icon left-menu-icon')
                        <span class="left-menu-title"> Weather App</span>
                    </a>
                </li>
                <li>
                    <a href="20-CalendarAndEvents-MonthlyCalendar.html">
                        @svg('calendar-icon', 'olymp-calendar-icon left-menu-icon')
                        <span class="left-menu-title"> Calendar and Events</span>
                    </a>
                </li>
                <li>
                    <a href="24-CommunityBadges.html">
                        @svg('badge-icon', 'olymp-badge-icon left-menu-icon')
                        <span class="left-menu-title"> Community Badges</span>
                    </a>
                </li>
                <li>
                    <a href="25-FriendsBirthday.html">
                        @svg('cupcake-icon', 'olymp-cupcake-icon left-menu-icon')
                        <span class="left-menu-title"> Friends Birthdays</span>
                    </a>
                </li>
                <li>
                    <a href="26-Statistics.html">
                        @svg('stats-icon', 'olymp-stats-icon left-menu-icon')
                        <span class="left-menu-title"> Account Stats</span>
                    </a>
                </li>
                <li>
                    <a href="27-ManageWidgets.html">
                        @svg('manage-widgets-icon', 'olymp-manage-widgets-icon left-menu-icon')
                        <span class="left-menu-title"> Manage Widgets</span>
                    </a>
                </li>
            </ul>

            <div class="profile-completion">

                <div class="skills-item">
                    <div class="skills-item-info">
                        <span class="skills-item-title">Profile Completion</span>
                        <span class="skills-item-count"><span class="count-animate" data-speed="1000"
                                                              data-refresh-interval="50" data-to="76"
                                                              data-from="0"></span><span class="units">76%</span></span>
                    </div>
                    <div class="skills-item-meter">
                        <span class="skills-item-meter-active bg-primary" style="width: 76%"></span>
                    </div>
                </div>

                <span>Complete <a href="#">your profile</a> so people can know more about you!</span>

            </div>
        </div>
    </div>
</div>

<div class="fixed-sidebar fixed-sidebar-responsive">

    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left-responsive">
        <a href="#" class="logo js-sidebar-open">
            <img src="{{asset('frontend/img/logo.png')}}" alt="Challenge">
        </a>

    </div>

    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1-responsive">
        <a href="#" class="logo">
            <div class="img-wrap">
                <img src="{{asset('frontend/img/logo.png')}}" alt="Challenge">
            </div>
            <div class="title-block">
                <h6 class="logo-title">Challenge</h6>
            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark">

            <div class="control-block">
                <div class="author-page author vcard inline-items">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/author-page.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>
                    <a href="{{route('profile.index')}}" class="author-name fn">
                        <div class="author-title">
                            {{Auth::user()->getFullName()}}
                            @svg('dropdown-arrow-icon', ['olymp-dropdown-arrow-icon'])
                        </div>
                        <span class="author-subtitle"></span>
                    </a>
                </div>
            </div>

            <div class="ui-block-title ui-block-title-small">
                <h6 class="title">MAIN SECTIONS</h6>
            </div>

            <ul class="left-menu">
                <li>
                    <a href="#" class="js-sidebar-open">
                        @svg('close-icon', 'olymp-close-icon left-menu-icon')
                        <span class="left-menu-title">@lang('messages.collapse_menu')</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('profile.index')}}">
                        @svg('newsfeed-icon', ['class' => 'olymp-newsfeed-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'NEWSFEED'])
                        <span class="left-menu-title">@lang('messages.feed')</span>
                    </a>
                </li>
                <li>
                    <a href="Mobile-28-YourAccount-PersonalInformation.html">
                        @svg('star-icon', ['class' => 'olymp-star-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'FAV PAGE'])
                        <span class="left-menu-title">Fav Pages Feed</span>
                    </a>
                </li>
                <li>
                    <a href="mobile-29-YourAccount-AccountSettings.html">
                        @svg('happy-faces-icon', ['class' => 'olymp-happy-faces-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'FRIEND GROUPS'])
                        <span class="left-menu-title">Friend Groups</span>
                    </a>
                </li>
                <li>
                    <a href="Mobile-30-YourAccount-ChangePassword.html">
                        @svg('headphones-icon', ['class' => 'olymp-headphones-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'MUSIC&PLAYLISTS'])
                        <span class="left-menu-title">Music & Playlists</span>
                    </a>
                </li>
                <li>
                    <a href="Mobile-31-YourAccount-HobbiesAndInterests.html">
                        @svg('weather-icon', ['class' => 'olymp-weather-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'WEATHER APP'])
                        <span class="left-menu-title">Weather App</span>
                    </a>
                </li>
                <li>
                    <a href="Mobile-32-YourAccount-EducationAndEmployement.html">
                        @svg('calendar-icon', ['class' => 'olymp-calendar-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'CALENDAR AND EVENTS'])
                        <span class="left-menu-title">Calendar and Events</span>
                    </a>
                </li>
                <li>
                    <a href="Mobile-33-YourAccount-Notifications.html">
                        @svg('badge-icon', ['class' => 'olymp-badge-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'Community Badges'])
                        <span class="left-menu-title">Community Badges</span>
                    </a>
                </li>
                <li>
                    <a href="Mobile-34-YourAccount-ChatMessages.html">
                        @svg('cupcake-icon', ['class' => 'olymp-cupcake-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'Friends Birthdays'])
                        <span class="left-menu-title">Friends Birthdays</span>
                    </a>
                </li>
                <li>
                    <a href="Mobile-35-YourAccount-FriendsRequests.html">
                        @svg('stats-icon', ['class' => 'olymp-stats-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'Account Stats'])
                        <span class="left-menu-title">Account Stats</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        @svg('manage-widgets-icon', ['class' => 'olymp-manage-widgets-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'Account Stats'])
                        <span class="left-menu-title">Manage Widgets</span>
                    </a>
                </li>
            </ul>

            <div class="ui-block-title ui-block-title-small">
                <h6 class="title">YOUR ACCOUNT</h6>
            </div>

            <ul class="account-settings">
                <li>
                    <a href="#">
                        @svg('menu-icon', ['olymp-menu-icon'])
                        <span>Profile Settings</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        @svg('star-icon', ['class' => 'olymp-star-icon left-menu-icon', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'data-original-title' => 'FAV PAGE'])
                        <span>Create Fav Page</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        @svg('logout-icon', 'olymp-logout-icon')
                        <span>Log Out</span>
                    </a>
                </li>
            </ul>

            <div class="ui-block-title ui-block-title-small">
                <h6 class="title">About Olympus</h6>
            </div>

            <ul class="about-olympus">
                <li>
                    <a href="#">
                        <span>Terms and Conditions</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>FAQs</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>Careers</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>Contact</span>
                    </a>
                </li>
            </ul>

        </div>
    </div>
</div>
