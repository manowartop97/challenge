<!DOCTYPE html>
<html lang="en">
<head>
    <title>@yield('title')</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Main Font -->
    <script src="{{asset('frontend/js/webfontloader.min.js')}}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap/bootstrap-reboot.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap/bootstrap-grid.css')}}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/fonts.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/overhang.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/preloader/preloader.css')}}">

</head>
<body>

<div id="preloader" class="preloader"></div>
<!-- Fixed Sidebar Left -->
<?= view('layouts.frontend.profile-layout.sidebar') ?>
<!-- ... end Fixed Sidebar Left -->

<!-- Fixed Sidebar Right -->
<?= view('layouts.frontend.profile-layout.chat') ?>
<!-- ... end Fixed Sidebar Right-Responsive -->


<!-- Header-BP -->
<?= view('layouts.frontend.profile-layout.header') ?>
<!-- ... end Responsive Header-BP -->


<!-- Top Header-Profile -->
@yield('content')
<!-- ... end Window-popup-CHAT for responsive min-width: 768px -->
<!-- ... end Playlist Popup -->


<a class="back-to-top" href="#">
    <img src="{{ asset('frontend/icons/back-to-top.svg') }}" alt="arrow" class="back-icon">
</a>


<!-- Window-popup-CHAT for responsive min-width: 768px -->

<div class="ui-block popup-chat popup-chat-responsive" tabindex="-1" role="dialog"
     aria-labelledby="update-header-photo"
     aria-hidden="true">

    <div class="modal-content">
        <div class="modal-header">
            <span class="icon-status online"></span>
            <h6 class="title">Chat</h6>
            <div class="more">
                @svg('three-dots-icon', ['olymp-three-dots-icon'])
                @svg('little-delete', ['olymp-little-delete'])
            </div>
        </div>
        <div class="modal-body">
            <div class="mCustomScrollbar">
                <ul class="notification-list chat-message chat-message-field">
                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar14-sm.jpg')}}" alt="author"
                                 class="mCS_img_loaded">
                        </div>
                        <div class="notification-event">
                            <span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
                        </div>
                    </li>

                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/author-page.jpg')}}" alt="author"
                                 class="mCS_img_loaded">
                        </div>
                        <div class="notification-event">
                            <span class="chat-message-item">Don’t worry Mathilda!</span>
                            <span class="chat-message-item">I already bought everything</span>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
                        </div>
                    </li>

                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar14-sm.jpg')}}" alt="author"
                                 class="mCS_img_loaded">
                        </div>
                        <div class="notification-event">
                            <span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
                        </div>
                    </li>
                </ul>
            </div>

            <form class="need-validation">

                <div class="form-group label-floating is-empty">
                    <label class="control-label">Press enter to post...</label>
                    <textarea class="form-control" placeholder=""></textarea>
                    <div class="add-options-message">
                        <a href="#" class="options-message">
                            @svg('computer-icon', ['olymp-computer-icon'])
                        </a>
                        <div class="options-message smile-block">

                            @svg('happy-sticker-icon', ['olymp-happy-sticker-icon'])

                            <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat1.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat2.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat3.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat4.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat5.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat6.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat7.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat8.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat9.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat10.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat11.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat12.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat13.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat14.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat15.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat16.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat17.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat18.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat19.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat20.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat21.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat22.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat23.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat24.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat25.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat26.png')}}" alt="icon">
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src="{{asset('frontend/img/icon-chat27.png')}}" alt="icon">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<span style="display: none;" class="storeTokenRoute" data-route="{{route('notification.store-token')}}"></span>
<span style="display: none;" class="checkTokenRoute" data-route="{{route('notification.check-token')}}"></span>
<!-- JS Scripts -->
<script src="{{asset('frontend/js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('frontend/js/jquery-ui.js')}}"></script>
<script src="{{asset('frontend/js/jquery.appear.js')}}"></script>
<script src="{{asset('frontend/js/jquery.mousewheel.js')}}"></script>
<script src="{{asset('frontend/js/perfect-scrollbar.js')}}"></script>
<script src="{{asset('frontend/js/jquery.matchHeight.js')}}"></script>
<script src="{{asset('frontend/js/svgxuse.js')}}"></script>
<script src="{{asset('frontend/js/imagesloaded.pkgd.js')}}"></script>
<script src="{{asset('frontend/js/Headroom.js')}}"></script>
<script src="{{asset('frontend/js/velocity.js')}}"></script>
<script src="{{asset('frontend/js/ScrollMagic.js')}}"></script>
<script src="{{asset('frontend/js/waypoints-init.js')}}"></script>
<script src="{{asset('frontend/js/jquery.countTo.js')}}"></script>
<script src="{{asset('frontend/js/popper.min.js')}}"></script>
<script src="{{asset('frontend/js/material.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap-select.js')}}"></script>
<script src="{{asset('frontend/js/smooth-scroll.js')}}"></script>
<script src="{{asset('frontend/js/selectize.js')}}"></script>
<script src="{{asset('frontend/js/swiper.jquery.js')}}"></script>
<script src="{{asset('frontend/js/moment.js')}}"></script>
<script src="{{asset('frontend/js/daterangepicker.js')}}"></script>
<script src="{{asset('frontend/js/simplecalendar.js')}}"></script>
<script src="{{asset('frontend/js/fullcalendar.js')}}"></script>
<script src="{{asset('frontend/js/isotope.pkgd.js')}}"></script>
<script src="{{asset('frontend/js/ajax-pagination.js')}}"></script>
<script src="{{asset('frontend/js/Chart.js')}}"></script>
<script src="{{asset('frontend/js/chartjs-plugin-deferred.js')}}"></script>
<script src="{{asset('frontend/js/loader.js')}}"></script>
<script src="{{asset('frontend/js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('frontend/js/jquery.gifplayer.js')}}"></script>
<script src="{{asset('frontend/js/mediaelement-and-player.js')}}"></script>
<script src="{{asset('frontend/js/mediaelement-playlist-plugin.min.js')}}"></script>
<script src="{{asset('frontend/js/sticky-sidebar.js')}}"></script>

<script defer src="{{asset('frontend/js/fontawesome-all.js')}}"></script>
<script defer src="{{asset('frontend/js/base-init.js')}}"></script>

<script src="{{asset('frontend/js/bootstrap/bootstrap.bundle.js')}}"></script>
<script src="{{asset('frontend/js/overhang.min.js')}}"></script>
<script src="{{asset('frontend/js/custom/header.js')}}"></script>
<script src="{{asset('frontend/js/custom/profile.js')}}"></script>
<script src="{{asset('frontend/js/custom/show-loader.js')}}"></script>

<script>
    $(document).ready(function () {
        $("#preloader").fadeOut(2000);
    })
</script>

<script src="https://www.gstatic.com/firebasejs/5.6.0/firebase.js"></script>

<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.6.0/firebase-app.js"></script>

<!-- Add additional services that you want to use -->
<script src="https://www.gstatic.com/firebasejs/5.6.0/firebase-messaging.js"></script>


<script src="{{asset('frontend/js/custom/push.js')}}"></script>
<script src="{{asset('frontend/js/custom/notifications/notifications.js')}}"></script>
<script src="{{asset('frontend/js/custom/friendship.js')}}"></script>

@yield('scripts')

</body>
</html>