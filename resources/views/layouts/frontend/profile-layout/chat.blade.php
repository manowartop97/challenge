<div class="fixed-sidebar right">
    <div class="fixed-sidebar-right sidebar--small" id="sidebar-right">

        <div class="mCustomScrollbar" data-mcs-theme="dark">
            <ul class="chat-users">
                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar67-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>
                </li>
                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar62-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>
                </li>

                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar68-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>
                </li>

                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar69-sm.jpg')}}" class="avatar">
                        <span class="icon-status away"></span>
                    </div>
                </li>

                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar70-sm.jpg')}}" class="avatar">
                        <span class="icon-status disconected"></span>
                    </div>
                </li>
                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar64-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>
                </li>
                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar71-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>
                </li>
                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar72-sm.jpg')}}" class="avatar">
                        <span class="icon-status away"></span>
                    </div>
                </li>
                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar63-sm.jpg')}}" class="avatar">
                        <span class="icon-status status-invisible"></span>
                    </div>
                </li>
                <li class="inline-items js-chat-open">
                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar72-sm.jpg')}}" class="avatar">
                        <span class="icon-status away"></span>
                    </div>
                </li>
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar71-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>
                </li>
            </ul>
        </div>

        <div class="search-friend inline-items">
            <a href="#" class="js-sidebar-open">
                @svg('menu-icon', ['olymp-menu-icon'])
            </a>
        </div>

        <a href="#" class="olympus-chat inline-items js-chat-open">
            @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
        </a>

    </div>

    <div class="fixed-sidebar-right sidebar--large" id="sidebar-right-1">

        <div class="mCustomScrollbar" data-mcs-theme="dark">

            <div class="ui-block-title ui-block-title-small">
                <a href="#" class="title">Close Friends</a>
                <a href="#">Settings</a>
            </div>

            <ul class="chat-users">
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar67-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Carol Summers</a>
                        <span class="status">ONLINE</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>

                </li>
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar62-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Mathilda Brinker</a>
                        <span class="status">AT WORK!</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>

                </li>

                <li class="inline-items js-chat-open">


                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar68-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Carol Summers</a>
                        <span class="status">ONLINE</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>


                </li>

                <li class="inline-items js-chat-open">


                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar69-sm.jpg')}}" class="avatar">
                        <span class="icon-status away"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Michael Maximoff</a>
                        <span class="status">AWAY</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>


                </li>

                <li class="inline-items js-chat-open">


                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar70-sm.jpg')}}" class="avatar">
                        <span class="icon-status disconected"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Rachel Howlett</a>
                        <span class="status">OFFLINE</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>


                </li>
            </ul>


            <div class="ui-block-title ui-block-title-small">
                <a href="#" class="title">MY FAMILY</a>
                <a href="#">Settings</a>
            </div>

            <ul class="chat-users">
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar64-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Sarah Hetfield</a>
                        <span class="status">ONLINE</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>
                </li>
            </ul>


            <div class="ui-block-title ui-block-title-small">
                <a href="#" class="title">UNCATEGORIZED</a>
                <a href="#">Settings</a>
            </div>

            <ul class="chat-users">
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar71-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Bruce Peterson</a>
                        <span class="status">ONLINE</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>


                </li>
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar72-sm.jpg')}}" class="avatar">
                        <span class="icon-status away"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Chris Greyson</a>
                        <span class="status">AWAY</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>

                </li>
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar63-sm.jpg')}}" class="avatar">
                        <span class="icon-status status-invisible"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Nicholas Grisom</a>
                        <span class="status">INVISIBLE</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar72-sm.jpg')}}" class="avatar">
                        <span class="icon-status away"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Chris Greyson</a>
                        <span class="status">AWAY</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>
                </li>
                <li class="inline-items js-chat-open">

                    <div class="author-thumb">
                        <img alt="author" src="{{asset('frontend/img/avatar71-sm.jpg')}}" class="avatar">
                        <span class="icon-status online"></span>
                    </div>

                    <div class="author-status">
                        <a href="#" class="h6 author-name">Bruce Peterson</a>
                        <span class="status">ONLINE</span>
                    </div>

                    <div class="more">
                        @svg('three-dots-icon', ['olymp-three-dots-icon'])

                        <ul class="more-icons">
                            <li>
                                @svg('comments-post-icon', ['olymp-comments-post-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'START CONVERSATION'])
                            </li>

                            <li>
                                @svg('add-to-conversation-icon', ['olymp-add-to-conversation-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'ADD TO CONVERSATION'])
                            </li>

                            <li>
                                @svg('block-from-chat-icon', ['olymp-block-from-chat-icon', 'data-placement'=> 'top',
                                'data-toggle' => 'tooltip', 'data-original-title' => 'BLOCK FROM CHAT'])
                            </li>
                        </ul>

                    </div>
                </li>
            </ul>

        </div>

        <div class="search-friend inline-items">
            <form class="form-group">
                <input class="form-control" placeholder="Search Friends..." value="" type="text">
            </form>

            <a href="{{route('profile-settings.account-settings')}}" class="settings">
                @svg('settings-icon', ['olymp-settings-icon'])
            </a>

            <a href="#" class="js-sidebar-open">
                @svg('close-icon', ['olymp-close-icon'])
            </a>
        </div>

        <a href="#" class="olympus-chat inline-items js-chat-open">

            <h6 class="olympus-chat-title">OLYMPUS CHAT</h6>
            @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
        </a>

    </div>
</div>

<!-- ... end Fixed Sidebar Right -->


<!-- Fixed Sidebar Right-Responsive -->

<div class="fixed-sidebar right fixed-sidebar-responsive">

    <div class="fixed-sidebar-right sidebar--small" id="sidebar-right-responsive">

        <a href="#" class="olympus-chat inline-items js-chat-open">
            @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
        </a>

    </div>

</div>