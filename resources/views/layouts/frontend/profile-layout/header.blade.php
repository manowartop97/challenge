<?php
/**
 * @var \App\Models\DB\Notification\Notification $notification
 */
?>
<header class="header" id="site-header">

    <div class="page-title">

    </div>

    <div class="header-content-wrapper">
        <form class="search-bar w-search notification-list friend-requests" method="get"
              action="{{route('search.people')}}">
            <div class="form-group with-button">
                <input class="form-control js-user-search" name="q" placeholder="@lang('messages.search_people')"
                       type="text">
                <button>
                    @svg('magnifying-glass-icon', ['olymp-magnifying-glass-icon'])
                </button>
            </div>
        </form>

        <a href="{{route('search.people')}}" class="link-find-friend">@lang('messages.search')</a>

        <div class="control-block">

            <div class="control-icon more has-items">
                @svg('happy-face-icon', ['olymp-happy-face-icon'])
                <div class="label-avatar bg-blue friendshipNotificationsCount">{{Auth::user()->unreadNotificationsCount()}}</div>

                <div class="more-dropdown more-with-triangle triangle-top-center">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">@lang('messages.profile.sidebar.friend_requests')</h6>
                        <a href="#" class="clearAllFriendshipNotifications">Settings</a>
                    </div>

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <ul class="notification-list friend-requests friendshipNotifications">
                            @forelse(Auth::user()->unreadNotifications() as $notification)
                                <?= view($notification->view_template, array_merge(json_decode($notification->arguments, true), ['notification_id' => $notification->id])) ?>
                            @empty
                                <li><h5 class="text-center noNotifications">@lang('messages.no_notifications')</h5></li>
                            @endforelse
                        </ul>
                    </div>

                    <a href="#" class="view-all bg-blue clearAllNotifications"
                       data-route="{{route('notification.clear-all', ['category' => \App\Services\Notification\NotificationService::CATEGORY_FRIENDSHIP])}}">@lang('messages.clear_all')</a>
                </div>
            </div>

            <div class="control-icon more has-items">
                @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
                <div class="label-avatar bg-purple">2</div>

                <div class="more-dropdown more-with-triangle triangle-top-center">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">Chat / Messages</h6>
                        <a href="#">Mark all as read</a>
                        <a href="#">Settings</a>
                    </div>

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <ul class="notification-list chat-message">
                            <li class="message-unread">
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar42-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">Diana Jameson</a>
                                    <span class="chat-message-item">Hi James! It’s Diana, I just wanted to let you know that we have to reschedule...</span>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">4 hours ago</time></span>
                                </div>
                                <span class="notification-icon">
                                     @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
								</span>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                </div>
                            </li>

                            <li>
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar42-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">Jake Parker</a>
                                    <span class="chat-message-item">Great, I’ll see you tomorrow!.</span>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">4 hours ago</time></span>
                                </div>
                                <span class="notification-icon">
                                     @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
								</span>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                </div>
                            </li>
                            <li>
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar42-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">Jake Parker</a>
                                    <span class="chat-message-item">Great, I’ll see you tomorrow!.</span>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">4 hours ago</time></span>
                                </div>
                                <span class="notification-icon">
                                     @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
								</span>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                </div>
                            </li>

                            <li class="chat-group">
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar11-sm.jpg')}}" alt="author">
                                    <img src="{{asset('frontend/img/avatar12-sm.jpg')}}" alt="author">
                                    <img src="{{asset('frontend/img/avatar13-sm.jpg')}}" alt="author">
                                    <img src="{{asset('frontend/img/avatar10-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <a href="#" class="h6 notification-friend">You, Faye, Ed &amp; Jet +3</a>
                                    <span class="last-message-author">Ed:</span>
                                    <span class="chat-message-item">Yeah! Seems fine by me!</span>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">March 16th at 10:23am</time></span>
                                </div>
                                <span class="notification-icon">
                                     @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
								</span>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                </div>
                            </li>
                        </ul>
                    </div>

                    <a href="#" class="view-all bg-purple">View All Messages</a>
                </div>
            </div>

            <div class="control-icon more has-items">
                @svg('thunder-icon', ['olymp-thunder-icon'])
                <div class="label-avatar bg-primary">8</div>

                <div class="more-dropdown more-with-triangle triangle-top-center">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">Notifications</h6>
                        <a href="#">Mark all as read</a>
                        <a href="#">Settings</a>
                    </div>

                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <ul class="notification-list">
                            <li>
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar62-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <div><a href="#" class="h6 notification-friend">Mathilda Brinker</a> commented on
                                        your new <a href="#" class="notification-link">profile status</a>.
                                    </div>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">4 hours ago</time></span>
                                </div>
                                <span class="notification-icon">
                                     @svg('comments-post-icon', ['olymp-comments-post-icon'])
									</span>

                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    @svg('little-delete', ['olymp-little-delete'])
                                </div>
                            </li>

                            <li class="un-read">
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar63-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <div>You and <a href="#" class="h6 notification-friend">Nicholas Grissom</a> just
                                        became friends. Write on <a href="#" class="notification-link">his wall</a>.
                                    </div>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">9 hours ago</time></span>
                                </div>
                                <span class="notification-icon">
										 @svg('happy-face-icon', ['olymp-happy-face-icon'])
									</span>

                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    @svg('little-delete', ['olymp-little-delete'])
                                </div>
                            </li>

                            <li class="with-comment-photo">
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar64-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <div><a href="#" class="h6 notification-friend">Sarah Hetfield</a> commented on your
                                        <a href="#" class="notification-link">photo</a>.
                                    </div>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">Yesterday at 5:32am</time></span>
                                </div>
                                <span class="notification-icon">
                                    @svg('comments-post-icon', ['olymp-comments-post-icon'])
									</span>

                                <div class="comment-photo">
                                    <img src="{{asset('frontend/img/choose-photo1.jpg')}}" alt="photo">
                                    <span>“She looks incredible in that outfit! We should see each...”</span>
                                </div>
                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    @svg('little-delete', ['olymp-little-delete'])
                                </div>
                            </li>

                            <li>
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar54-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <div><a href="#" class="h6 notification-friend">Green Goo Rock</a> invited you to
                                        attend to his event Goo in <a href="#" class="notification-link">Gotham Bar</a>.
                                    </div>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">March 5th at 6:43pm</time></span>
                                </div>
                                <span class="notification-icon">
										 @svg('happy-face-icon', ['olymp-happy-face-icon'])
									</span>

                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    @svg('little-delete', ['olymp-little-delete'])
                                </div>
                            </li>

                            <li>
                                <div class="author-thumb">
                                    <img src="{{asset('frontend/img/avatar66-sm.jpg')}}" alt="author">
                                </div>
                                <div class="notification-event">
                                    <div><a href="#" class="h6 notification-friend">James Summers</a> commented on your
                                        new <a href="#" class="notification-link">profile status</a>.
                                    </div>
                                    <span class="notification-date"><time class="entry-date updated"
                                                                          datetime="2004-07-24T18:18">March 2nd at 8:29pm</time></span>
                                </div>
                                <span class="notification-icon">
                                     @svg('heart-icon', ['olymp-heart-icon'])
									</span>

                                <div class="more">
                                    @svg('three-dots-icon', ['olymp-three-dots-icon'])
                                    @svg('little-delete', ['olymp-little-delete'])
                                </div>
                            </li>
                        </ul>
                    </div>

                    <a href="#" class="view-all bg-primary">View All Notifications</a>
                </div>
            </div>

            <div class="author-page author vcard inline-items more">
                <div class="author-thumb">
                    <span class="header-avatar">
                    <img alt="author" src="{{url('storage/'.Auth::user()->getAvatar())}}" class="avatar avatar-small">
                        </span>
                    <span class="icon-status online"></span>
                    <div class="more-dropdown more-with-triangle">
                        <div class="mCustomScrollbar" data-mcs-theme="dark">
                            <div class="ui-block-title ui-block-title-small">
                                <h6 class="title">Your Account</h6>
                            </div>

                            <ul class="account-settings">
                                <li>
                                    <a href="{{route('profile-settings.account-settings')}}">
                                        @svg('menu-icon', ['olymp-menu-icon'])
                                        <span>@lang('messages.profile.sidebar.account_settings')</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="36-FavPage-SettingsAndCreatePopup.html">
                                        @svg('star-icon', ['olymp-star-icon', 'data-toggle' => 'tooltip',
                                        'data-placement' => 'right', 'data-original-title' => 'FAV PAGE'])
                                        <span>Create Fav Page</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('logout')}}">
                                        @svg('logout-icon', ['olymp-logout-icon'])
                                        <span>@lang('messages.logout')</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="ui-block-title ui-block-title-small">
                                <h6 class="title">@lang('messages.chat_status')</h6>
                            </div>

                            <ul class="chat-settings">
                                <li>
                                    <a href="#">
                                        <span class="icon-status online"></span>
                                        <span>@lang('messages.online')</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon-status away"></span>
                                        <span>@lang('messages.away')</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="icon-status disconected"></span>
                                        <span>@lang('messages.disconnected')</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <span class="icon-status status-invisible"></span>
                                        <span>@lang('messages.invisible')</span>
                                    </a>
                                </li>
                            </ul>

                            <div class="ui-block-title ui-block-title-small">
                                <h6 class="title">@lang('messages.status')</h6>
                            </div>
                            <form class="form-group with-button custom-status status-form" method="post"
                                  data-route="{{route('profile-settings.update-status')}}">
                                {{csrf_field()}}
                                <input class="form-control status_string_input" name="status_string" placeholder=""
                                       type="text" value="{{Auth::user()->profile->status_string}}">
                                <button class="bg-purple status-form-submit">
                                    @svg('check-icon', ['olymp-check-icon'])
                                </button>
                            </form>

                            <div class="ui-block-title ui-block-title-small">
                                <h6 class="title">About Olympus</h6>
                            </div>

                            <ul>
                                <li>
                                    <a href="#">
                                        <span>Terms and Conditions</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>FAQs</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Careers</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>Contact</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <a href="{{route('profile.index')}}" class="author-name fn">
                    <div class="author-title">
                        {{Auth::user()->getFullName()}}
                        @svg('dropdown-arrow-icon', ['olymp-dropdown-arrow-icon'])
                    </div>
                </a>
            </div>

        </div>
    </div>

</header>

<!-- ... end Header-BP -->


<!-- Responsive Header-BP -->

<header class="header header-responsive" id="site-header-responsive">

    <div class="header-content-wrapper">
        <ul class="nav nav-tabs mobile-app-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#request" role="tab">
                    <div class="control-icon has-items">
                        @svg('happy-face-icon', ['olymp-happy-face-icon'])
                        <div class="label-avatar bg-blue friendshipNotificationsCount">{{Auth::user()->unreadNotificationsCount()}}</div>
                    </div>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#chat" role="tab">
                    <div class="control-icon has-items">
                        @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
                        <div class="label-avatar bg-purple">2</div>
                    </div>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#notification" role="tab">
                    <div class="control-icon has-items">
                        @svg('thunder-icon', ['olymp-thunder-icon'])
                        <div class="label-avatar bg-primary">8</div>
                    </div>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#search" role="tab">
                    @svg('magnifying-glass-icon', ['olymp-magnifying-glass-icon'])
                    @svg('close-icon', ['olymp-close-icon'])
                </a>
            </li>
        </ul>
    </div>

    <!-- Tab panes -->
    <div class="tab-content tab-content-responsive">

        <div class="tab-pane " id="request" role="tabpanel">

            <div class="mCustomScrollbar" data-mcs-theme="dark">
                <div class="ui-block-title ui-block-title-small">
                    <h6 class="title">@lang('messages.profile.sidebar.friend_requests')</h6>
                    <a href="#" class="clearAllFriendshipNotifications">Settings</a>
                </div>
                <ul class="notification-list friend-requests friendshipNotifications">
                    @forelse(Auth::user()->unreadNotifications() as $notification)
                        <?= view($notification->view_template, array_merge(json_decode($notification->arguments, true), ['notification_id' => $notification->id])) ?>
                    @empty
                        <li><h5 class="text-center noNotifications">@lang('messages.no_notifications')</h5></li>
                    @endforelse
                </ul>
                <a href="javascript:void(0)" class="view-all bg-blue clearAllNotifications"
                   data-route="{{route('notification.clear-all', ['category' => \App\Services\Notification\NotificationService::CATEGORY_FRIENDSHIP])}}">@lang('messages.clear_all')</a>
            </div>

        </div>

        <div class="tab-pane " id="chat" role="tabpanel">

            <div class="mCustomScrollbar" data-mcs-theme="dark">
                <div class="ui-block-title ui-block-title-small">
                    <h6 class="title">Chat / Messages</h6>
                    <a href="#">Mark all as read</a>
                    <a href="#">Settings</a>
                </div>

                <ul class="notification-list chat-message">
                    <li class="message-unread">
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar59-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Diana Jameson</a>
                            <span class="chat-message-item">Hi James! It’s Diana, I just wanted to let you know that we have to reschedule...</span>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">4 hours ago</time></span>
                        </div>
                        <span class="notification-icon">
                             @svg('chat---messages-icon', ['olymp-chat---messages-icon'])
									</span>
                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        </div>
                    </li>

                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar60-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Jake Parker</a>
                            <span class="chat-message-item">Great, I’ll see you tomorrow!.</span>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">4 hours ago</time></span>
                        </div>
                        <span class="notification-icon">
										@svg('chat---messages-icon', ['olymp-chat---messages-icon'])
									</span>

                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        </div>
                    </li>
                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar61-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">Elaine Dreyfuss</a>
                            <span class="chat-message-item">We’ll have to check that at the office and see if the client is on board with...</span>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">Yesterday at 9:56pm</time></span>
                        </div>
                        <span class="notification-icon">
												@svg('chat---messages-icon', ['olymp-chat---messages-icon'])
										</span>
                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        </div>
                    </li>

                    <li class="chat-group">
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar11-sm.jpg')}}" alt="author">
                            <img src="{{asset('frontend/img/avatar12-sm.jpg')}}" alt="author">
                            <img src="{{asset('frontend/img/avatar13-sm.jpg')}}" alt="author">
                            <img src="{{asset('frontend/img/avatar10-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <a href="#" class="h6 notification-friend">You, Faye, Ed &amp; Jet +3</a>
                            <span class="last-message-author">Ed:</span>
                            <span class="chat-message-item">Yeah! Seems fine by me!</span>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">March 16th at 10:23am</time></span>
                        </div>
                        <span class="notification-icon">
											@svg('chat---messages-icon', ['olymp-chat---messages-icon'])
										</span>
                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                        </div>
                    </li>
                </ul>

                <a href="#" class="view-all bg-purple">View All Messages</a>
            </div>

        </div>

        <div class="tab-pane " id="notification" role="tabpanel">

            <div class="mCustomScrollbar" data-mcs-theme="dark">
                <div class="ui-block-title ui-block-title-small">
                    <h6 class="title">Notifications</h6>
                    <a href="#">Mark all as read</a>
                    <a href="#">Settings</a>
                </div>

                <ul class="notification-list">
                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar62-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <div><a href="#" class="h6 notification-friend">Mathilda Brinker</a> commented on your new
                                <a href="#" class="notification-link">profile status</a>.
                            </div>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">4 hours ago</time></span>
                        </div>
                        <span class="notification-icon">
                             @svg('comments-post-icon', ['olymp-comments-post-icon'])

										</span>

                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                            @svg('little-delete', ['olymp-little-delete'])
                        </div>
                    </li>

                    <li class="un-read">
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar63-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <div>You and <a href="#" class="h6 notification-friend">Nicholas Grissom</a> just became
                                friends. Write on <a href="#" class="notification-link">his wall</a>.
                            </div>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">9 hours ago</time></span>
                        </div>
                        <span class="notification-icon">
                            @svg('happy-face-icon', ['olymp-happy-face-icon'])
                        </span>

                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                            @svg('little-delete', ['olymp-little-delete'])
                        </div>
                    </li>

                    <li class="with-comment-photo">
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar64-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <div><a href="#" class="h6 notification-friend">Sarah Hetfield</a> commented on your <a
                                        href="#" class="notification-link">photo</a>.
                            </div>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">Yesterday at 5:32am</time></span>
                        </div>
                        <span class="notification-icon">
                            @svg('comments-post-icon', ['olymp-comments-post-icon'])
                        </span>

                        <div class="comment-photo">
                            <img src="{{asset('frontend/img/comment-photo1.jpg')}}" alt="photo">
                            <span>“She looks incredible in that outfit! We should see each...”</span>
                        </div>

                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                            @svg('little-delete', ['olymp-little-delete'])
                        </div>
                    </li>

                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar65-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <div><a href="#" class="h6 notification-friend">Green Goo Rock</a> invited you to attend to
                                his event Goo in <a href="#" class="notification-link">Gotham Bar</a>.
                            </div>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">March 5th at 6:43pm</time></span>
                        </div>
                        <span class="notification-icon">
                            @svg('happy-face-icon', ['olymp-happy-face-icon'])
                        </span>

                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                            @svg('little-delete', ['olymp-little-delete'])
                        </div>
                    </li>

                    <li>
                        <div class="author-thumb">
                            <img src="{{asset('frontend/img/avatar66-sm.jpg')}}" alt="author">
                        </div>
                        <div class="notification-event">
                            <div><a href="#" class="h6 notification-friend">James Summers</a> commented on your new <a
                                        href="#" class="notification-link">profile status</a>.
                            </div>
                            <span class="notification-date"><time class="entry-date updated"
                                                                  datetime="2004-07-24T18:18">March 2nd at 8:29pm</time></span>
                        </div>
                        <span class="notification-icon">
                            @svg('heart-icon', ['olymp-heart-icon'])
                        </span>

                        <div class="more">
                            @svg('three-dots-icon', ['olymp-three-dots-icon'])
                            @svg('little-delete', ['olymp-little-delete'])
                        </div>
                    </li>
                </ul>

                <a href="#" class="view-all bg-primary">View All Notifications</a>
            </div>

        </div>

        <div class="tab-pane " id="search" role="tabpanel">


            <form class="search-bar w-search notification-list friend-requests" method="get"
                  action="{{route('search.people')}}">
                <div class="form-group with-button">
                    <input class="form-control js-user-search" name="q" placeholder="@lang('messages.search_people')"
                           type="text">
                    <button>
                        @svg('magnifying-glass-icon', ['olymp-magnifying-glass-icon'])
                    </button>
                </div>
            </form>


        </div>

    </div>
    <!-- ... end  Tab panes -->

</header>
