<!DOCTYPE html>
<html lang="en">
<head>

    <title>@yield('title')</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Main Font -->
    <script src="{{asset('frontend/js/webfontloader.min.js')}}"></script>

    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap/bootstrap-reboot.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap/bootstrap-grid.css')}}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/main.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/fonts.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/preloader/preloader.css')}}">


</head>

<body class="body-bg-white">

<div id="preloader" class="preloader"></div>

<!-- Header Standard Landing  -->
<!-- ... end Header Standard Landing  -->
@yield('content')
<span style="display: none;" class="storeTokenRoute" data-route="{{route('notification.store-token')}}"></span>
<!-- JS Scripts -->
<script src="{{asset('frontend/js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('frontend/js/jquery.appear.js')}}"></script>
<script src="{{asset('frontend/js/jquery.mousewheel.js')}}"></script>
<script src="{{asset('frontend/js/perfect-scrollbar.js')}}"></script>
<script src="{{asset('frontend/js/jquery.matchHeight.js')}}"></script>
<script src="{{asset('frontend/js/svgxuse.js')}}"></script>
<script src="{{asset('frontend/js/imagesloaded.pkgd.js')}}"></script>
<script src="{{asset('frontend/js/Headroom.js')}}"></script>
<script src="{{asset('frontend/js/velocity.js')}}"></script>
<script src="{{asset('frontend/js/ScrollMagic.js')}}"></script>
<script src="{{asset('frontend/js/jquery.waypoints.js')}}"></script>
<script src="{{asset('frontend/js/jquery.countTo.js')}}"></script>
<script src="{{asset('frontend/js/popper.min.js')}}"></script>
<script src="{{asset('frontend/js/material.min.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap-select.js')}}"></script>
<script src="{{asset('frontend/js/smooth-scroll.js')}}"></script>
<script src="{{asset('frontend/js/selectize.js')}}"></script>
<script src="{{asset('frontend/js/swiper.jquery.js')}}"></script>
<script src="{{asset('frontend/js/moment.js')}}"></script>
<script src="{{asset('frontend/js/daterangepicker.js')}}"></script>


<script src="{{asset('frontend/js/simplecalendar.js')}}"></script>
<script src="{{asset('frontend/js/fullcalendar.js')}}"></script>
<script src="{{asset('frontend/js/isotope.pkgd.js')}}"></script>
<script src="{{asset('frontend/js/ajax-pagination.js')}}"></script>
<script src="{{asset('frontend/js/Chart.js')}}"></script>
<script src="{{asset('frontend/js/chartjs-plugin-deferred.js')}}"></script>
<script src="{{asset('frontend/js/circle-progress.js')}}"></script>
<script src="{{asset('frontend/js/loader.js')}}"></script>
<script src="{{asset('frontend/js/run-chart.js')}}"></script>
<script src="{{asset('frontend/js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('frontend/js/jquery.gifplayer.js')}}"></script>
<script src="{{asset('frontend/js/mediaelement-and-player.js')}}"></script>
<script src="{{asset('frontend/js/mediaelement-playlist-plugin.min.js')}}"></script>


<script src="{{asset('frontend/js/base-init.js')}}"></script>
<script defer src="{{asset('frontend/js/fontawesome-all.js')}}"></script>

<script src="{{asset('frontend/js/bootstrap/bootstrap.bundle.js')}}"></script>
<script src="{{asset('frontend/js/custom/show-loader.js')}}"></script>


<script>
    $(document).ready(function () {
        $("#preloader").fadeOut(2000);
    })
</script>
@yield('scripts')

</body>
</html>