<br>
<hr>
<div class="row">
    <div class="btn-group-sm">
        <a href="" class="btn btn-outline-danger pull-right mr-1 mb-1 removeMappingFieldItem">x</a>
    </div>
    <br>
    <br>
    <div class="col-md-6">
        <input type="text" name="fieldsFrom[{{$index}}]" id="fieldsFrom-{{$index}}" class="form-control"
               title="FieldFrom">
        <span class="help-block"></span>
    </div>
    <div class="col-md-6">
        <input type="text" name="fieldsTo[{{$index}}]" id="fieldsTo-{{$index}}" class="form-control" title="FieldsTo">
        <span class="help-block"></span>
    </div>

</div>