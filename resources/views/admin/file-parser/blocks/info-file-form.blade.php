<div class="form-group mapping_fields_block card container">
    <div class="col-md-12 mt-1 mb-2">
        <label for="fileDelimiter">Разделитель значений в файле</label>
        <input type="text" name="fileDelimiter" id="fileDelimiter" class="form-control" title="fileDelimiter">
        <span class="help-block"></span>
    </div>
    <a href="" data-route="{{route('admin.file-parser.get-additional-mapping-field')}}"
       class="btn btn-success append_db_mapping_field"
       data-id="{{\App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_FILE}}">+</a>
    <hr>
    <table class="table">
        <tr>
            <th>Поля файла</th>
        </tr>
    </table>
    <div class="row">
        <div class="col-md-11">
            <input type="text" name="fieldsFrom[0]" id="fieldsFrom-0" class="form-control" title="FieldFrom">
            <span class="help-block"></span>
        </div>
        <div class="col-md-1">
            <div class="btn-group-sm">
                <a href="" class="btn btn-block btn-outline-danger pull-right mr-1 mb-1 removeMappingFieldItem" style="height: 33px">x</a>
            </div>
        </div>
    </div>
</div>