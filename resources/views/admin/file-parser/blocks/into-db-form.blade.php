<div class="form-group mapping_fields_block">
    <a href="" data-route="{{route('admin.file-parser.get-additional-mapping-field')}}"
       class="btn btn-success append_db_mapping_field"
       data-id="{{\App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_DB}}">+</a>
    <hr>
    <div class="col-md-12">
        <label for="parseOnlyUnique">Парсить только уникальные значения</label>
        <input type="checkbox" name="parseOnlyUnique" id="parseOnlyUnique" title="Парсить только уникальные значения" value="1">
        <input type="hidden" name="parseOnlyUnique" id="parseOnlyUnique" title="Парсить только уникальные значения" value="0">
        <span class="help-block"></span>
    </div>
    <br>
    <table class="table">
        <tr>
            <th>Поля файла</th>
            <th>Поля таблицы</th>
        </tr>
    </table>
    <div class="row">
        <div class="btn-group-sm">
            <a href="" class="btn btn-outline-danger pull-right mr-1 mb-1 removeMappingFieldItem">x</a>
        </div>
        <br>
        <br>
        <div class="col-md-6">
            <input type="text" name="fieldsFrom[0]" id="fieldsFrom-0" class="form-control" title="FieldFrom">
            <span class="help-block"></span>
        </div>
        <div class="col-md-6">
            <input type="text" name="fieldsTo[0]" id="fieldsTo-0" class="form-control" title="FieldsTo">
            <span class="help-block"></span>
        </div>
    </div>
</div>