<?php
/**
 * @var \App\Components\Parser\Models\DB\FileParser $parser
 */

?>
@extends('layouts.admin.main')
@section('content')
    <div class="ml-3">
        <div class="alert alert-danger mb-2" role="alert">
            <strong>Инструкция к парсингу!</strong>
            <br>
            <p>
                <h5>Пример парсинга xlxs:</h5>
            <b>Маппинг поле файла - таблица.поле (в случае с нескольними таблицами)
            <br>
            Иначе просто поле файла - поле таблицы или файла</b>
            <br>
            </p>
        </div>
    </div>
    <div class="card">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-form">Запуск парсера</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <form id="parsing_form" method="post" data-route="{{route('admin.file-parser.run')}}"
                              enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="parser_id">Парсер</label>
                                        <select name="parser_id" id="parser_id" class="form-control parser_select"
                                                data-route="{{route('admin.file-parser.get-mapping-form')}}">
                                            <option value="">Выбор парсера</option>
                                            @foreach($parsersList as $id => $parser)
                                                <option value="{{$id}}">{{$parser}}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <h4 class="form-section mapping_heading hidden"><i class="icon-clipboard4"></i>
                                    Маппинга полей файла и
                                    сущности</h4>

                                <div class="form_block"></div>

                                <h4 class="form-section"><i class="icon-clipboard4"></i> Настройки парсинга</h4>
                                <div class="form-group">
                                    <label for="companyName">Парсить в</label>
                                    <input type="text" id="destinationEntityName" class="form-control"
                                           placeholder="Парсить в" name="destinationEntityName">
                                    <span class="help-block"></span>
                                </div>

                                <div class="form-group">
                                    <label>Выбрать файл из которого парсить</label>
                                    <br>
                                    <label id="sourceFile" class="file center-block">
                                        <input type="file"
                                               name="{{\App\Components\Parser\Classes\File\AbstractBaseFileParser::FILE_FIELD}}[]"
                                               id="sourceFile" multiple>
                                        <span class="help-block"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Парсить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/parser/parser.js') }}" type="text/javascript"></script>
@stop