<?php

/**
 * @var \App\Components\Parser\Models\DB\FileParser $parser
 */
$route = $parser->exists ? route('admin.file-parser.update', ['id' => $parser->id]) : route('admin.file-parser.create');
?>

@extends('layouts.admin.main')
@section('content')
    <div class="container">
        <div class="card">
            <div class="col-md-12">
                <div class="card" style="height: 936px;">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-round-controls">Добавление парсера</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                                <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                                <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">

                            <div class="card-text">
                                <p>Создание шаблона парсера, на основе которого будет производится парсинг</p>
                            </div>

                            <form class="form" action="{{$route}}" method="post">
                                <div class="form-body">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="parser_name">Название парсера</label>
                                        <input type="text" id="parser_name" class="form-control round"
                                               placeholder="Название парсера" name="parser_name"
                                               value="{{$parser->exists ? $parser->parser_name : old('parser_name')}}">
                                        @if ($errors->has('parser_name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('parser_name') }}</strong>
                                             </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="complaintinput2">Тип парсера</label>
                                        <div class="input-group">
                                            <label class="display-inline-block custom-control custom-radio ml-1">
                                                <input type="radio" name="parser_type" class="custom-control-input"
                                                       {{
                                                       $parser->parser_type == \App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_FILE
                                                       || old('parser_type') == \App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_FILE
                                                       ? 'checked'
                                                       : ''
                                                       }}
                                                       value="{{\App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_FILE}}">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description ml-0">Парсинг данных в файл</span>
                                            </label>
                                            <label class="display-inline-block custom-control custom-radio">
                                                <input type="radio" name="parser_type"
                                                       {{
                                                       $parser->parser_type == \App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_DB
                                                       || old('parser_type') == \App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_DB
                                                       ? 'checked'
                                                       : ''
                                                       }}
                                                       class="custom-control-input"
                                                       value="{{\App\Components\Parser\Classes\File\AbstractBaseFileParser::PARSE_TYPE_INTO_DB}}">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description ml-0">Парсинг данных в таблицу БД</span>
                                            </label>
                                        </div>
                                        @if ($errors->has('parser_type'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('parser_type') }}</strong>
                                             </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="records_limit">Лимит записей парсинга (спарсить только N
                                            записей)</label>
                                        <input type="number" id="records_limit" class="form-control round"
                                               placeholder="Лимит записей парсинга" name="records_limit"
                                               value="{{$parser->exists ? $parser->records_limit : old('records_limit')}}">
                                        @if ($errors->has('records_limit'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('records_limit') }}</strong>
                                             </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="runtime_delay_in_seconds">Задержка запуска парсера(в
                                            секундах)</label>
                                        <input type="number" id="runtime_delay_in_seconds" class="form-control round"
                                               placeholder="Задержка запуска парсера" name="runtime_delay_in_seconds"
                                               value="{{$parser->exists ? $parser->runtime_delay_in_seconds : old('runtime_delay_in_seconds')}}">
                                        @if ($errors->has('runtime_delay_in_seconds'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('runtime_delay_in_seconds') }}</strong>
                                             </span>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="status">Активность</label>
                                        <input type="checkbox" name="status" value="1"
                                               @if(old('status', $parser->status))
                                               checked
                                               @endif
                                               title="Активность">
                                        @if ($errors->has('status'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('status') }}</strong>
                                             </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="icon-check2"></i> Сохранить
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection