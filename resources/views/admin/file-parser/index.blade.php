<?php
/**
 * @var \App\Components\Parser\Models\DB\FileParser $parser
 */
?>
@extends('layouts.admin.main')
@section('content')
    <div class="card">
        <a href="{{route('admin.file-parser.form')}}" class="btn btn-success mt-2 ml-2 mb-2">Создать</a>
        <table class="table table-bordered">
            <tr>
                <th>Имя парсера</th>
                <th>Активность</th>
                <th>Действия</th>
            </tr>
            @foreach($parsers as $parser)
                <tr>
                    <td>{{$parser->parser_name}}</td>
                    <td>
                        @if($parser->status)
                            <a href="{{route('admin.file-parser.deactivate', ['id' => $parser->id])}}"
                               class="btn btn-success">Активен</a>
                        @else
                            <a href="{{route('admin.file-parser.activate', ['id' => $parser->id])}}"
                               class="btn btn-danger">Не активен</a>
                        @endif
                    </td>
                    <td>
                        <a href="{{route('admin.file-parser.form', ['id' => $parser->id])}}"
                           class="btn btn-warning">Изменить</a>
                    </td>
                </tr>
            @endforeach
        </table>
        <div class="clearfix">
            {{$parsers->links()}}
        </div>
    </div>
@endsection