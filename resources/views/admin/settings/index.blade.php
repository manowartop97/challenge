<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 02.05.18
 * Time: 9:31
 */
?>
<title>Настройки</title>
@extends('layouts.admin.main')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <br>
                <div class="col-md-12">
                    <a href="{{route('admin.setting.form')}}" class="btn btn-success">Создать</a>
                    <br>
                    <br>
                </div>
                <br>
                <table class="table table-bordered">
                    <tr>
                        <th>systemName</th>
                        <th>Description</th>
                        <th>value</th>
                        <th>Действия</th>
                    </tr>
                    @foreach($settings as $setting)
                        <tr>
                            <td>{{$setting->systemName}}</td>
                            <td>{{$setting->description}}</td>
                            <td>{{$setting->value}}</td>
                            <td><a href="{{route('admin.setting.form', ['id' => $setting->id])}}"
                                   class="btn btn-warning btn-sm">Изменить</a><a
                                        href="{{route('admin.setting.delete', ['id' => $setting->id])}}"
                                        class="btn btn-danger btn-sm">Удалить</a></td>
                        </tr>
                    @endforeach
                </table>
                {{$settings->links()}}
            </div>
        </div>
    </div>
@endsection