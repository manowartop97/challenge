<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 02.05.18
 * Time: 9:31
 */
?>
<title>Логи</title>
@extends('layouts.admin.main')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <br>
                <a href="{{route('admin.log.delete-all')}}" class="btn btn-danger">Удалить все</a>
                <br>
                <table class="table table-bordered table-responsive">
                    <tr>
                        <th>logName</th>
                        <th>logCategory</th>
                        <th>Message</th>
                        <th>CreatedAt</th>
                        <th>Действия</th>
                    </tr>
                    @foreach($logs as $log)
                        <tr>
                            <td>{{$log->log_name}}</td>
                            <td>{{\App\Components\Logger\Models\DB\DbLog::LOG_CATEGORY_LIST[$log->log_category]}}</td>
                            <td>{{$log->message}}</td>
                            <td>{{$log->created_at->format('d M Y H:i')}}</td>
                            <td>
                                <a
                                        href="{{route('admin.log.delete', ['id' => $log->id])}}"
                                        class="btn btn-danger btn-sm">Удалить</a></td>
                        </tr>
                    @endforeach
                </table>
                {{$logs->links()}}
            </div>
        </div>
    </div>
@endsection