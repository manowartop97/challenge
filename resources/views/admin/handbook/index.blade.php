<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 02.05.18
 * Time: 9:31
 */
?>

@extends('layouts.admin.main')
<title>
    Справочники
</title>
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <br>
                <div class="col-md-12">
                    <a href="{{route('admin.handbook.form')}}" class="btn btn-success">Создать</a>
                    <a href="{{route('admin.handbook.refresh-cache')}}" class="btn btn-warning">Сбросить кэш</a>
                    <br>
                    <br>
                </div>

                <table class="table table-bordered">
                    <tr>
                        <th>systemName</th>
                        <th>Description</th>
                        <th>Действия</th>
                    </tr>
                    @foreach($handbooks as $handbook)
                        <tr>
                            <td>{{$handbook->systemName}}</td>
                            <td>{{$handbook->description}}</td>
                            <td><a href="{{route('admin.handbook.show-data', ['id' => $handbook->id])}}"
                                   class="btn btn-warning btn-sm">Изменить</a><a
                                        href="{{route('admin.handbook.delete', ['id' => $handbook->id])}}"
                                        class="btn btn-danger btn-sm">Удалить</a></td>
                        </tr>
                    @endforeach
                </table>
                {{$handbooks->links()}}
            </div>
        </div>
    </div>
@endsection