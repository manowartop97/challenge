@extends('layouts.admin.main')
@section('content')

    <div class="card">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-form">Запуск парсера</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="icon-minus4"></i></a></li>
                            <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                            <li><a data-action="expand"><i class="icon-expand2"></i></a></li>
                            <li><a data-action="close"><i class="icon-cross2"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <form id="parsing_form" method="post" action="{{route('admin.instagram-parser.run')}}">
                            {{csrf_field()}}
                            <div class="form-body">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="dataType">Что будем парсить?</label>
                                        <br>
                                        <span> <input type="radio" name="dataType" id="dataType" value="1" checked> Парсим пользователей</span>
                                        <br>
                                        <span> <input type="radio" name="dataType" id="dataType" value="2"> Парсим медиа</span>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="amountToParse">Количество аккаунтов</label>
                                        <select name="amountToParse" id="amountToParse" class="form-control">
                                            <option value="">Выбор количества</option>
                                            <option value="5">Первых 5</option>
                                            <option value="10">Первых 10</option>
                                            <option value="25">Первых 25</option>
                                            <option value="100">Первых 100</option>
                                            <option value="500">Первых 500</option>
                                            <option value="5000">Первых 5000</option>

                                        </select>
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="tag">Тег(чтобы привязать данные к какой-то категории(спорт,
                                            кулинария и т д))</label>
                                        <input type="text" name="tag" id="tag" class="form-control">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="username">Имя пользователя от аккаунта</label>
                                            <input type="text" name="username" id="username" class="form-control">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="password">Пароль от аккаунта</label>
                                            <input type="text" name="password" id="password" class="form-control">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="keywords">Ключевые слова для поиска аккаунтов</label>
                                        <textarea name="keywords" class="form-control" id="keywords"></textarea>
                                        <span class="help-block"></span>
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="onlyHashtags">Парсим хештеги(и только их)(Работает только для медиа)</label>
                                        <br>
                                        <input type="checkbox" name="onlyHashtags" id="onlyHashtags" value="1">
                                        <span class="help-block"></span>
                                    </div>
                                </div>


                            </div>

                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Парсить
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection