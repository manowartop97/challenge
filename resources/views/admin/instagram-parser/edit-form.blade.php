@extends('layouts.admin.main')
@section('content')

    <div class="card">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-form">Редактирование поста</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <form class="" method="post" action="{{route('admin.instagram-parser.store', ['id' => $model->id])}}">
                            {{csrf_field()}}
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="type" class="sr-only">Тип медиа</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="">Тип медиа</option>
                                        <option value="1" {{old('type') ?: $model->type == 1 ? 'selected' : ''}}>
                                            Карусель
                                        </option>
                                        <option value="2" {{old('type') ?: $model->type == 2 ? 'selected' : ''}}>Видео
                                        </option>
                                        <option value="3" {{old('type') ?: $model->type == 3 ? 'selected' : ''}}>
                                            Изображение
                                        </option>
                                    </select>
                                    <div class="errors">
                                        @if($errors->has('type'))
                                            {{$errors->first('type')}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status" class="sr-only">Статус</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="" selected>Статус</option>
                                        <option value="1" {{old('status') ?: $model->status == 1 ? 'selected' : ''}}>ОК
                                        </option>
                                        <option value="2" {{old('status') ?: $model->status == 2 ? 'selected' : ''}}>
                                            Требует
                                            модерации
                                        </option>
                                        <option value="3" {{old('status') ?: $model->status == 3 ? 'selected' : ''}}>
                                          Автоописание
                                        </option>
                                    </select>
                                    <div class="errors">
                                        @if($errors->has('status'))
                                            {{$errors->first('status')}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="rating" class="sr-only">Рейтинг</label>
                                    <select class="form-control" name="rating" id="rating">
                                        <option value="" selected>Рейтинг</option>
                                        <option value="1" {{old('rating') ?: $model->rating == 1 ? 'selected' : ''}}>1
                                        </option>
                                        <option value="2" {{old('rating') ?: $model->rating == 2 ? 'selected' : ''}}>2
                                        </option>
                                        <option value="3" {{old('rating') ?: $model->rating == 3 ? 'selected' : ''}}>3
                                        </option>
                                        <option value="4" {{old('rating') ?: $model->rating == 4 ? 'selected' : ''}}>4
                                        </option>
                                        <option value="5" {{old('rating') ?: $model->rating == 5 ? 'selected' : ''}}>5
                                        </option>
                                    </select>
                                    <div class="errors">
                                        @if($errors->has('rating'))
                                            {{$errors->first('rating')}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="tag" class="sr-only">Теги</label>
                                <input type="text" class="form-control" name="tag" id="tag" placeholder="Теги"
                                       value="{{old('tag') ?: $model->tag}}">
                                <div class="errors">
                                    @if($errors->has('tag'))
                                        {{$errors->first('tag')}}
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="hashtags">
                                    Чтобы сгенерировать хэштеги - вводите их через пробел
                                </label>
                                <div class="input-group">
                                    <input type="text" id="hashtags" class="form-control input-mask-date">
                                    <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary generate_hashtags"
                                            data-route="{{route('admin.instagram-parser.hashtags')}}">
                                        Генерировать
                                    </button>
                                </span>
                                </div>
                                <br>
                            </div>

                            <div role="alert" class="alert alert-info hashtagsDiv hidden">
                                <a href="" class="close copyToClipboard"
                                   data-clipboard-target="#outHashtags">
                                    <i class="fa fa-clipboard"></i>
                                </a>
                                <strong class="hashtagsOutput" id="outHashtags"></strong>
                            </div>
                            <div class="form-group">
                                <label for="caption" class="sr-only">Описание</label>
                                <textarea class="form-control" name="caption" id="caption" cols="30"
                                          rows="10">{{old('caption') ?: $model->caption}}</textarea>

                                <div class="errors">
                                    @if($errors->has('caption'))
                                        {{$errors->first('caption')}}
                                    @endif
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mb-2">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script src="{{asset('frontend/js/clipboard.js')}}"></script>
    <script src="{{asset('frontend/js/custom/post.js')}}"></script>
@endsection