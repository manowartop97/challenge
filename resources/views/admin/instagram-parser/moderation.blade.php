<?php
/**
 * @var \Illuminate\Contracts\Pagination\Paginator $items
 */
?>

@extends('layouts.admin.main')
@section('title') Модерация @endsection
@section('content')
    <div class="card">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-form">Модерация медиафайлов (всего {{$items->total()}})</h4>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">


                        <div class="col-md-12">
                            <form class="form-inline" method="get">
                                <div class="form-group mx-sm-4 mb-2">
                                    <label for="caption" class="sr-only">Описание</label>
                                    <input type="text" class="form-control" name="caption" id="caption"
                                           placeholder="Описание" value="{{Request::get('caption')}}">
                                </div>
                                <div class="form-group mx-sm-4 mb-2">
                                    <label for="tag" class="sr-only">Теги</label>
                                    <input type="text" class="form-control" name="tag" id="tag" placeholder="Теги"
                                           value="{{Request::get('tag')}}">
                                </div>
                                <div class="form-group mx-sm-4 mb-2">
                                    <label for="type" class="sr-only">Тип медиа</label>
                                    <select class="form-control" name="type" id="type">
                                        <option value="" selected>Тип медиа</option>
                                        <option value="1" {{Request::get('type') == 1 ? 'selected' : ''}}>Карусель
                                        </option>
                                        <option value="2" {{Request::get('type') == 2 ? 'selected' : ''}}>Видео</option>
                                        <option value="3" {{Request::get('type') == 3 ? 'selected' : ''}}>Изображение
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group mx-sm-4 mb-2">
                                    <label for="status" class="sr-only">Статус</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="" selected>Статус</option>
                                        <option value="1" {{Request::get('status') == 1 ? 'selected' : ''}}>ОК</option>
                                        <option value="2" {{Request::get('status') == 2 ? 'selected' : ''}}>Требует
                                            модерации
                                        </option>
                                        <option value="3" {{Request::get('status') == 3 ? 'selected' : ''}}>
                                            Автоописание
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group mx-sm-4 mb-2">
                                    <label for="rating" class="sr-only">Рейтинг</label>
                                    <select class="form-control" name="rating" id="rating">
                                        <option value="" selected>Рейтинг</option>
                                        <option value="1" {{Request::get('rating') == 1 ? 'selected' : ''}}>1</option>
                                        <option value="2" {{Request::get('rating') == 2 ? 'selected' : ''}}>2</option>
                                        <option value="3" {{Request::get('rating') == 3 ? 'selected' : ''}}>3</option>
                                        <option value="4" {{Request::get('rating') == 4 ? 'selected' : ''}}>4</option>
                                        <option value="5" {{Request::get('rating') == 5 ? 'selected' : ''}}>5</option>
                                    </select>
                                </div>

                                <div class="form-group mx-sm-4 mb-2">
                                    <label for="username" class="sr-only">Имя пользователя</label>
                                    <input type="text" class="form-control" name="username" id="username" value="{{Request::get('username')}}">
                                </div>
                                <div class="form-group mx-sm-4 mb-2">
                                    <label for="notInAccountId" class="sr-only">Игнорировать аккаунт ИД</label>
                                    <input type="text" class="form-control" name="notInAccountId" id="notInAccountId" value="{{Request::get('notInAccountId')}}">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2">Искать</button>
                                <a href="{{route('admin.instagram-parser.moderate')}}" class="btn btn-danger mb-2">Сбросить
                                    фильтры</a>
                            </form>
                        </div>

                        <table class="table table-responsive table-condensed table-bordered">
                            <tr>
                                <th>Медиа</th>
                                <th>Описание</th>
                                <th>Теги</th>
                                <th>Статус</th>
                                <th>Тип</th>
                                <th>Username</th>
                                <th>Операции</th>
                            </tr>

                            @foreach($items as $id => $item)
                                <tr>
                                    <td>
                                        @php
                                            $resultUrls = [];
                                            foreach (json_decode($item->urls) as $url) {
                                                $resultUrls[] = [
                                                        'type' => explode('.', $url) === 'mp4'
                                                        ? \App\Components\Parser\Instagram\Classes\MediaParser::TYPE_VIDEO
                                                        : \App\Components\Parser\Instagram\Classes\MediaParser::TYPE_IMAGE,
                                                        'url'  => $url
                                                    ];
                                            }
                                        @endphp
                                        @if($item->type === \App\Components\Parser\Instagram\Classes\MediaParser::TYPE_CAROUSEL)



                                            @foreach($resultUrls as $mediaId => $mediaUrl)
                                                @if($mediaUrl['type'] == \App\Components\Parser\Instagram\Classes\MediaParser::TYPE_VIDEO)
                                                    <video id="vid{{$id.' '.$mediaId}}"
                                                           class="video-responsive carousel-item video-js vjs-default-skin {{!$loop->first ? 'hidden' : ''}}"
                                                           controls
                                                           preload="auto"
                                                           poster="{{$mediaUrl['url']}}"
                                                           data-setup="{}">
                                                        <source src="{{$mediaUrl['url']}}"
                                                                type='video/mp4'>
                                                    </video>
                                                @else
                                                    <img class="image-responsive carousel-item {{!$loop->first ? 'hidden' : ''}}"
                                                         src="{{$mediaUrl['url']}}"
                                                         style="width: 250px"
                                                         alt="...">
                                                @endif

                                            @endforeach
                                            <a href="" class="img-modal-btn left"><i class="fa fa-arrow-left"></i></a>
                                            <a href="" class="img-modal-btn right"><i class="fa fa-arrow-right"></i></a>

                                        @elseif($item->type === \App\Components\Parser\Instagram\Classes\MediaParser::TYPE_IMAGE)

                                            <img class="" style="width: 300px;"
                                                 src="{{json_decode($item->urls)[0]}}"
                                                 itemprop="thumbnail" alt="Image description">

                                        @else
                                            <div class="card">
                                                <div class="card-img-top">
                                                    <video width="300" height="250" controls>
                                                        <source src="{{json_decode($item->urls)[0]}}" type="video/mp4">
                                                    </video>
                                                </div>
                                            </div>
                                        @endif

                                    </td>
                                    <td class="fixed_width">{!! $item->caption !!}</td>
                                    <td class="fixed_smaller_width">{{ $item->tag }}</td>
                                    <td>
                                        <?php
                                        if ($item->status === \App\Components\Parser\Instagram\Classes\MediaParser::STATUS_OK) {
                                            $class = 'success';
                                            $text = 'OK';
                                        } elseif ($item->status === \App\Components\Parser\Instagram\Classes\MediaParser::STATUS_NEED_MODERATION) {
                                            $class = 'danger';
                                            $text = 'Требует модерации';
                                        } else {
                                            $class = 'warning';
                                            $text = 'Автоописание';
                                        }

                                        ?>

                                        <span class="text-{{$class}}">{{$text}}</span>
                                    </td>
                                    <td>
                                        @if($item->type === \App\Components\Parser\Instagram\Classes\MediaParser::TYPE_CAROUSEL)
                                            Карусель
                                        @elseif($item->type === \App\Components\Parser\Instagram\Classes\MediaParser::TYPE_VIDEO)
                                            Видео
                                        @else
                                            Изображение
                                        @endif
                                    </td>
                                    <td>{{$item->data_source_username}}</td>
                                    <td class="fixed_smaller_width">
                                        <a href="{{route('admin.instagram-parser.edit', ['id' => $item->id])}}"
                                           class="btn btn-warning btn-sm">Изменить</a>
                                        <hr>
                                        <a href=""
                                           data-route="{{route('admin.instagram-parser.delete', ['id' => $item->id])}}"
                                           class="btn btn-sm btn-danger delete_btn">Удалить</a>
                                        <hr>
                                        <a href=""
                                           data-route="{{route('admin.instagram-parser.set-status-ok', ['id' => $item->id])}}"
                                           class="btn btn-sm btn-success set_status_ok">OK</a>
                                    </td>
                                </tr>
                            @endforeach


                        </table>
                        @if(!empty($items))
                            {{$items->appends(['caption' => Request::get('caption'), 'tag' => Request::get('tag'), 'type' => Request::get('type'), 'status' => Request::get('status'), 'rating' => Request::get('rating'), 'username' => Request::get('username')])->links()}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).on('click', '.set_status_ok', function (e) {
            e.preventDefault();
            var btn = $(this);
            $.ajax({
                method: 'get',
                url: $(this).data('route'),
                success: function () {
                    btn.parent().parent().remove();
                },
                error: function () {

                }
            });
        });

        $(document).on('click', '.delete_btn', function (e) {
            e.preventDefault();
            var btn = $(this);
            $.ajax({
                method: 'get',
                url: $(this).data('route'),
                success: function () {
                    btn.parent().parent().remove();
                },
                error: function () {

                }
            });
        });


        $(function () {
            $(".img-modal-btn.right").on('click', function (e) {
                e.preventDefault();
                var cur = $(this).parent().find('.carousel-item:visible()');
                if (typeof cur[0].player !== 'undefined') {
                    cur[0].player.pause();
                }
                var next = cur.next('.carousel-item');
                var par = cur.parent();
                if (!next.length) {
                    next = $(cur.parent().find(".carousel-item").get(0))
                }
                cur.addClass('hidden');
                next.removeClass('hidden');

                return false;
            });

            $(".img-modal-btn.left").on('click', function (e) {
                e.preventDefault();
                var cur = $(this).parent().find('.carousel-item:visible()');
                if (typeof cur[0].player !== 'undefined') {
                    cur[0].player.pause();
                }
                var next = cur.prev('.carousel-item');
                var par = cur.parent();
                var children = cur.parent().find(".carousel-item");
                if (!next.length) {
                    next = $(children.get(children.length - 1))
                }
                cur.addClass('hidden');
                next.removeClass('hidden');

                return false;
            })

            /**
             * Стопаем видео при закрытии модалки
             */
            $(document).on('hidden.bs.modal', '.mediaModal', function () {
                // $('body video').each(function (index, element) {
                //     $(this).attr('src','')
                // })
                $(this).find('.media-content').html('');
            });
        });
    </script>
@endsection

<style>

    .fixed_width {
        word-wrap: break-word;
        max-width: 350px;
    }

    .fixed_smaller_width {
        word-wrap: break-word;
        max-width: 250px;
    }

    .pagination {
        list-style-type: none;
    }

    .pagination li {
        display: -webkit-inline-flex;
    }
</style>