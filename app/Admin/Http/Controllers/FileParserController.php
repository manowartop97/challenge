<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 21.07.2018
 * Time: 10:42
 */

namespace App\Admin\Http\Controllers;

use App\Components\Parser\Classes\File\AbstractBaseFileParser;
use App\Components\Parser\Exceptions\InvalidParserDataException;
use App\Components\Parser\Repositories\FileParserRepository;
use App\Components\Parser\Requests\FileParserRequest;
use App\Components\Parser\Requests\RunParserProcessRequest;
use App\Components\Parser\Services\FileParserService;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Request;

/**
 * Контроллер файловых парсеров
 *
 * Class FileParserController
 * @package App\Admin\Http\Controllers
 */
class FileParserController extends Controller
{
    /**
     * @var FileParserRepository
     */
    private $parserRepository;

    /**
     * FileParserController constructor.
     * @param FileParserRepository $parserRepository
     */
    function __construct(FileParserRepository $parserRepository)
    {
        $this->parserRepository = $parserRepository;
    }

    /**
     * Пагинированный список парсеров
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin.file-parser.index', [
            'parsers' => $this->parserRepository->getAllPaginated(),
        ]);
    }

    /**
     * Создание парсера
     *
     * @param FileParserRequest $request
     * @return RedirectResponse
     */
    public function create(FileParserRequest $request): RedirectResponse
    {
        if ($this->parserRepository->create($request->all())) {
            flash('Парсер создан')->success();

            return redirect(route('admin.file-parsers'));
        }

        return redirect()->back()->withInput();
    }

    /**
     * Обновление парсера
     *
     * @param int $id
     * @param FileParserRequest $request
     * @return FileParserController|RedirectResponse
     */
    public function update(int $id, FileParserRequest $request)
    {
        if ($this->parserRepository->update($id, $request->all())) {
            flash('Парсер обновлен')->warning();

            return redirect(route('admin.file-parsers'));
        }

        return redirect()->back()->withInput();
    }

    /**
     * Форма создания/изменения парсера
     *
     * @param int|null $id
     * @return View
     */
    public function form(int $id = null): View
    {
        return view('admin.file-parser.form', [
            'parser' => $this->parserRepository->get($id),
        ]);
    }

    /**
     * Включить парсер
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function activate(int $id): RedirectResponse
    {
        $this->parserRepository->activate($id)
            ? flash('Парсер активирован')->success()
            : flash('Ошибка во время активации парсера')->warning();

        return redirect(route('admin.file-parsers'));
    }

    /**
     * Отключить парсер
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function deactivate(int $id): RedirectResponse
    {
        $this->parserRepository->disable($id)
            ? flash('Парсер деактивинован')->error()
            : flash('Ошибка во время деактивании парсера')->warning();

        return redirect(route('admin.file-parsers'));
    }

    /**
     * Форма запуска парсера
     *
     * @return View
     */
    public function runForm(): View
    {
        return view('admin.file-parser.run-form', [
            'parsersList' => $this->parserRepository->getForDropdownList(),
        ]);
    }

    /**
     * Получить блок формы для настройки парсера
     *
     * @return View
     */
    public function getMappingForm(): View
    {
        return $this->parserRepository->get(Request::get('parser_id'))->parser_type == AbstractBaseFileParser::PARSE_TYPE_INTO_FILE
            ? view('admin.file-parser.blocks.info-file-form')
            : view('admin.file-parser.blocks.into-db-form');
    }

    /**
     * Получить доп поля для маппинга
     *
     * @return View
     */
    public function getAdditionalMappingField(): View
    {
        return Request::get('type') == AbstractBaseFileParser::PARSE_TYPE_INTO_FILE
            ? view('admin.file-parser.blocks.file-mapping-field', ['index' => Request::get('index')])
            : view('admin.file-parser.blocks.db-mapping-field', ['index' => Request::get('index')]);
    }

    /**
     * Установка парсинга в очередь
     *
     * @param RunParserProcessRequest $request
     * @param FileParserService $parserService
     * @throws InvalidParserDataException
     */
    public function run(RunParserProcessRequest $request, FileParserService $parserService)
    {
        if (!$parserService->setToQueue($request->all())) {
            throw new InvalidParserDataException($parserService->validator->getError());
        }
    }
}