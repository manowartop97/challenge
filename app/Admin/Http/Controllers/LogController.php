<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 09.11.2018
 * Time: 15:27
 */

namespace App\Admin\Http\Controllers;

use App\Components\Logger\Repositories\LoggerRepository;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Class LogController
 * @package App\Admin\Http\Controllers
 */
class LogController extends Controller
{
    /**
     * @var LoggerRepository
     */
    private $loggerRepository;

    /**
     * LogController constructor.
     * @param LoggerRepository $loggerRepository
     */
    public function __construct(LoggerRepository $loggerRepository)
    {
        $this->loggerRepository = $loggerRepository;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('admin.logs.index', ['logs' => $this->loggerRepository->getFiltered()]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws Exception
     */
    public function delete(int $id): RedirectResponse
    {
        if ($this->loggerRepository->delete($id)) {
            return redirect(route('admin.logs'));
        }

        throw new Exception('Error');
    }

    /**
     * @return RedirectResponse
     * @throws Exception
     */
    public function deleteAll(): RedirectResponse
    {
        $this->loggerRepository->deleteAll();
        return redirect(route('admin.logs'));
    }
}