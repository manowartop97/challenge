<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 19.10.2018
 * Time: 13:07
 */

namespace App\Admin\Http\Controllers;

use App\Components\AdminPage\Requests\ModerationEditFormRequest;
use App\Components\AdminPage\Requests\ModerationSearchRequest;
use App\Components\Instagram\Facades\Instagram;
use App\Components\Instagram\Service\InstagramService;
use App\Components\Parser\Instagram\Models\InstagramMediaFeed;
use App\Components\Parser\Instagram\Requests\InstagramParserRequest;
use App\Components\Parser\Instagram\Services\InstagramParserService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use InstagramAPI\Exception\BadRequestException;
use Session;

/**
 * Контроллер для управления парсингом инстаграм базы
 *
 * Class InstagramParser
 * @package App\Admin\Http\Controllers
 */
class InstagramParserController extends Controller
{
    /**
     * @var InstagramParserService
     */
    private $parserService;

    /**
     * InstagramParserController constructor.
     * @param InstagramParserService $parserService
     */
    public function __construct(InstagramParserService $parserService)
    {
        $this->parserService = $parserService;
    }

    /**
     * Форма парсера
     *
     * @return View
     */
    public function form(): View
    {
        return view('admin.instagram-parser.form');
    }

    /**
     * @param InstagramParserRequest $request
     * @throws \App\Components\Parser\Instagram\Exceptions\InvalidParserConfigurationException
     */
    public function run(InstagramParserRequest $request)
    {
        $this->parserService->runParser($request->validated());
    }

    /**
     * Страница модерации
     *
     * @param ModerationSearchRequest $request
     * @return View
     */
    public function moderate(ModerationSearchRequest $request): View
    {
        Session::put('moderationUrl', request()->fullUrl());

        return view('admin.instagram-parser.moderation', [
            'items' => $this->parserService->getMediaForModetation($request->validated())
        ]);
    }

    /**
     * Форма редактирования
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        return view('admin.instagram-parser.edit-form', [
            'model' => InstagramMediaFeed::query()->findOrFail($id)
        ]);
    }

    /**
     * Обеновляет пост
     *
     * @param int $id
     * @param ModerationEditFormRequest $request
     * @return RedirectResponse
     */
    public function store(int $id, ModerationEditFormRequest $request): RedirectResponse
    {
        if ($this->parserService->storeMediaRecord($id, $request->validated())) {
            return redirect(Session::get('moderationUrl'));
        }

        throw new \InvalidArgumentException('Не удалось обновить медиа');
    }

    /**
     * Генерация хэштегов по запросу пользователя
     * TODO забить в настройки аккаунт для таких случаев
     * @return JsonResponse
     */
    public function generateHashtags(): JsonResponse
    {
        /** @var InstagramService $client */
        $client = Instagram::getInstagramClient();
        $client->login('parser_top', 'asdfasdf1asdf');

        return response()->json(['data' => $client->generateHashtags(\Request::post('query'))], 200);
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(int $id): JsonResponse
    {
        if (!$this->parserService->deleteMedia($id)) {
            throw new BadRequestException();
        }

        return response()->json([], 200);
    }

    /**
     * Переводим в статус ок
     *
     * @param int $id
     * @return JsonResponse
     */
    public function setStatusOk(int $id): JsonResponse
    {
        if (!$this->parserService->setStatusOk($id)) {
            throw new BadRequestException();
        }

        return response()->json([], 200);
    }
}