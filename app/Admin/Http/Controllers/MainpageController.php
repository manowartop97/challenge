<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 01.05.18
 * Time: 13:37
 */

namespace App\Admin\Http\Controllers;

use App\Components\AdminPage\Services\StatisticsService;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use LanguageDetection\Language;

/**
 * Class MainpageController
 * @package App\Admin\Http\Controllers
 */
class MainpageController extends Controller
{
    /**
     * @var StatisticsService
     */
    private $statisticsService;

    /**
     * MainpageController constructor.
     * @param StatisticsService $statisticsService
     */
    public function __construct(StatisticsService $statisticsService)
    {
        $this->statisticsService = $statisticsService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(): View
    {
        return view('admin.mainpage.index', [
            'statistics' => $this->statisticsService->getStatisticsForMainpage()
        ]);
    }

}