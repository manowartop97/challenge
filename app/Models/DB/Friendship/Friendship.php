<?php

namespace App\Models\DB\Friendship;

use App\Models\DB\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $friend_id
 * @property int $user_id
 * @property string $created_at
 * @property int $friendship_status
 * @property string $updated_at
 * @property User $user
 * @property User $friend
 */
class Friendship extends Model
{
    /**
     * Статусы дружбы - новый, когда заявка только создана
     *
     * @const
     */
    const FRIENDSHIP_STATUS_NEW = 1;

    /**
     * Статус дружбы - принят
     *
     * @const
     */
    const FRIENDSHIP_STATUS_ACCEPTED = 2;

    /**
     * Статус дружбы - отклонен потенциальным френдом
     *
     * @const
     */
    const FRIENDSHIP_STATUS_REJECTED_BY_POTENTIAL_FRIEND = 3;

    /**
     * Статус дружбы - отклонен пригласителем
     *
     * @const
     */
    const FRIENDSHIP_STATUS_REJECTED_BY_INVITOR = 4;

    /**
     * Статус лист
     *
     * @const
     */
    const FRIENDSHIP_STATUSES_LIST = [
        self::FRIENDSHIP_STATUS_NEW                          => ['ua' => 'Новий', 'ru' => 'Новый', 'en' => 'New'],
        self::FRIENDSHIP_STATUS_ACCEPTED                     => ['ua' => 'Прийтяний', 'ru' => 'Принятый', 'en' => 'Accepted'],
        self::FRIENDSHIP_STATUS_REJECTED_BY_POTENTIAL_FRIEND => ['ua' => 'Відхилений', 'ru' => 'Отклонен', 'en' => 'Rejected'],
        self::FRIENDSHIP_STATUS_REJECTED_BY_INVITOR          => ['ua' => 'Відмінений', 'ru' => 'Отменен', 'en' => 'Declined'],
    ];

    /**
     * @var array
     */
    protected $fillable = ['friend_id', 'user_id', 'created_at', 'friendship_status', 'updated_at'];

    protected $casts = [
        'friend_id'         => 'integer',
        'user_id'           => 'integer',
        'friendship_status' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function friend()
    {
        return $this->belongsTo(User::class, 'friend_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
