<?php

namespace App\Models\DB\Challenge;

use App\Models\DB\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $group_id
 * @property string $created_at
 * @property string $updated_at
 * @property ChallengeGroup $challengeGroup
 * @property User $user
 */
class ChallengeGroupUser extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'group_id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'user_id'  => 'integer',
        'group_id' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function challengeGroup()
    {
        return $this->belongsTo(ChallengeGroup::class, 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
