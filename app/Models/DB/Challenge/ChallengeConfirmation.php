<?php

namespace App\Models\DB\Challenge;

use App\Models\DB\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $challenge_id
 * @property int $invitation_id
 * @property string $comment
 * @property string $additional_media
 * @property int $user_id
 * @property int $status
 * @property string $rejection_comment
 * @property string $moderator_comment
 * @property string $created_at
 * @property string $updated_at
 * @property Challenge $challenge
 * @property ChallengeInvitation $challengeInvitation
 * @property User $user
 */
class ChallengeConfirmation extends Model
{
    /**
     * Статусы
     *
     * @const
     */
    const STATUS_NEW = 1;
    const STATUS_ACCEPTED_BY_AUTHOR = 2;
    const STATUS_REJECTED_BY_AUTHOR = 3;
    const STATUS_ACCEPTED_BY_MODERATOR = 4;
    const STATUS_REJECTED_BY_MODERATOR = 5;

    /**
     * @const
     */
    const STATUS_LIST = [
        self::STATUS_NEW                   => ['ua' => 'Новий', 'ru' => 'Новое', 'en' => 'New'],
        self::STATUS_ACCEPTED_BY_AUTHOR    => ['ua' => 'Підтверджене', 'ru' => 'Принято', 'en' => 'Confirmed'],
        self::STATUS_REJECTED_BY_AUTHOR    => ['ua' => 'Відхилене', 'ru' => 'Отклонено', 'en' => 'Declined'],
        self::STATUS_ACCEPTED_BY_MODERATOR => ['ua' => 'Підтверджене модератором', 'ru' => 'Принято модератором', 'en' => 'Confirmed by moderator'],
        self::STATUS_REJECTED_BY_MODERATOR => ['ua' => 'Відхилене модератором', 'ru' => 'Отклонено модератором', 'en' => 'Declined by moderator'],
    ];

    /**
     * @var array
     */
    protected $fillable = ['challenge_id', 'user_id', 'invitation_id', 'comment', 'additional_media', 'status', 'rejection_comment', 'moderator_comment', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'challenge_id'  => 'integer',
        'invitation_id' => 'integer',
        'status'        => 'integer',
        'user_id'       => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function challengeInvitation()
    {
        return $this->belongsTo(ChallengeInvitation::class, 'invitation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Собираем массив дополнительных картинок
     *
     * @return array
     */
    public function getAdditionalMedia(): array
    {
        $results = [];

        if (!$this->additional_media) {
            return $results;
        }

        $userFolder = explode('@', $this->user->email)[0];

        foreach (json_decode($this->additional_media, true) as $additional_media) {

            $results[] = [
                'type'      => $additional_media['type'],
                'name'      => '/storage/challenge/'.$userFolder.'/'.$additional_media['name'],
                'thumbnail' => isset($additional_media['thumbnail']) ? '/storage/challenge/'.$userFolder.'/'.$additional_media['thumbnail'] : null,
            ];
        }

        return $results;
    }
}
