<?php

namespace App\Models\DB\Challenge;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property ChallengeGroupUser[] $challengeGroupUsers
 */
class ChallengeGroup extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function challengeGroupUsers()
    {
        return $this->hasMany(ChallengeGroupUser::class, 'group_id');
    }
}
