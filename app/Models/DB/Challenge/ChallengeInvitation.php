<?php

namespace App\Models\DB\Challenge;

use App\Models\DB\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $challenge_id
 * @property int $participant_id
 * @property int $group_id
 * @property int $invitor_id
 * @property string $invitation_text
 * @property string $rejection_text
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property Challenge $challenge
 * @property User $participant
 * @property User $invitor
 */
class ChallengeInvitation extends Model
{
    /**
     * Новое приглашение
     *
     * @const
     */
    const STATUS_SENT = 1;

    /**
     * Просроченное приглашение (если есть дедлайн у челленджа)
     *
     * @const
     */
    const STATUS_EXPIRED = 2;

    /**
     * Принят
     *
     * @const
     */
    const STATUS_ACCEPTED = 3;

    /**
     * Отклонен
     *
     * @const
     */
    const STATUS_REJECTED = 4;

    /**
     * Вступил сам
     *
     * @const
     */
    const STATUS_TAKE_PART_MANUALLY = 5;

    /**
     * Выполнил
     *
     * @const
     */
    const STATUS_DONE = 6;

    /**
     * Выполнил
     *
     * @const
     */
    const STATUS_CANCELLED_MANUALLY = 7;

    /**
     * @const
     */
    const STATUS_LIST = [
        self::STATUS_SENT               => ['ua' => 'Новий', 'ru' => 'Новый', 'en' => 'New'],
        self::STATUS_EXPIRED            => ['ua' => 'Просрочен', 'ru' => 'Просрочен', 'en' => 'Expired'],
        self::STATUS_ACCEPTED           => ['ua' => 'Принята', 'ru' => 'Принято', 'en' => 'Accepted'],
        self::STATUS_REJECTED           => ['ua' => 'Відхилена', 'ru' => 'Отклонено', 'en' => 'Rejected'],
        self::STATUS_TAKE_PART_MANUALLY => ['ua' => 'Вступив', 'ru' => 'Вступил', 'en' => 'Taking part manually'],
        self::STATUS_DONE               => ['ua' => 'Виконав', 'ru' => 'Выполнен', 'en' => 'Done'],
        self::STATUS_CANCELLED_MANUALLY => ['ua' => 'Відмінено вручну', 'ru' => 'Отменено вручную', 'en' => 'Cancelled manually'],
    ];

    /**
     * @var array
     */
    protected $fillable = ['invitor_id', 'challenge_id', 'participant_id', 'group_id', 'invitation_text', 'rejection_text', 'status', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'challenge_id'   => 'integer',
        'participant_id' => 'integer',
        'group_id'       => 'integer',
        'status'         => 'integer',
        'invitor_id'     => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function challenge()
    {
        return $this->belongsTo(Challenge::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function participant()
    {
        return $this->belongsTo(User::class, 'participant_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invitor()
    {
        return $this->belongsTo(User::class, 'invitor_id');
    }
}
