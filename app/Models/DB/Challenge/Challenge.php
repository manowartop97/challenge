<?php

namespace App\Models\DB\Challenge;

use App\Models\DB\User\User;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\HasTags;

/**
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property boolean $is_group_challenge
 * @property string $title_image
 * @property string $additional_media
 * @property string $deadline
 * @property string $description
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property ChallengeInvitation[] $challengeInvitations
 * @property ChallengeConfirmation[] $confirmations
 */
class Challenge extends Model
{
    use HasTags;

    /**
     * @const
     */
    const STATUS_IN_MODERATION = 1;
    const STATUS_ACTIVE = 2;
    const STATUS_DELETED = 3;
    const STATUS_DELETED_BY_MODERATOR = 4;
    const STATUS_NEEDS_MANUAL_MODERATION = 5;

    /**
     * @const
     */
    const STATUS_LIST = [
        self::STATUS_IN_MODERATION           => ['ua' => 'На модерації', 'ru' => 'На модерации', 'en' => 'In moderation'],
        self::STATUS_ACTIVE                  => ['ua' => 'Активний', 'ru' => 'Активный', 'en' => 'Active'],
        self::STATUS_DELETED                 => ['ua' => 'Видалений', 'ru' => 'Удален', 'en' => 'Deleted'],
        self::STATUS_DELETED_BY_MODERATOR    => ['ua' => 'Видалений модератором', 'ru' => 'Удален модератором', 'en' => 'Deleted by moderator'],
        self::STATUS_NEEDS_MANUAL_MODERATION => ['ua' => 'На ручній модерації', 'ru' => 'Требует ручной модерации', 'en' => 'Needs manual moderation'],
    ];

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'status', 'title', 'is_group_challenge', 'title_image', 'additional_media', 'deadline', 'description', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'user_id'            => 'integer',
        'category'           => 'integer',
        'is_group_challenge' => 'bool',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function challengeInvitations()
    {
        return $this->hasMany(ChallengeInvitation::class);
    }

    /**
     * Тайтл имедж челленджа
     *
     * @return string
     */
    public function getTitleImage(): string
    {
        return '/storage/challenge/'.explode('@', $this->user->email)[0].'/'.json_decode($this->title_image)->name;
    }

    /**
     * Собираем массив дополнительных картинок
     *
     * @return array
     */
    public function getAdditionalMedia(): array
    {
        $results = [];

        if (!$this->additional_media) {
            return $results;
        }

        $userFolder = explode('@', $this->user->email)[0];

        foreach (json_decode($this->additional_media, true) as $additional_media) {

            $results[] = [
                'type'      => $additional_media['type'],
                'name'      => '/storage/challenge/'.$userFolder.'/'.$additional_media['name'],
                'thumbnail' => isset($additional_media['thumbnail']) ? '/storage/challenge/'.$userFolder.'/'.$additional_media['thumbnail'] : null,
            ];
        }

        return $results;
    }

    /**
     * Проверка выполнен ли челлендж юзером
     *
     * @param int|null $id
     * @return bool
     */
    public function isChallengeDone(int $id = null): bool
    {
        return ChallengeInvitation::query()
            ->where([
                ['participant_id', is_null($id) ? Auth::id() : $id],
                ['status', ChallengeInvitation::STATUS_DONE],
                ['challenge_id', $this->id],
            ])
            ->exists();
    }

    /**
     * Существует ли активное приглашение юзера в этот челлендж
     *
     * @return bool
     */
    public function isActiveInvitationExists(): bool
    {
        return ChallengeInvitation::query()
            ->where([['challenge_id', $this->id], ['participant_id', Auth::id()]])
            ->whereIn('status', [ChallengeInvitation::STATUS_SENT, ChallengeInvitation::STATUS_ACCEPTED, ChallengeInvitation::STATUS_TAKE_PART_MANUALLY])
            ->exists();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function confirmations()
    {
        return $this->hasMany(ChallengeConfirmation::class);
    }

    /**
     * Создано ли подтверждение на челлендж пользователем
     *
     * @param int|null $id
     * @return bool
     */
    public function isConfirmationCreated(int $id = null): bool
    {
        /** @var ChallengeInvitation $invitation */
        $invitation = $this
            ->challengeInvitations()
            ->where('participant_id', is_null($id) ? Auth::id() : $id)
            ->get()
            ->first();

        return $this->confirmations()->where('invitation_id', $invitation->id)->exists();
    }
}
