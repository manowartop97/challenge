<?php

namespace App\Models\DB\User;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $who_can_view_posts
 * @property int $who_can_request_friends
 * @property int $who_can_invite_to_challenge
 * @property int $who_can_invite_to_event
 * @property boolean $is_telegram_notifications
 * @property boolean $is_email_notifications
 * @property boolean $is_friends_birthday_notifications
 * @property boolean $is_notification_sound_on
 * @property boolean $is_chat_sound_on
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class AccountSetting extends Model
{
    /**
     * @const
     */
    const VARIANT_EVERYBODY = 1;
    const VARIANT_ONLY_FRIENDS = 2;
    const VARIANT_NOBODY = 3;

    /**
     * @const
     */
    const VARIANTS_LIST = [
        self::VARIANT_EVERYBODY    => ['ua' => 'Всі користувачі', 'ru' => 'Все пользователи', 'en' => 'Everyone'],
        self::VARIANT_ONLY_FRIENDS => ['ua' => 'Тільки друзі', 'ru' => 'Только друзья', 'en' => 'Friends only'],
        self::VARIANT_NOBODY       => ['ua' => 'Ніхто', 'ru' => 'Никто', 'en' => 'Everyone']
    ];

    /**
     * Вариант для настроек с друзьями
     *
     * @const
     */
    const VARIANTS_LIST_FOR_FRIENDS = [
        self::VARIANT_EVERYBODY => self::VARIANTS_LIST[self::VARIANT_EVERYBODY],
        self::VARIANT_NOBODY    => self::VARIANTS_LIST[self::VARIANT_NOBODY],
    ];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['who_can_view_posts', 'who_can_request_friends', 'who_can_invite_to_challenge', 'who_can_invite_to_event', 'is_telegram_notifications', 'is_email_notifications', 'is_friends_birthday_notifications', 'is_notification_sound_on', 'is_chat_sound_on', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'who_can_view_posts'          => 'integer',
        'who_can_request_friends'     => 'integer',
        'who_can_invite_to_challenge' => 'integer',
        'who_can_invite_to_event'     => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
