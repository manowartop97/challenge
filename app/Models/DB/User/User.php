<?php

namespace App\Models\DB\User;

use App\Components\Rbac\Models\Role;
use App\Components\Rbac\Traits\Rbac;
use App\Models\DB\Challenge\Challenge;
use App\Models\DB\Challenge\ChallengeConfirmation;
use App\Models\DB\Challenge\ChallengeInvitation;
use App\Models\DB\Friendship\Friendship;
use App\Models\DB\Notification\Notification;
use App\Repositories\User\ProfileRepository;
use Auth;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $email
 * @property string $password
 * @property boolean $is_verified
 * @property string $confirmation_token
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Role[] $roles
 * @property UserProfile $profile
 * @property AccountSetting $settings
 * @property Friendship $friends
 * @property Challenge[] $challenges
 * @property ChallengeInvitation[] $invitationsSend
 * @property ChallengeInvitation[] $invitationsTakingPart
 * @property Notification[] $notifications
 * @property ChallengeConfirmation[] $confirmations
 */
class User extends \Illuminate\Foundation\Auth\User
{
    use Rbac;

    /**
     * Пол пользователя
     *
     * @const
     */
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * @const
     */
    const GENDER_LIST = [
        self::GENDER_MALE   => ['ua' => 'Чоловіча', 'en' => 'Male', 'ru' => 'Мужской'],
        self::GENDER_FEMALE => ['ua' => 'Жіноча', 'en' => 'Female', 'ru' => 'Женский'],
    ];

    /**
     * @var array
     */
    protected $fillable = ['email', 'password', 'is_verified', 'confirmation_token', 'remember_token', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(UserProfile::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function settings()
    {
        return $this->hasOne(AccountSetting::class, 'user_id');
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->profile->surname . ' ' . $this->profile->name;
    }

    /**
     * Путь к аватарке
     *
     * @return string
     */
    public function getAvatar(): string
    {
        return ProfileRepository::MEDIA_TYPE_AVATAR . '/' . explode('@', $this->email)[0] . '/' . $this->profile->avatar;
    }

    /**
     * Путь к фону
     *
     * @return string
     */
    public function getBackground(): string
    {
        return ProfileRepository::MEDIA_TYPE_AVATAR . '/' . explode('@', $this->email)[0] . '/' . $this->profile->profile_background;
    }

    /**
     * Является ли юзер френдом
     *
     * @param int $id
     * @return bool
     */
    public function isFriend(int $id): bool
    {
        return Friendship::query()->where([['user_id', Auth::id()], ['friend_id', $id]])->whereIn('friendship_status', [Friendship::FRIENDSHIP_STATUS_NEW, Friendship::FRIENDSHIP_STATUS_ACCEPTED])->exists();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function friends()
    {
        return $this->hasMany(Friendship::class);
    }

    /**
     * Исходящие запросы в друзья
     *
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function friendsOutcomeRequests(int $pageSize = 10): LengthAwarePaginator
    {
        return $this->friends()->where('friendship_status', Friendship::FRIENDSHIP_STATUS_NEW)->paginate($pageSize);
    }

    /**
     * Входящие запросы в друзья
     *
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function friendsIncomeRequests(int $pageSize = 10): LengthAwarePaginator
    {
        return Friendship::query()->where([['friend_id', $this->id], ['friendship_status', Friendship::FRIENDSHIP_STATUS_NEW]])->paginate($pageSize);
    }

    /**
     * Есть ли у меня входящая заявку в друзья от юзера
     *
     * @param int $userId
     * @return bool
     */
    public function isInvitedMeToFriendship(int $userId): bool
    {
        return Friendship::query()->where([['user_id', $userId], ['friend_id', Auth::id()], ['friendship_status', Friendship::FRIENDSHIP_STATUS_NEW]])->exists();
    }

    /**
     * Проверка есть ли уже юзер в друзьях
     *
     * @param int $userId
     * @return bool
     */
    public function isUserInFriendList(int $userId): bool
    {
        return Friendship::query()->where([['user_id', $userId], ['friend_id', Auth::id()]])->whereIn('friendship_status', [Friendship::FRIENDSHIP_STATUS_ACCEPTED, Friendship::FRIENDSHIP_STATUS_NEW])->exists()
            || Friendship::query()->where([['user_id', Auth::id()], ['friend_id', $userId]])->whereIn('friendship_status', [Friendship::FRIENDSHIP_STATUS_ACCEPTED, Friendship::FRIENDSHIP_STATUS_NEW])->exists();
    }

    /**
     * Френды
     *
     * @return array
     */
    public function getFriends(): array
    {
        $userId = $this->id;

        return Friendship::query()->where(function (Builder $query) use ($userId) {
            $query->where('user_id', $userId)
                ->orWhere('friend_id', $userId);
        })->where('friendship_status', Friendship::FRIENDSHIP_STATUS_ACCEPTED)
            ->get()
            ->all();
    }

    /**
     * Количество друзей пользователя
     *
     * @return int
     */
    public function getFriendsCount(): int
    {
        return Friendship::query()->where([['user_id', $this->id], ['friendship_status', Friendship::FRIENDSHIP_STATUS_ACCEPTED]])->count()
            + Friendship::query()->where([['friend_id', $this->id], ['friendship_status', Friendship::FRIENDSHIP_STATUS_ACCEPTED]])->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function challenges()
    {
        return $this->hasMany(Challenge::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invitationsSend()
    {
        return $this->hasMany(ChallengeInvitation::class, 'invitor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invitationsTakingPart()
    {
        return $this->hasMany(ChallengeInvitation::class, 'participant_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    /**
     * Непрочитанные уведомления
     *
     * @return array
     */
    public function unreadNotifications(): array
    {
        return $this->notifications()->where('status', Notification::STATUS_NEW)->get()->all();
    }

    /**
     * Непрочитанные уведомления(количество)
     *
     * @return int
     */
    public function unreadNotificationsCount(): int
    {
        return $this->notifications()->where('status', Notification::STATUS_NEW)->get()->count();
    }

    /**
     * Выполняет ли юзер челлендж
     *
     * @param int $challengeId
     * @return bool
     */
    public function isAlreadyDoingChallenge(int $challengeId): bool
    {
        return ChallengeInvitation::query()
            ->where([['participant_id', $this->id], ['challenge_id', $challengeId]])
            ->whereIn('status', [ChallengeInvitation::STATUS_SENT, ChallengeInvitation::STATUS_ACCEPTED, ChallengeInvitation::STATUS_TAKE_PART_MANUALLY])
            ->exists();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function confirmations()
    {
        return $this->hasMany(ChallengeConfirmation::class);
    }
}
