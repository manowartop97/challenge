<?php

namespace App\Models\DB\User;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property string $surname
 * @property string $name
 * @property int $gender
 * @property int $country_id
 * @property int $city_id
 * @property string $facebook_link
 * @property string $twitter_link
 * @property string $instagram_link
 * @property string $device_tokens
 * @property string $vk_link
 * @property string $status_string
 * @property string $birthday
 * @property string $phone
 * @property string $avatar
 * @property string $profile_background
 * @property string $about
 * @property string $last_visit
 * @property string $hobbies_and_interests
 * @property string $favourite_music
 * @property string $favourite_games
 * @property string $favourite_movies
 * @property boolean $is_online
 * @property string $chat_id
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class UserProfile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profile';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = [
        'surname',
        'name',
        'gender',
        'country_id',
        'city_id',
        'facebook_link',
        'twitter_link',
        'instagram_link',
        'vk_link',
        'status_string',
        'hobbies_and_interests',
        'favourite_music',
        'favourite_games',
        'favourite_movies',
        'birthday',
        'phone',
        'avatar',
        'profile_background',
        'about',
        'last_visit',
        'device_tokens',
        'is_online',
        'chat_id',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
