<?php

namespace App\Models\DB\Posts;

use App\Models\DB\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $likable_id
 * @property string $likable_type
 * @property boolean $is_liked
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Like extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'likable_id', 'likable_type', 'is_liked', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function likable()
    {
        return $this->morphTo();
    }
}
