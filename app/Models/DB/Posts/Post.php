<?php

namespace App\Models\DB\Posts;

use App\Models\DB\User\User;
use Auth;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $author_id
 * @property int $user_id
 * @property string $text
 * @property string $attachments_json
 * @property int $likes_count
 * @property int $comments_count
 * @property boolean $is_featured
 * @property string $created_at
 * @property string $updated_at
 * @property User $author
 * @property User $user
 * @property Like[] $likes
 * @property Comment[] $comments
 */
class Post extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['author_id', 'user_id', 'text', 'attachments_json', 'likes_count', 'comments_count', 'is_featured', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'author_id' => 'integer',
        'user_id'   => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function likes()
    {
        return $this->morphMany(Like::class, 'likable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получаем собранный массив декодированных картинок
     *
     * @return array
     */
    public function getAttachments(): array
    {
        $results = [];

        if (!$this->attachments_json) {
            return $results;
        }

        $userFolder = explode('@', $this->user->email)[0];

        foreach (json_decode($this->attachments_json, true) as $additional_media) {

            $results[] = [
                'type'      => $additional_media['type'],
                'name'      => '/storage/posts/' . $userFolder . '/' . $additional_media['name'],
                'thumbnail' => isset($additional_media['thumbnail']) ? '/storage/posts/' . $userFolder . '/' . $additional_media['thumbnail'] : null
            ];
        }

        return $results;
    }

    /**
     * Лайкал ли я этот пост
     *
     * @return bool
     */
    public function isLiked(): bool
    {
        return Like::query()->where([['likable_id', $this->id], ['likable_type', Post::class], ['user_id', Auth::id()], ['is_liked', true]])->exists();
    }

    /**
     * Является ли текущий пользователь - овнером поста
     *
     * @return bool
     */
    public function isCurrentUserOwner(): bool
    {
        return $this->author_id === Auth::id();
    }

    /**
     * Является ли пользователь получаетем поста
     *
     * @return bool
     */
    public function isCurrentUserReceiver(): bool
    {
        return $this->user_id === Auth::id();
    }
}
