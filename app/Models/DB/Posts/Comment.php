<?php

namespace App\Models\DB\Posts;

use App\Models\DB\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $commentable_id
 * @property string $commentable_type
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Comment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'commentable_id', 'commentable_type', 'text', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }
}
