<?php

namespace App\Models\DB\Notification;

use App\Models\DB\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $category
 * @property string $view_template
 * @property string $arguments
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Notification extends Model
{
    /**
     * @const
     */
    const STATUS_NEW = 1;
    const STATUS_READ = 2;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'category', 'view_template', 'arguments', 'status', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'user_id'  => 'integer',
        'category' => 'string',
        'status'   => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
