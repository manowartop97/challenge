<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.11.2018
 * Time: 14:48
 */

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Реквест апдейта стутса
 *
 * Class UpdateStatusRequest
 * @package App\Http\Requests\User
 */
class UpdateStatusRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'status_string' => 'nullable|string|max:100'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}