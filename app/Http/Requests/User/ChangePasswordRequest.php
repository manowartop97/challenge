<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.11.2018
 * Time: 09:38
 */

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Реквест смены пароля
 *
 * Class ChangePasswordRequest
 * @package App\Http\Requests\User
 */
class ChangePasswordRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'old_password'              => 'required|string|min:6',
            'new_password'              => 'required|string|min:6|confirmed|different:old_password',
            'new_password_confirmation' => 'required|string|min:6',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'new_password.different' => trans('messages.validation.different_passwords')
        ];
    }
}