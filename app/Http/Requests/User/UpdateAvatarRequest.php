<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.11.2018
 * Time: 17:07
 */

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Реквест апдейта аватара
 *
 * Class UpdateAvatarRequest
 * @package App\Http\Requests\User
 */
class UpdateAvatarRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => 'required|file|mimes:jpeg,png'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}