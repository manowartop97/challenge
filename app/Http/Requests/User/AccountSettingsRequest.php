<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 16:39
 */

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Настройки профиля пользователя
 *
 * Class AccountSettingsRequest
 * @package App\Http\Requests\User
 */
class AccountSettingsRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'who_can_view_posts'                => 'required|integer|in:1,2,3',
            'who_can_request_friends'           => 'required|integer|in:1,3',
            'who_can_invite_to_challenge'       => 'required|integer|in:1,2,3',
            'who_can_invite_to_event'           => 'required|integer|in:1,2,3',
            'is_telegram_notifications'         => 'boolean',
            'is_email_notifications'            => 'boolean',
            'is_friends_birthday_notifications' => 'boolean',
            'is_notification_sound_on'          => 'boolean',
            'is_chat_sound_on'                  => 'boolean',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}