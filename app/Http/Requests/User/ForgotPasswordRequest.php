<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.11.2018
 * Time: 10:55
 */

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Восстановление пароля со страницы авторизации
 *
 * Class ForgotPasswordRequest
 * @package App\Http\Requests\User
 */
class ForgotPasswordRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|exists:users'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }

}