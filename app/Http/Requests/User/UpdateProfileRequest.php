<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 17:22
 */

namespace App\Http\Requests\User;

use App;
use App\Http\Requests\Request;

/**
 * Личная инфа профиля
 *
 * Class UpdateProfileRequest
 * @package App\Http\Requests\User
 */
class UpdateProfileRequest extends Request
{
    /**
     * TODO валидация телефонов и ссылок по регуляркам
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id'        => 'required|integer|exists:users,id',
            'surname'        => 'required|string|max:50',
            'name'           => 'required|string|max:50',
            'gender'         => 'required|integer|in:1,2',
            'email'          => 'required|email|unique:users,id,'.$this->get('user_id'),
            'birthday'       => 'required|date|date_format:d.m.Y',
            //            'phone'          => 'nullable|phone:'.strtoupper(App::getLocale()),
            //            'phone'          => 'nullable|phone:UA',
            'phone'          => 'nullable|string|max:12',
            'country_id'     => 'nullable', // TODO
            'city_id'        => 'nullable', //TODO
            'facebook_link'  => 'nullable|string|max:100',
            'twitter_link'   => 'nullable|string|max:100',
            'instagram_link' => 'nullable|string|max:100',
            'vk_link'        => 'nullable|string|max:100',
            'about'          => 'nullable|string|max:500',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}