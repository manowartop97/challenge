<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.11.2018
 * Time: 13:58
 */

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Реквест апдейта хобби и интересов
 *
 * Class UpdateInterestsRequest
 * @package App\Http\Requests\User
 */
class UpdateInterestsRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'hobbies_and_interests' => 'nullable|string|max:1000',
            'favourite_music'       => 'nullable|string|max:1000',
            'favourite_games'       => 'nullable|string|max:1000',
            'favourite_movies'      => 'nullable|string|max:1000',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}