<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 22.11.2018
 * Time: 18:32
 */

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

/**
 * Реквест для апдейта фото-хедера
 *
 * Class UpdateHeaderRequest
 * @package App\Http\Requests\User
 */
class UpdateHeaderRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => 'required|file|mimes:jpeg,png,gif'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}