<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 27.11.2018
 * Time: 12:30
 */

namespace App\Http\Requests\Post;

use App\Http\Requests\Request;

/**
 * Создание поста на стене
 *
 * Class CreatePostRequest
 * @package App\Http\Requests\Post
 */
class UpdatePostRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'text'               => 'nullable|string|required_if:is_files,0',
            'post_attachments.*' => 'nullable|file|mimes:jpeg,bmp,png,avi,mp4,mpeg4,ogg,qt',
            'post_attachments'   => 'nullable',
            'user_id'            => 'required|integer|exists:users,id',
            'author_id'          => 'required|integer|exists:users,id',
            'is_files'           => 'boolean'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'text.required_without'             => trans('messages.post_cant_be_empty'),
            'post_attachments.required_without' => '',
        ];
    }
}