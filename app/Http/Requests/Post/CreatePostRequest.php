<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 27.11.2018
 * Time: 12:30
 */

namespace App\Http\Requests\Post;

use App\Http\Requests\Request;

/**
 * Создание поста на стене
 *
 * Class CreatePostRequest
 * @package App\Http\Requests\Post
 */
class CreatePostRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'text'               => 'nullable|string|required_without:post_attachments',
            'post_attachments.*' => 'nullable|file|mimes:jpeg,bmp,png,avi,mp4,mpeg4,ogg,qt',
            'post_attachments'   => 'nullable|required_without:text',
            'user_id'            => 'required|integer|exists:users,id',
            'author_id'          => 'required|integer|exists:users,id',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'text.required_without'             => trans('messages.post_cant_be_empty'),
            'post_attachments.required_without' => '',
        ];
    }
}