<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 07.12.2018
 * Time: 12:13
 */

namespace App\Http\Requests\Challenge;

use App\Http\Requests\Request;

/**
 * Class ConfirmationSearchRequest
 * @package App\Http\Requests\Challenge
 */
class ConfirmationSearchRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'type'   => 'nullable|integer|in:1,2',
            'status' => 'nullable|integer|in:0,1,2,3,4,5'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}