<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 30.11.2018
 * Time: 12:57
 */

namespace App\Http\Requests\Challenge;

use App\Http\Requests\Request;

/**
 * Приглашения в челлендж
 *
 * Class InvitationsRequest
 * @package App\Http\Requests\Challenge
 */
class InvitationsRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'ids'             => 'required|array',
            'ids.*'           => 'required|integer|exists:users,id',
            'invitation_text' => 'nullable|string|min:15|max:255',
            'challenge_id'    => 'required|integer|exists:challenges,id',
            'invitor_id'      => 'required|integer|exists:users,id'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'ids.required' => trans('messages.invitation_users_not_chosen')
        ];
    }
}