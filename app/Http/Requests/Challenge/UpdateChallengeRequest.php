<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 28.11.2018
 * Time: 16:12
 */

namespace App\Http\Requests\Challenge;

use App\Http\Requests\Request;

/**
 * Обновляем челлендж
 *
 * Class CreateChallengeRequest
 * @package App\Http\Requests\Challenge
 */
class UpdateChallengeRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id'             => 'required|integer|exists:users,id',
            'is_title_image'      => 'required|boolean',
            'is_additional_media' => 'required|boolean',
            'title'               => 'required|string|min:10|max:255',
            'description'         => 'required|string|min:50|max:1000',
            'tags'                => 'required|string',
            'deadline'            => 'nullable|date|date_format:d.m.Y|after_or_equal:today',
            'is_group_challenge'  => 'required|boolean',
            'title_image'         => 'nullable|required_if:is_title_image,0|image',
            'additional_media.*'  => 'nullable|file|mimes:jpeg,bmp,png,avi,mp4,mpeg4,mov,ogg,qt',
            'additional_media'    => 'nullable',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}