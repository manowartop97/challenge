<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 06.12.2018
 * Time: 16:43
 */

namespace App\Http\Requests\Challenge;

use App\Http\Requests\Request;

/**
 * Реквест подтверждения челленджа
 *
 * Class ConfirmationRequest
 * @package App\Http\Requests\Challenge
 */
class ConfirmationRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'challenge_id'       => 'required|integer|exists:challenges,id',
            'comment'            => 'nullable|required_without:additional_media|string|max:255',
            'additional_media.*' => 'nullable|file|mimes:jpeg,bmp,png,avi,mp4,mpeg4,mov,ogg,qt',
            'additional_media'   => 'nullable|required_without:comment',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [
            'comment.required_without'          => trans('messages.confirmation_add_media_of_comment'),
            'additional_media.required_without' => trans('messages.confirmation_add_media_of_comment'),
        ];
    }
}