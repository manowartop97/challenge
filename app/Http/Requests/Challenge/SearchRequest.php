<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 01.12.2018
 * Time: 14:09
 */

namespace App\Http\Requests\Challenge;

use App\Http\Requests\Request;

/**
 * Фильтрация челленджей
 *
 * Class SearchRequest
 * @package App\Http\Requests\Challenge
 */
class SearchRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'status' => 'nullable|integer',
            'title'  => 'nullable|string',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}