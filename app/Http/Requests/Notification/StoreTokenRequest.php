<?php

namespace App\Http\Requests\Notification;

use App\Http\Requests\Request;

/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 03.12.2018
 * Time: 19:22
 */
class StoreTokenRequest extends Request
{
    public function rules(): array
    {
        return [
            'token' => 'required|string',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}