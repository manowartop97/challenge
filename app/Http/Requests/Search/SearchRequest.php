<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 24.11.2018
 * Time: 12:26
 */

namespace App\Http\Requests\Search;

use App\Http\Requests\Request;

/**
 * Реквест на поиск по пользователям
 *
 * Class SearchRequest
 * @package App\Http\Requests\Search
 */
class SearchRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'q'      => 'nullable|string',
            'gender' => 'nullable|integer|in:1,2',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}