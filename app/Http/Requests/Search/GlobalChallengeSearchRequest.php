<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 06.12.2018
 * Time: 14:22
 */

namespace App\Http\Requests\Search;

use App\Http\Requests\Request;

/**
 * Глобальный реквест поиска челленджей
 *
 * Class GlobalChallengeSearchRequest
 * @package App\Http\Requests\Search
 */
class GlobalChallengeSearchRequest extends Request
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'q'   => 'nullable|string',
            'tag' => 'nullable|string'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }
}