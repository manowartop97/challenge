<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 22.11.2018
 * Time: 21:07
 */

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Http\Requests\Challenge\SearchRequest;
use App\Models\DB\User\User;
use App\Repositories\Challenge\ChallengeRepository;
use App\Repositories\Friendship\FriendshipRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\User\ProfileRepository;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Request;

/**
 * Страница других пользователей
 *
 * Class PageController
 * @package App\Http\Controllers\Page
 */
class PageController extends Controller
{
    /**
     * Показать профиль пользователя
     *
     * @param int $id
     * @param FriendshipRepository $friendshipRepository
     * @param PostRepository $postRepository
     * @return View|RedirectResponse
     */
    public function showPage(int $id, FriendshipRepository $friendshipRepository, PostRepository $postRepository)
    {
        if ($id === Auth::id()) {
            return redirect(route('profile.index'));
        }

        /** @var User $user */
        $user = User::query()->findOrFail($id);

        if (Auth::user()->cant('visit', $user)) {
            return $this->redirectToPrivateProfileView($user, trans('messages.user_profile'));
        }

        return view('frontend.page.index', [
            'user'         => $user,
            'link'         => ProfileRepository::LINK_FEED,
            'friends'      => $friendshipRepository->getFriends($id, FriendshipRepository::FRIENDS_FOR_FRONT_DISPLAY, FriendshipRepository::FRIENDS_FOR_FRONT_DISPLAY),
            'friendsCount' => $friendshipRepository->getTotalFriends($id),
            'posts'        => $postRepository->getPosts($id)
        ]);
    }

    /**
     * Показать профиль пользователя
     *
     * @param int $id
     * @return View|RedirectResponse
     */
    public function showPageInfo(int $id)
    {
        if ($id === Auth::id()) {
            return redirect(route('profile.about'));
        }

        /** @var User $user */
        $user = User::query()->findOrFail($id);

        if (Auth::user()->cant('visit', $user)) {
            return $this->redirectToPrivateProfileView($user, trans('messages.personal_info'));
        }

        return view('frontend.page.about', [
            'user' => $user,
            'link' => ProfileRepository::LINK_ABOUT,
        ]);
    }

    /**
     * Список друзей юзера
     *
     * @param int $id
     * @param FriendshipRepository $friendshipRepository
     * @return View|RedirectResponse
     */
    public function friends(int $id, FriendshipRepository $friendshipRepository)
    {
        if ($id === Auth::id()) {
            return redirect(route('profile.friends'));
        }

        /** @var User $user */
        $user = User::query()->findOrFail($id);

        if (Auth::user()->cant('visit', $user)) {
            return $this->redirectToPrivateProfileView($user, trans('messages.friends'));
        }

        return view('frontend.page.friends', [
            'profile' => $user->profile,
            'link'    => ProfileRepository::LINK_FRIENDS,
            'friends' => $friendshipRepository->getFriends($id, 12),
        ]);
    }

    /**
     * Просмотр челленджей пользователя
     *
     * @param User $user
     * @param SearchRequest $request
     * @param ChallengeRepository $challengeRepository
     * @param ProfileRepository $profileRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|View
     * @throws \Throwable
     */
    public function challenges(User $user, SearchRequest $request, ChallengeRepository $challengeRepository, ProfileRepository $profileRepository)
    {
        if ($user->id === Auth::id()) {
            return redirect(route('profile.friends'));
        }

        // Значит фильтрация
        if (Request::ajax()) {
            return response()->json([
                'data' => [
                    'view' => view('frontend.profile.challenge.filtered-challenges', [
                        'challenges' => $challengeRepository->getChallenges($user->id, $request->validated()),
                    ])->render()
                ]
            ]);
        }

        return view('frontend.profile.challenge.index', [
            'profile'     => $user->profile,
            'link'        => ProfileRepository::LINK_CHALLENGES,
            'challenges'  => $challengeRepository->getChallenges($user->id),
            'friendships' => $user->getFriends(),
        ]);
    }

    /**
     * Редирект на вьюху приватного профиля
     *
     * @param User $target
     * @param string $title
     * @return View
     */
    protected function redirectToPrivateProfileView(User $target, string $title): View
    {
        return view('frontend.page.private-page', ['user' => $target, 'title' => $title]);
    }
}