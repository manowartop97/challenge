<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 15:50
 */

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Requests\Notification\StoreTokenRequest;
use App\Models\DB\Notification\Notification;
use App\Repositories\Notification\NotificationRepository;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

/**
 * Уведомления
 *
 * Class NotificationController
 * @package App\Http\Controllers\Notification
 */
class NotificationController extends Controller
{
    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    /**
     * NotificationController constructor.
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Список уведомлений в настройках профиля
     *
     * @return View
     */
    public function index(): View
    {
        return view('frontend.profile.notifications.all');
    }

    /**
     * Есть ли у юзера токен
     *
     * @return JsonResponse
     */
    public function checkToken(): JsonResponse
    {
        return response()->json(null, $this->notificationRepository->checkToken() ? 200 : 500);
    }

    /**
     * Сохраняем токен юзера
     *
     * @param StoreTokenRequest $request
     */
    public function storeToken(StoreTokenRequest $request): void
    {
        $this->notificationRepository->storeToken($request->get('token'));
    }

    /**
     * Очищаем уведомления(по одному)
     *
     * @param Notification $notification
     * @return JsonResponse
     */
    public function clear(Notification $notification): JsonResponse
    {
        if (Auth::user()->cant('clear', $notification)) {
            return response()->json(['data' => ['message' => trans('messages.you_cant_remove_notification')]], 500);
        }

        if ($this->notificationRepository->clearNotification($notification)) {
            return response()->json(['data' => ['count' => Auth::user()->unreadNotificationsCount()]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Чистим все уведомления в баре по категории
     *
     * @param string $category
     * @return JsonResponse
     */
    public function clearAll(string $category): JsonResponse
    {
        if ($this->notificationRepository->clearAllNotifications($category)) {
            return response()->json(['data' => ['message' => trans('messages.no_notifications')]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }
}