<?php

namespace App\Http\Controllers\Search;

use App\Http\Requests\Search\GlobalChallengeSearchRequest;
use App\Http\Requests\Search\SearchRequest;
use App\Repositories\Challenge\ChallengeRepository;
use App\Repositories\User\ProfileRepository;
use App\Services\Search\SearchService;
use Auth;
use Illuminate\View\View;

/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 24.11.2018
 * Time: 12:21
 */
class SearchController
{
    /**
     * @var SearchService
     */
    private $searchService;

    /**
     * SearchController constructor.
     * @param SearchService $searchService
     */
    public function __construct(SearchService $searchService)
    {
        $this->searchService = $searchService;
    }

    /**
     * Поиск по юзерам
     *
     * @param SearchRequest $request
     * @return View
     */
    public function people(SearchRequest $request): View
    {
        return view('frontend.search.search-results', [
            'users' => $this->searchService->search($request->validated()),
            'query' => $request->get('q'),
        ]);
    }

    /**
     * Просмотр челленджей пользователя
     *
     * @param GlobalChallengeSearchRequest $request
     * @param ChallengeRepository $challengeRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|View
     * @throws \Throwable
     */
    public function challenges(GlobalChallengeSearchRequest $request, ChallengeRepository $challengeRepository)
    {
        return view('frontend.search.challenges-search', [
            'profile'     => Auth::user()->profile,
            'link'        => ProfileRepository::LINK_CHALLENGES,
            'challenges'  => $challengeRepository->getAllChallenges($request->validated()),
            'friendships' => Auth::user()->getFriends(),
            'query'       => $request->get('q'),
        ]);
    }
}