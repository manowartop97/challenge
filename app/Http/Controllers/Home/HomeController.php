<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 19.11.2018
 * Time: 09:55
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;

/**
 * Class HomeController
 * @package App\Http\Controllers\Home
 */
class HomeController extends Controller
{
    /**
     * Пока редиректим на авторизацию
     *
     * @return RedirectResponse
     */
    public function index(): RedirectResponse
    {
        return redirect(route('auth'));
    }
}