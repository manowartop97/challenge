<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 23.11.2018
 * Time: 12:23
 */

namespace App\Http\Controllers\Friendship;

use App\Http\Controllers\Controller;
use App\Models\DB\Friendship\Friendship;
use App\Repositories\Friendship\FriendshipRepository;
use Auth;
use Illuminate\Http\JsonResponse;

/**
 * Контроллер с функционалом френдов
 *
 * Class FriendshipController
 * @package App\Http\Controllers\Friendship
 */
class FriendshipController extends Controller
{
    /**
     * @var FriendshipRepository
     */
    private $friendshipRepository;

    /**
     * FriendshipController constructor.
     * @param FriendshipRepository $friendshipRepository
     */
    public function __construct(FriendshipRepository $friendshipRepository)
    {
        $this->friendshipRepository = $friendshipRepository;
    }

    /**
     * Отправляем приглашение в друзья
     *
     * @param int $id
     * @return JsonResponse
     */
    public function inviteToFriends(int $id): JsonResponse
    {
        if (Auth::user()->cant('inviteToFriendship', [Friendship::class, $id])) {
            return response()->json(['data' => ['message' => trans('messages.cant_be_invited_to_friendship')]], 500);
        }

        if ($this->friendshipRepository->createFriendship($id)) {

            return response()->json([
                'data' => [
                    'message'        => trans('messages.friendship_created'),
                    'route'          => route('friendship.remove', ['id' => $id]),
                    'toggle_message' => trans('messages.remove_from_friends'),
                ],
            ], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Удаляем из друзей
     *
     * @param int $id
     * @return JsonResponse
     */
    public function removeFromFriends(int $id): JsonResponse
    {
        if ($this->friendshipRepository->removeFriend($id)) {
            return response()->json([
                'data' => [
                    'message'        => trans('messages.friendship_declined'),
                    'route'          => route('friendship.new', ['id' => $id]),
                    'toggle_message' => trans('messages.add_to_friends'),
                ],
            ], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Принять дружбу
     *
     * @param int $id
     * @return JsonResponse
     */
    public function acceptFriendship(int $id): JsonResponse
    {
        if ($this->friendshipRepository->acceptFriendship($id)) {
            return response()->json([
                'data' => [
                    'message' => trans('messages.friendship_accepted'),
                    'route'   => route('friendship.remove', ['id' => $id]),
                ],
            ], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Отклонить дружбу
     *
     * @param int $id
     * @return JsonResponse
     */
    public function rejectFriendship(int $id): JsonResponse
    {
        if ($this->friendshipRepository->rejectFriendship($id)) {
            return response()->json([
                'data' => [
                    'message' => trans('messages.friendship_rejected'),
                    'route'   => route('friendship.new', ['id' => $id]),
                ],
            ], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }
}