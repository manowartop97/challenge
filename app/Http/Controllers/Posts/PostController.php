<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 27.11.2018
 * Time: 11:41
 */

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Models\DB\Posts\Post;
use App\Repositories\Post\PostRepository;
use Auth;
use Illuminate\Http\JsonResponse;

/**
 * Посты на стене и все что с ними связанно
 *
 * Class PostController
 * @package App\Http\Controllers\Posts
 */
class PostController extends Controller
{
    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * Создаем пост на странице пользователя
     *
     * @param CreatePostRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function createPost(CreatePostRequest $request): JsonResponse
    {
        if (!is_null($post = $this->postRepository->createPost($request->validated()))) {
            return response()->json([
                'data' => [
                    'post' => view('frontend.post.post', ['post' => $post])->render(),
                ],
            ], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Обновляем пост
     *
     * @param int $id
     * @param UpdatePostRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function editPost(int $id, UpdatePostRequest $request): JsonResponse
    {
        /** @var Post $post */
        $post = Post::query()->findOrFail($id);
        if (Auth::user()->cant('edit', $post)) {
            return response()->json(['data' => ['message' => trans('messages.you_cant_update_post')]], 500);
        }

        if (!is_null($post = $this->postRepository->editPost($post, $request->validated()))) {
            return response()->json(['data' => ['view' => view('frontend.post.new-post', ['post' => $post])->render()]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Форма для редактирования поста
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Throwable
     */
    public function editForm(int $id): JsonResponse
    {
        return response()->json(['data' => ['view' => view('frontend.profile.blocks.post-edit-form', ['post' => Post::query()->findOrFail($id)])->render()]], 200);
    }

    /**
     * Удаление поста
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function deletePost(int $id): JsonResponse
    {
        /** @var Post $post */
        $post = Post::query()->findOrFail($id);
        if (Auth::user()->cant('delete', $post)) {
            return response()->json(['data' => ['message' => trans('messages.you_cant_delete_post')]], 500);
        }

        if ($this->postRepository->deletePost($post)) {
            return response()->json(null, 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Ставим лайк на пост
     *
     * @param int $id
     * @return JsonResponse
     */
    public function like(int $id): JsonResponse
    {
        if ($this->postRepository->like($id)) {
            return response()->json(['data' => ['route' => route('post.unlike', ['id' => $id]), 'count' => $this->postRepository->getActualLikesCount($id)]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Снимаем лайк
     *
     * @param int $id
     * @return JsonResponse
     */
    public function unlike(int $id): JsonResponse
    {
        if ($this->postRepository->unlike($id)) {
            return response()->json(['data' => ['route' => route('post.like', ['id' => $id]), 'count' => $this->postRepository->getActualLikesCount($id)]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Закрепить пост на стене
     *
     * @param int $id
     * @return JsonResponse
     */
    public function makeFeatured(int $id): JsonResponse
    {
        /** @var Post $post */
        $post = Post::query()->findOrFail($id);
        if (Auth::user()->cant('makeFeatured', $post)) {
            return response()->json(['data' => ['message' => trans('messages.you_cant_make_featured')]], 500);
        }

        if ($this->postRepository->makeFeatured($post)) {
            return response()->json(null, 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }
}