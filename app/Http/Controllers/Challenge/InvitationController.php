<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 30.11.2018
 * Time: 13:30
 */

namespace App\Http\Controllers\Challenge;

use App\Http\Controllers\Controller;
use App\Http\Requests\Challenge\InvitationsRequest;
use App\Models\DB\Challenge\Challenge;
use App\Repositories\Challenge\InvitationRepository;
use Auth;
use DB;
use Illuminate\Http\JsonResponse;

/**
 * Менеджим работу инвайтов в челлендж
 *
 * Class InvitationController
 * @package App\Http\Controllers\Challenge
 */
class InvitationController extends Controller
{
    /**
     * @var InvitationRepository
     */
    private $invitationRepository;

    /**
     * InvitationController constructor.
     * @param InvitationRepository $invitationRepository
     */
    public function __construct(InvitationRepository $invitationRepository)
    {
        $this->invitationRepository = $invitationRepository;
    }

    /**
     * Рассылаем пришлашения в челлендж
     *
     * @param InvitationsRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function sendInvitations(InvitationsRequest $request): JsonResponse
    {
        DB::beginTransaction();
        if ($this->invitationRepository->sendInvitations($request->validated())) {
            DB::commit();
            /** @var Challenge $challenge */
            $challenge = Challenge::query()->findOrFail($request->get('challenge_id'));

            return response()->json([
                'data' => [
                    'message'              => trans('messages.invitations_were_sent'),
                    'participantsCount'    => $this->invitationRepository->getParticipantsCount($challenge),
                    'invitationsCount'     => $this->invitationRepository->invitationsCount($challenge),
                    'yourInvitationsCount' => $this->invitationRepository->yourInvitationsCount($challenge),
                ],
            ], 200);
        }

        DB::rollBack();

        return response()->json(['data' => ['message' => $this->invitationRepository->getError() ?: trans('messages.smth_wrong')]], 500);
    }

    /**
     * Поучавствовать в челлендже по собственному желанию
     *
     * @param Challenge $challenge
     * @return JsonResponse
     * @internal param int $id
     */
    public function takePart(Challenge $challenge): JsonResponse
    {
        if (Auth::user()->cant('takePart', $challenge)) {
            return response()->json(['data' => ['message' => trans('messages.challenge_you_cant_take-part')]], 500);
        }

        if ($this->invitationRepository->takePart($challenge)) {
            return response()->json(['data' => [
                'message'           => trans('messages.challenge_you_are_taking_part'),
                'route'             => route('invitation.cancel-participation', ['id' => $challenge->id]),
                'btnText'           => trans('messages.challenge_cancel_take-part'),
                'participantsCount' => $this->invitationRepository->getParticipantsCount($challenge),
                'invitationsCount'  => $this->invitationRepository->invitationsCount($challenge),
            ]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Отменяем вступление
     *
     * @param Challenge $challenge
     * @return JsonResponse
     */
    public function cancelParticipation(Challenge $challenge): JsonResponse
    {
        if (Auth::user()->cant('cancelParticipation', $challenge)) {
            return response()->json(['data' => ['message' => trans('messages.challenge_you_cant_cancel_take-part')]], 500);
        }

        if ($this->invitationRepository->cancelParticipation($challenge)) {
            return response()->json(['data' => [
                'message'           => trans('messages.challenge_you_cancelled_taking_part'),
                'route'             => route('invitation.take-part', ['id' => $challenge->id]),
                'btnText'           => trans('messages.challenge_take-part'),
                'participantsCount' => $this->invitationRepository->getParticipantsCount($challenge),
                'invitationsCount'  => $this->invitationRepository->invitationsCount($challenge),
            ]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }
}