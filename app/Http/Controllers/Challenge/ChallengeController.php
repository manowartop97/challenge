<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 28.11.2018
 * Time: 11:30
 */

namespace App\Http\Controllers\Challenge;

use App\Http\Controllers\Controller;
use App\Http\Requests\Challenge\ConfirmationRequest;
use App\Http\Requests\Challenge\CreateChallengeRequest;
use App\Http\Requests\Challenge\SearchRequest;
use App\Http\Requests\Challenge\UpdateChallengeRequest;
use App\Models\DB\Challenge\Challenge;
use App\Repositories\Challenge\ChallengeRepository;
use App\Repositories\Challenge\InvitationRepository;
use App\Repositories\User\ProfileRepository;
use Auth;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Request;
use Spatie\Tags\Tag;
use Throwable;

/**
 * Менеджим челленджи
 *
 * Class ChallengeController
 * @package App\Http\Controllers\Challenge
 */
class ChallengeController extends Controller
{
    /**
     * @var ChallengeRepository
     */
    private $challengeRepository;

    /**
     * ChallengeController constructor.
     * @param ChallengeRepository $challengeRepository
     */
    public function __construct(ChallengeRepository $challengeRepository)
    {
        $this->challengeRepository = $challengeRepository;
    }

    /**
     * Список челленджей
     *
     * @param SearchRequest $request
     * @param ProfileRepository $profileRepository
     * @return View|JsonResponse
     * @throws Throwable
     */
    public function index(SearchRequest $request, ProfileRepository $profileRepository)
    {
        // Значит фильтрация
        if (Request::ajax()) {
            return response()->json([
                'data' => [
                    'view' => view('frontend.profile.challenge.filtered-challenges', [
                        'challenges' => $this->challengeRepository->getChallenges(Auth::id(), $request->validated()),
                    ])->render()
                ]
            ]);
        }

        return view('frontend.profile.challenge.index', [
            'profile'     => $profileRepository->getProfile(),
            'link'        => ProfileRepository::LINK_CHALLENGES,
            'challenges'  => $this->challengeRepository->getChallenges(),
            'friendships' => Auth::user()->getFriends(),
        ]);
    }

    /**
     * Форма создания челленджа
     *
     * @return View
     */
    public function newForm(): View
    {
        return view('frontend.profile.challenge.create-form', [
            'tags' => Tag::all(),
        ]);
    }

    /**
     * Форма апдейта челленджа
     *
     * @param int $id
     * @return View
     */
    public function editForm(int $id): View
    {
        return view('frontend.profile.challenge.edit-form', [
            'challenge' => $this->challengeRepository->getChallenge($id),
            'tags'      => Tag::all(),
        ]);
    }

    /**
     * Сохраняем челлендж
     *
     * @param CreateChallengeRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function store(CreateChallengeRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            if ($this->challengeRepository->createChallenge($request->validated())) {
                DB::commit();

                return response()->json(['data' => ['route' => route('challenge.all')]], 200);
            }
            DB::rollBack();
            $this->challengeRepository->removeTmpFiles();

            return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
        } catch (Throwable $exception) {
            DB::rollBack();
            $this->challengeRepository->removeTmpFiles();

            return response()->json(['data' => ['message' => $exception->getMessage()]], 500);
        }
    }

    /**
     * Обновление челленджа
     *
     * @param Challenge $challenge
     * @param UpdateChallengeRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(Challenge $challenge, UpdateChallengeRequest $request): JsonResponse
    {
        if (Auth::user()->cant('edit', $challenge)) {
            return response()->json(['data' => ['message' => trans('messages.challenge_cant_edit')]], 500);
        }

        try {
            DB::beginTransaction();
            if ($this->challengeRepository->updateChallenge($challenge, $request->validated())) {
                DB::commit();

                return response()->json(['data' => ['route' => route('challenge.all')]], 200);
            }
            DB::rollBack();
            $this->challengeRepository->removeTmpFiles();

            return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
        } catch (Throwable $exception) {
            DB::rollBack();
            $this->challengeRepository->removeTmpFiles();

            return response()->json(['data' => ['message' => $exception->getMessage()]], 500);
        }
    }

    /**
     * Удаление челленджа
     *
     * @param Challenge $challenge
     * @return JsonResponse
     */
    public function delete(Challenge $challenge): JsonResponse
    {
        if (Auth::user()->cant('delete', $challenge)) {
            return response()->json(['data' => ['message' => trans('messages.challenge_cant_delete')]], 500);
        }

        if ($this->challengeRepository->delete($challenge)) {
            return response()->json(['data' => ['message' => trans('messages.challenge_deleted'), 'route' => route('challenge.all')]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Просмотр челленджа
     *
     * @param int $id
     * @param InvitationRepository $invitationRepository
     * @return View
     */
    public function show(int $id, InvitationRepository $invitationRepository): View
    {
        $challenge = $this->challengeRepository->getChallenge($id);

        return view('frontend.profile.challenge.detail', [
            'challenge'            => $challenge,
            'participantsCount'    => $invitationRepository->getParticipantsCount($challenge),
            'invitationsCount'     => $invitationRepository->invitationsCount($challenge),
            'yourInvitationsCount' => $invitationRepository->yourInvitationsCount($challenge),
        ]);
    }

    /**
     * Возобновить удаленный челлендж
     *
     * @param Challenge $challenge
     * @return JsonResponse
     */
    public function reNewChallenge(Challenge $challenge): JsonResponse
    {
        if (Auth::user()->cant('reNew', $challenge)) {
            return response()->json(['data' => ['message' => trans('messages.challenge_cant_re-new')]], 500);
        }

        if ($this->challengeRepository->reNewChallenge($challenge)) {
            return response()->json(['data' => ['message' => trans('messages.challenge_re-newed')]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Форма подтверждения челленджа
     *
     * @param Challenge $challenge
     * @return View|RedirectResponse
     */
    public function confirmationForm(Challenge $challenge)
    {
        if (Auth::user()->cant('confirm', $challenge)) {
            flash(trans('messages.challenge_cant_confirm'))->error();
            return redirect()->back();
        }

        return view('frontend.profile.challenge.confirmation-form', [
            'challenge' => $challenge
        ]);
    }

    /**
     * Создать подтверждение выполнения челленджа
     *
     * @param ConfirmationRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function confirmChallenge(ConfirmationRequest $request): JsonResponse
    {
        try {
            DB::beginTransaction();
            if ($this->challengeRepository->createConfirmation($request->validated())) {
                DB::commit();
                return response()->json(['data' => ['message' => trans('messages.confirmation_created'), 'route' => route('profile.challenges')]], 200);
            }
            DB::rollBack();
            $this->challengeRepository->removeTmpFiles();

            return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
        } catch (Throwable $exception) {
            DB::rollBack();
            $this->challengeRepository->removeTmpFiles();

            return response()->json(['data' => ['message' => $exception->getMessage()]], 500);
        }
    }
}