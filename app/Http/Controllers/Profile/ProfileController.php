<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 19.11.2018
 * Time: 13:23
 */

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Http\Requests\Challenge\ConfirmationSearchRequest;
use App\Http\Requests\Challenge\SearchRequest;
use App\Models\DB\Challenge\ChallengeConfirmation;
use App\Repositories\Challenge\ChallengeRepository;
use App\Repositories\Challenge\ConfirmationsRepository;
use App\Repositories\Friendship\FriendshipRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\User\ProfileRepository;
use Auth;
use Illuminate\View\View;
use Request;

/**
 * Профиль юзера
 *
 * Class ProfileController
 * @package App\Http\Controllers\Profile
 */
class ProfileController extends Controller
{
    /**
     * @var ProfileRepository
     */
    private $profileRepository;

    /**
     * ProfileController constructor.
     * @param ProfileRepository $profileRepository
     */
    public function __construct(ProfileRepository $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    /**
     * Главная профиля
     *
     * @param FriendshipRepository $friendshipRepository
     * @param PostRepository $postRepository
     * @return View
     */
    public function index(FriendshipRepository $friendshipRepository, PostRepository $postRepository): View
    {

        $profile = $this->profileRepository->getProfile();

        return view('frontend.profile.index', [
            'profile'      => $profile,
            'link'         => ProfileRepository::LINK_FEED,
            'friends'      => $friendshipRepository->getFriends(Auth::id(), FriendshipRepository::FRIENDS_FOR_FRONT_DISPLAY, FriendshipRepository::FRIENDS_FOR_FRONT_DISPLAY),
            'friendsCount' => $friendshipRepository->getTotalFriends(Auth::id()),
            'posts'        => $postRepository->getPosts(),
        ]);
    }

    /**
     * Линчая информация профиля
     *
     * @return View
     */
    public function about(): View
    {
        return view('frontend.profile.about', [
            'profile' => $this->profileRepository->getProfile(),
            'link'    => ProfileRepository::LINK_ABOUT,
        ]);
    }

    /**
     * Страница друзей
     *
     * @param FriendshipRepository $friendshipRepository
     * @return View
     */
    public function friends(FriendshipRepository $friendshipRepository): View
    {
        return view('frontend.profile.friends', [
            'profile' => $this->profileRepository->getProfile(),
            'link'    => ProfileRepository::LINK_FRIENDS,
            'friends' => $friendshipRepository->getFriends(Auth::id(), 12),
        ]);
    }

    /**
     * Просмотр челленджей со страницы профайла
     *
     * @param SearchRequest $request
     * @param ChallengeRepository $challengeRepository
     * @param ProfileRepository $profileRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|View
     * @throws \Throwable
     */
    public function challenges(SearchRequest $request, ChallengeRepository $challengeRepository, ProfileRepository $profileRepository)
    {
        // Значит фильтрация
        if (Request::ajax()) {
            return response()->json([
                'data' => [
                    'view' => view('frontend.profile.challenge.filtered-challenges', [
                        'challenges' => $challengeRepository->getChallenges(Auth::id(), $request->validated()),
                    ])->render()
                ]
            ]);
        }

        return view('frontend.profile.challenge.index', [
            'profile'     => $profileRepository->getProfile(),
            'link'        => ProfileRepository::LINK_CHALLENGES,
            'challenges'  => $challengeRepository->getChallenges(),
            'friendships' => Auth::user()->getFriends(),
        ]);
    }

    /**
     * Страница подтверждения челленджей
     *
     * @param ConfirmationSearchRequest $request
     * @param ConfirmationsRepository $confirmationsRepository
     * @return View
     */
    public function challengeConfirmations(ConfirmationSearchRequest $request, ConfirmationsRepository $confirmationsRepository): View
    {
        return view('frontend.profile.challenge.confirmations', [
            'profile'       => Auth::user()->profile,
            'confirmations' => $confirmationsRepository->getConfirmations($request->validated()),
            'type'          => $confirmationsRepository->getType()
        ]);
    }

    /**
     * Детальное подтверждение челленджа
     *
     * @param ChallengeConfirmation $challengeConfirmation
     * @return View
     * @throws \Exception
     */
    public function confirmationDetail(ChallengeConfirmation $challengeConfirmation): View
    {
        if (Auth::user()->cant('view', $challengeConfirmation)) {
            throw new \Exception(trans('messages.challenge_cant_view_confirmation'));
        }

        return view('frontend.profile.challenge.confirmation-detail', [
            'confirmation' => $challengeConfirmation
        ]);
    }
}