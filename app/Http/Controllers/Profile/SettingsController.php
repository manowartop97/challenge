<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 12:29
 */

namespace App\Http\Controllers\Profile;

use App\Exceptions\User\UnableToUpdateProfileException;
use App\Exceptions\User\UnableToUpdateSettingsExceptions;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\AccountSettingsRequest;
use App\Http\Requests\User\ChangePasswordRequest;
use App\Http\Requests\User\ForgotPasswordRequest;
use App\Http\Requests\User\UpdateAvatarRequest;
use App\Http\Requests\User\UpdateHeaderRequest;
use App\Http\Requests\User\UpdateInterestsRequest;
use App\Http\Requests\User\UpdateProfileRequest;
use App\Http\Requests\User\UpdateStatusRequest;
use App\Repositories\User\Account\AccountSettingsRepository;
use App\Repositories\User\ProfileRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

/**
 * Настройки профиля
 *
 * Class SettingsController
 * @package App\Http\Controllers\Profile
 */
class SettingsController extends Controller
{
    /**
     * Форма редактирования персональной инфы
     *
     * @param ProfileRepository $profileRepository
     * @return View
     */
    public function personalInfoForm(ProfileRepository $profileRepository): View
    {
        return view('frontend.profile.settings.personal-info', [
            'profile' => $profileRepository->getProfile(),
        ]);
    }

    /**
     * Обновляем инфу о профиле
     *
     * @param UpdateProfileRequest $request
     * @param ProfileRepository $profileRepository
     * @return RedirectResponse
     * @throws UnableToUpdateProfileException
     */
    public function updateProfileInfo(UpdateProfileRequest $request, ProfileRepository $profileRepository): RedirectResponse
    {
        if ($profileRepository->updateProfileInfo($request->validated())) {
            flash(trans('messages.profile_info_updated'))->success();

            return redirect()->back();
        }

        throw new UnableToUpdateProfileException('Unable to update profile info');
    }

    /**
     * Форма редактирования настроек аккаунта
     *
     * @param AccountSettingsRepository $settingsRepository
     * @return View
     */
    public function accountSettingsForm(AccountSettingsRepository $settingsRepository): View
    {
        return view('frontend.profile.settings.account-settings', [
            'settings' => $settingsRepository->getSettings(),
        ]);
    }

    /**
     * Сохраняем новые настройки
     *
     * @param AccountSettingsRequest $request
     * @param AccountSettingsRepository $settingsRepository
     * @return RedirectResponse
     * @throws UnableToUpdateSettingsExceptions
     */
    public function updateAccountSettings(AccountSettingsRequest $request, AccountSettingsRepository $settingsRepository): RedirectResponse
    {
        if ($settingsRepository->updateSettings($request->validated())) {
            flash(trans('messages.settings_updated'))->success();

            return redirect()->back();
        }

        throw new UnableToUpdateSettingsExceptions('Unable to update settings');
    }

    /**
     * Форма смены пароля
     *
     * @return View
     */
    public function passwordChangeForm(): View
    {
        return view('frontend.profile.settings.change-password');
    }

    /**
     * Изменение пароля
     *
     * @param ChangePasswordRequest $request
     * @param UserRepository $userRepository
     * @return RedirectResponse
     */
    public function changePassword(ChangePasswordRequest $request, UserRepository $userRepository): RedirectResponse
    {
        if ($userRepository->changePassword($request->validated())) {
            flash(trans('messages.password_changed'))->success();

            return redirect()->back();
        }

        flash(is_null($userRepository->getErrorMessage()) ? trans('messages.smth_wrong') : $userRepository->getErrorMessage())->error();

        return redirect()->back()->withInput();
    }

    /**
     * Генерация и отправка нового пароля
     *
     * @param UserRepository $userRepository
     * @return RedirectResponse
     */
    public function resetPassword(UserRepository $userRepository): RedirectResponse
    {
        if (!$userRepository->resetPassword()) {
            flash(trans('messages.smth_wrong'))->error();

            return redirect()->back();
        }

        flash(trans('messages.new_password_was_sent'))->success();

        return redirect()->back();
    }

    /**
     * Восстановление забытого пароля
     *
     * @param ForgotPasswordRequest $request
     * @param UserRepository $userRepository
     * @return JsonResponse
     * @throws \App\Exceptions\User\UserNotFoundException
     */
    public function forgotPassword(ForgotPasswordRequest $request, UserRepository $userRepository): JsonResponse
    {
        if ($userRepository->recoverPassword($request->get('email'))) {
            return response()->json(['data' => ['message' => trans('messages.new_password_was_sent')]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Форма редактирования хобби и интересов
     *
     * @param ProfileRepository $profileRepository
     * @return View
     */
    public function hobbiesForm(ProfileRepository $profileRepository): View
    {
        return view('frontend.profile.settings.hobbies-form', [
            'profile' => $profileRepository->getProfile(),
        ]);
    }

    /**
     * Обновляем хобби и интересы
     *
     * @param UpdateInterestsRequest $request
     * @param ProfileRepository $profileRepository
     * @return RedirectResponse
     * @throws UnableToUpdateProfileException
     */
    public function updateHobbies(UpdateInterestsRequest $request, ProfileRepository $profileRepository): RedirectResponse
    {
        if ($profileRepository->updateProfileInfo($request->validated())) {
            flash(trans('messages.profile_info_updated'))->success();

            return redirect()->back();
        }

        throw new UnableToUpdateProfileException('Unable to update profile info');
    }

    /**
     * Форма образования (не нужна скорее всего)
     *
     * @return View
     */
    public function educationAndEmploymentForm(): View
    {
        return view('frontend.profile.settings.education-and-employment');
    }

    /**
     * Обновляем статус профиля
     *
     * @param UpdateStatusRequest $request
     * @param ProfileRepository $profileRepository
     * @return JsonResponse
     */
    public function updateStatus(UpdateStatusRequest $request, ProfileRepository $profileRepository)
    {
        if ($profileRepository->updateProfileInfo($request->validated())) {
            return response()->json(['data' => ['message' => trans('messages.status_updated')]], 200);
        }

        return response()->json(['data' => ['message' => trans('messages.smth_wrong')]], 500);
    }

    /**
     * Обновляем аватар
     *
     * @param UpdateAvatarRequest $request
     * @param ProfileRepository $profileRepository
     * @return RedirectResponse
     */
    public function updateAvatar(UpdateAvatarRequest $request, ProfileRepository $profileRepository): RedirectResponse
    {
        if ($profileRepository->updateAvatar($request->validated()['file'])) {
            return redirect()->back();
        }

        return redirect()->back();
    }

    /**
     * Обновляем аватар
     *
     * @param UpdateHeaderRequest $request
     * @param ProfileRepository $profileRepository
     * @return RedirectResponse
     */
    public function updateHeader(UpdateHeaderRequest $request, ProfileRepository $profileRepository): RedirectResponse
    {
        if ($profileRepository->updateAvatar($request->validated()['file'], false)) {
            return redirect()->back();
        }

        return redirect()->back();
    }
}