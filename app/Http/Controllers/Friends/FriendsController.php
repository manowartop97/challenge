<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 16:01
 */

namespace App\Http\Controllers\Friends;

use App\Http\Controllers\Controller;
use Illuminate\View\View;

/**
 * Друзья и все что с этим связано
 *
 * Class FriendsController
 * @package App\Http\Controllers\Friends
 */
class FriendsController extends Controller
{
    /**
     * Запросы в друзья(входящие)
     *
     * @return View
     */
    public function requests(): View
    {
        return view('frontend.profile.friends.requests', [
            'friends'  => \Auth::user()->friendsIncomeRequests(),
            'isIncome' => true,
        ]);
    }

    /**
     * Исходящие запросы в друзья
     *
     * @return View
     */
    public function outcomeRequests(): View
    {
        return view('frontend.profile.friends.requests', [
            'friends' => \Auth::user()->friendsOutcomeRequests(),
            'isIncome' => false
        ]);
    }
}