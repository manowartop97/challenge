<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 31.10.2018
 * Time: 11:51
 */

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepository;
use Illuminate\Http\RedirectResponse;

/**
 * Class ConfirmationController
 * @package App\Http\Controllers\Auth
 */
class ConfirmationController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ConfirmationController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $token
     * @return RedirectResponse
     * @throws Exception
     */
    public function confirm(string $token): RedirectResponse
    {
        if (!$this->userRepository->confirmUser($token)) {
            throw new Exception('Не удалось подтвердить аккаунт');
        }

        return redirect(route('profile.index'));
    }
}