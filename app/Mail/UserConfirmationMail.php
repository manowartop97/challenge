<?php

namespace App\Mail;

use App\Models\DB\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


/**
 * Class UserConfirmationMail
 * @package App\Mail
 */
class UserConfirmationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Подтверждение регистрации')->from('insta.hurricane@gmail.com', 'Challenge');
        return $this->view('mail.user.registration', [
            'user' => $this->user
        ]);
    }
}
