<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.11.2018
 * Time: 10:31
 */

namespace App\Mail\User;

use App\Models\DB\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Письмо по ресет пассворду
 *
 * Class ResetPasswordMail
 * @package App\Mail\User
 */
class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    private $user;
    /**
     * @var string
     */
    private $plainPassword;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $plainPassword
     */
    public function __construct(User $user, string $plainPassword)
    {
        $this->user = $user;
        $this->plainPassword = $plainPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject(trans('mail.change_password_subject'))->from('insta.hurricane@gmail.com', 'Challenge');
        return $this->view('mail.user.reset-password', [
            'user'          => $this->user,
            'plainPassword' => $this->plainPassword
        ]);
    }
}