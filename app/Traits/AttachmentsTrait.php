<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 30.11.2018
 * Time: 15:34
 */

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Storage;

/**
 * Обработка аттачей(вынес ибо повторяется)
 *
 * Class AttachmentsTrait
 * @package App\Traits
 */
trait AttachmentsTrait
{
    /**
     * @const
     */
    protected $imageExtensions = [
        'jpeg', 'bmp', 'png', 'jpg', 'gif',
    ];

    /**
     * Картинка ли файл
     *
     * @param UploadedFile $file
     * @return bool
     */
    private function isFileImage(UploadedFile $file): bool
    {
        return in_array($file->extension(), $this->imageExtensions);
    }

    /**
     * Создать директорию, если ее нету
     *
     * @param string $path
     * @rreturn void
     */
    private function createFolderIfNotExists(string $path): void
    {
        if (!Storage::exists($path)) {
            Storage::makeDirectory($path);
        }
    }
}