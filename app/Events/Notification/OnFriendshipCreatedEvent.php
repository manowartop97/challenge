<?php

namespace App\Events\Notification;

use App\Models\DB\User\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

/**
 * Срабатывает когда отправляет запрос о дружбе
 *
 * Class OnFriendshipCreatedEvent
 * @package App\Events\Notification
 */
class OnFriendshipCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var User
     */
    public $friend;

    /**
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     * @param User $user
     * @param User $friend - друг, который получит уведомление
     */
    public function __construct(User $user, User $friend)
    {
        $this->friend = $friend;
        $this->user = $user;
    }

}
