<?php

namespace App\Observers\Notification;

use App\Models\DB\Notification\Notification;
use App\Services\Notification\NotificationService;

/**
 * Обзервер уведомлений
 *
 * Class NotificationObserver
 * @package App\Observers\Notification
 */
class NotificationObserver
{
    /**
     * Тут отправляем уведомление
     *
     * @param Notification $notification
     * @return void
     * @throws \Throwable
     */
    public function created(Notification $notification)
    {
        (new NotificationService())->sendNotification($notification);
    }
}
