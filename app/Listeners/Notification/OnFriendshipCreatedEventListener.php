<?php

namespace App\Listeners\Notification;

use App\Events\Notification\OnFriendshipCreatedEvent;
use App\Models\DB\Notification\Notification;
use App\Repositories\Notification\NotificationRepository;
use App\Services\Notification\NotificationService;

/**
 * Class OnFriendshipCreatedEventListener
 * @package App\Listeners\Notification
 */
class OnFriendshipCreatedEventListener
{
    /**
     * Handle the event.
     *
     * @param OnFriendshipCreatedEvent $event
     * @return void
     */
    public function handle(OnFriendshipCreatedEvent $event)
    {
        (new NotificationRepository())->createNotification([
            'user_id'       => $event->friend->id,
            'category'      => NotificationService::CATEGORY_FRIENDSHIP,
            'view_template' => 'frontend.notification.friendship.new-friendship',
            'status'        => Notification::STATUS_NEW,
            'arguments'     => json_encode([
                'user_avatar' => 'storage/' . $event->user->getAvatar(),
                'user_id'     => $event->user->id,
                'fullName'    => $event->user->getFullName()
            ]),
        ]);
    }
}
