<?php

namespace App\Listeners\Notification;


use App\Events\Notification\OnFriendshipAcceptedEvent;
use App\Models\DB\Notification\Notification;
use App\Repositories\Notification\NotificationRepository;
use App\Services\Notification\NotificationService;

/**
 * Когда дружба принята
 *
 * Class OnFriendshipAcceptedEventListener
 * @package App\Listeners\Notification
 */
class OnFriendshipAcceptedEventListener
{

    /**
     * Handle the event.
     *
     * @param OnFriendshipAcceptedEvent $event
     * @return void
     */
    public function handle(OnFriendshipAcceptedEvent $event)
    {
        (new NotificationRepository())->createNotification([
            'user_id'       => $event->friend->id,
            'category'      => NotificationService::CATEGORY_FRIENDSHIP,
            'view_template' => 'frontend.notification.friendship.friendship-approved',
            'status'        => Notification::STATUS_NEW,
            'arguments'     => json_encode([
                'user_avatar' => 'storage/' . $event->user->getAvatar(),
                'user_id'     => $event->user->id,
                'fullName'    => $event->user->getFullName()
            ]),
        ]);
    }
}
