<?php

namespace App\Listeners\User;

use App\Events\User\OnUserRegisterEvent;
use App\Mail\UserConfirmationMail;
use Illuminate\Support\Facades\Mail;

/**
 * Class OnUserRegisterEventListener
 * @package App\Listeners\User
 */
class OnUserRegisterEventListener
{
    /**
     * Handle the event.
     *
     * @param OnUserRegisterEvent $event
     * @return void
     */
    public function handle(OnUserRegisterEvent $event)
    {
        Mail::to($event->user->email)->send(new UserConfirmationMail($event->user));
    }
}
