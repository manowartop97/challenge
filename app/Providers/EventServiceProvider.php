<?php

namespace App\Providers;

use App\Events\Notification\OnFriendshipAcceptedEvent;
use App\Events\Notification\OnFriendshipCreatedEvent;
use App\Events\OnTaskStatusChangeEvent;
use App\Events\User\OnUserRegisterEvent;
use App\Listeners\CommandTermitateListener;
use App\Listeners\Notification\OnFriendshipAcceptedEventListener;
use App\Listeners\Notification\OnFriendshipCreatedEventListener;
use App\Listeners\OnTaskStatusChangeListener;
use App\Listeners\User\OnUserRegisterEventListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        OnUserRegisterEvent::class       => [
            OnUserRegisterEventListener::class,
        ],
        OnFriendshipCreatedEvent::class  => [
            OnFriendshipCreatedEventListener::class,
        ],
        OnFriendshipAcceptedEvent::class => [
            OnFriendshipAcceptedEventListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
