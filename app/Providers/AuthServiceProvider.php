<?php

namespace App\Providers;

use App\Models\DB\Challenge\Challenge;
use App\Models\DB\Challenge\ChallengeConfirmation;
use App\Models\DB\Friendship\Friendship;
use App\Models\DB\Notification\Notification;
use App\Models\DB\Posts\Post;
use App\Models\DB\User\User;
use App\Policies\Challenge\ChallengePolicy;
use App\Policies\Challenge\ConfirmationChallengePolicy;
use App\Policies\FriendshipPolicy;
use App\Policies\Notification\NotificationPolicy;
use App\Policies\Page\PagePolicy;
use App\Policies\Post\PostPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Friendship::class            => FriendshipPolicy::class,
        Post::class                  => PostPolicy::class,
        User::class                  => PagePolicy::class,
        Challenge::class             => ChallengePolicy::class,
        Notification::class          => NotificationPolicy::class,
        ChallengeConfirmation::class => ConfirmationChallengePolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
