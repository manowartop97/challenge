<?php

namespace App\Components\Cache\Models\DB;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $cache_name
 * @property int $cache_duration
 * @property int $data_source
 * @property boolean $is_enabled
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Cache extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['cache_name', 'cache_duration', 'data_source', 'is_enabled'];

}
