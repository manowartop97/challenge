<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 11.07.18
 * Time: 9:08
 */

namespace App\Components\Cache\Traits;

use App\Components\Cache\Classes\AbstractCache;
use App\Components\Cache\Classes\DataCache;
use App\Components\Cache\Classes\ModelCache;
use App\Components\Cache\Classes\Wrappers\AbstractWrapper;
use App\Components\Cache\Interfaces\CacheInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Подключаемый трейт к моделям и классам,
 * данные которых будут кэшироваться
 *
 * Trait ModelCacheTrait
 * @package App\Components\Cache\Traits
 */
trait CacheTrait
{
    /**
     * @var CacheInterface
     */
    private static $cacheComponent;

    /**
     * Тип кэширования по умолчанию
     *
     * @var int
     */
    protected static $durationType = AbstractCache::DURATION_IN_MINUTES;

    /**
     * Время кэширования по умолчанию
     *
     * @var int
     */
    protected static $durationAmount = 100;

    /**
     * Кэшированные данные
     *
     * @var AbstractWrapper
     */
    private static $cachedData;

    /**
     * Замена конструктора
     *
     * ModelCacheTrait constructor.
     * @throws \App\Components\Cache\Exceptions\InvalidCacheTypeException
     */
    private static function init(): void
    {
        self::$cacheComponent = self::getCacheInstance();
        self::$cacheComponent->setCacheName(self::getCacheName());
        self::$cacheComponent->setCacheDuration(self::$durationType, self::$durationAmount);
    }

    /**
     * Получить закэшированную дату
     *
     * @return AbstractWrapper
     * @throws \App\Components\Cache\Exceptions\InvalidCacheTypeException
     */
    public static function getCachedData(): AbstractWrapper
    {
        self::init();

        self::$cachedData = self::$cacheComponent->getCachedData();

        if (is_null(self::$cachedData)) {
            self::$cacheComponent->setToCache(self::getDataForCaching());
        }

        return self::$cacheComponent->getCachedData();
    }

    /**
     * Сброс кэша
     *
     * @return void
     */
    public static function refreshCache(): void
    {
        self::init();
        self::$cacheComponent->setToCache(self::getDataForCaching());
    }

    /**
     * Получение инстанса кэша
     *
     * @return AbstractCache
     */
    private static function getCacheInstance(): AbstractCache
    {
        switch (true) {
            case new self() instanceof Model:
                return ModelCache::getInstance();
                break;
            default:
                return DataCache::getInstance();
                break;
        }
    }

    /**
     * Получить данные, которые будем кэшировать
     *
     * @return mixed
     */
    abstract public static function getDataForCaching();

    /**
     * Задается имя кэша
     *
     * @return string
     */
    abstract public static function getCacheName(): string;
}