<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 13.07.18
 * Time: 15:09
 */

namespace App\Components\Cache\Interfaces;

use App\Components\Cache\Classes\Wrappers\AbstractWrapper;

/**
 * Интерфейс кэширователей
 *
 * Interface CacheInterface
 * @package App\Components\Cache\Interfaces
 */
interface CacheInterface
{
    /**
     * Установка имени кэша
     *
     * @param string $cacheName
     */
    public function setCacheName(string $cacheName): void;

    /**
     * Получить имя кэша
     *
     * @return string
     */
    public function getCacheName(): string;

    /**
     * Получить тип длительность кэша
     *
     * @return int
     */
    public function getDurationType(): int;

    /**
     * Получить длительность кэширования
     *
     * @return int
     */
    public function getCacheDuration(): int;

    /**
     * Установка времени длительности кэша
     *
     * @param int $durationType
     * @param int $durationAmount
     * @throws InvalidCacheTypeException
     */
    public function setCacheDuration(int $durationType, int $durationAmount): void;

    /**
     * Добавление данных в кэш
     *
     * @param $data
     * @return void
     */
    public function setToCache($data): void;

    /**
     * Получить закэшированную информацию
     *
     * @return AbstractWrapper|null
     */
    public function getCachedData();
}