<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 13.07.18
 * Time: 14:47
 */

namespace App\Components\Cache\Classes\Wrappers;

/**
 * Обертка кэшированных данных (только моделей)
 *
 * Class CachedDataObject
 * @package App\Components\Cache\Classes\Wrappers
 */
class ModelCacheObject extends AbstractWrapper
{
    /**
     * Получение данных из обертки
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}
