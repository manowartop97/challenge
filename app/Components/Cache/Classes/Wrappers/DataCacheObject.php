<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 13.07.18
 * Time: 15:08
 */

namespace App\Components\Cache\Classes\Wrappers;

/**
 * Обертка для кэшированных данных (не моделей)
 *
 * Class DataCacheObject
 * @package App\Components\Cache\Classes\Wrappers
 */
class DataCacheObject extends AbstractWrapper
{
    /**
     * Получение данных из обертки
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
