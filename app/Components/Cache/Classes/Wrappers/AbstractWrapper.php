<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 13.07.18
 * Time: 14:57
 */

namespace App\Components\Cache\Classes\Wrappers;

use App\Components\Cache\Classes\AbstractCache;
use Illuminate\Database\Eloquent\Collection;

/**
 * Базовая обертка для кэшированных данных
 *
 * Class AbstractWrapper
 * @package App\Components\Cache\Classes\Wrappers
 */
abstract class AbstractWrapper
{
    /**
     * Имя кэша
     *
     * @var string
     */
    private $cacheName;

    /**
     * Время жизни кэша
     *
     * @var int
     */
    private $cacheDuration;

    /**
     * Тип длительности кэша
     *
     * @var int
     */
    private $durationType;

    /**
     * Кэшированная информация
     *
     * @var mixed
     */
    protected $data;

    /**
     * Обернуть данные
     *
     * @param AbstractCache $abstractCache
     * @param $data
     * @return void
     */
    public function wrapData(AbstractCache $abstractCache, $data): void
    {
        $this->cacheName = $abstractCache->getCacheName();
        $this->cacheDuration = $abstractCache->getCacheDuration();
        $this->durationType = $abstractCache->getDurationType();
        $this->data = $data;
    }

    /**
     * Получение данных из обертки
     *
     * @return Collection|array
     */
    abstract public function getData();

    /**
     * @return string
     */
    public function getCacheName(): string
    {
        return $this->cacheName;
    }

    /**
     * @return int
     */
    public function getCacheDuration(): int
    {
        return $this->cacheDuration;
    }

    /**
     * @return int
     */
    public function getDurationType()
    {
        return $this->durationType;
    }
}
