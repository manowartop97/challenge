<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 11.07.18
 * Time: 9:01
 */

namespace App\Components\Cache\Classes;

use App\Components\Cache\Classes\Wrappers\AbstractWrapper;
use App\Components\Cache\Classes\Wrappers\ModelCacheObject;

/**
 * Кэширование моделей
 *
 * Class ModelCache
 * @package App\Components\Cache\Classes
 */
class ModelCache extends AbstractCache
{
    /**
     * Получить нужную обертку
     *
     * @return AbstractWrapper
     */
    protected function getWrapper(): AbstractWrapper
    {
        return new ModelCacheObject();
    }
}