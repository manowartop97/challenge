<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 11.07.18
 * Time: 8:52
 */

namespace App\Components\Cache\Classes;

use App\Components\Cache\Classes\Wrappers\AbstractWrapper;
use App\Components\Cache\Exceptions\InvalidCacheTypeException;
use App\Components\Cache\Interfaces\CacheInterface;
use App\Components\Cache\Models\DB\Cache;

/**
 * Базовый класс - кеширователь данных
 *
 * Class AbstractCache
 * @package App\Components\Cache\Classes
 */
abstract class AbstractCache implements CacheInterface
{
    /**
     * Длительность в минутах
     *
     * @const
     */
    const DURATION_IN_MINUTES = 1;

    /**
     * Длительность в часах
     *
     * @const
     */
    const DURATION_IN_HOURS = 2;

    /**
     * Уникальный ключ кэша
     *
     * @var string
     */
    private $cacheName;

    /**
     * Длительность кэширования данных
     *
     * @var int
     */
    private $cacheDuration;

    /**
     * Тип времени кэширования
     *
     * @var int
     */
    private $durationType;

    /**
     * Имя таблицы которая отвечает за кэширование
     *
     * @var string
     */
    private $cacheTableName = Cache::class;

    /**
     * @var AbstractCache
     */
    private static $instance;

    /**
     * Запрещаем создание объекта через new
     *
     * AbstractCache constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param int $durationType
     */
    public function setDurationType(int $durationType)
    {
        $this->durationType = $durationType;
    }

    /**
     * Получить имя таблицы управляющей кешированием
     *
     * @return string
     */
    public function getCacheTableName(): string
    {
        return $this->cacheTableName;
    }

    /**
     * Предотвращаем клонирование
     */
    protected function __clone()
    {
    }

    /**
     * Реализация singleton
     *
     * @return AbstractCache
     */
    public static function getInstance(): AbstractCache
    {
        $class = get_called_class();

        if (is_null(self::$instance)) {
            self::$instance = new $class();
        }

        return self::$instance;
    }

    /**
     * Установка имени кэша
     *
     * @param string $cacheName
     */
    public function setCacheName(string $cacheName): void
    {
        $this->cacheName = $cacheName;
    }

    /**
     * Получить имя кэша
     *
     * @return string
     */
    public function getCacheName(): string
    {
        return $this->cacheName;
    }

    /**
     * Получить тип длительность кэша
     *
     * @return int
     */
    public function getDurationType(): int
    {
        return $this->durationType;
    }

    /**
     * Получить длительность кэширования
     *
     * @return int
     */
    public function getCacheDuration(): int
    {
        return $this->cacheDuration;
    }

    /**
     * Добавление данных в кэш
     *
     * @param $data
     * @return void
     */
    public function setToCache($data): void
    {
        $wrapper = $this->getWrapper();
        $wrapper->wrapData($this, $data);

        \Cache::put($this->cacheName, $wrapper, $this->cacheDuration);
    }

    /**
     * Получить закэшированную информацию
     *
     * @return AbstractWrapper|null
     */
    public function getCachedData()
    {
        return \Cache::get($this->cacheName);
    }

    /**
     * Установка времени длительности кэша
     *
     * @param int $durationType
     * @param int $durationAmount
     * @throws InvalidCacheTypeException
     */
    public function setCacheDuration(int $durationType, int $durationAmount): void
    {
        $this->setDurationType($durationType);

        switch ($durationType) {
            case self::DURATION_IN_MINUTES:
                $this->setCacheDurationInMinutes($durationAmount);
                break;
            case self::DURATION_IN_HOURS:
                $this->setCacheDurationInHours($durationAmount);
                break;
            default:
                throw new InvalidCacheTypeException('Cache type is Invalid');
                break;
        }
    }

    /**
     * Получить нужную обертку
     *
     * @return AbstractWrapper
     */
    abstract protected function getWrapper(): AbstractWrapper;

    /**
     * Установить длительность кэширования в минутах
     *
     * @param int $durationMinutes - длительность кэширования в минутах
     */
    private function setCacheDurationInMinutes(int $durationMinutes)
    {
        $this->cacheDuration = $durationMinutes;
    }

    /**
     * Установить длительность кэширования в часах
     *
     * @param int $durationHours - длительность кэширования в часах
     */
    private function setCacheDurationInHours(int $durationHours)
    {
        $this->cacheDuration = $durationHours * 60;
    }
}