<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 11.07.18
 * Time: 9:03
 */

namespace App\Components\Cache\Classes;

use App\Components\Cache\Classes\Wrappers\AbstractWrapper;
use App\Components\Cache\Classes\Wrappers\DataCacheObject;

/**
 * Кэширователь данных
 *
 * Class DataCache
 * @package App\Components\Cache\Classes
 */
class DataCache extends AbstractCache
{
    /**
     * Получить нужную обертку
     *
     * @return AbstractWrapper
     */
    protected function getWrapper(): AbstractWrapper
    {
       return new DataCacheObject();
    }
}