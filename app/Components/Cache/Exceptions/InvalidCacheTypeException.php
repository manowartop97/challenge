<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 12.07.2018
 * Time: 21:43
 */

namespace App\Components\Cache\Exceptions;

use Exception;
use Throwable;

/**
 * Class InvalidCacheTypeException
 * @package App\Components\Cache\Exceptions
 */
class InvalidCacheTypeException extends Exception
{
    /**
     * InvalidCacheTypeException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}