<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 19.11.2018
 * Time: 12:18
 */

namespace App\Components\Translator\Helpers;

use App;
use App\Components\Translator\Exception\WrongDataFormatException;

/**
 * Берет данные из массива относительно локали юзера
 *
 * Class ArrayTranslator
 * @package App\Components\Translator\Helpers
 */
class ArrayTranslator
{
    /**
     * Получает массив вида
     *
     * id => ['ua' => 'title', 'ru'=> 'title', 'en'=> 'title']
     *
     * и отдает
     * id => title
     *
     * @param array $array
     * @return array
     * @throws WrongDataFormatException
     */
    public static function translate(array $array): array
    {
        $result = [];

        foreach ($array as $key => $data) {
            if (!isset($data[App::getLocale()])) {
                throw new WrongDataFormatException('Invalid array data:' . json_encode($array));
//                continue;
            }
            $result[$key] = $data[App::getLocale()];
        }

        return $result;
    }
}