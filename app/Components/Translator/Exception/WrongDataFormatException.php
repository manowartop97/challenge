<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 19.11.2018
 * Time: 12:21
 */

namespace App\Components\Translator\Exception;

use Exception;

/**
 * Class WrongDataFormatException
 * @package App\Components\Translator\Exception
 */
class WrongDataFormatException extends Exception
{

}