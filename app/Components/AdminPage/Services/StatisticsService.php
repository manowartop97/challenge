<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 19.10.2018
 * Time: 13:10
 */

namespace App\Components\AdminPage\Services;

use App\Components\Dashboard\Models\DB\UserAccount;
use App\Components\User\Models\User;

/**
 * Сервис по сбору статистика для админпадели суперадмина
 *
 * Class StatisticsService
 * @package App\Components\AdminPage\Services
 */
class StatisticsService
{
    /**
     * Сбор статистики для главной страницы
     *
     * @return array
     */
    public function getStatisticsForMainpage(): array
    {
        return [
            'clientsCount'  => User::query()->count(),
            'accountsCount' => UserAccount::query()->count()
        ];
    }
}