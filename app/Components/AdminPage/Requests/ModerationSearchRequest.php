<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 22.10.2018
 * Time: 13:32
 */

namespace App\Components\AdminPage\Requests;

use App\Http\Requests\Request;

/**
 * Class ModerationSearchRequest
 * @package App\Components\AdminPage\Requests
 */
class ModerationSearchRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'caption'        => 'string|nullable',
            'type'           => 'integer|nullable',
            'status'         => 'integer|nullable',
            'tag'            => 'string|nullable',
            'rating'         => 'integer|nullable',
            'username'       => 'string|nullable',
            'notInAccountId' => 'integer|nullable'
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }

    /**
     * Get current data_id form values for custom validation
     *
     * @return string
     */
    private function getDataIdValues()
    {
        return implode(' ', collect($this->data)->map(function ($data) {
            return $data['data_id'];
        })->all());
    }
}