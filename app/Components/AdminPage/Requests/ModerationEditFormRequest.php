<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 22.10.2018
 * Time: 13:32
 */

namespace App\Components\AdminPage\Requests;

use App\Http\Requests\Request;

/**
 * Class ModerationSearchRequest
 * @package App\Components\AdminPage\Requests
 */
class ModerationEditFormRequest extends Request
{

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'caption'  => 'string|required',
            'type'     => 'integer|required',
            'status'   => 'integer|required',
            'tag'      => 'string|required',
            'rating'   => 'integer|required',
        ];
    }

    /**
     * @return array
     */
    public function messages(): array
    {
        return [

        ];
    }

    /**
     * Get current data_id form values for custom validation
     *
     * @return string
     */
    private function getDataIdValues()
    {
        return implode(' ', collect($this->data)->map(function ($data) {
            return $data['data_id'];
        })->all());
    }
}