<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 11:24
 */

namespace App\Components\Logger\Repositories;

use App\Components\Logger\Models\DB\DbLog;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class LoggerRepository
 * @package App\Components\Logger\Repositories
 */
class LoggerRepository
{
    /**
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getFiltered(array $search = [], int $pageSize = 10): LengthAwarePaginator
    {
        $query = DbLog::query();

        return $query->orderBy('id', 'desc')->paginate($pageSize);
    }

    /**
     * @param int $id
     * @return bool|null
     * @throws \Exception
     */
    public function delete(int $id): ?bool
    {
        return DbLog::query()->findOrFail($id)->delete();
    }

    /**
     *
     */
    public function deleteAll()
    {
        DbLog::query()->truncate();
    }
}