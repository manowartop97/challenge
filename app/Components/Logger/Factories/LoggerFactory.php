<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 11:04
 */

namespace App\Components\Logger\Factories;
use App\Components\Logger\Classes\AbstractLogger;
use App\Components\Logger\Classes\DbLogger;
use App\Components\Logger\Classes\FileLogger;
use App\Components\Logger\Exceptions\InvalidLoggerType;

/**
 * Фабрика логгеров
 *
 * Class LoggerFactory
 * @package App\Components\Logger\Factories
 */
class LoggerFactory
{
    /**
     * LoggerFactory constructor.
     */
    private function __construct()
    {
    }

    /**
     *
     */
    private function __clone()
    {
    }

    /**
     * Фабричный метод получения нужный инстанс логгера
     *
     * @param int $loggerType
     * @return AbstractLogger
     * @throws InvalidLoggerType
     */
    public static function getLogger(int $loggerType): AbstractLogger
    {
        switch ($loggerType){
            case AbstractLogger::LOGGER_TYPE_DB:
                return new DbLogger();
                break;
            case AbstractLogger::LOGGER_TYPE_FILE:
                return new FileLogger();
                break;
            default:
                throw new InvalidLoggerType("Logger Type {$loggerType} is Invalid");
                break;
        }
    }
}