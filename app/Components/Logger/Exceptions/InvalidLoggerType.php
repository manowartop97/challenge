<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 11:06
 */

namespace App\Components\Logger\Exceptions;

use Exception;
use Throwable;

/**
 * Class InvalidLoggerType
 * @package App\Components\Logger\Exceptions
 */
class InvalidLoggerType extends Exception
{
    /**
     * InvalidLoggerType constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}