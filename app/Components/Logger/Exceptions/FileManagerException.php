<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 12:07
 */

namespace App\Components\Logger\Exceptions;

use Exception;
use Throwable;

/**
 * Class FileManagerException
 * @package App\Components\Logger\Exceptions
 */
class FileManagerException extends Exception
{
    /**
     * FileManagerException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}