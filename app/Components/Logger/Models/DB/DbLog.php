<?php

namespace App\Components\Logger\Models\DB;

use App\Components\Logger\Classes\AbstractLogger;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $log_name
 * @property int $log_category
 * @property string $message
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class DbLog extends Model
{
    /**
     * @const
     */
    const LOG_CATEGORY_LIST = [
        AbstractLogger::LOG_CATEGORY_ERROR          => 'Error',
        AbstractLogger::LOG_CATEGORY_SUCCESS        => 'Success',
        AbstractLogger::LOG_CATEGORY_INFO           => 'Info',
        AbstractLogger::LOG_CATEGORY_WARNING        => 'Warning',
        AbstractLogger::LOG_CATEGORY_CRITICAL_ERROR => 'Critical Errors'
    ];

    /**
     * @var array
     */
    protected $fillable = ['log_name', 'log_category', 'message', 'status', 'created_at', 'updated_at'];

}
