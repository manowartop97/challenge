<?php

namespace App\Components\Logger\Models\DB;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $log_name
 * @property string $file_path
 * @property int $log_category
 * @property string $message
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class FileLog extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['log_name', 'file_path', 'log_category', 'message', 'status', 'created_at', 'updated_at'];

}
