<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 11:51
 */

namespace App\Components\Logger\Managers;

use File;
use Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Базовый файловый менеджер
 *
 * Class AbstractFileManager
 * @package App\Components\Logger\Managers
 */
abstract class AbstractFileManager
{
    /**
     * Расширение файла
     *
     * @const
     */
    const FILE_EXTENSION = 'txt';

    /**
     * Мод чтения файла
     *
     * @const
     */
    const FILE_MODE_READ = 'r';

    /**
     * Мод записи в файл
     *
     * @const
     */
    const FILE_MODE_WRITE = 'w';

    /**
     * Мод дополнения файла
     *
     * @const
     */
    const FILE_MODE_APPEND = 'a';

    /**
     * Имя файла
     *
     * @var string
     */
    protected $fileName;

    /**
     * Путь к файлу
     *
     * @var string
     */
    protected $filePath;

    /**
     * Файл
     *
     * @var resource
     */
    private $file;

    /**
     * Устанавливаем путь к файлу, и создаем директорию, если ее еще нет
     *
     * @param string $fileName
     * @param string $filePath
     * @param string $extension
     * @return void
     */
    public function setFilePath(string $fileName, string $filePath, string $extension): void
    {
        $this->createFolderIfNotExists($filePath);
        $this->filePath = Storage::path($filePath.'/'.$fileName.'.'.$extension);
    }

    /**
     * Чтение и получение содержимого файла
     *
     * @return string
     */
    public function read(): string
    {
        return htmlentities(file_get_contents($this->filePath));
    }

    /**
     * Запись в файл
     *
     * @param mixed $data
     * @return void
     */
    public function write($data): void
    {
        $this->file = $this->openFile(self::FILE_MODE_WRITE);
        fwrite($this->file, $this->getPreparedMessage($data));
        $this->closeFile();
    }

    /**
     * Дописываем данные в файл
     *
     * @param mixed $data
     * @return void
     */
    public function append($data): void
    {
        $this->file = $this->openFile(self::FILE_MODE_APPEND);
        fwrite($this->file, PHP_EOL.$this->getPreparedMessage($data));
        $this->closeFile();
    }

    /**
     * Получить путь к файлу
     *
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * Создать папку если ее нет
     *
     * @param string $filePath
     */
    protected function createFolderIfNotExists(string $filePath): void
    {
        if (!File::exists(Storage::path($filePath))) {
            File::makeDirectory(Storage::path($filePath), 0775, true);
        }
    }

    /**
     * Открываем файл, если не получилось - кидаем эксепшн
     *
     * @param string $mode
     * @return resource
     */
    private function openFile(string $mode)
    {
        $file = fopen($this->filePath, $mode);

        if (is_bool($file)) {
            throw new FileException("Unable to open file '{$this->filePath}' in '{$mode}' mode");
        }

        return $file;
    }

    /**
     * Закрываем открытый файл
     *
     * @return void
     */
    private function closeFile(): void
    {
        if (!fclose($this->file)) {
            throw new FileException("Unable to close file {$this->fileName}");
        }
    }

    /**
     * Получаем подготовленное к записи в файл сообщение
     *
     * @param mixed $data
     * @return string
     */
    private function getPreparedMessage($data): string
    {
        return is_string($data) ? $data : json_encode($data);
    }
}