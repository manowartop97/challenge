<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 9:59
 */

namespace App\Components\Logger\Classes;

/**
 * Базовый класс для реализации логики логирования
 *
 * Class AbstractLogger
 * @package App\Components\Logger\Classes
 */
abstract class AbstractLogger
{
    /**
     * Логгер в таблицу БД
     *
     * @const
     */
    const LOGGER_TYPE_DB = 1;

    /**
     * Логгер в файл
     *
     * @const
     */
    const LOGGER_TYPE_FILE = 2;

    /**
     * Категории логов
     * - ошибка
     * - успех
     * - инфо
     * - предупреждение
     * - критическая ошибка
     *
     * @const
     */
    const LOG_CATEGORY_ERROR = 1;
    const LOG_CATEGORY_SUCCESS = 2;
    const LOG_CATEGORY_INFO = 3;
    const LOG_CATEGORY_WARNING = 4;
    const LOG_CATEGORY_CRITICAL_ERROR = 5;

    /**
     * Имя лога (в таблице или имя файла)
     *
     * @var string
     */
    private $logName;

    /**
     * Категория лога (ошибка/предупреждение/успех...)
     *
     * @var int
     */
    private $logCategory;

    /**
     * Сообщение лога
     *
     * @var string
     */
    protected $message;

    /**
     * Маппинг категорий и описания
     *
     * @var array
     */
    private $logCategoryMappingList = [
        self::LOG_CATEGORY_ERROR          => 'Error',
        self::LOG_CATEGORY_SUCCESS        => 'Success',
        self::LOG_CATEGORY_INFO           => 'Info',
        self::LOG_CATEGORY_WARNING        => 'Warning',
        self::LOG_CATEGORY_CRITICAL_ERROR => 'Critical Errors'
    ];

    /**
     * Получить название текущей категории лога
     *
     * @return string
     */
    public function getLogCategoryName(): string
    {
        return isset($this->logCategoryMappingList[$this->logCategory]) ?: 'Not Set';
    }

    /**
     * Метод логирования
     *
     * @param string $logName
     * @param mixed $messageData
     * @param int $logCategory
     * @return void
     */
    abstract public function writeToLog(string $logName, $messageData, int $logCategory): void;

    /**
     * @return int
     */
    public function getLogCategory(): int
    {
        return $this->logCategory;
    }

    /**
     * @return string
     */
    public function getLogName(): string
    {
        return $this->logName;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return is_string($this->message)
            ? $this->message
            : json_encode($this->message);
    }

    /**
     * Создание записи в БД
     *
     * @return bool
     */
    abstract protected function createLogRecordInDatabase(): bool;

    /**
     * Сеттер опций логирования
     *
     * @param string $logName
     * @param mixed $messageData
     * @param int $logCategory
     * @return void
     */
    protected function setAttributes(string $logName, $messageData, int $logCategory): void
    {
        $this->logName = $logName;
        $this->logCategory = $logCategory;
        $this->message = $messageData;
    }
}