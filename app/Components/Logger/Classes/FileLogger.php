<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 9:59
 */

namespace App\Components\Logger\Classes;

use App\Components\Logger\Managers\AbstractFileManager;
use App\Components\Logger\Models\DB\FileLog;

/**
 * Логгер, для записи сообщений в файл
 *
 * Class FileLogger
 * @package App\Components\Logger\Classes
 */
class FileLogger extends AbstractLogger
{
    const LOG_FILE_PATH = 'customLogs';

    /**
     * @var AbstractFileManager
     */
    private $fileManager;

    /**
     * Метод логирования
     *
     * @param string $logName
     * @param mixed $messageData
     * @param int $logCategory
     * @return void
     */
    public function writeToLog(string $logName, $messageData, int $logCategory): void
    {
        $this->setAttributes($logName, $messageData, $logCategory);

        $this->fileManager->append($messageData);

//        $this->checkIfLogAlreadyExists()
//            ? $this->fileManager->append($messageData)
//            : $this->fileManager->write($messageData);

        if (!$this->createLogRecordInDatabase()) {
            throw new LoggerException('An error occurred while writing File log');
        }
    }


    /**
     * Инициализируем файловый менеджер
     *
     * @param string $logName
     * @param AbstractFileManager $fileManager
     * @return void
     */
    public function initFileManager(string $logName, AbstractFileManager $fileManager): void
    {
        // TODO через DI
        $this->fileManager = $fileManager;
        $this->fileManager->setFilePath($logName, self::LOG_FILE_PATH, AbstractFileManager::FILE_EXTENSION);
    }

    /**
     * Создание записи в БД
     *
     * @return bool
     */
    protected function createLogRecordInDatabase(): bool
    {
        return (new FileLog([
            'log_name'     => $this->getLogName(),
            'log_category' => $this->getLogCategory(),
            'file_path'    => $this->fileManager->getFilePath(),
            'message'      => mb_strimwidth($this->getMessage(), 0, 10, "..."),
        ]))->save();
    }

    /**
     * Проверка на существование заранее созданного файла с такой категорией
     * и именем лога
     * Нужно для выставление мода записи ("a" или "w")
     *
     * @return bool
     */
    private function checkIfLogAlreadyExists(): bool
    {
        return FileLog::query()->where([
            ['log_name', '=', $this->getLogName()],
            ['log_category', '=', $this->getLogCategory()],
        ])->exists();
    }
}