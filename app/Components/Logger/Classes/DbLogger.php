<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.07.18
 * Time: 9:59
 */

namespace App\Components\Logger\Classes;

use App\Components\Logger\Exceptions\LoggerException;
use App\Components\Logger\Models\DB\DbLog;

/**
 * Логгер для записи сообщений в БД
 *
 * Class DbLogger
 * @package App\Components\Logger\Classes
 */
class DbLogger extends AbstractLogger
{
    /**
     * Метод логирования
     *
     * @param string $logName
     * @param mixed $messageData
     * @param int $logCategory
     * @return void
     * @throws LoggerException
     */
    public function writeToLog(string $logName, $messageData, int $logCategory): void
    {
        $this->setAttributes($logName, $messageData, $logCategory);

        if(!$this->createLogRecordInDatabase()){
            throw new LoggerException('An error occurred while writing DB log');
        }
    }

    /**
     * Создание записи в БД
     *
     * @return bool
     */
    protected function createLogRecordInDatabase(): bool
    {
        /** @var DbLog $logModel */
        $logModel = new DbLog([
            'log_name'     => $this->getLogName(),
            'log_category' => $this->getLogCategory(),
            'message'      => $this->getMessage()
        ]);

        return $logModel->save();
    }
}