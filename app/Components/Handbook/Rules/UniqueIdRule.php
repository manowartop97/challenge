<?php

namespace App\Components\Handbook\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 31.10.2018
 * Time: 19:32
 */
class UniqueIdRule implements Rule
{
    /**
     * @var array
     */
    private $params;

    /**
     * UniqueIdRule constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($this->params)) {
            return false;
        }

        if (count(array_keys($this->getArrayOfValuesFromParams($this->params), $value)) > 1) {
            return false;
        }

        return true;
    }

    /**
     * Getting values of all form attributes from params
     *
     * @param array $params
     * @return array
     */
    private function getArrayOfValuesFromParams($params)
    {
        return explode(' ', array_pop($params));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Значение должно быть уникальным';
    }
}