<?php

namespace App\Components\Handbook\Models;

use Illuminate\Database\Eloquent\Model;
use stdClass;

/**
 * @property int $id
 * @property string $systemName
 * @property string $description
 * @property string|array $additionalFields
 * @property int $relation
 * @property string $created_at
 * @property string $updated_at
 * @property HandbookData[] $handbookData
 * @property mixed $handbook_data
 */
class Handbook extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['systemName', 'description', 'additionalFields', 'relation', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $casts = [
        'relation' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function handbookData()
    {
        return $this->hasMany(HandbookData::class);
    }

    /**
     * Получение данных в декодированном виде
     *
     * @return array
     */
    public function getDecodedData(): array
    {
        $result = [];
        foreach ($this->handbookData as $data) {
            $attributes = $data->toArray();
            foreach ($data->getDecodedData() as $key => $value) {
                $attributes[$key] = $value;
            }
            unset($attributes['additionalFieldsData']);
            $result[] = (object)$attributes;
        }

        return $result;
    }

    /**
     * Json Encoding fields
     *
     * @return void
     */
    public function encodeFields()
    {
        if ($this->additionalFields) {
            // Переиндексируем поля с помощью array_values, чтобы не затирались при обновлении
            $this->additionalFields = json_encode(array_values($this->additionalFields));
        }
    }

    /**
     * Get decoded fields
     *
     * @return stdClass[]
     */
    public function getDecodedFields()
    {
        return $this->additionalFields ? json_decode($this->additionalFields) : [];
    }

    /**
     * Get parent handbook
     *
     * @return string
     */
    public function getParent()
    {
        if ($this->relation) {

            /** @var Handbook $handbook */
            $handbook = Handbook::query()->findOrFail($this->relation);

            return $handbook->systemName;
        }

        return 'Not Set';
    }
}
