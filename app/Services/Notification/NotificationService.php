<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 03.12.2018
 * Time: 20:54
 */

namespace App\Services\Notification;

use App\Models\DB\Notification\Notification;
use App\Models\DB\User\User;
use Fcm;

/**
 * Сервис по отправке уведомлений
 *
 * Class NotificationService
 * @package App\Services\Notification
 */
class NotificationService
{
    /**
     * Категория уведомлений - дружба
     *
     * @const
     */
    const CATEGORY_FRIENDSHIP = 'friendship';

    /**
     * Отправляем уведомление
     *
     * @param Notification $notification
     * @return bool
     * @throws \Throwable
     */
    public function sendNotification(Notification $notification): bool
    {
        /** @var User $receiver */
        $receiver = User::query()->findOrFail($notification->user_id);

        if (!$receiver->profile->device_tokens) {
            return false;
        }

        Fcm::to(json_decode($receiver->profile->device_tokens))
            ->data([
                'data'     => view($notification->view_template, array_merge(json_decode($notification->arguments, true), ['notification_id' => $notification->id]))->render(),
                'category' => $notification->category,
                'count'    => $receiver
                    ->notifications()
                    ->where([['category', $notification->category], ['status', Notification::STATUS_NEW]])
                    ->get()
                    ->count(),
            ])
            ->send();

        return true;
    }
}