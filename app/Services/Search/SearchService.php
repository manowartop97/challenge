<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 24.11.2018
 * Time: 12:28
 */

namespace App\Services\Search;

use App\Models\DB\User\UserProfile;
use Auth;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

/**
 * Поисковый сервис по пользователям
 *
 * Class SearchService
 * @package App\Services\Search
 */
class SearchService
{
    /**
     * Поиск по юзерам
     *
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function search(array $search, int $pageSize = 10): LengthAwarePaginator
    {
        $query = UserProfile::query()->with('user')->where('user_id', '!=', Auth::id());

        if (isset($search['q'])) {
            $query->where(function (Builder $query) use ($search) {
                $query->where('surname', 'like', '%'.$search['q'].'%')
                    ->orWhere('name', 'like', '%'.$search['q'].'%');
            });
        }

        if (isset($search['gender'])) {
            $query->where('gender', $search['gender']);
        }

        return $query->paginate($pageSize);
    }
}