<?php

namespace App\Policies\Page;

use App\Models\DB\Friendship\Friendship;
use App\Models\DB\User\AccountSetting;
use App\Models\DB\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Проверка доступа к страницам пользователей
 *
 * Class PagePolicy
 * @package App\Policies\Page
 */
class PagePolicy
{
    use HandlesAuthorization;

    /**
     * Кто может просматривать страницу пользователя
     *
     * @param User $user
     * @param User $target
     * @return bool
     */
    public function visit(User $user, User $target): bool
    {
        // Кто может просматривать страницу
        if ($target->settings->who_can_view_posts === AccountSetting::VARIANT_NOBODY) {
            return false;
        }

        // Если только друзья - проверяем являюсь ли я другом йопта
        if ($target->settings->who_can_view_posts === AccountSetting::VARIANT_ONLY_FRIENDS) {
            return Friendship::query()->where([['user_id', $user->id], ['friend_id', $target->id], ['friendship_status', Friendship::FRIENDSHIP_STATUS_ACCEPTED]])->exists()
                || Friendship::query()->where([['user_id', $target->id], ['friend_id', $user->id], ['friendship_status', Friendship::FRIENDSHIP_STATUS_ACCEPTED]])->exists();
        }

        return true;
    }
}
