<?php

namespace App\Policies;

use App\Models\DB\User\AccountSetting;
use App\Models\DB\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Проверки доступов по дружбе
 *
 * Class FriendshipPolicy
 * @package App\Policies
 */
class FriendshipPolicy
{
    use HandlesAuthorization;

    public function inviteToFriendship(User $user, int $id)
    {
        /**
         * @var User $friend
         */
        $friend = User::query()->findOrFail($id);

        return $friend->settings->who_can_request_friends !== AccountSetting::VARIANT_NOBODY;
    }
}
