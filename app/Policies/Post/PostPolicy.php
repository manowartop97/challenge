<?php

namespace App\Policies\Post;

use App\Models\DB\Posts\Post;
use App\Models\DB\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Проверка доступов при манипуляции с постами
 *
 * Class PostPolicy
 * @package App\Policies\Post
 */
class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Может ли юзер обновлять пост
     *
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function edit(User $user, Post $post): bool
    {
        return $post->author_id === $user->id;
    }

    /**
     * Может ли юзер удалять пост
     *
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function delete(User $user, Post $post): bool
    {
        // Удалять может как владелец, так и тот, кому скинули пост
        return $post->author_id === $user->id || $post->user_id === $user->id;
    }

    /**
     * Может ли юзер закрепить пост на стене
     *
     * @param User $user
     * @param Post $post
     * @return bool
     */
    public function makeFeatured(User $user, Post $post): bool
    {
        return $post->user_id === $user->id;
    }
}
