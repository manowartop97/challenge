<?php

namespace App\Policies\Notification;

use App\Models\DB\Notification\Notification;
use App\Models\DB\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Проверка доступов по уведомлениям
 *
 * Class NotificationPolicy
 * @package App\Policies\Notification
 */
class NotificationPolicy
{
    use HandlesAuthorization;

    /**
     * Может ли юзер очистить уведомление
     *
     * @param User $user
     * @param Notification $notification
     * @return bool
     */
    public function clear(User $user, Notification $notification): bool
    {
        return $notification->user_id === $user->id;
    }
}
