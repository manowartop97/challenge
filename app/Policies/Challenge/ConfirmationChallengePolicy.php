<?php

namespace App\Policies\Challenge;

use App\Models\DB\Challenge\ChallengeConfirmation;
use App\Models\DB\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ConfirmationChallengePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param ChallengeConfirmation $challengeConfirmation
     * @return bool
     */
    public function view(User $user, ChallengeConfirmation $challengeConfirmation): bool
    {
        return $user->id !== $challengeConfirmation->user_id || $challengeConfirmation->challengeInvitation->participant_id !== $user;
    }
}
