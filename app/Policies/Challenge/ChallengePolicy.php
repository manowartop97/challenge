<?php

namespace App\Policies\Challenge;

use App\Models\DB\Challenge\Challenge;
use App\Models\DB\Challenge\ChallengeInvitation;
use App\Models\DB\User\User;
use Illuminate\Auth\Access\HandlesAuthorization;

/**
 * Проверка прав и доступов к челленджам
 *
 * Class ChallengePolicy
 * @package App\Policies\Challenge
 */
class ChallengePolicy
{
    use HandlesAuthorization;

    /**
     * Проверка может ли юзер учавствовать в челлендже
     *
     * @param User $user
     * @param Challenge $challenge
     * @return bool
     */
    public function takePart(User $user, Challenge $challenge): bool
    {
        if ($challenge->user_id === $user->id) {
            return false;
        }

        // Если ему уже слали приглашение, или есть принятое приглашение, или уже сам вступил - нахуй
        if ($challenge->isActiveInvitationExists()) {
            return false;
        }

        return true;
    }

    /**
     * Можно ли отменить участие в челлендже
     *
     * @param User $user
     * @param Challenge $challenge
     * @return bool
     */
    public function cancelParticipation(User $user, Challenge $challenge): bool
    {
        if ($challenge->user_id === $user->id) {
            return false;
        }

        // Если ему уже слали приглашение, или есть принятое приглашение, или уже сам вступил - нахуй
        if (!$challenge->isActiveInvitationExists()) {
            return false;
        }

        return true;
    }

    /**
     * Может ли юзер слать приглашение в челлендж (может только если он его создал либо он его выполнил)
     *
     * @param User $user
     * @param Challenge $challenge
     * @return bool
     */
    public function sendInvitations(User $user, Challenge $challenge): bool
    {
        // TODO проверка - выполнял ли он его
        return $challenge->user_id === $user->id;
    }

    /**
     * Может ли юзер едитить челлендж
     *
     * @param User $user
     * @param Challenge $challenge
     * @return bool
     */
    public function edit(User $user, Challenge $challenge): bool
    {
        return $challenge->user_id === $user->id;
    }

    /**
     * Можно ли удалять челлендж
     *
     * @param User $user
     * @param Challenge $challenge
     * @return bool
     */
    public function delete(User $user, Challenge $challenge): bool
    {
        if ($challenge->user_id !== $user->id) {
            return false;
        }

        // Если кто-то его выполняет
        if (ChallengeInvitation::query()->where('challenge_id', $challenge->id)->whereIn('status', [ChallengeInvitation::STATUS_TAKE_PART_MANUALLY, ChallengeInvitation::STATUS_ACCEPTED])->exists()) {
            return false;
        }

        return true;
    }

    /**
     * Возобновление челленджа
     *
     * @param User $user
     * @param Challenge $challenge
     * @return bool
     */
    public function reNew(User $user, Challenge $challenge): bool
    {
        return $user->id === $challenge->user_id && $challenge->status === Challenge::STATUS_DELETED;
    }

    /**
     * Можно ли подтвердить выполнение челленджа
     *
     * @param User $user
     * @param Challenge $challenge
     * @return bool
     */
    public function confirm(User $user, Challenge $challenge): bool
    {
        if ($challenge->user_id === $user->id) {
            return false;
        }

        return $challenge
            ->challengeInvitations()
            ->where([['participant_id', $user->id]])
            ->whereIn('status', [ChallengeInvitation::STATUS_ACCEPTED, ChallengeInvitation::STATUS_TAKE_PART_MANUALLY])
            ->exists();
    }
}
