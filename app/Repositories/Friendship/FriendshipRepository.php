<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 23.11.2018
 * Time: 12:29
 */

namespace App\Repositories\Friendship;

use App\Events\Notification\OnFriendshipAcceptedEvent;
use App\Events\Notification\OnFriendshipCreatedEvent;
use App\Models\DB\Friendship\Friendship;
use App\Models\DB\User\User;
use Auth;
use Event;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

/**
 * Репозиторий отвечающий за друзей
 *
 * Class FriendshipRepository
 * @package App\Repositories\Friendship
 */
class FriendshipRepository
{
    const FRIENDS_FOR_FRONT_DISPLAY = 14;

    /**
     * Получить список френдов свой/чей-то
     *
     * @param int|null $id
     * @param int $pageSize
     * @param int|null $limit
     * @return LengthAwarePaginator
     */
    public function getFriends(int $id = null, int $pageSize = 10, int $limit = null): LengthAwarePaginator
    {
        $query = Friendship::query();

        $query->where('friendship_status', Friendship::FRIENDSHIP_STATUS_ACCEPTED);
        $query->where(function (Builder $query) use ($id) {
            $query->where('user_id', is_null($id) ? Auth::id() : $id)
                ->orWhere('friend_id', is_null($id) ? Auth::id() : $id);
        });

        return is_null($limit) ? $query->paginate($pageSize) : $query->limit($limit)->paginate($pageSize);
    }

    /**
     * Количество друзей
     *
     * @param int $id
     * @return int
     */
    public function getTotalFriends(int $id): int
    {
        $query = Friendship::query()->where('friendship_status', Friendship::FRIENDSHIP_STATUS_ACCEPTED)->where(function (Builder $query) use ($id) {
            $query->where('user_id', $id)
                ->orWhere('friend_id', $id);
        });

        return $query->count();
    }

    /**
     * Создание заявки в друзья
     *
     * @param int $id
     * @return bool
     */
    public function createFriendship(int $id): bool
    {
        if (Auth::user()->isUserInFriendList($id)) {
            return false;
        }

        if (Friendship::query()->where([['user_id', Auth::id()], ['friend_id', $id]])->exists() || Friendship::query()->where([['friend_id', Auth::id()], ['user_id', $id]])->exists()) {
            /** @var Friendship $friendship */
            $friendship = Friendship::query()->where([['user_id', Auth::id()], ['friend_id', $id]])->get()->first();

            // Не угадали, пробуем по другому искать
            if (!$friendship) {
                $friendship = Friendship::query()->where([['friend_id', Auth::id()], ['user_id', $id]])->get()->first();
            }

            // Если опять не нашли - тогда хуйня какая-то
            if (!$friendship) {
                return false;
            }

            $friendship->fill([
                'user_id'           => Auth::id(),
                'friend_id'         => $id,
                'friendship_status' => Friendship::FRIENDSHIP_STATUS_NEW,
            ]);

        } else {
            $friendship = new Friendship([
                'user_id'           => Auth::id(),
                'friend_id'         => $id,
                'friendship_status' => Friendship::FRIENDSHIP_STATUS_NEW,
            ]);
        }

        /** @var User $friend */
        $friend = User::query()->findOrFail($id);
        Event::fire(new OnFriendshipCreatedEvent(Auth::user(), $friend));

        return $friendship->save();
    }

    /**
     * Удаление из друзей пользователем
     *
     * @param int $friendId
     * @return bool
     */
    public function removeFriend(int $friendId): bool
    {
        if (!Auth::user()->isUserInFriendList($friendId)) {
            return false;
        }
        /**
         * @var Friendship $friendship
         */
        $friendship = Friendship::query()->where([['user_id', Auth::id()], ['friend_id', $friendId]])->get()->first();

        // Пробуем другую последовательность полей
        if (!$friendship) {
            $friendship = Friendship::query()->where([['friend_id', Auth::id()], ['user_id', $friendId]])->get()->first();
        }

        if (!$friendship) {
            return false;
        }

        $friendship->friendship_status = Friendship::FRIENDSHIP_STATUS_REJECTED_BY_INVITOR;

        return $friendship->save();
    }

    /**
     * Принятие дружбы
     *
     * @param int $id
     * @return bool
     */
    public function acceptFriendship(int $id): bool
    {
        /** @var Friendship $friendship */
        $friendship = Friendship::query()->where([['user_id', $id], ['friend_id', Auth::id()]])->get()->first();

        if (!$friendship) {
            return false;
        }

        // Если она уже принята или отклонена, то какое нахуй принять заново
        if (in_array($friendship->friendship_status, [Friendship::FRIENDSHIP_STATUS_REJECTED_BY_POTENTIAL_FRIEND, Friendship::FRIENDSHIP_STATUS_ACCEPTED])) {
            return false;
        }

        $friendship->friendship_status = Friendship::FRIENDSHIP_STATUS_ACCEPTED;

        /** @var User $friend */
        $friend = User::query()->findOrFail($id);
        Event::fire(new OnFriendshipAcceptedEvent(Auth::user(), $friend));

        return $friendship->save();
    }

    /**
     * Отклонение дружбы
     *
     * @param int $id
     * @return bool
     */
    public function rejectFriendship(int $id): bool
    {
        /** @var Friendship $friendship */
        $friendship = Friendship::query()->where([['user_id', $id], ['friend_id', Auth::id()]])->get()->first();

        if (!$friendship) {
            return false;
        }

        // Если она уже принята или отклонена, то какое нахуй принять заново
        if (in_array($friendship->friendship_status, [Friendship::FRIENDSHIP_STATUS_REJECTED_BY_POTENTIAL_FRIEND, Friendship::FRIENDSHIP_STATUS_ACCEPTED])) {
            return false;
        }

        $friendship->friendship_status = Friendship::FRIENDSHIP_STATUS_REJECTED_BY_POTENTIAL_FRIEND;

        return $friendship->save();
    }
}