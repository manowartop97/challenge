<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 19.11.2018
 * Time: 18:40
 */

namespace App\Repositories\User;

use App\Models\DB\User\UserProfile;
use Auth;
use Illuminate\Http\UploadedFile;
use Image;
use Intervention\Image\Image as InterventionImage;
use Storage;

/**
 * Репозиторий профайла пользователя
 *
 * Class ProfileRepository
 * @package App\Repositories\User
 */
class ProfileRepository
{
    /**
     * @const
     */
    const LINK_FEED = 1;
    const LINK_ABOUT = 2;
    const LINK_FRIENDS = 3;
    const LINK_CHALLENGES = 4;
    const LINK_ACHIEVEMENTS = 5;

    /**
     * Размер аватарки для ресайза
     *
     * @const
     */
    const AVATAR_SM = 120;

    /**
     * @const
     */
    const MEDIA_TYPE_AVATAR = 'avatar';

    /**
     * @return UserProfile
     */
    public function getProfile(): UserProfile
    {
        return Auth::user()->profile;
    }

    /**
     * Обновляем инфу профиля
     *
     * @param array $data
     * @return bool
     */
    public function updateProfileInfo(array $data): bool
    {
        return Auth::user()->fill($data)->save() && Auth::user()->profile->fill($data)->save();
    }

    /**
     * Обновляем аватар
     *
     * @param UploadedFile $file
     * @param bool $isMainAvatar
     * @return bool
     */
    public function updateAvatar(UploadedFile $file, bool $isMainAvatar = true): bool
    {
        $avatarName = $this->uploadImageToUserFolder($file, self::MEDIA_TYPE_AVATAR, $isMainAvatar);

        if ($avatarName === false) {
            return false;
        }

        return $this->updateProfileInfo($isMainAvatar ? ['avatar' => $avatarName] : ['profile_background' => $avatarName]);
    }

    /**
     * Грузим картинку и возвращаем имя
     *
     * PUBLIC - ибо юзаю в сидах
     *
     * @param UploadedFile $file
     * @param string $mediaType
     * @param bool $isMainAvatar
     * @return bool|string
     */
    protected function uploadImageToUserFolder(UploadedFile $file, string $mediaType = self::MEDIA_TYPE_AVATAR, bool $isMainAvatar = true)
    {
        $userFolder = 'public/'.$mediaType.'/'.explode('@', Auth::user()->email)[0];
        $imageName = str_random(10).'.'.$file->extension();

        $this->deleteOldAvatarFile($isMainAvatar);

        $image = Image::make($file);

        if ($isMainAvatar) {
            $image = $this->calculateResizeParams($image);
        }

        $image->orientate();

        if (!$image->save(Storage::path($userFolder.'/'.$imageName)) instanceof Image) {
            return $imageName;
        }

        return false;
    }

    /**
     * Удаляем старый аватар
     *
     * @param bool $isMainAvatar
     * @return void
     */
    protected function deleteOldAvatarFile(bool $isMainAvatar): void
    {
        $imageName = $isMainAvatar ? Auth::user()->profile->avatar : Auth::user()->profile->profile_background;
        $avatarPath = 'public/avatar/'.explode('@', Auth::user()->email)[0].'/'.$imageName;
        if (is_file(Storage::path($avatarPath))) {
            Storage::delete($avatarPath);
        }
    }

    /**
     * Ресайзим картинку
     *
     * @param InterventionImage $image
     * @return InterventionImage
     */
    protected function calculateResizeParams(InterventionImage $image): InterventionImage
    {
//        if ($image->getWidth() > $image->getHeight()) {
//            $smallerSize = $image->getHeight();
//        } else {
//            $smallerSize = $image->getHeight();
//        }

        // меньшая сторона = 100%
        // $smallerSize-120 - х%

//        $percent = ($smallerSize - self::AVATAR_SM) * 100 / $smallerSize;

        // ширина = 100%
        // х = $percent%

        // Перемасштабирование картинки с учетом новых размеров
//        $image->resize($image->getWidth() - $image->getWidth() * $percent / 100, $image->getHeight() - $image->getHeight() * $percent / 100);

        return $image;
    }
}