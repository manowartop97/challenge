<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 16:36
 */

namespace App\Repositories\User\Account;

use App\Models\DB\User\AccountSetting;
use Auth;

/**
 * Работаем с настройками профиля
 *
 * Class AccountSettingsRepository
 * @package App\Repositories\User\Account
 */
class AccountSettingsRepository
{
    /**
     * Получение настроек пользователя
     *
     * @return AccountSetting
     */
    public function getSettings(): AccountSetting
    {
        return Auth::user()->settings;
    }

    /**
     * Обновление настроек пользователя
     *
     * @param array $data
     * @return bool
     */
    public function updateSettings(array $data): bool
    {
        return Auth::user()->settings->fill($data)->save();
    }
}