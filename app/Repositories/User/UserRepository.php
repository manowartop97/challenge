<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 19.11.2018
 * Time: 12:13
 */

namespace App\Repositories\User;

use DB;
use Auth;
use File;
use Event;
use Avatar;
use Storage;
use Throwable;
use App\Models\DB\User\User;
use App\Mail\User\ResetPasswordMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Mail\User\ForgotPasswordMail;
use App\Events\User\OnUserRegisterEvent;
use App\Exceptions\User\UserNotFoundException;

/**
 * Class UserRepository
 * @package App\Repositories\User
 */
class UserRepository
{
    /**
     * @var string
     */
    private $errorMessage = null;

    /**
     * Регистрация проходит тут
     *
     * @param array $data
     * @return User|bool
     * @throws \Exception
     */
    public function registerUser(array $data)
    {
        DB::beginTransaction();

        $user = new User($data);
        $user->password = bcrypt($data['password']);
        $user->confirmation_token = $this->createConfirmationToken();

        if (!$user->save()) {
            DB::rollBack();

            return false;
        }

        $user->profile()->create($data);
        $user->settings()->create();
        $this->generateDefaultAvatar($user);
        try {
            Event::fire(new OnUserRegisterEvent($user));
        } catch (Throwable $exception) {
            // TODO Письмо в очередь на отправление
            echo '<pre>';
            print_r($exception->getMessage());
            die();
        }

        DB::commit();

        return $user;
    }

    /**
     * Подтверджение аккаунта пользователя
     *
     * @param string $token
     * @return bool
     */
    public function confirmUser(string $token): bool
    {
        /** @var User $user */
        $user = User::query()->where('confirmation_token', $token)->get()->first();

        $user->confirmation_token = null;
        $user->is_verified = true;

        return $user->save();
    }

    /**
     * Изменение пароля
     *
     * @param array $data
     * @return bool
     */
    public function changePassword(array $data): bool
    {
        if (!Hash::check($data['old_password'], Auth::user()->password)) {
            $this->errorMessage = trans('messages.validation.old_password_not_matched');

            return false;
        }

        return Auth::user()->fill(['password' => Hash::make($data['new_password'])])->save();
    }

    /**
     * TODO в письме кнопка "я не просил новый пароль"
     *
     * Генерация нового пароля и отправка его на почту
     *
     * @return bool
     */
    public function resetPassword(): bool
    {
        $newPlainPassword = str_random(8);

        if (!Auth::user()->fill(['password' => Hash::make($newPlainPassword)])->save()) {
            return false;
        }
        try {
            Mail::to(Auth::user()->email)->send(new ResetPasswordMail(Auth::user(), $newPlainPassword));
        } catch (Throwable $exception) {
            // TODO мыло в очередь
        }

        return true;
    }

    /**
     * Восстановление забытого пароля
     *
     * @param string $email
     * @return bool
     * @throws UserNotFoundException
     */
    public function recoverPassword(string $email): bool
    {
        /** @var User $user */
        $user = User::query()->where('email', $email)->get()->first();

        // На этапе валидации это проверялось, но мало ли
        if (!$user) {
            throw new UserNotFoundException("User with email {$email} Not Found");
        }

        $newPassword = str_random(8);

        if (!$user->fill(['password' => Hash::make($newPassword)])->save()) {
            return false;
        }

        try {
            Mail::to($user->email)->send(new ForgotPasswordMail($user, $newPassword));
        } catch (Throwable $exception) {
            // TODO мыло в очередь
        }

        return true;
    }

    /**
     * @return null|string
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    /**
     * Генерим дефолтный аватар
     *
     * @param User $user
     * @param string $mediaType
     * @return bool
     */
    public function generateDefaultAvatar(User $user, string $mediaType = ProfileRepository::MEDIA_TYPE_AVATAR): bool
    {
        $userFolder = 'public/'.$mediaType.'/'.explode('@', $user->email)[0];
        $imageName = 'default.jpg';
        $backgroundName = 'default.gif';

        if (!is_dir(Storage::path($userFolder))) {
            Storage::makeDirectory($userFolder);
        }
        $randImage = rand(1, 5);
        $randBg = rand(1, 3);

        if ($user->profile->gender === User::GENDER_MALE) {
            File::copy(public_path()."/frontend/img/avatars/m{$randImage}.jpg", Storage::path($userFolder.'/'.$imageName));
        } else {
            File::copy(public_path()."/frontend/img/avatars/f{$randImage}.jpg", Storage::path($userFolder.'/'.$imageName));
        }

        File::copy(public_path()."/frontend/img/headers/bg{$randBg}.gif", Storage::path($userFolder.'/'.$backgroundName));
        $user->profile->avatar = $imageName;
        $user->profile->profile_background = $backgroundName;

        return $user->profile->save();
    }

    /**
     * @return string
     */
    protected function createConfirmationToken(): string
    {
        return str_random(30);
    }
}