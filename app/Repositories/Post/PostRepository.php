<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 27.11.2018
 * Time: 11:42
 */

namespace App\Repositories\Post;

use App\Exceptions\Challenge\ThumbnailGenerationException;
use App\Models\DB\Posts\Like;
use App\Models\DB\Posts\Post;
use App\Models\DB\User\User;
use App\Repositories\Challenge\ChallengeRepository;
use App\Traits\AttachmentsTrait;
use Auth;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\UploadedFile;
use Image;
use Lakshmaji\Thumbnail\Facade\Thumbnail;
use Storage;

/**
 * Репа по созданию постов
 *
 * Class PostRepository
 * @package App\Repositories\Post
 */
class PostRepository
{
    use AttachmentsTrait;

    /**
     * Пагинированный список постов юзера
     *
     * @param int|null $id
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getPosts(int $id = null, int $pageSize = 7): LengthAwarePaginator
    {
        $query = Post::query()->where('user_id', is_null($id) ? Auth::id() : $id);

        return $query->orderBy('is_featured', 'desc')->orderBy('id', 'desc')->paginate($pageSize);
    }

    /**
     * Создаем пост
     *
     * @param array $data
     * @return Post|null
     * @throws ThumbnailGenerationException
     */
    public function createPost(array $data): ?Post
    {
        $post = new Post([
            'user_id'          => $data['user_id'],
            'author_id'        => $data['author_id'],
            'text'             => isset($data['text']) ? $data['text'] : null,
            'attachments_json' => isset($data['post_attachments']) ? json_encode($this->uploadAttachments($data['post_attachments'], $data['user_id'])) : null,

        ]);

        if (!$post->save()) {
            return null;
        }

        return $post;
    }

    /**
     * Апдейтим пост
     *
     * @param Post $post
     * @param array $data
     * @return Post|null
     * @throws ThumbnailGenerationException
     */
    public function editPost(Post $post, array $data): ?Post
    {
        // Значит все файлы удалили
        if ($data['is_files'] <= 0) {
            $this->deleteAttachments($post->getAttachments());
            $post->attachments_json = null;
        }

        // Если пришли новые фотки, то они заменили старые - старые удаляем
        if (isset($data['post_attachments'])) {
            $this->deleteAttachments($post->getAttachments());
            $post->attachments_json = json_encode($this->uploadAttachments($data['post_attachments'], $data['user_id']));
        }

        if (!$post->fill($data)->save()) {
            return null;
        }

        return $post;
    }

    /**
     * Ставим лайк на пост/коммент
     *
     * @param int $postId
     * @return bool
     */
    public function like(int $postId): bool
    {
        /** @var Like $like */
        $like = Like::query()->where([['likable_type', Post::class], ['user_id', Auth::id()], ['likable_id', $postId]])->get()->first();
        /** @var Post $post */
        $post = Post::query()->findOrFail($postId);
        $post->likes_count++;

        if ($like) {
            $like->is_liked = true;

            return $like->save() && $post->save();
        }

        $like = new Like([
            'user_id'      => Auth::id(),
            'likable_id'   => $postId,
            'likable_type' => Post::class,
            'is_liked'     => true,
        ]);

        return $like->save() && $post->save();
    }

    /**
     * Снимаем лайк
     *
     * @param int $postId
     * @return bool
     */
    public function unlike(int $postId): bool
    {
        /** @var Like $like */
        $like = Like::query()->where([['likable_type', Post::class], ['user_id', Auth::id()], ['likable_id', $postId]])->get()->first();

        if (!$like) {
            return false;
        }

        $like->is_liked = false;
        /** @var Post $post */
        $post = Post::query()->findOrFail($postId);
        $post->likes_count--;

        return $like->save() && $post->save();
    }

    /**
     * Получить актуальное кол-во лайков поста
     *
     * @param int $id
     * @return int
     */
    public function getActualLikesCount(int $id): int
    {
        /** @var Post $post */
        $post = Post::query()->findOrFail($id);

        return $post->likes_count;
    }

    /**
     * Удаление поста
     *
     * @param Post $post
     * @return bool|null
     * @throws \Exception
     */
    public function deletePost(Post $post): ?bool
    {
        $this->deleteAttachments($post->getAttachments());

        return $post->delete();
    }

    /**
     * Закрепляем пост на стене
     *
     * @param Post $post
     * @return bool
     */
    public function makeFeatured(Post $post): bool
    {
        $post->is_featured = !$post->is_featured;

        return $post->save();
    }

    /**
     * Аплоадим медиа в папку юзера, которому адресован пост
     *
     * @param array $attachments
     * @param int $user_id
     * @return array
     * @throws ThumbnailGenerationException
     */
    protected function uploadAttachments(array $attachments, int $user_id): array
    {
        /**
         * @var UploadedFile $attachment
         * @var User $user
         */
        $mediaNames = [];
        $user = User::query()->findOrFail($user_id);
        $userFolder = 'public/posts/' . explode('@', $user->email)[0];
        $this->createFolderIfNotExists($userFolder);
        foreach ($attachments as $attachment) {
            if ($this->isFileImage($attachment)) {
                $randomName = str_random(10) . '.' . $attachment->extension();
                $image = Image::make($attachment);
                $image->orientate();
                $image->save(Storage::path($userFolder . '/' . $randomName));
                $mediaNames[] = [
                    'type' => ChallengeRepository::TYPE_IMAGE,
                    'name' => $randomName,
                ];
            } else {
                $randomName = str_random(10) . '.mp4';
                $attachment->storeAs($userFolder, $randomName);
                $thumbnailName = str_random(10) . '.jpg';
                $response = Thumbnail::getThumbnail(Storage::path($userFolder . '/' . $randomName), Storage::path($userFolder), $thumbnailName, 1);
                if (!$response) {
                    throw new ThumbnailGenerationException('Unable to generate thumbnail');
                }
                $mediaNames[] = [
                    'type'      => ChallengeRepository::TYPE_VIDEO,
                    'thumbnail' => $thumbnailName,
                    'name'      => $randomName,
                ];
            }

        }

        return $mediaNames;
    }

    /**
     * Удаляем аттачи
     *
     * @param array $attachments
     */
    protected function deleteAttachments(array $attachments): void
    {
        $strToReplace = '/storage/';

        foreach ($attachments as $attachment) {
            Storage::delete('public/' . str_replace($strToReplace, '', $attachment['name']));
        }
    }
}