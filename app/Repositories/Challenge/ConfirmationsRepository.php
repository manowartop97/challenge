<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 07.12.2018
 * Time: 10:46
 */

namespace App\Repositories\Challenge;

use App\Models\DB\Challenge\ChallengeConfirmation;
use App\Models\DB\Challenge\ChallengeInvitation;
use Auth;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Репа для подтверждений челленджей
 *
 * Class ConfirmationsRepository
 * @package App\Repositories\Challenge
 */
class ConfirmationsRepository
{
    /**
     * Входящие/исходящие уведомления
     *
     * @const
     */
    const TYPE_INCOME = 1;
    const TYPE_OUTCOME = 2;

    /**
     * @var int
     */
    private $type;

    /**
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getConfirmations(array $search = [], int $pageSize = 15): LengthAwarePaginator
    {
        $query = ChallengeConfirmation::query();

        if (!isset($search['type']) || (isset($search['type']) && (int)$search['type'] === self::TYPE_INCOME)) {
            $this->setType(self::TYPE_INCOME);
            $query->where('user_id', Auth::id());
        } else {
            $this->setType(self::TYPE_OUTCOME);
            $query->whereIn('invitation_id', ChallengeInvitation::query()->where('participant_id', Auth::id())->pluck('id', 'id')->all());
        }

        if (isset($search['status']) && $search['status'] !== 0) {
            $query->where('status', $search['status']);
        }

        return $query->paginate($pageSize);
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    private function setType(int $type): void
    {
        $this->type = $type;
    }
}