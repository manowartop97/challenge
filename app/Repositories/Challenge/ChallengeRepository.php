<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 28.11.2018
 * Time: 11:33
 */

namespace App\Repositories\Challenge;

use App\Exceptions\Challenge\ThumbnailGenerationException;
use App\Models\DB\Challenge\Challenge;
use App\Models\DB\Challenge\ChallengeConfirmation;
use App\Models\DB\Challenge\ChallengeInvitation;
use App\Traits\AttachmentsTrait;
use Auth;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Lakshmaji\Thumbnail\Facade\Thumbnail;
use Storage;

/**
 * Репозиторий челленджев
 *
 * Class ChallengeRepository
 * @package App\Repositories\Challenge
 */
class ChallengeRepository
{
    use AttachmentsTrait;

    /**
     * @const
     */
    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';

    /**
     * Имена файлов, чтобы в случае еррора - удалить их и роллбекнуть все
     *
     * @var array
     */
    private $tmpFiles;

    /**
     * Файлы которые нужно удалить если все сохранили и т д
     *
     * @var array
     */
    private $filesToDelete;

    /**
     * Пагинация/фильтрация челленджей
     *
     * @param int|null $id
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getChallenges(int $id = null, array $search = [], int $pageSize = 15): LengthAwarePaginator
    {
        $query = Challenge::query()->where('user_id', is_null($id) ? Auth::id() : $id);

        if (isset($search['status']) && $search['status'] != 0) {
            $query->where('status', $search['status']);
        }

        if (isset($search['title'])) {
            $query->where('title', 'like', '%' . $search['title'] . '%');
        }

        return $query->orderBy('id', 'desc')->orderBy('status', 'asc')->paginate($pageSize);
    }

    /**
     * Пагинация/фильтрация челленджей
     *
     * @param array $search
     * @param int $pageSize
     * @return LengthAwarePaginator
     */
    public function getAllChallenges(array $search = [], int $pageSize = 15): LengthAwarePaginator
    {
        $query = Challenge::query()->with('tags')->where('status', Challenge::STATUS_ACTIVE);

        if (isset($search['q']) && $search['q'] !== '') {
            $query->where('title', 'like', '%' . $search['q'] . '%');
        }

        return $query->orderBy('challenges.id', 'desc')->orderBy('status', 'asc')->paginate($pageSize);
    }

    /**
     * Получаем челлендж
     *
     * @param int $id
     * @return Challenge|Builder|Builder[]|Collection|Model
     */
    public function getChallenge(int $id)
    {
        return Challenge::query()->findOrFail($id);
    }

    /**
     * Создаем челлендж
     *
     * @param array $data
     * @return bool
     * @throws ThumbnailGenerationException
     */
    public function createChallenge(array $data): bool
    {
        $challenge = new Challenge();

        $challenge->fill([
            'user_id'            => $data['user_id'],
            'title'              => $data['title'],
            'title_image'        => json_encode($this->uploadMedia([$data['title_image']])[0]), // Тайтл имедж обязательна и только одна
            'additional_media'   => isset($data['additional_media']) ? json_encode($this->uploadMedia($data['additional_media'])) : null,
            'is_group_challenge' => $data['is_group_challenge'],
            'deadline'           => isset($data['deadline']) ? $data['deadline'] : null,
            'description'        => $data['description'],
            'status'             => Challenge::STATUS_IN_MODERATION,
        ]);

        if (!$challenge->save()) {
            return false;
        }

        $challenge->syncTags(explode('|', $data['tags']));

        return true;
    }

    /**
     * Апдейтим челлендж
     *
     * @param Challenge $challenge
     * @param array $data
     * @return bool
     * @throws ThumbnailGenerationException
     */
    public function updateChallenge(Challenge $challenge, array $data): bool
    {
        // Значит доп медиа файлы удалили
        if ($data['is_additional_media'] <= 0) {
            if ($challenge->additional_media) { // Если были старые файлы - удаляем
                $this->addFilesToDeleteArray(json_decode($challenge->additional_media, true));
                $challenge->additional_media = null;
            }
        }

        // Если пришли новые фотки, то они заменили старые - старые удаляем
        if (isset($data['additional_media'])) {
            if ($challenge->additional_media) { // Если были старые файлы - удаляем
                $this->addFilesToDeleteArray(json_decode($challenge->additional_media, true));
            }
            $challenge->additional_media = json_encode($this->uploadMedia($data['additional_media']));
        }

        // Пришел новый аватар челленджа - старый удалить
        if (isset($data['title_image'])) {
            $this->addFilesToDeleteArray([json_decode($challenge->title_image, true)]);
            $challenge->title_image = json_encode($this->uploadMedia([$data['title_image']])[0]);
        }

        $challenge->fill([
            'user_id'            => $data['user_id'],
            'title'              => $data['title'],
            'is_group_challenge' => $data['is_group_challenge'],
            'deadline'           => isset($data['deadline']) ? $data['deadline'] : null,
            'description'        => $data['description'],
            'status'             => Challenge::STATUS_IN_MODERATION, // Отправляем опять на автомодерацию, вдруг какую-то хуйню сделали
        ]);

        if (!$challenge->save()) {
            return false;
        }
        $challenge->syncTags(explode('|', $data['tags']));
        $this->deleteFilesFromArray();

        return true;
    }

    /**
     * Софт удаление челленджа
     *
     * @param Challenge $challenge
     * @return bool
     */
    public function delete(Challenge $challenge): bool
    {
        $challenge->status = Challenge::STATUS_DELETED;

        return $challenge->save();
    }

    /**
     * Удаляем файлы в случае неудачи
     *
     * @return void
     */
    public function removeTmpFiles(): void
    {
        if ($this->tmpFiles) {
            $userFolder = 'public/challenge/' . explode('@', Auth::user()->email)[0];
            foreach ($this->tmpFiles as $tmpFileName) {
                if (Storage::exists($userFolder . '/' . $tmpFileName)) {
                    Storage::delete($userFolder . '/' . $tmpFileName);
                }
            }
        }
    }

    /**
     * Возобновляем челлендж
     *
     * @param Challenge $challenge
     * @return bool
     */
    public function reNewChallenge(Challenge $challenge): bool
    {
        return $challenge->fill([
            'status' => Challenge::STATUS_IN_MODERATION
        ])->save();
    }

    /**
     * Создать подтверждение выполнения челленджа
     *
     * TODO оставил пока тут из-за медиа аплоадов
     *
     * @param array $data
     * @return bool
     * @throws ThumbnailGenerationException
     */
    public function createConfirmation(array $data): bool
    {
        /** @var ChallengeInvitation $invitation */
        $invitation = ChallengeInvitation::query()
            ->where([
                ['participant_id', Auth::id()],
                ['challenge_id', $data['challenge_id']]
            ])
            ->whereIn('status', [ChallengeInvitation::STATUS_ACCEPTED, ChallengeInvitation::STATUS_TAKE_PART_MANUALLY])
            ->get()
            ->first();

        if (!$invitation) {
            return false;
        }

        $confirmation = new ChallengeConfirmation([
            'comment'          => isset($data['comment']) ? $data['comment'] : null,
            'additional_media' => isset($data['additional_media']) ? json_encode($this->uploadMedia($data['additional_media'])) : null,
            'challenge_id'     => $data['challenge_id'],
            'invitation_id'    => $invitation->id,
            'user_id'          => $invitation->challenge->user_id,
            'status'           => ChallengeConfirmation::STATUS_NEW
        ]);

        return $confirmation->save();
    }

    /**
     * Трошки некрасиво. Добавляем файлы в тмп - и потом удаляем
     *
     * @param array $files
     * @return void
     */
    protected function addFilesToDeleteArray(array $files): void
    {
        foreach ($files as $file) {
            if (isset($file['name'])) {
                $this->filesToDelete[] = $file['name'];
            }
        }
    }

    /**
     * Удаляем файлы в случае неудачи
     *
     * @return void
     */
    protected function deleteFilesFromArray(): void
    {
        if ($this->tmpFiles) {
            $userFolder = 'public/challenge/' . explode('@', Auth::user()->email)[0];
            foreach ($this->filesToDelete as $tmpFileName) {
                if (Storage::exists($userFolder . '/' . $tmpFileName)) {
                    Storage::delete($userFolder . '/' . $tmpFileName);
                }
            }
        }
    }

    /**
     * Аплоад медиа файлов в папку storage/app/public/challenge/username
     *
     * @param array $media
     * @return array
     * @throws ThumbnailGenerationException
     */
    protected function uploadMedia(array $media): array
    {
        /**
         * @var UploadedFile $mediaItem
         */
        $mediaNames = [];
        $userFolder = 'public/challenge/' . explode('@', Auth::user()->email)[0];

        $this->createFolderIfNotExists($userFolder);

        foreach ($media as $mediaItem) {
            $randomName = str_random(10) . '.jpg';
            $this->tmpFiles[] = $randomName;
            if ($this->isFileImage($mediaItem)) {
                $image = Image::make($mediaItem);
                $image->orientate();
                $image->save(Storage::path($userFolder . '/' . $randomName));
                $mediaNames[] = [
                    'type' => self::TYPE_IMAGE,
                    'name' => $randomName,
                ];
            } else {
                $randomName = str_random(10) . '.mp4';
                $mediaItem->storeAs($userFolder, $randomName);
                $thumbnailName = str_random(10) . '.jpg';
                $response = Thumbnail::getThumbnail(Storage::path($userFolder . '/' . $randomName), Storage::path($userFolder), $thumbnailName, 1);
                if (!$response) {
                    throw new ThumbnailGenerationException('Unable to generate thumbnail');
                }
                $this->tmpFiles[] = $thumbnailName;
                $mediaNames[] = [
                    'type'      => self::TYPE_VIDEO,
                    'thumbnail' => $thumbnailName,
                    'name'      => $randomName,
                ];
            }
        }

        return $mediaNames;
    }
}