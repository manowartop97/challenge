<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 30.11.2018
 * Time: 13:32
 */

namespace App\Repositories\Challenge;

use App\Models\DB\Challenge\Challenge;
use App\Models\DB\Challenge\ChallengeInvitation;
use App\Models\DB\User\User;
use Auth;

/**
 * Репозиторий инвайтов в челленджи
 *
 * Class InvitationRepository
 * @package App\Repositories\Challenge
 */
class InvitationRepository
{
    /**
     * Сообщение об ошибке
     *
     * @var string
     */
    protected $errorMessage;

    /**
     * Создаем приглашения в челлендж
     *
     * @param array $data
     * @return bool
     */
    public function sendInvitations(array $data): bool
    {
        foreach ($data['ids'] as $participantId) {

            /** @var User $user */
            $user = User::query()->findOrFail($participantId);

            if ($user->isAlreadyDoingChallenge($data['challenge_id'])) {
                $this->errorMessage = $user->getFullName() . ' ' . trans('messages.invitations_already_sent');

                return false;
            }

            if (ChallengeInvitation::query()->where([['challenge_id', $data['challenge_id']], ['participant_id', $participantId]])->exists()) {
                /** @var ChallengeInvitation $invitation */
                $invitation = ChallengeInvitation::query()
                    ->where([['challenge_id', $data['challenge_id']], ['participant_id', $participantId]])
                    ->get()
                    ->first();
                $invitation->status = ChallengeInvitation::STATUS_SENT;
            } else {
                $invitation = new ChallengeInvitation([
                    'invitor_id'      => $data['invitor_id'],
                    'challenge_id'    => $data['challenge_id'],
                    'invitation_text' => $data['invitation_text'],
                    'participant_id'  => $participantId,
                ]);
            }

            if (!$invitation->save()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Принять участие вручную
     *
     * @param Challenge $challenge
     * @return bool
     */
    public function takePart(Challenge $challenge): bool
    {
        $challengeInvitation = ChallengeInvitation::query()
            ->where([['participant_id', Auth::id()], ['challenge_id', $challenge->id]])->get()->first();

        if (!$challengeInvitation) {
            $challengeInvitation = new ChallengeInvitation();
        }

        $challengeInvitation->fill([
            'participant_id' => Auth::id(),
            'invitor_id'     => Auth::id(),
            'challenge_id'   => $challenge->id,
            'status'         => ChallengeInvitation::STATUS_TAKE_PART_MANUALLY,
        ]);

        return $challengeInvitation->save();
    }

    /**
     * Отменяем участие в челлендже
     *
     * @param Challenge $challenge
     * @return bool
     */
    public function cancelParticipation(Challenge $challenge): bool
    {
        /** @var ChallengeInvitation $challengeInvitation */
        $challengeInvitation = ChallengeInvitation::query()
            ->where([['participant_id', Auth::id()], ['challenge_id', $challenge->id]])->get()->first();

        if (!$challenge) {
            return false;
        }

        $challengeInvitation->status = ChallengeInvitation::STATUS_CANCELLED_MANUALLY;

        return $challengeInvitation->save();
    }

    /**
     * Участники челленджа с активным статусом
     *
     * @param Challenge $challenge
     * @return int
     */
    public function getParticipantsCount(Challenge $challenge): int
    {
        return $challenge
            ->challengeInvitations()
            ->where('invitor_id', '!=', 'participant_id')
            ->whereIn('status', [ChallengeInvitation::STATUS_ACCEPTED, ChallengeInvitation::STATUS_TAKE_PART_MANUALLY, ChallengeInvitation::STATUS_DONE])
            ->get()
            ->count();
    }

    /**
     * Всего сделанных приглашений в челлендж
     *
     * @param Challenge $challenge
     * @return int
     */
    public function invitationsCount(Challenge $challenge): int
    {
        return $challenge->challengeInvitations()->count();
    }

    /**
     * Всего приглашений высланных вами
     *
     * @param Challenge $challenge
     * @return int
     */
    public function yourInvitationsCount(Challenge $challenge): int
    {
        return $challenge->challengeInvitations()->where([['invitor_id', Auth::id()], ['participant_id', '!=', Auth::id()]])->count();
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->errorMessage;
    }
}