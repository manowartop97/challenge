<?php
/**
 * Created by PhpStorm.
 * User: manowartop
 * Date: 03.12.2018
 * Time: 19:23
 */

namespace App\Repositories\Notification;

use App\Models\DB\Notification\Notification;
use Auth;

/**
 * Class NotificationRepository
 * @package App\Repositories\Notification
 */
class NotificationRepository
{
    /**
     * Сохраняем токен
     *
     * @param string $token
     * @return bool
     */
    public function storeToken(string $token): bool
    {
        $profile = Auth::user()->profile;
        if ($profile->device_tokens) {
            $tokens = json_decode(Auth::user()->profile->device_tokens);

            // Если такой токен уже есть
            if (in_array($token, $tokens)) {
                return true;
            }
        }

        $tokens[] = $token;

        $profile->device_tokens = \GuzzleHttp\json_encode($tokens);

        return $profile->save();
    }

    /**
     * Есть ли токен у юзера
     *
     * @return bool
     */
    public function checkToken(): bool
    {
        return Auth::user()->profile->device_tokens !== '';
    }

    /**
     * Создаем уведомление
     *
     * @param array $data
     * @return bool
     */
    public function createNotification(array $data): bool
    {
        $notification = new Notification($data);

        return $notification->save();
    }

    /**
     * Убираем уведомление из бара
     *
     * @param Notification $notification
     * @return bool
     */
    public function clearNotification(Notification $notification): bool
    {
        return $notification->fill(['status' => Notification::STATUS_READ])->save();
    }

    /**
     * Чистим все уведомления в категории
     *
     * @param string $category
     * @return bool
     */
    public function clearAllNotifications(string $category): bool
    {
        foreach (Notification::query()->where([['category', $category], ['status', Notification::STATUS_NEW]])->get()->all() as $notifiation) {
            if (!$this->clearNotification($notifiation)) {
                return false;
            }
        }

        return true;
    }

}