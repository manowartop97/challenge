<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 29.11.2018
 * Time: 14:04
 */

namespace App\Exceptions\Challenge;

use Exception;

/**
 * Экзепшн при генерации превью видео
 *
 * Class ThumbnailGenerationException
 * @package App\Exceptions\Challenge
 */
class ThumbnailGenerationException extends Exception
{

}