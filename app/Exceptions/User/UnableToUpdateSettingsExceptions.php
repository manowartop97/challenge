<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 16:42
 */

namespace App\Exceptions\User;

use Exception;

/**
 * Если не можем обновить настройки пользователя
 *
 * Class UnableToUpdateSettingsExceptions
 * @package App\Exceptions\User
 */
class UnableToUpdateSettingsExceptions extends Exception
{

}