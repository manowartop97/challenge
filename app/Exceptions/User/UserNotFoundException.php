<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 21.11.2018
 * Time: 12:18
 */

namespace App\Exceptions\User;

use Exception;
use Throwable;

/**
 * Class UserNotFoundException
 * @package App\Exceptions\User
 */
class UserNotFoundException extends Exception
{
    /**
     * UserNotFoundException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}