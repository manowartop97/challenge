<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 20.11.2018
 * Time: 17:24
 */

namespace App\Exceptions\User;

use Exception;

/**
 * Ошибка про неудачу обновления профиля
 *
 * Class UnableToUpdateProfileException
 * @package App\Exceptions\User
 */
class UnableToUpdateProfileException extends Exception
{

}