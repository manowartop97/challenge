<?php

namespace App\Console\Commands;

use App\Models\DB\User\User;
use App\Repositories\User\UserRepository;
use Illuminate\Console\Command;

class GenerateDefaultAvatarCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'default-avatars:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        foreach (User::all() as $user) {

            (new UserRepository())->generateDefaultAvatar($user);
            echo $user->email . PHP_EOL;
        }

        echo 'Finish' . PHP_EOL;
        return true;
    }
}
