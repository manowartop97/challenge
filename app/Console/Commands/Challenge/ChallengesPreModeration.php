<?php

namespace App\Console\Commands\Challenge;

use App\Models\DB\Challenge\Challenge;
use Illuminate\Console\Command;

class ChallengesPreModeration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'challenges:pre-moderate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pre-moderation of newly-created challenges';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** @var Challenge $challenge */
        foreach (Challenge::query()->where('status', Challenge::STATUS_IN_MODERATION)->get()->all() as $challenge) {
            // TODO логика проверок текста для модерации
            $challenge->status = Challenge::STATUS_ACTIVE;
            $challenge->save();
        }

        return true;
    }
}
