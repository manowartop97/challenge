<?php

namespace App\Console;

use App\Components\Settings\Facades\Settings;
use App\Console\Commands\ResetRewardStatusCommand;
use App\Console\Commands\Statistics\CollectStatisticsCommand;
use App\Console\Commands\Task\ProcessTask;
use App\Console\Commands\Task\ResumeErrorTasksCommand;
use App\Console\Commands\Task\ResumeTaskCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
