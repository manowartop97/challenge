<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 27.11.2018
 * Time: 14:02
 */

namespace App\Helpers;

/**
 * Проверяем тип файла
 *
 * Class FileTypeHelper
 * @package App\Helpers
 */
class FileTypeHelper
{
    /**
     * @var array
     */
    protected static $imageExtensions = [
        'jpg',
        'jpeg',
        'png',
        'bmp'
    ];

    /**
     * Проверка является ли файл картинкой
     *
     * @param string $fileName
     * @return bool
     */
    public static function isFileImage(string $fileName): bool
    {
        $exploded = explode('/', $fileName);
        $exploded = $exploded[count($exploded) - 1]; // Режем по слэшам, чтобы точно получить имя файла
        return in_array(explode('.', $exploded)[1], self::$imageExtensions);
    }
}