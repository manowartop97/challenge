<?php

namespace Tests\Unit\Friendship;

use App\Models\DB\Friendship\Friendship;
use App\Repositories\Friendship\FriendshipRepository;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\UserTrait;

/**
 * Тестирование френдов
 *
 * Class FriendshipTest
 * @package Tests\Unit\Friendship
 */
class FriendshipTest extends TestCase
{
    use DatabaseTransactions, UserTrait;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @var FriendshipRepository
     */
    private $friendshipRepository;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create('ru');
        $this->friendshipRepository = new FriendshipRepository();
    }

    /**
     * Создаем новую дружбу - должно вернуть тру
     *
     * @throws \Exception
     */
    public function testFriendshipStatusNew(): void
    {
        $this->withoutNotifications();
        $this->withoutEvents();
        $user = $this->createUser($this->faker);
        $this->actingAs($user);

        $friend = $this->createUser($this->faker);

        $response = $this->friendshipRepository->createFriendship($friend->id);

        /** @var Friendship $friendship */
        $friendship = Friendship::query()->where([['user_id', $user->id], ['friend_id', $friend->id]])->get()->first();

        $this->assertTrue($response);
        $this->assertTrue($friendship->friendship_status === Friendship::FRIENDSHIP_STATUS_NEW);
    }

    /**
     * Сначала кидаем дружбу, а потом отменяем - проверяем статус
     *
     * @throws \Exception
     */
    public function testFriendshipStatusRejectedByInvitor(): void
    {
        $this->withoutNotifications();
        $this->withoutEvents();
        $user = $this->createUser($this->faker);
        $this->actingAs($user);

        $friend = $this->createUser($this->faker);

        $response = $this->friendshipRepository->createFriendship($friend->id);

        $friendship = $this->getFriendshipModel($user->id, $friend->id);

        $this->assertTrue($response);
        $this->assertTrue($friendship->friendship_status === Friendship::FRIENDSHIP_STATUS_NEW);

        $response = $this->friendshipRepository->removeFriend($friend->id);

        $friendship = $this->getFriendshipModel($user->id, $friend->id);

        $this->assertTrue($response);
        $this->assertTrue($friendship->friendship_status === Friendship::FRIENDSHIP_STATUS_REJECTED_BY_INVITOR);
    }

    /**
     * Оклонияем предложение о дружбе другом
     *
     * @throws \Exception
     */
    public function testFriendshipRejectedByPotentialFriend(): void
    {
        $this->withoutNotifications();
        $this->withoutEvents();
        $user = $this->createUser($this->faker);
        $this->actingAs($user);

        $friend = $this->createUser($this->faker);

        $response = $this->friendshipRepository->createFriendship($friend->id);

        $friendship = $this->getFriendshipModel($user->id, $friend->id);

        $this->assertTrue($response);
        $this->assertTrue($friendship->friendship_status === Friendship::FRIENDSHIP_STATUS_NEW);

        $this->actingAs($friend);
        $response = $this->friendshipRepository->rejectFriendship($user->id);
        $this->assertTrue($response);
        $friendship = $this->getFriendshipModel($user->id, $friend->id);
        $this->assertTrue($friendship->friendship_status === Friendship::FRIENDSHIP_STATUS_REJECTED_BY_POTENTIAL_FRIEND);
    }

    /**
     * Создаем дружбу, отменяем, а после перелогиниваемся за друга и создаем дружбу этому пользователю
     * user_id и friend_id должны поменяться местами
     *
     * @throws \Exception
     */
    public function testCreateAndRemoveFriendshipAndCreateByPotentialFriend(): void
    {
        $this->withoutNotifications();
        $this->withoutEvents();
        $user = $this->createUser($this->faker);
        $this->actingAs($user);

        $friend = $this->createUser($this->faker);

        $this->friendshipRepository->createFriendship($friend->id);
        $this->friendshipRepository->removeFriend($friend->id);

        $this->actingAs($friend);
        $this->friendshipRepository->createFriendship($user->id);
        // По идее должны поменяться user_id и friend_id (по моей логике)
        $friendship = $this->getFriendshipModel($friend->id, $user->id);

        $this->assertTrue($friendship->friendship_status === Friendship::FRIENDSHIP_STATUS_NEW);
    }

    /**
     * Принимаем дружбу
     *
     * @throws \Exception
     */
    public function testAcceptFriendship(): void
    {
        $this->withoutNotifications();
        $this->withoutEvents();
        $user = $this->createUser($this->faker);
        $this->actingAs($user);

        $friend = $this->createUser($this->faker);

        $response = $this->friendshipRepository->createFriendship($friend->id);
        $this->assertTrue($response);

        $this->actingAs($friend);
        $response = $this->friendshipRepository->acceptFriendship($user->id);
        $friendship = $this->getFriendshipModel($user->id, $friend->id);
        $this->assertTrue($response);
        $this->assertTrue($friendship->friendship_status === Friendship::FRIENDSHIP_STATUS_ACCEPTED);
    }

    /**
     * @param int $userId
     * @param int $friendId
     * @return Friendship
     */
    protected function getFriendshipModel(int $userId, int $friendId): Friendship
    {
        return Friendship::query()->where([['user_id', $userId], ['friend_id', $friendId]])->get()->first();
    }
}
