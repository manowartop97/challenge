<?php

namespace Tests\Unit\Auth;

use App\Exceptions\User\UserNotFoundException;
use App\Repositories\User\UserRepository;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Тест возобновления пароля
 *
 * Class ForgotPasswordTest
 * @package Tests\Unit\Auth
 */
class ForgotPasswordTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var Generator
     */
    private $faker;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create('ru');
    }

    /**
     * Просим восстановления пароля с левым мылом через ajax
     *
     * @return void
     */
    public function testForgotPasswordWithWrongEmailViaAjax(): void
    {
        $email = str_random(50) . '@gmail.com';
        $response = $this->post(route('profile-settings.forgot-password'), ['email' => $email], ['HTTP_X-Requested-With' => 'XMLHttpRequest']);
        // Получаем ошибки валидации
        $response->assertSessionHasErrors();
    }

    /**
     * Тестируем сброс пароля с левым мылом напрямую
     *
     * @throws UserNotFoundException
     */
    public function testForgotPasswordWithWrongEmail()
    {
        $this->expectException(UserNotFoundException::class);
        (new UserRepository())->recoverPassword(str_random(50) . '@gmail.com');
    }

    /**
     * Пробуем удачный рекавер пароль
     * @throws UserNotFoundException
     */
    public function testForgotPasswordSuccess()
    {
        $this->withoutNotifications();
        $email = $this->faker->email;

        \DB::table('users')->insert([
            'email'    => $email,
            'password' => bcrypt($this->faker->password)
        ]);

        $this->assertTrue((new UserRepository())->recoverPassword($email));
    }
}
