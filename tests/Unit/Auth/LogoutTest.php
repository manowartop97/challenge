<?php

namespace Tests\Unit\Auth;

use App\Models\DB\User\User;
use Tests\TestCase;

/**
 * Class LogoutTest
 * @package Tests\Unit\Auth
 */
class LogoutTest extends TestCase
{
    /**
     * Логаут редирект
     */
    public function testLogoutTest(): void
    {
        /** @var User $user */
        $user = User::query()->findOrFail(1);
        $this->actingAs($user)->get(route('logout'))->assertLocation('/auth');
    }
}
