<?php

namespace Tests\Unit\Auth;

use App\Models\DB\User\User;
use App\Repositories\User\UserRepository;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Тест сброса пароля
 *
 * Class ResetPasswordTest
 * @package Tests\Unit
 */
class ResetPasswordTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var Generator
     */
    private $faker;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create('ru');
    }


    /**
     * Тестируем сброс пароля с невалидными данными
     */
    public function testResetPasswordWithErrors()
    {
        $this->actingAs($this->registerUserAndGetUser());

        $this->assertFalse((new UserRepository())->changePassword([
            'old_password'              => $this->faker->password,
            'new_password'              => $this->faker->password,
            'new_password_confirmation' => $this->faker->password
        ]));
    }

    /**
     * Делаем сброс пароля с валидными данными
     */
    public function testResetPasswordWithSuccess()
    {
        $this->withoutNotifications();
        $this->actingAs($this->registerUserAndGetUser('123456'));

        $newPassword = $this->faker->password;
        $this->assertTrue((new UserRepository())->changePassword([
            'old_password'              => '123456',
            'new_password'              => $newPassword,
            'new_password_confirmation' => $newPassword
        ]));
    }

    /**
     * Регистрируем рандомного юзера и возвращаем его
     *
     * @param string|null $password
     * @return User
     */
    private function registerUserAndGetUser(string $password = null): User
    {
        $password = is_null($password) ? $this->faker->password : $password;
        $email = $this->faker->email;

        \DB::table('users')->insert([
            'email'    => $email,
            'password' => bcrypt($password)
        ]);

        return User::query()->where('email', $email)->get()->first();
    }
}
