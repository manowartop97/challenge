<?php

namespace Tests\Unit\Auth;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class LoginTest
 * @package Tests\Unit\Auth
 */
class LoginTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create('ru');
    }


    /**
     * Логинимся с невалидными данными
     */
    public function testInvalidLoginCredentials()
    {
        $data = [
            'email'    => $this->faker->email,
            'password' => $this->faker->password
        ];

        $response = $this->post(route('login'), $data);
        $response->assertLocation('/');
    }

    /**
     * Логинимся с валидными данными
     */
    public function testValidLoginCredentials()
    {
        $password = $this->faker->password;
        $email = $this->faker->email;

        \DB::table('users')->insert([
            'email'    => $email,
            'password' => bcrypt($password)
        ]);

        $response = $this->post(route('login'), ['email' => $email, 'password' => $password]);
        $response->assertLocation('/profile/feed');
    }
}
