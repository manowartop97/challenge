<?php

namespace Tests\Unit\Auth;

use Faker\Factory;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Проверка регистрации
 *
 * Class RegistrationTest
 * @package Tests\Feature\Auth
 */
class RegistrationTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create('ru');
    }

    /**
     * Даем на вход неверную дату - ждем ошибки валидации
     *
     * @return void
     */
    public function testRegistrationValidationFail(): void
    {
        $data = [
            'surname'               => $this->faker->firstName,
            'name'                  => $this->faker->lastName,
            'email'                 => $this->faker->email,
            'password'              => '123456',
            'password_confirmation' => '123456',
            'birthday'              => '01/01/2001',
            'gender'                => 1,
            'agreed'                => 'on'
        ];

        $response = $this->post(route('register'), $data);
        $response->assertSessionHasErrors();
    }

    /**
     * Успешна регистрация
     *
     * @return void
     */
    public function testRegistrationWithSuccess(): void
    {
        $this->withoutNotifications();
        $data = [
            'surname'               => $this->faker->firstName,
            'name'                  => $this->faker->lastName,
            'email'                 => $this->faker->email,
            'password'              => '123456',
            'password_confirmation' => '123456',
            'birthday'              => '01.01.2001',
            'gender'                => 1,
            'agreed'                => 'on'
        ];

        $response = $this->post(route('register'), $data);
        $response->assertLocation('/profile/feed');
    }
}
