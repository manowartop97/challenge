<?php
/**
 * Created by PhpStorm.
 * User: hellenmicky
 * Date: 27.11.2018
 * Time: 10:53
 */

namespace Tests\Traits;

use App\Models\DB\User\User;
use Exception;
use Faker\Generator;

/**
 * Trait UserTrait
 * @package Tests\Traits
 */
trait UserTrait
{
    /**
     * @param Generator $faker
     * @return User
     * @throws Exception
     */
    public function createUser(Generator $faker): User
    {
        $user = new User([
            'email'    => $faker->email,
            'password' => bcrypt($faker->password)
        ]);

        if (!$user->save()) {
            throw new Exception('Error while user creation in UserTrait');
        }

        return $user;
    }
}