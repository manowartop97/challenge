$(document).ready(function () {

    var loader = $('#preloader');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var title = document.getElementById('title_image');

    if (title) {
        /**
         * Тайтл имедж превью
         */
        title.addEventListener('change', function (event) {
            loader.show();
            var previewImage = $('.title_preview').find('img');
            previewImage.attr('src', '');

            $.each(event.target.files, function (index, file) {
                var fileReader = new FileReader();
                fileReader.onload = function () {
                    previewImage.attr('src', fileReader.result);
                };
                fileReader.readAsDataURL(file);
            });
            loader.hide();
        });

        $('#tags').tagInput({
            tagDataSeparator: '|',
            allowDuplicates: false,
            typeahead: true,
            typeaheadOptions: {
                highlight: true
            },
            // typehead dataset options
            typeaheadDatasetOptions: {
                displayKey: 'tag',
                source: tags.ttAdapter()
            }
        });
    }

    /**
     * Превьюим доп аттачи
     */
    var element = document.getElementById('additional_media');
    if (element) {
        element.addEventListener('change', function (event) {
            loader.show();
            $('#preview').html('');
            $.each(event.target.files, function (index, file) {
                var fileReader = new FileReader();
                if (file.type.match('image')) {
                    fileReader.onload = function () {

                        // var imageDiv = document.createElement('div');
                        // imageDiv.className = 'image-block';
                        // imageDiv.style.backgroundImage = "url("+ fileReader.result +")";
                        // document.getElementById('preview').appendChild(imageDiv);
                        var html = '<a href="' + fileReader.result + '" class="col col-4-width"><img src="' + fileReader.result + '" alt="photo"></a>';
                        $(html).appendTo('#preview');

                    };
                    fileReader.readAsDataURL(file);
                } else {
                    fileReader.onload = function () {
                        var blob = new Blob([fileReader.result], {type: file.type});
                        var url = URL.createObjectURL(blob);
                        console.log(file);
                        var video = document.createElement('video');
                        var timeupdate = function () {
                            if (snapImage()) {
                                video.removeEventListener('timeupdate', timeupdate);
                                video.pause();
                            }
                        };
                        video.addEventListener('loadeddata', function () {
                            if (snapImage()) {
                                video.removeEventListener('timeupdate', timeupdate);
                            }
                        });
                        var snapImage = function () {
                            var canvas = document.createElement('canvas');
                            canvas.width = video.videoWidth;
                            canvas.height = video.videoHeight;
                            canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
                            var image = canvas.toDataURL();
                            var success = image.length > 100000;
                            if (success) {
                                var html = '<div class="video-player custom col col-4-width" style="margin: 0px;">\n' +
                                    '<img src="' + image + '" alt="photo">\n' +
                                    '<a href="' + url + '" class="play-video" style="opacity: 1">\n' +
                                    ' <svg class="olymp-play-icon" xmlns="http://www.w3.org/2000/svg" width="14" height="16" viewBox="0 0 14 16"><path fill="#9A9FBF" d="M12.355 5.332L4.548.545C3.975.196 3.392.021 2.809.021 1.413.021 0 1.115 0 3.205V6h2V3.204c0-.749.314-1.184.809-1.184.204 0 .438.074.694.229l7.815 4.792c.879.533.924 1.406.044 1.939l-7.859 4.79c-.256.157-.491.23-.695.23C2.314 14 2 13.566 2 12.818v-.793-.024H0V12.818C0 14.907 1.413 16 2.808 16c.583 0 1.167-.175 1.735-.521l7.86-4.79C13.416 10.074 14 9.107 14 8.038c.001-1.086-.599-2.072-1.645-2.706z"></path><path fill="#9A9FBF" d="M0 8h2v2H0z"></path></svg>                                                        </a>\n' +
                                    '\n' +
                                    '<div class="video-content">\n' +
                                    '</div>\n' +
                                    '\n' +
                                    '<div class="overlay"></div>\n' +
                                    '</div>';

                                $(html).appendTo('#preview');

                                URL.revokeObjectURL(url);
                            }
                            return success;
                        };
                        video.addEventListener('timeupdate', timeupdate);
                        video.preload = 'metadata';
                        video.src = url;
                        // Load video in Safari / IE11
                        video.muted = true;
                        video.playsInline = true;
                        video.play();
                    };
                    fileReader.readAsArrayBuffer(file);
                }
            });
            $('.clearMedia').show();
            loader.hide();
        });
    }

    /**
     * Форма создания челленджа
     */
    $(document).on('submit', '.challenge_form', function (e) {
        e.preventDefault();
        loader.show();
        var form = $($(this));

        var formData = new FormData();

        form.find('.text-error').text('');

        formData.append('user_id', form.find('#user_id').val());
        formData.append('title_image', form.find('#title_image')[0].files[0]);
        formData.append('deadline', form.find('#deadline').val());
        formData.append('title', form.find('#title').val());
        formData.append('tags', form.find('#tags').val());
        // formData.append('is_group_challenge', form.find('#is_group_challenge').is(':checked') ? 1 : 0);
        formData.append('is_group_challenge', 0);
        formData.append('description', form.find('#description').val());

        // Собираем аттачменты
        $.each(form.find('#additional_media')[0].files, function (index, element) {
            formData.append('additional_media[' + index + ']', element);
        });

        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                window.location.replace(response.data.route);
            },
            error: function (response) {
                console.log(response);
                loader.hide();
                if (typeof response.responseJSON.data !== 'undefined') {
                    $('body').overhang({
                        type: "error",
                        message: response.responseJSON.data.message,
                        duration: '5'
                    });
                } else {
                    if (typeof response.responseText !== 'undefined') {
                        var errors = $.parseJSON(response.responseText);
                        $.each(errors.errors, function (key, val) {
                            if ($.isNumeric(key.search('.'))) {
                                key = key.split('.')[0];
                            }

                            form.find('#' + key + '-error').text(val[0]);
                        });
                    }
                }
            }
        });
    });

    /**
     * Форма подтверждения челленджа
     */
    $(document).on('submit', '.confirm_challenge_form', function (e) {
        e.preventDefault();
        loader.show();
        var form = $($(this));

        var formData = new FormData();

        form.find('.text-error').text('');

        formData.append('challenge_id', form.find('#challenge_id').val());
        formData.append('comment', form.find('#comment').val());
        // Собираем аттачменты
        $.each(form.find('#additional_media')[0].files, function (index, element) {
            formData.append('additional_media[' + index + ']', element);
        });

        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '5'
                });
                setTimeout(function () {
                    window.location.replace(response.data.route);
                }, 2000);

            },
            error: function (response) {
                console.log(response);
                loader.hide();
                if (typeof response.responseJSON.data !== 'undefined') {
                    $('body').overhang({
                        type: "error",
                        message: response.responseJSON.data.message,
                        duration: '5'
                    });
                } else {
                    if (typeof response.responseText !== 'undefined') {
                        var errors = $.parseJSON(response.responseText);
                        $.each(errors.errors, function (key, val) {
                            if ($.isNumeric(key.search('.'))) {
                                key = key.split('.')[0];
                            }

                            form.find('#' + key + '-error').text(val[0]);
                        });
                    }
                }
            }
        });
    });

    /**
     * Форма обновления челленджа
     */
    $(document).on('submit', '.edit_challenge_form', function (e) {
        e.preventDefault();
        loader.show();
        var form = $($(this));

        var formData = new FormData();

        form.find('.text-error').text('');

        formData.append('user_id', form.find('#user_id').val());
        formData.append('is_title_image', form.find('#is_title_image').val());
        formData.append('is_additional_media', form.find('#is_additional_media').val());
        formData.append('title_image', form.find('#title_image')[0].files[0] ? form.find('#title_image')[0].files[0] : '');
        formData.append('deadline', form.find('#deadline').val());
        formData.append('title', form.find('#title').val());
        formData.append('tags', form.find('#tags').val());
        // formData.append('is_group_challenge', form.find('#is_group_challenge').is(':checked') ? 1 : 0);
        formData.append('is_group_challenge', 0);
        formData.append('description', form.find('#description').val());

        // Собираем аттачменты
        $.each(form.find('#additional_media')[0].files, function (index, element) {
            formData.append('additional_media[' + index + ']', element);
        });

        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                window.location.replace(response.data.route);
            },
            error: function (response) {
                console.log(response);
                loader.hide();
                if (typeof response.responseJSON.data !== 'undefined') {
                    $('body').overhang({
                        type: "error",
                        message: response.responseJSON.data.message,
                        duration: '5'
                    });
                } else {
                    if (typeof response.responseText !== 'undefined') {
                        var errors = $.parseJSON(response.responseText);
                        $.each(errors.errors, function (key, val) {
                            if ($.isNumeric(key.search('.'))) {
                                key = key.split('.')[0];
                            }

                            form.find('#' + key + '-error').text(val[0]);
                        });
                    }
                }
            }
        });
    });

    /**
     * Очистка дополнительных аттачментов
     */
    $(document).on('click', '.clear_additional_media', function (e) {
        e.preventDefault();

        $(this).closest('form').find('#preview').html('');
        $(this).closest('form').find('#additional_media').val('');
        $(this).closest('form').find('#is_additional_media').val(0);
        $(this).hide();
    });

});