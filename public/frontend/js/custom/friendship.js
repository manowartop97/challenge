$(document).ready(function () {

    var loader = $('#preloader');

    /**
     * Отправление заявки в друзья
     */
    $(document).on('click', '.invite_friend_button', function (e) {
        e.preventDefault();
        loader.show();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {

                button.data('route', response.data.route);
                button.removeClass('bg-green invite_friend_button');
                button.addClass('bg-danger remove_friend_button');
                button.attr('data-original-title', response.data.toggle_message);
                button.find('.icon-add').removeClass('icon-add').addClass('icon-minus');
                loader.hide();
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '2'
                });
            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Отмена заявки в друзья
     */
    $(document).on('click', '.remove_friend_button', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.data('route', response.data.route);
                button.removeClass('bg-danger remove_friend_button');
                button.addClass('bg-green invite_friend_button');
                button.attr('data-original-title', response.data.toggle_message);
                button.find('.icon-minus').removeClass('icon-minus').addClass('icon-add');
                $('body').overhang({
                    type: "warn",
                    message: response.data.message,
                    duration: '2'
                });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Принять заявку в друзья со страницы реквестов
     */
    $(document).on('click', '.accept_friendship_request_page', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.parent().parent().remove();
                // $('body').overhang({
                //     type: "success",
                //     message: response.data.message,
                //     duration: '2'
                // });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Принять заявку в друзья со страницы реквестов
     */
    $(document).on('click', '.accept_friendship_from_notifications, .reject_friendship_from_notifications', function (e) {
        loader.show();
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function () {
                var removeBtn = button.closest('li').find('.removeNotification');
                $.ajax({
                    method: 'get',
                    url: removeBtn.data('route'),
                    success: function (response) {
                        $('.friendshipNotificationsCount').text(response.data.count);
                        removeBtn.closest('li').fadeOut(300, function () {
                            $(this).remove();
                        });
                        loader.hide();
                    },
                    error: function (response) {
                        loader.hide();
                        $('body').overhang({
                            type: "error",
                            message: response.responseJSON.data.message,
                            duration: '5'
                        });
                    }
                });
            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Отклонить заявку в друзья со страницы реквестов
     */
    $(document).on('click', '.reject_friendship_request_page', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.parent().parent().remove();
                // $('body').overhang({
                //     type: "warn",
                //     message: response.data.message,
                //     duration: '2'
                // });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Отозвать заявку в друзья со страницы реквестов
     */
    $(document).on('click', '.remove_friend_button_request_page', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.parent().parent().remove();
                // $('body').overhang({
                //     type: "warn",
                //     message: response.data.message,
                //     duration: '2'
                // });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Принять заявку в друзья со страницы юзера
     */
    $(document).on('click', '.accept_friendship_invitation', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.parent().remove();
                var html = '<a href="" data-route="' + response.data.route + '" class="btn btn-control bg-danger remove_friend_button"><span class="icon-minus" style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;"> <svg class="" olymp-happy-face-icon="" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"> <title>happy-face-icon</title> <path d="M16 0c-8.837 0-16 7.16-16 15.989 0 7.166 4.715 13.227 11.213 15.262v-3.39c-4.69-1.899-8-6.49-8-11.859 0-7.070 5.731-12.802 12.8-12.802s12.8 5.731 12.8 12.802c0 5.37-3.312 9.96-8 11.859v3.378c6.485-2.040 11.187-8.094 11.187-15.25 0-8.829-7.165-15.989-16-15.989zM11.211 12.8h-3.2v3.202h3.2v-3.202zM20.813 12.8v3.202h3.2v-3.202h-3.2zM11.198 19.365c0 1.675 2.146 3.032 4.794 3.032s4.794-1.357 4.794-3.032v-0.16h-9.587v0.16zM14.413 32.002h3.2v-3.2h-3.2v3.2z"></path> </svg></span></a>';
                $(html).prependTo('.control-block-button');
                // $('body').overhang({
                //     type: "warn",
                //     message: response.data.message,
                //     duration: '2'
                // });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Отозвать заявку в друзья со страницы юзера
     */
    $(document).on('click', '.reject_friendship_invitation', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.parent().remove();
                var html = '<a href="#" data-route="' + response.data.route + '" class="btn btn-control bg-green invite_friend_button"><span class="icon-add" style=" margin-right: 0px !important;padding-right: 5px;margin-bottom: 3px;"> <svg class="" olymp-happy-face-icon="" version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"> <title>happy-face-icon</title> <path d="M16 0c-8.837 0-16 7.16-16 15.989 0 7.166 4.715 13.227 11.213 15.262v-3.39c-4.69-1.899-8-6.49-8-11.859 0-7.070 5.731-12.802 12.8-12.802s12.8 5.731 12.8 12.802c0 5.37-3.312 9.96-8 11.859v3.378c6.485-2.040 11.187-8.094 11.187-15.25 0-8.829-7.165-15.989-16-15.989zM11.211 12.8h-3.2v3.202h3.2v-3.202zM20.813 12.8v3.202h3.2v-3.202h-3.2zM11.198 19.365c0 1.675 2.146 3.032 4.794 3.032s4.794-1.357 4.794-3.032v-0.16h-9.587v0.16zM14.413 32.002h3.2v-3.2h-3.2v3.2z"></path> </svg></span></a>';
                $(html).prependTo('.control-block-button');
                // $('body').overhang({
                //     type: "warn",
                //     message: response.data.message,
                //     duration: '2'
                // });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Удалить из друзей со страницы друзей в профиле
     */
    $(document).on('click', '.remove_from_friendlist', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function () {

                button.parent().parent().parent().parent().parent().parent().parent().parent().fadeOut(300, function () {
                    $(this).remove();
                });

                // $('body').overhang({
                //     type: "warn",
                //     message: response.data.message,
                //     duration: '2'
                // });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Отправка заявки в друзья со страницы чужих друзей
     */
    $(document).on('click', '.add_friend_from_page_friends', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.data('route', response.data.route);
                button.find('.icon-add').removeClass('icon-add').addClass('icon-minus');
                button.removeClass('bg-green add_friend_from_page_friends');
                button.addClass('bg-danger remove_friend_from_page_friends');
                button.attr('data-original-title', response.data.toggle_message);
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '2'
                });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Отмена заявки в друзья со страницы чужих друзей
     */
    $(document).on('click', '.remove_friend_from_page_friends', function (e) {
        e.preventDefault();
        var button = $(this);
        $.ajax({
            url: button.data('route'),
            method: 'GET',
            success: function (response) {
                button.data('route', response.data.route);
                button.find('.icon-minus').removeClass('icon-minus').addClass('icon-add');
                button.removeClass('bg-danger remove_friend_from_page_friends');
                button.addClass('bg-green add_friend_from_page_friends');
                button.attr('data-original-title', response.data.toggle_message);
                $('body').overhang({
                    type: "warn",
                    message: response.data.message,
                    duration: '2'
                });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

});