$(document).ready(function () {

    /**
     * Открываем файлинпут при клике на загрузить с компа
     */
    $(document).on('click', '.avatar_from_computer', function (e) {
        e.preventDefault();
        $('.upload_from_computer_input').trigger('click');
    });

    /**
     * Открываем файлинпут при клике на загрузить с компа
     */
    $(document).on('click', '.background_from_computer', function (e) {
        e.preventDefault();
        $('.upload_background_from_computer_input').trigger('click');
    });

    // /**
    //  * Сабмит формы при выборе картинки
    //  */
    // $(document).on('change', '.upload_from_computer_input', function () {
    //
    // });

});