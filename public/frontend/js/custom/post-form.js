$(document).ready(function () {
    var loader = $('#preloader');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Форма создания поста
     */
    $(document).on('click', '.submit-post-button', function (e) {
        e.preventDefault();
        loader.show();
        var form = $('.create_post_form'),
            postText = form.find('.post-text');

        var formData = new FormData();

        form.find('.text-error').text('');

        // Собираем аттачменты
        $.each(form.find('#post_attachments')[0].files, function (index, element) {
            formData.append('post_attachments[' + index + ']', element);
        });

        formData.append('text', postText.val());
        formData.append('user_id', form.find('.hidden_user_id').val());
        formData.append('author_id', form.find('.hidden_author_id').val());

        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {

                $('#preview').html('');
                postText.val('');

                var lastFeatured = $('[data-featured="1"]').last();
                if (lastFeatured.length <= 0) {
                    $(response.data.post).prependTo('#newsfeed-items-grid');
                } else {
                    lastFeatured.after(response.data.post);
                }
                loader.hide();

            },
            error: function (response) {
                loader.hide();
                if (typeof response.responseText !== 'undefined') {
                    var errors = $.parseJSON(response.responseText);
                    $.each(errors.errors, function (key, val) {
                        if ($.isNumeric(key.search('.'))) {
                            key = key.split('.')[0];
                        }

                        form.find('#' + key + '-error').text(val[0]);
                    });
                }
            }
        });
    });


    /**
     * Ставим/снимаем лукасы
     */
    $(document).on('click', '.like_btn', function (e) {
        e.preventDefault();

        var btn = $(this);
        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function (response) {
                if (btn.parent().parent().find('.like_btn_sm').hasClass('is-liked-sm')) {
                    btn.parent().parent().find('.like_btn_sm').data('route', response.data.route).removeClass('is-liked-sm').find('span').text(response.data.count);
                    btn.parent().parent().find('.like_btn_lg').data('route', response.data.route).removeClass('is-liked-lg');
                } else {
                    btn.parent().parent().find('.like_btn_sm').data('route', response.data.route).addClass('is-liked-sm').find('span').text(response.data.count);
                    btn.parent().parent().find('.like_btn_lg').data('route', response.data.route).addClass('is-liked-lg');
                }
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Удалить пост
     */
    $(document).on('click', '.delete_btn', function (e) {
        e.preventDefault();
        loader.show();
        var btn = $(this);
        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function () {
                btn.closest('.ui-block').fadeOut(300, function () {
                    $(this).remove();
                });
                loader.hide();
            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
        // $.sweetModal.confirm(btn.data('message'), function () {
        //
        // });
    });


    /**
     * Форма редактирования поста
     */
    $(document).on('click', '.edit_post_btn', function (e) {
        e.preventDefault();
        loader.show();
        var btn = $(this);
        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function (response) {
                btn.parent().parent().parent().parent().parent().parent().html(response.data.view);
                loader.hide();
            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Очищаем аттачменты формы
     */
    $(document).on('click', '.clearAttachments', function (e) {
        e.preventDefault();
        $(this).parent().find('#preview').html('');
        var fileInput = $(this).closest('form').find('[type=file]');
        fileInput.val('').attr('data-files', 0);
        // $.sweetModal.confirm($(this).data('message'), function () {
        //
        // });
    });

    /**
     * Апдейт поста
     */
    $(document).on('click', '.edit-post-submit-button', function (e) {
        e.preventDefault();
        loader.show();
        var form = $('.update_post_form'),
            postText = form.find('.post-text'),
            formData = new FormData(),
            postAttachmentsInput = form.find('#post_edit_attachments');


        form.find('.text-error').text('');
        // Собираем аттачменты
        $.each(postAttachmentsInput[0].files, function (index, element) {
            formData.append('post_attachments[' + index + ']', element);
        });

        formData.append('text', postText.val());
        formData.append('user_id', form.find('.hidden_user_id').val());
        formData.append('author_id', form.find('.hidden_author_id').val());
        formData.append('is_files', postAttachmentsInput.attr('data-files'));

        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (response) {
                form.parent().html(response.data.view);
                loader.hide();
            },
            error: function (response) {
                loader.hide();
                if (typeof response.responseText !== 'undefined') {
                    var errors = $.parseJSON(response.responseText);
                    $.each(errors.errors, function (key, val) {
                        if ($.isNumeric(key.search('.'))) {
                            key = key.split('.')[0];
                        }
                        form.find('#' + key + '-error').text(val[0]);
                    });
                }
            }
        });
    });

    /**
     * Закрепить запись на стене
     */
    $(document).on('click', '.make_featured', function (e) {
        e.preventDefault();
        loader.show();
        var btn = $(this);

        btn.hasClass('is_featured') ? btn.removeClass('is_featured') : btn.addClass('is_featured');

        $.ajax({
            method: 'GET',
            url: btn.data('route'),
            success: function () {
                window.location.reload();
            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        })
    });

});