$(document).ready(function () {

    /**
     * Сабмит формы изменения статуса
     */
    $(document).on('click', '.status-form-submit', function (e) {
        e.preventDefault();
        var form = $('.status-form');

        $.ajax({
            method: 'POST',
            url: form.data('route'),
            data: form.serialize(),
            success: function (response) {
                $('.status_string_text').text($('.status_string_input').val());
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '2'
                });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

});
