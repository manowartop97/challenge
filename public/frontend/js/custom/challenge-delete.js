$(document).ready(function () {

    var loader = $('#preloader');

    /**
     * Удаление челленджа с индекс страницы
     */
    $(document).on('click', '.deleteChallengeFromIndex', function (e) {
        e.preventDefault();
        var btn = $(this);
        var yes = btn.data('yes');
        var no = btn.data('no');
        $.confirm({
            title: btn.data('title'),
            content: '',
            buttons: {
                yes: {
                    text: yes,
                    action: function () {
                        loader.show();
                        $.ajax({
                            method: 'GET',
                            url: btn.data('route'),
                            success: function (response) {
                                renewChallenges();
                                loader.hide();
                                $('body').overhang({
                                    type: "success",
                                    message: response.data.message,
                                    duration: '2'
                                });
                            },
                            error: function (response) {
                                loader.hide();
                                $('body').overhang({
                                    type: "error",
                                    message: response.responseJSON.data.message,
                                    duration: '5'
                                });
                            }
                        });
                    },
                },
                no: {
                    text: no,
                    action: function () {

                    }
                },

            }
        });
    });

    /**
     * Возобновление челленджа
     */
    $(document).on('click', '.renewChallengeBtn', function (e) {
        e.preventDefault();

        var btn = $(this);
        loader.show();
        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function (response) {
                // Возобновляем поиск
                renewChallenges();
                loader.hide();
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '2'
                });


            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Удаление челленджа с detail страницы
     */
    $(document).on('click', '.deleteChallengeFromDetail', function (e) {
        e.preventDefault();
        var btn = $(this);
        var yes = btn.data('yes');
        var no = btn.data('no');
        $.confirm({
            title: btn.data('title'),
            content: '',
            buttons: {
                yes: {
                    text: yes,
                    action: function () {
                        loader.show();
                        $.ajax({
                            method: 'GET',
                            url: btn.data('route'),
                            success: function (response) {
                                window.location.replace(response.data.route);
                            },
                            error: function (response) {
                                loader.hide();
                                $('body').overhang({
                                    type: "error",
                                    message: response.responseJSON.data.message,
                                    duration: '5'
                                });
                            }
                        });
                    },
                },
                no: {
                    text: no,
                    action: function () {

                    }
                },

            }
        });
    });

    function renewChallenges() {
        var links = document.location.search;
        if (links) {
            var params = links.split('?');
            params = params[1].split('&');
            var status = 0,
                title = '',
                page = '';

            $.each(params, function (index, value) {
                var splitVal = value.split('=');

                if (splitVal[0] === 'status') {
                    status = splitVal[1];
                } else if (splitVal[0] === 'title') {
                    title = splitVal[1];
                } else {
                    page = splitVal[1];
                }


            }, status, title, page);

            $.ajax({
                method: 'GET',
                url: $('.filter_route').data('route'),
                data: {status: status, title: title, page: page},
                success: function (response) {
                    $('.challenges_data').html(response.data.view);
                    loader.hide();
                }
            });

        }
    }

});