$(document).ready(function () {

    var loader = $('#preloader');

    var activeTab = localStorage.getItem('tab');
    if (typeof activeTab !== 'undefined') {
        var loginTab = $('.loginTab'),
            registerTab = $('.registerTab'),
            registerTabPane = $('#register'),
            loginTabPane = $('#login');
        if (activeTab == 1) {
            loginTab.removeClass('active');
            loginTabPane.removeClass('active');
            registerTab.addClass('active');
            registerTabPane.addClass('active');
        } else {
            registerTab.removeClass('active');
            registerTabPane.removeClass('active');
            loginTab.addClass('active');
            loginTabPane.addClass('active');
        }
    }

    /**
     * Активный таб при перезагрузке страницы
     */
    $(document).on('click', '.authTabLink', function () {
        localStorage.setItem('tab', $(this).data('id'));
    });


    /**
     * Ajax submit формы восстановления пароля
     */
    $(document).on('submit', '.forgot-form', function (e) {
        e.preventDefault();
        loader.show();
        var form = $(this);

        $.ajax({
            url: form.data('route'),
            method: 'POST',
            data: form.serialize(),
            success: function (response) {
                loader.hide();
                $('#forgot-password').modal('toggle');
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '5'
                });
            },
            error: function (response) {
                loader.hide();
                if (typeof response.responseJSON.data === 'undefined') {
                    $('#email-error').text(response.responseJSON.errors.email[0]);
                } else {
                    $('body').overhang({
                        type: "error",
                        message: response.responseJSON.data.message,
                        duration: '5'
                    });
                }
            }
        });
    })

});