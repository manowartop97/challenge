/**
 * Created by manowartop on 01.12.2018.
 */
$(document).ready(function () {

    var loader = $('#preloader');

    // Возобновляем поиск
    var links = document.location.search;
    if (links) {
        var params = links.split('?');
        params = params[1].split('&');
        var status = 0,
            title = '',
            page = '';

        $.each(params, function (index, value) {
            var splitVal = value.split('=');

            if (splitVal[0] === 'status') {
                status = splitVal[1];
            } else if (splitVal[0] === 'title') {
                title = splitVal[1];
            } else {
                page = splitVal[1];
            }


        }, status, title, page);

        $.ajax({
            method: 'GET',
            url: $('.filter_route').data('route'),
            data: {status: status, title: title, page: page},
            success: function (response) {
                $('.challenges_data').html(response.data.view);
                loader.hide();
            }
        });

    }

    var updateQueryStringParam = function (key, value) {
        if (!value) {
            return;
        }
        var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
            urlQueryString = document.location.search,
            newParam = key + '=' + value,
            params = '?' + newParam;

        // If the "search" string exists, then build params from it
        if (urlQueryString) {
            keyRegex = new RegExp('([\?&])' + key + '[^&]*');

            // If param exists already, update it
            if (urlQueryString.match(keyRegex) !== null) {
                params = urlQueryString.replace(keyRegex, "$1" + newParam);
            } else { // Otherwise, add it to end of query string
                params = urlQueryString + '&' + newParam;
            }
        }
        window.history.replaceState({}, "", baseUrl + params);
    };

    /**
     * Фильтрация по статусу
     */
    $(document).on('click', '.filter_status', function (e) {
        e.preventDefault();
        loader.show();
        var btn = $(this),
            status = btn.parent().data('filter'),
            title = $('.challengeTitle').val();
        $.ajax({
            method: 'GET',
            url: btn.closest('ul').data('route'),
            data: {status: status, title: title},
            success: function (response) {
                updateQueryStringParam("status", status);
                updateQueryStringParam("title", title);
                btn.closest('ul').find('.active').removeClass('active');
                btn.parent().addClass('active');
                $('.challenges_data').html(response.data.view);
                loader.hide();
            },
            error: function () {
                loader.hide();
            }
        });
    });

    var filterBtns = $('.filter_route');

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        loader.show();
        var btn = $(this);
        var pageToSearch = '',
            status = filterBtns.find('.active').data('filter'),
            title = $('.challengeTitle').val();

        if (btn.text() === '›') {
            pageToSearch = parseInt(btn.closest('ul').find('.active').find('span').text()) + 1;
            console.log();
        } else if (btn.text() === '‹') {
            pageToSearch = parseInt(btn.closest('ul').find('.active').find('span').text()) - 1;
        } else {
            pageToSearch = btn.text();
        }
        $.ajax({
            method: 'GET',
            url: filterBtns.data('route'),
            data: {page: pageToSearch, status: status, title: title},
            success: function (response) {
                updateQueryStringParam("status", status);
                updateQueryStringParam("title", title);
                updateQueryStringParam("page", pageToSearch);
                btn.parent().addClass('active');
                $('.challenges_data').html(response.data.view);
                loader.hide();
            },
            error: function () {
                loader.hide();
            }
        });
    });

    /**
     * Тот же поиск только форм сабмит
     */
    $(document).on('click', '.searchSubmit', function (e) {
        e.preventDefault();
        loader.show();
        var status = filterBtns.find('.active').data('filter'),
            title = $('.challengeTitle').val();
        $.ajax({
            method: 'GET',
            url: filterBtns.data('route'),
            data: {status: status, title: title},
            success: function (response) {
                updateQueryStringParam("status", status);
                updateQueryStringParam("title", title);
                $('.challenges_data').html(response.data.view);
                loader.hide();
            },
            error: function () {
                loader.hide();
            }
        });
    });

    $(document).on('submit', '.challengeFilterForm', function (e) {
        e.preventDefault();
        loader.show();
        $.ajax({
            method: 'GET',
            url: filterBtns.data('route'),
            data: {status: filterBtns.find('.active').data('filter'), title: $('.challengeTitle').val()},
            success: function (response) {
                $('.challenges_data').html(response.data.view);
                loader.hide();
            },
            error: function () {
                loader.hide();
            }
        });
    })
});