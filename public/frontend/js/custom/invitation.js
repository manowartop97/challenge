$(document).ready(function () {

    var loader = $('#preloader');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Подставляем айдишку в форму попапа
     */
    $(document).on("click", ".invitation_btn", function (e) {
        e.preventDefault();
        var challengeId = $(this).data('id'),
            popup = $('#invite_to_challenge');
        popup.find('#challenge_id').val(challengeId);
        // var newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + '/' + challengeId;
        // window.history.pushState({path: newUrl}, '', newUrl);
        popup.show();
    });
    //
    // /**
    //  * Закрытие модалки - убираем из урла challenge
    //  */
    // $(document).on('hidden.bs.modal', '#invite_to_challenge', function () {
    //     console.log(window.location.pathname);
    //     // var newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?challenge=' + challengeId;
    //     // window.history.pushState({path: newUrl}, '', newUrl);
    // });

    /**
     * Попап инвайта френдов
     */
    $(document).on('click', '.invite_friends_popup', function (e) {
        e.preventDefault();
        loader.show();
        var form = $('.invitations_form');

        $.ajax({
            method: 'post',
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '5'
                });
                if (form.data('reload') === 1) {
                    $('.participantsCount').text(response.data.participantsCount);
                    $('.invitationsCount').text(response.data.invitationsCount);
                    $('.yourInvitationsCount').text(response.data.yourInvitationsCount);
                }

            },
            error: function (response) {
                loader.hide();
                if (typeof response.responseJSON.data !== 'undefined') {
                    $('body').overhang({
                        type: "error",
                        message: response.responseJSON.data.message,
                        duration: '5'
                    });
                } else {
                    if (typeof response.responseText !== 'undefined') {
                        var errors = $.parseJSON(response.responseText);
                        $.each(errors.errors, function (key, val) {
                            if ($.isNumeric(key.search('.'))) {
                                key = key.split('.')[0];
                            }

                            form.find('#' + key + '-error').text(val[0]);
                        });
                    }
                }
            }
        })
    });

    /**
     * Вступление в челлендж
     */
    $(document).on('click', '.takePartBtn', function (e) {
        e.preventDefault();
        loader.show();
        var btn = $(this);
        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function (response) {
                btn.removeClass('btn-success takePartBtn');
                btn.addClass('btn-danger cancelTakePartBtn');
                btn.data('route', response.data.route);
                btn.text(response.data.btnText);
                $('.participantsCount').text(response.data.participantsCount);
                $('.invitationsCount').text(response.data.invitationsCount);
                $('.confirmChallengeBtn').show();
                loader.hide();
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '2'
                });
            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Отмена вступления в челлендж
     */
    $(document).on('click', '.cancelTakePartBtn', function (e) {
        e.preventDefault();
        var btn = $(this);
        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function (response) {
                btn.removeClass('btn-danger cancelTakePartBtn');
                btn.addClass('btn-success takePartBtn');
                btn.data('route', response.data.route);
                btn.text(response.data.btnText);
                $('.participantsCount').text(response.data.participantsCount);
                $('.invitationsCount').text(response.data.invitationsCount);
                $('.confirmChallengeBtn').hide();
                loader.hide();
                $('body').overhang({
                    type: "success",
                    message: response.data.message,
                    duration: '2'
                });
            },
            error: function (response) {
                loader.hide();
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

});