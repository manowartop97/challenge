var date_select_field = $('.datetimepicker');
if (date_select_field.length) {
    var start = moment().subtract(29, 'days');

    date_select_field.daterangepicker({
        startDate: start,
        autoUpdateInput: false,
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'DD.MM.YYYY',
            daysOfWeek: [
                "Вс",
                "Пн",
                "Вт",
                "Ср",
                "Чт",
                "Пт",
                "Сб"
            ],
            monthNames: [
                "Январь",
                "Февраль",
                "Март",
                "Апрель",
                "Май",
                "Июнь",
                "Июль",
                "Август",
                "Сентябрь",
                "Октябрь",
                "Ноябрь",
                "Декабрь"
            ],
            firstDay: 1
        }
    });
    date_select_field.on('focus', function () {
        $(this).closest('.form-group').addClass('is-focused');
    });
    date_select_field.on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD.MM.YYYY'));
        $(this).closest('.form-group').addClass('is-focused');
    });
    date_select_field.on('hide.daterangepicker', function () {
        if ('' === $(this).val()){
            $(this).closest('.form-group').removeClass('is-focused');
        }
    });

}