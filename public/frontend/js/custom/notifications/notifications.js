$(document).ready(function () {

    messaging.onMessage(function (payload) {
        // console.log('Message received. ', payload);

        var object = payload.data;
        if (object.category === 'friendship') {

            $(object.data).prependTo('.friendshipNotifications');
            $('.friendshipNotificationsCount').text(object.count);
            $('.noNotifications').parent().remove();
        }

    });

    /**
     * Удаляем нотификашки по одной
     */
    $(document).on('click', '.removeNotification', function (e) {
        e.preventDefault();

        var btn = $(this);

        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function (response) {
                $('.friendshipNotificationsCount').text(response.data.count);
                btn.closest('li').fadeOut(300, function () {
                    $(this).remove();
                });
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        });
    });

    /**
     * Очистить все уведомления сразу
     */
    $(document).on('click', '.clearAllNotifications', function (e) {
        e.preventDefault();
        var btn = $(this);

        $.ajax({
            method: 'get',
            url: btn.data('route'),
            success: function (response) {
                btn.parent().find('ul').html('<li><h5 class="text-center noNotifications">' + response.data.message + '</h5></li>');
                $('.friendshipNotificationsCount').text(0);
            },
            error: function (response) {
                $('body').overhang({
                    type: "error",
                    message: response.responseJSON.data.message,
                    duration: '5'
                });
            }
        })

    })

});