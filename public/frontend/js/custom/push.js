// Initialize Firebase
var config = {
    apiKey: "AIzaSyDG92REL47OrDJZmylXKFIVgUnCJ7nHxXo",
    authDomain: "challenge-loc.firebaseapp.com",
    databaseURL: "https://challenge-loc.firebaseio.com",
    projectId: "challenge-loc",
    storageBucket: "challenge-loc.appspot.com",
    messagingSenderId: "1048927037621"
};
firebase.initializeApp(config);
// браузер поддерживает уведомления
// вообще, эту проверку должна делать библиотека Firebase, но она этого не делает
if ('Notification' in window) {
    var messaging = firebase.messaging();

    // пользователь уже разрешил получение уведомлений
    // подписываем на уведомления если ещё не подписали
//        if (Notification.permission === 'granted') {
//            subscribe();
//        }
    subscribe();
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


function subscribe() {
    // запрашиваем разрешение на получение уведомлений
    messaging.requestPermission()
        .then(function () {
            // получаем ID устройства
            messaging.getToken()
                .then(function (currentToken) {
                    if (currentToken) {
                        sendTokenToServer(currentToken);
                    } else {
                        console.warn('Не удалось получить токен.');
                        setTokenSentToServer(false);
                    }
                })
                .catch(function (err) {
                    console.warn('При получении токена произошла ошибка.', err);
                    setTokenSentToServer(false);
                });
        })
        .catch(function (err) {
            console.warn('Не удалось получить разрешение на показ уведомлений.', err);
        });
}

// отправка ID на сервер
function sendTokenToServer(currentToken) {
// Пока что так
    var url = $('.storeTokenRoute').data('route'); // адрес скрипта на сервере который сохраняет ID устройства
    $.post(url, {
        token: currentToken,
    });

    setTokenSentToServer(currentToken);

    // if (!isTokenSentToServer(currentToken)) {
    //     console.log('Отправка токена на сервер...');
    //     var url = $('.storeTokenRoute').data('route'); // адрес скрипта на сервере который сохраняет ID устройства
    //     $.post(url, {
    //         token: currentToken,
    //     });
    //
    //     setTokenSentToServer(currentToken);
    // } else {
    //     console.log('Токен уже отправлен на сервер.');
    // }
}

// используем localStorage для отметки того,
// что пользователь уже подписался на уведомления
function isTokenSentToServer(currentToken) {
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currentToken;
}

function setTokenSentToServer(currentToken) {
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currentToken ? currentToken : ''
    );
}