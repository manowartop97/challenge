$(document).ready(function () {

    // Получение блока формы для маппинга полей при парсинге
    $(document).on('change', '.parser_select', function () {
        var select = $(this);
        if (!select.val()) {
            $('.form_block').empty();
            $('.mapping_heading').addClass('hidden');
            return false;
        }

        $.ajax({
            url: select.data('route'),
            type: 'GET',
            data: {parser_id: select.val()},
            success: function (response) {
                $('.mapping_heading').removeClass('hidden');
                $('.form_block').html(response);
            }
        });
    });

    // Получие дополнительных полей при создании маппинга парсинга
    $(document).on('click', '.append_db_mapping_field', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).data('route'),
            type: 'GET',
            data: {type: $(this).data('id'), index: $('.mapping_fields_block .row').length},
            success: function (response) {
                $(response).appendTo('.mapping_fields_block');
            }
        });
    });

    // Удаление дополнительных полей при создании маппинга парсинга
    $(document).on('click', '.removeMappingFieldItem', function (e) {
        e.preventDefault();
       $(this).parent().parent().remove();
    });

    // Валидация и сабмит формы
    $(document).on('submit', '#parsing_form', function (e) {
        e.preventDefault();

        $('.help-block').empty();
        var formData = new FormData($(this)[0]);
        $.ajax({
            type: 'POST',
            url: $(this).data('route'),
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                $(location).attr("href", response.url);
            },
            error: function (data) {
                var errors = $.parseJSON(data.responseText);
                $.each(errors.errors, function (key, val) {
                    var input = $('#' + key.replace(/\./g, "-"));
                    input.parent().find('.help-block').html('<strong>' + val[0] + '</strong>');
                    input.parent().find('span').removeClass('hidden');
                });
            }
        });
    });
});