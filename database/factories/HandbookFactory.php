<?php

use App\Components\Handbook\Models\Handbook;
use App\Components\Handbook\Models\HandbookData;
use Faker\Generator as Faker;


$factory->define(Handbook::class, function (Faker $faker) {
    return [
        'systemName'  => $faker->word,
        'description' => $faker->words(3, true),
    ];
});

$factory->define(HandbookData::class, function (Faker $faker) {
    return [
        'data_id' => rand(1, 200),
        'value'   => $faker->word,
        'title'   => $faker->word
    ];
});