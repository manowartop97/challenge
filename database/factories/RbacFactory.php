<?php

use App\Components\Rbac\Models\Permission;
use App\Components\Rbac\Models\PermissionGroup;
use App\Components\Rbac\Models\Role;


$factory->define(Role::class, function () {
    return [
        'slug' => 'admin',
        'name' => 'Admin'
    ];
});

$factory->define(Permission::class, function () {
    return [
        'slug' => 'adminPermission',
        'name' => 'AdminPermission'
    ];
});

$factory->define(PermissionGroup::class, function () {
    return [
        'module' => 'adminGroup',
        'name'   => 'AdminGroup'
    ];
});
