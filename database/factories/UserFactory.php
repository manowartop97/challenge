<?php

use App\Components\Admin\Models\Admin;
use App\Components\Admin\Models\Profile;
use App\Models\DB\User\AccountSetting;
use App\Models\DB\User\User;
use App\Models\DB\User\UserProfile;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email'          => $faker->unique()->safeEmail,
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'is_verified'    => true,
    ];
});

$factory->define(UserProfile::class, function (Faker $faker) {
    return [
        'surname'  => $faker->firstName,
        'name'     => $faker->lastName,
        'phone'    => $faker->phoneNumber,
        'gender'   => rand(1, 2),
        'birthday' => $faker->date()
    ];
});


$factory->define(AccountSetting::class, function () {
    return [
    ];
});

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'email'          => $faker->unique()->safeEmail,
        'password'       => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'surname' => $faker->firstName,
        'name'    => $faker->lastName,
    ];
});
