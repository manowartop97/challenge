<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AccountSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_settings', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('who_can_view_posts')->default(1);
            $table->integer('who_can_request_friends')->default(1);
            $table->integer('who_can_invite_to_challenge')->default(1);
            $table->integer('who_can_invite_to_event')->default(1);
            $table->boolean('is_telegram_notifications')->default(true);
            $table->boolean('is_email_notifications')->default(true);
            $table->boolean('is_friends_birthday_notifications')->default(true);
            $table->boolean('is_notification_sound_on')->default(true);
            $table->boolean('is_chat_sound_on')->default(true);
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->primary(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_settings');
    }
}
