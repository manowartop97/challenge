<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Challenges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Сущность челленджа
        Schema::create('challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->string('title', 255);
//            $table->string('tags');
            $table->boolean('is_group_challenge')->default(0);
            $table->string('title_image')->nullable();
            $table->string('additional_media', 1000)->nullable();
            $table->string('deadline')->nullable();
            $table->text('description');
            $table->integer('status');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        // Инвайт в челлендж
        Schema::create('challenge_invitations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('challenge_id')->unsigned()->index();
            $table->integer('group_id')->index()->nullable();
            $table->integer('participant_id')->unsigned()->index();
            $table->integer('invitor_id')->unsigned()->index();
            $table->string('invitation_text', 255)->nullable();
            $table->string('rejection_text', 255)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();

            $table->foreign('challenge_id')
                ->references('id')
                ->on('challenges')
                ->onDelete('cascade');

            $table->foreign('invitor_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('participant_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });

        // Группа для групового участия
        Schema::create('challenge_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
        });

        // Юзер-группа
        Schema::create('challenge_group_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('group_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('group_id')
                ->references('id')
                ->on('challenge_groups')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_invitations');
        Schema::dropIfExists('challenges');
        Schema::dropIfExists('challenge_group_users');
        Schema::dropIfExists('challenge_groups');
    }
}
