<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('is_verified')->default(false);
            $table->string('confirmation_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('user_profile', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->string('surname');
            $table->string('name');
            $table->integer('gender');
            $table->string('birthday');
            $table->string('phone')->nullable();

            $table->string('avatar')->nullable();
            $table->string('profile_background')->nullable(); // Можно сделать это за рейтинг

            $table->integer('country_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->string('facebook_link', 100)->nullable();
            $table->string('twitter_link', 100)->nullable();
            $table->string('instagram_link', 100)->nullable();
            $table->string('vk_link', 100)->nullable();
            $table->string('status_string', 100)->nullable();

            $table->text('hobbies_and_interests')->nullable();
            $table->text('favourite_music')->nullable();
            $table->text('favourite_games')->nullable();
            $table->text('favourite_movies')->nullable();

            $table->text('about')->nullable();

            $table->text('device_tokens')->nullable();

            $table->string('last_visit')->nullable();
            $table->boolean('is_online')->default(0);
            $table->string('chat_id')->nullable();
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->primary(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profile');
        Schema::dropIfExists('users');
    }
}
