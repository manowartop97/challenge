<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Logs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('log_name', 100);
            $table->integer('log_category');
            $table->text('message');
            $table->integer('status')->default(1);
            $table->timestamps();
        });

        Schema::create('file_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('log_name', 100);
            $table->string('file_path', 100);
            $table->integer('log_category');
            $table->text('message');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_logs');
        Schema::dropIfExists('file_logs');
    }
}
