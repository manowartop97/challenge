<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChallengeConfirmation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenge_confirmations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('challenge_id')->unsigned()->index();
            $table->integer('invitation_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->string('comment')->nullable();
            $table->text('additional_media')->nullable();
            $table->integer('status')->default(1);
            $table->string('rejection_comment')->nullable();
            $table->string('moderator_comment')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('challenge_id')->references('id')->on('challenges')->onDelete('cascade');
            $table->foreign('invitation_id')->references('id')->on('challenge_invitations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenge_confirmations');
    }
}
