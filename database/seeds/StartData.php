<?php


use App\Components\Admin\Models\Admin;
use App\Components\Admin\Models\Profile;
use App\Components\Rbac\Models\Permission;
use App\Components\Rbac\Models\PermissionGroup;
use App\Components\Rbac\Models\Role;
use App\Models\DB\User\AccountSetting;
use App\Models\DB\User\User;
use App\Models\DB\User\UserProfile;
use Illuminate\Database\Seeder;

class StartData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles
        factory(Role::class)->create();
        factory(Role::class)->create(['slug' => 'user', 'name' => 'User']);
        factory(Role::class)->create(['slug' => 'manger', 'name' => 'Manager']);

        // Разрешения
        factory(Permission::class)->create();
        factory(Permission::class)->create(['slug' => 'userPermission', 'name' => 'UserPermission']);
        factory(Permission::class)->create(['slug' => 'mangerPermission', 'name' => 'ManagerPermission']);

        // Группы разрешений
        factory(PermissionGroup::class)->create();
        factory(PermissionGroup::class)->create(['module' => 'guestPermission', 'name' => 'GuestPermission']);
        factory(PermissionGroup::class)->create(['module' => 'userPermission', 'name' => 'UserPermission']);
        factory(PermissionGroup::class)->create(['module' => 'managerPermission', 'name' => 'ManagerPermission']);

        // Админ
        factory(Admin::class)->create([
            'email'    => 'manowartop97@gmail.com',
            'password' => bcrypt('123456'),
        ])->each(/**
         * @param Admin $admin
         */
            function (Admin $admin) {
                $admin->profile()->save(factory(Profile::class)->create([
                    'admin_id' => $admin->id,
                    'surname'  => 'Добровольский',
                    'name'     => 'Андрей',
                ]));
            });

        // Тестовый юзер
        factory(User::class)->create([
            'email'    => 'manowartop97@gmail.com',
            'password' => bcrypt('123456'),
        ])->each(function (User $user) {
            $user->attachRole(1);
            $user->profile()->save(factory(UserProfile::class)->create([
                'user_id' => $user->id,
                'surname' => 'Добровольский',
                'name'    => 'Андрей',
            ]));
            (new \App\Repositories\User\UserRepository())->generateDefaultAvatar($user);
            $user->settings()->save(factory(AccountSetting::class)->create([
                'user_id' => $user->id,
            ]));
        });

        // Тестовый юзер
        factory(User::class, 300)->create()->each(function (User $user) {
            $user->attachRole(1);
            $user->profile()->save(factory(UserProfile::class)->create(['user_id' => $user->id]));
            $user->settings()->save(factory(AccountSetting::class)->create(['user_id' => $user->id]));
            (new \App\Repositories\User\UserRepository())->generateDefaultAvatar($user);
        });
    }
}
