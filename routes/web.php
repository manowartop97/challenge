<?php
Route::get('/', 'Home\HomeController@index')->name('home');
Auth::routes();

/**
 * Авторизация
 */
Route::get('auth', 'Auth\RegisterController@showForm')->name('auth');
Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::get('confirm/{token}', 'Auth\ConfirmationController@confirm')->name('confirm-registration');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/forgot-password', 'Profile\SettingsController@forgotPassword')->middleware('ajax')->name('profile-settings.forgot-password');

/**
 * Профиль юзера
 */
Route::group(['prefix' => 'profile', 'middleware' => ['auth']], function () {
    Route::get('/feed', 'Profile\ProfileController@index')->name('profile.index');
    Route::get('/about', 'Profile\ProfileController@about')->name('profile.about');
    Route::get('/friends', 'Profile\ProfileController@friends')->name('profile.friends');
    Route::get('/challenges', 'Profile\ProfileController@challenges')->name('profile.challenges');
    Route::get('/challenge-confirmations', 'Profile\ProfileController@challengeConfirmations')->name('profile.challenge-confirmations');
    Route::get('/confirmation-detail/{challengeConfirmation}', 'Profile\ProfileController@confirmationDetail')->name('profile.confirmation-detail');

    /**
     * Настройки профиля
     */
    Route::group(['prefix' => 'settings', 'middleware' => ['auth']], function () {
        Route::get('/personal-info', 'Profile\SettingsController@personalInfoForm')->name('profile-settings.personal');
        Route::post('/update-personal-info', 'Profile\SettingsController@updateProfileInfo')->name('profile-settings.update-personal');
        Route::get('/account-settings', 'Profile\SettingsController@accountSettingsForm')->name('profile-settings.account-settings');
        Route::post('/update-account-settings', 'Profile\SettingsController@updateAccountSettings')->name('profile-settings.update-account-settings');
        Route::get('/password', 'Profile\SettingsController@passwordChangeForm')->name('profile-settings.change-password');
        Route::post('/change-password', 'Profile\SettingsController@changePassword')->name('profile-settings.change-password-action');
        Route::get('/reset-password', 'Profile\SettingsController@resetPassword')->name('profile-settings.reset-password');
        Route::get('/hobbies', 'Profile\SettingsController@hobbiesForm')->name('profile-settings.hobbies');
        Route::post('/update-hobbies', 'Profile\SettingsController@updateHobbies')->name('profile-settings.update-hobbies');
        Route::get('/education-and-employment', 'Profile\SettingsController@educationAndEmploymentForm')->name('profile-settings.adu-and-employment');
        Route::post('update-status', 'Profile\SettingsController@updateStatus')->middleware('ajax')->name('profile-settings.update-status');
        Route::post('/update-avatar', 'Profile\SettingsController@updateAvatar')->name('profile-settings.update-avatar');
        Route::post('/update-header', 'Profile\SettingsController@updateHeader')->name('profile-settings.update-header');
    });
});

/**
 * Уведомления
 */
Route::group(['prefix' => 'notification', 'middleware' => ['auth']], function () {
    Route::get('/all', 'Notification\NotificationController@index')->name('notifications');
    Route::get('/check-token', 'Notification\NotificationController@checkToken')->name('notification.check-token');
    Route::post('store-token', 'Notification\NotificationController@storeToken')->middleware('ajax')->name('notification.store-token');
    Route::get('clear/{notification}', 'Notification\NotificationController@clear')->middleware('ajax')->name('notification.clear');
    Route::get('clear-all/{category}', 'Notification\NotificationController@clearAll')->middleware('ajax')->name('notification.clear-all');
});

/**
 * Друзья
 */
Route::group(['prefix' => 'friends', 'middleware' => ['auth']], function () {
    Route::get('/income-requests', 'Friends\FriendsController@requests')->name('friends.requests');
    Route::get('/outcome-requests', 'Friends\FriendsController@outcomeRequests')->name('friends.outcome-requests');
});

/**
 * Страница пользователя
 */
Route::group(['prefix' => 'page', 'middleware' => ['auth']], function () {
    Route::get('/uid{id}', 'Page\PageController@showPage')->name('page.show-user');
    Route::get('info/uid{id}', 'Page\PageController@showPageInfo')->name('page.show-user-info');
    Route::get('friends/uid{id}', 'Page\PageController@friends')->name('page.friends');
    Route::get('challenges/uid{user}', 'Page\PageController@challenges')->name('page.challenges');
});

/**
 * Действия с друзьями
 */
Route::group(['prefix' => 'friendship', 'middleware' => ['auth']], function () {
    Route::get('/new/{id}', 'Friendship\FriendshipController@inviteToFriends')->middleware('ajax')->name('friendship.new');
    Route::get('/remove/{id}', 'Friendship\FriendshipController@removeFromFriends')->middleware('ajax')->name('friendship.remove');
    Route::get('/accept/{id}', 'Friendship\FriendshipController@acceptFriendship')->middleware('ajax')->name('friendship.accept');
    Route::get('/reject/{id}', 'Friendship\FriendshipController@rejectFriendship')->middleware('ajax')->name('friendship.reject');
});

/**
 * Поиск
 */
Route::group(['prefix' => 'search', 'middleware' => ['auth']], function () {
    Route::get('/people', 'Search\SearchController@people')->name('search.people');
    Route::get('/challenges', 'Search\SearchController@challenges')->name('search.challenges');
});

/**
 * Посты на стене
 */
Route::group(['prefix' => 'post', 'middleware' => ['auth']], function () {
    Route::post('/create', 'Posts\PostController@createPost')->middleware('ajax')->name('post.create');
    Route::get('/like/{id}', 'Posts\PostController@like')->middleware('ajax')->name('post.like');
    Route::get('/unlike/{id}', 'Posts\PostController@unlike')->middleware('ajax')->name('post.unlike');
    Route::post('/edit/{id}', 'Posts\PostController@editPost')->middleware('ajax')->name('post.edit');
    Route::get('/edit-form/{id}', 'Posts\PostController@editForm')->middleware('ajax')->name('post.edit-form');
    Route::get('/delete/{id}', 'Posts\PostController@deletePost')->middleware('ajax')->name('post.delete');
    Route::get('/make-featured/{id}', 'Posts\PostController@makeFeatured')->middleware('ajax')->name('post.make-featured');
});

/**
 * Челленджи
 */
Route::group(['prefix' => 'challenges', 'middleware' => ['auth']], function () {
    Route::get('/all', 'Challenge\ChallengeController@index')->name('challenge.all');
    Route::get('/new', 'Challenge\ChallengeController@newForm')->name('challenge.new-form');
    Route::get('/edit/{id}', 'Challenge\ChallengeController@editForm')->name('challenge.edit-form');
    Route::post('/create', 'Challenge\ChallengeController@store')->name('challenge.create');
    Route::post('/update/{challenge}', 'Challenge\ChallengeController@update')->name('challenge.update');
    Route::get('/delete/{challenge}', 'Challenge\ChallengeController@delete')->name('challenge.delete');
    Route::get('/show/{id}', 'Challenge\ChallengeController@show')->name('challenge.show');
    Route::get('/re-new/{challenge}', 'Challenge\ChallengeController@reNewChallenge')->name('challenge.re-new');
    Route::get('/confirmation/{challenge}', 'Challenge\ChallengeController@confirmationForm')->name('challenge.confirmation');
    Route::post('/confirm', 'Challenge\ChallengeController@confirmChallenge')->name('challenge.confirm');
});

/**
 * Инвайты в челленджи
 */
Route::group(['prefix' => 'invitation', 'middleware' => ['auth']], function () {
    Route::post('/send', 'Challenge\InvitationController@sendInvitations')->middleware('ajax')->name('invitation.send');
    Route::get('take-part/{challenge}', 'Challenge\InvitationController@takePart')->middleware('ajax')->name('invitation.take-part');
    Route::get('cancel-participation/{challenge}', 'Challenge\InvitationController@cancelParticipation')->middleware('ajax')->name('invitation.cancel-participation');
});